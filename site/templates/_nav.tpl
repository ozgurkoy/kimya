<ul id="nav">
    <li>
        <a href="{$SITEROOT}/home">
            <i class="icon-dashboard"></i>
            Panel
        </a>
    </li>
	<li>
		<a href="{$SITEROOT}/proposals">
			<i class="icon-external-link"></i>
			TEKLIFLER
		</a>

	</li>
	<li>
		<a href="{$SITEROOT}/activities">
			<i class="icon-coffee"></i>
			AKTIVITELER
		</a>

	</li>
	<li>
		<a href="javascript:void(0);">
			<i class="icon-group"></i>
			MUSTERILER
		</a>
		<ul class="sub-menu">
			<li>
				<a href="{$SITEROOT}/addCustomer" style="display:none"></a>
				<a href="{$SITEROOT}/customers">
					<i class="icon-group"></i>
					Musteriler
				</a>
			</li>
			<li>
				<a href="{$SITEROOT}/contacts">
					<i class="icon-phone"></i>
					Kontaklar
				</a>
			</li>
		</ul>

	</li>
    <li>
        <a href="javascript:void(0);">
            <i class="icon-fire"></i>
                URUNLER
        </a>
        <ul class="sub-menu">
            <li>
                <a href="{$SITEROOT}/products">
                    <i class="icon-fire"></i>
                    Urunler
                </a>
            </li>
            <li>
                <a href="{$SITEROOT}/pgroups">
                    <i class="icon-building"></i>
                    Urun Gruplari
                </a>
            </li>
        </ul>
    </li>
	<li>
		<a href="javascript:void(0);">
			<i class="icon-leaf"></i>
			GENEL
		</a>
		<ul class="sub-menu">
			<li>
				<a href="{$SITEROOT}/currencies">
					<i class="icon-dollar"></i>
					Para Birimleri
				</a>
			</li>
			<li>
				<a href="{$SITEROOT}/sectors">
					<i class="icon-briefcase"></i>
					Sektorler
				</a>
			</li>

		</ul>
	</li>

	<li>
		<a href="javascript:void(0);">
			<i class="icon-user"></i>
			KULLANICI YONETIMI
		</a>
		<ul class="sub-menu">
			<li>
				<a href="{$SITEROOT}/addUser" style="display: none"></a>

				<a href="{$SITEROOT}/users">
					<i class="icon-user"></i>
					&nbsp;Kullanicilar
				</a>
			</li>
			<li>
				<a href="{$SITEROOT}/addRole" style="display: none"></a>
				<a href="{$SITEROOT}/roleMod" style="display: none"></a>
				<a href="{$SITEROOT}/roles">
					<i class="icon-code"></i>
					Roller
				</a>
			</li>
			<li>
				<a href="{$SITEROOT}/actions">
					<i class="icon-road"></i>
					Aksiyonlar
				</a>
			</li>
		</ul>
	</li>

</ul>