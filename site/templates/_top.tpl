{*<meta charset="utf-8" />*}
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">

<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<title>Gultas CRM</title>

<!--=== CSS ===-->

<!-- Bootstrap -->
<link href="{$SITEROOT}/site/layout/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

<!-- jQuery UI -->
<!--<link href="{$SITEROOT}/site/layout/plugins/jquery-ui/jquery-ui-1.10.2.custom.css" rel="stylesheet" type="text/css" />-->
<!--[if lt IE 9]>
<link rel="stylesheet" type="text/css" href="{$SITEROOT}/site/layout/plugins/jquery-ui/jquery.ui.1.10.2.ie.css"/>
<![endif]-->

<!-- Theme -->
<link href="{$SITEROOT}/site/layout/assets/css/main.css" rel="stylesheet" type="text/css" />
<link href="{$SITEROOT}/site/layout/assets/css/plugins.css" rel="stylesheet" type="text/css" />
<link href="{$SITEROOT}/site/layout/assets/css/responsive.css" rel="stylesheet" type="text/css" />
<link href="{$SITEROOT}/site/layout/assets/css/icons.css" rel="stylesheet" type="text/css" />

<link rel="stylesheet" href="{$SITEROOT}/site/layout/assets/css/fontawesome/font-awesome.css">
<!--[if IE 7]>
<link rel="stylesheet" href="{$SITEROOT}/site/layout/assets/css/fontawesome/font-awesome-ie7.css">
<![endif]-->

<!--[if IE 8]>
<link href="{$SITEROOT}/site/layout/assets/css/ie8.css" rel="stylesheet" type="text/css" />
<![endif]-->
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>
<link href="{$SITEROOT}/site/layout/css/custom.css" rel="stylesheet" type="text/css" />

<!--=== JavaScript ===-->

<script type="text/javascript" src="{$SITEROOT}/site/layout/js/main.js"></script>
<script type="text/javascript" src="{$SITEROOT}/site/layout/assets/js/libs/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="{$SITEROOT}/site/layout/plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>

<script type="text/javascript" src="{$SITEROOT}/site/layout/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="{$SITEROOT}/site/layout/assets/js/libs/lodash.compat.min.js"></script>

<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
<script src="{$SITEROOT}/site/layout/assets/js/libs/html5shiv.js"></script>
<![endif]-->

<!-- Smartphone Touch Events -->
<script type="text/javascript" src="{$SITEROOT}/site/layout/plugins/touchpunch/jquery.ui.touch-punch.min.js"></script>
<script type="text/javascript" src="{$SITEROOT}/site/layout/plugins/event.swipe/jquery.event.move.js"></script>
<script type="text/javascript" src="{$SITEROOT}/site/layout/plugins/event.swipe/jquery.event.swipe.js"></script>

<!-- General -->
<script type="text/javascript" src="{$SITEROOT}/site/layout/assets/js/libs/breakpoints.js"></script>
<script type="text/javascript" src="{$SITEROOT}/site/layout/plugins/respond/respond.min.js"></script> <!-- Polyfill for min/max-width CSS3 Media Queries (only for IE8) -->
<script type="text/javascript" src="{$SITEROOT}/site/layout/plugins/cookie/jquery.cookie.min.js"></script>
<script type="text/javascript" src="{$SITEROOT}/site/layout/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script type="text/javascript" src="{$SITEROOT}/site/layout/plugins/slimscroll/jquery.slimscroll.horizontal.min.js"></script>

<!-- Page specific plugins -->
<!-- Charts -->
<!--[if lt IE 9]>
<script type="text/javascript" src="{$SITEROOT}/site/layout/plugins/flot/excanvas.min.js"></script>
<![endif]-->
<script type="text/javascript" src="{$SITEROOT}/site/layout/plugins/sparkline/jquery.sparkline.min.js"></script>
<script type="text/javascript" src="{$SITEROOT}/site/layout/plugins/flot/jquery.flot.min.js"></script>
<script type="text/javascript" src="{$SITEROOT}/site/layout/plugins/flot/jquery.flot.tooltip.min.js"></script>
<script type="text/javascript" src="{$SITEROOT}/site/layout/plugins/flot/jquery.flot.resize.min.js"></script>
<script type="text/javascript" src="{$SITEROOT}/site/layout/plugins/flot/jquery.flot.time.min.js"></script>
<script type="text/javascript" src="{$SITEROOT}/site/layout/plugins/flot/jquery.flot.growraf.min.js"></script>
<script type="text/javascript" src="{$SITEROOT}/site/layout/plugins/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
<script type="text/javascript" src="{$SITEROOT}/site/layout/plugins/tagsinput/jquery.tagsinput.min.js"></script>
<script type="text/javascript" src="{$SITEROOT}/site/layout/plugins/typeahead/typeahead.min.js"></script> <!-- AutoComplete -->
<script type="text/javascript" src="{$SITEROOT}/site/layout/plugins/bootstrap-inputmask/jquery.inputmask.min.js"></script>

<script type="text/javascript" src="{$SITEROOT}/site/layout/plugins/daterangepicker/moment.min.js"></script>
<script type="text/javascript" src="{$SITEROOT}/site/layout/plugins/daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="{$SITEROOT}/site/layout/plugins/blockui/jquery.blockUI.min.js"></script>

<script type="text/javascript" src="{$SITEROOT}/site/layout/plugins/fullcalendar/fullcalendar.min.js"></script>

<!-- Noty -->
<script type="text/javascript" src="{$SITEROOT}/site/layout/plugins/noty/jquery.noty.js"></script>
<script type="text/javascript" src="{$SITEROOT}/site/layout/plugins/noty/layouts/top.js"></script>
<!-- Forms -->
<script type="text/javascript" src="{$SITEROOT}/site/layout/plugins/uniform/jquery.uniform.min.js"></script>

<script type="text/javascript" src="{$SITEROOT}/site/layout/plugins/noty/themes/default.js"></script>

<!-- DataTables -->
<script type="text/javascript" src="{$SITEROOT}/site/layout/plugins/datatables/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="{$SITEROOT}/site/layout/plugins/datatables/jquery.dataTables.columnFilter.js"></script>
<script type="text/javascript" src="{$SITEROOT}/site/layout/plugins/datatables/tabletools/TableTools.min.js"></script> <!-- optional -->
<script type="text/javascript" src="{$SITEROOT}/site/layout/plugins/datatables/colvis/ColVis.min.js"></script> <!-- optional -->
<script type="text/javascript" src="{$SITEROOT}/site/layout/plugins/datatables/DT_bootstrap.js"></script>

<!-- App -->
<script type="text/javascript" src="{$SITEROOT}/site/layout/assets/js/app.js"></script>
<script type="text/javascript" src="{$SITEROOT}/site/layout/assets/js/plugins.js"></script>
<script type="text/javascript" src="{$SITEROOT}/site/layout/assets/js/plugins.form-components.js"></script>
<!-- Demo JS -->
<script type="text/javascript" src="{$SITEROOT}/site/layout/plugins/bootstrap-multiselect/bootstrap-multiselect.min.js"></script>

<!-- Form Validation -->
<script type="text/javascript" src="{$SITEROOT}/site/layout/plugins/validation/jquery.validate.min.js"></script>
<script type="text/javascript" src="{$SITEROOT}/site/layout/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="{$SITEROOT}/site/layout/plugins/duallistbox/jquery.duallistbox.min.js"></script>
<script type="text/javascript" src="{$SITEROOT}/site/layout/assets/js/custom.js"></script>

<script type="text/javascript">
	var SITEROOT = '{$SITEROOT}';
</script>