<div class="col-md-12">
	<div class="widget box">
		<div class="widget-header">
			<h4><i class="icon-reorder"></i> Stok Hareketleri : {$product->name} </h4>
		</div>
		<div class="widget-content">
			<div class="tabbable box-tabs">
				<ul class="nav nav-tabs">
					<li><a href="#box_tabAdd" data-toggle="tab">Yeni Stok Hareketi</a></li>
					<li class="active"><a href="#box_tabList" data-toggle="tab">Stok Hareketleri</a></li>
				</ul>
				<div class="tab-content">
					<div class="tab-pane active" id="box_tabList">
						<table class="table table-hover table-striped table-bordered table-highlight-head">
							<thead>
							<tr>
								<th>Tarih</th>
								<th>Miktar</th>
								<th>Oncesi</th>
								<th>Sonrasi</th>
								<th>Bilgi</th>
								{*<th class="col-md-2">Islem</th>*}
							</tr>
							</thead>
							<tbody>
							{if $stocks && $stocks->gotValue}
								{doWhile}
								{callUserFunc $stocks->loadOrderProduct()}

								{if $stocks->stock}
								{callUserFunc $stocks->stock->loadBOrder()}
								{/if}
									<tr>
										<td>{reverseDate d=$stocks->opDate}</td>
										<td>{$stocks->amount}</td>
										<td>{$stocks->beforeOp}</td>
										<td>{$stocks->afterOp}</td>
										<td>
											{if $stocks->stock && $stocks->stock->gotValue}
												Ilgili Siparis : <a href="#">{$stocks->orderProduct->border->orderCode}</a>
											{else}
												{$stocks->info}
											{/if}
										</td>
										{*<td>
											<button class="btn btn-xs" href="javascript:goIfOk('{$SITEROOT}/backend/delStock/{$stocks->id}')"><i class="icon-remove"></i></button>
										</td>*}
									</tr>
								{/doWhile ($stocks->populate() != false)}
							{/if}
							</tbody>
						</table>
					</div>
					<div class="tab-pane" id="box_tabAdd">
						<!--new stock-->
							<form class="form-horizontal row-border" action="{$SITEROOT}/backend/stockAction" name="stockAction" id="stockAction" method="POST" target="ps">
								<input type="hidden" name="productId" value="{$product->id}"/>
								{getFormToken f="stockAction"}
								{formBox name="amount" label="Miktar" value="" type="text"}
								{formBox name="direction" label="Hareket Yonu" options=$directions value="1" type="radio" info="Stok girisi icin iceri seciniz."}
								{formBox name="opDate" label="Tarihi" value=$today type="date"}
								{formBox name="info" label="Bilgi" value="" type="textarea"}
								<div class="form-actions">
									<button type="submit" class="submit btn btn-primary pull-right">
										Submit <i class="icon-angle-right"></i>
									</button>
								</div>
							</form>
						<!--/new stock-->
					</div>
				</div>
			</div> <!-- /.tabbable portlet-tabs -->
		</div> <!-- /.widget-content -->
	</div> <!-- /.widget .box -->
</div> <!-- /.col-md-12 -->
