<table class="table table-hover table-striped table-bordered table-highlight-head">
	<tbody>
	<tr>
		<td><b>OLD VERSION ({intToDate t=$entry->jdate type="d-M-Y H:i"})</b></td>
		<td align="right"><button class="btn btn-sm btn-danger versionButton" stId="{$entry->id}" >Use this version</button></td>
	</tr>
	<tr>
		<td>Adding User</td>
		<td>{$entry->addedBy->username}</td>
	</tr>
	<tr>
		<td>State</td>
		<td>{$entry->state}</td>
	</tr>
	<tr>
		<td>Language</td>
		<td>{$entry->stLang->label}</td>
	</tr>
	<tr>
		<td>Content</td>
		<td>{$entry->content}</td>
	</tr>
	{if strlen($entry->body)>0}
	<tr>
		<td>Body</td>
		<td>{$entry->body}</td>
	</tr>
	{/if}
</table>

<script>
	{literal}
    $( '.versionButton' ).click( function () {
        if ( confirm( 'Are you sure? Current definition will change.' ) ) {
	        window.frames[0].location.href = '/changeVersion/' + $( this ).attr( 'stId' );
        }
    } )
    {/literal}
</script>