<!DOCTYPE html>
<html lang="en">
<head>
	{include file="_top.tpl"}
	<script>
		{literal}
		$(document).ready(function(){
			"use strict";

			App.init(); // Init layout and core plugins
			Plugins.init(); // Init all plugins
			FormComponents.init(); // Init all form-specific plugins
		});
		{/literal}
	</script>

</head>

<body>

<!-- Header -->
<header class="header navbar navbar-fixed-top" role="banner">
	{include file="_header.tpl"}
</header>
<!-- /.header -->

<div id="container">
	<div id="sidebar" class="sidebar-fixed">
		<div id="sidebar-content">

			<!-- Search Input -->
			<form class="sidebar-search">
				<div class="input-box">
					<button type="submit" class="submit">
						<i class="icon-search"></i>
					</button>
				<span>
					<input type="text" name="quickWord" id="quickWord" placeholder="Search for word...">
				</span>
				</div>
			</form>


			<!--=== Navigation ===-->
			{include file="_nav.tpl"}
			<!-- /Navigation -->

		</div>
		<div id="divider" class="resizeable"></div>
	</div>
	<div id="content">
		<div class="container">
			<!-- Breadcrumbs line -->
			<div class="crumbs">
				<ul id="breadcrumbs" class="breadcrumb">
					<li>
						<i class="icon-home"></i>
						<a href="{$SITEROOT}/home">Panel</a>
					</li>
					<li class="current">
						<a href="{$SITEROOT}/customers" title="">Musteriler</a>
					</li>
				</ul>
				<ul class="crumb-buttons">
					<li class="first"><a href="{$SITEROOT}/addCustomer" title=""><i class="icon-plus"></i><span>Yeni Musteri</span></a></li>
				</ul>

			</div>
			<div class="page-header"></div>
			<!-- /Breadcrumbs line -->
			<div class="row">
				<div class="col-md-12">
					<div class="widget box">
						<div class="widget-header">
							<h4><i class="icon-reorder"></i> Musteriler</h4>
							<div class="toolbar no-padding">
								<div class="btn-group">
									<span class="btn btn-xs widget-collapse"><i class="icon-angle-down"></i></span>
								</div>
							</div>
						</div>
						<div class="widget-content no-padding">
							<table class="table table-hover table-striped table-bordered table-highlight-head">
								<thead>
								<tr>
									<th>Firma</th>
									<th>Il</th>
									<th>Sektor</th>
									<th class="col-md-2">Islem</th>
								</tr>
								</thead>
								<tbody>
								{if $customers->gotValue}
									{doWhile}
									{callUserFunc $customers->loadCity()}
									{callUserFunc $customers->loadCountry()}

										<tr>
											<td>{$customers->name}</td>
											<td>{$customers->city->name}</td>
											<td>{$customers->country->name}</td>
											<td>
												<button class="btn btn-xs" href="{$SITEROOT}/addCustomer/{$customers->id}"><i class="icon-cog"></i></button>
												<button class="btn btn-xs" href="javascript:goIfOk('{$SITEROOT}/delCustomer/{$customers->id}')"><i class="icon-remove"></i></button>
												<button class="btn btn-xs" href="{$SITEROOT}/customerContacts/{$customers->id}"><i class="icon-user"></i></button>
											</td>
										</tr>
									{/doWhile ($customers->populate() != false)}
								{/if}
								</tbody>
							</table>
						</div>
					</div>
				</div>


			</div>
			<!-- /.row -->
			<!-- /Page Content -->
		</div>
		<!-- /.container -->

	</div>
</div>
{include file="footer.tpl"}
</body>
</html>