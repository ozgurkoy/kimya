<!-- Footer -->
<div class="footer">
	{*<a href="#" class="sign-up">Don't have an account yet? <strong>Sign Up</strong></a>*}
</div>
<!-- /Footer -->
<iframe name="ps" id="ps" src="{$SITEROOT}/site/blank.html" style="width:500px;float:right"></iframe>
<script type="text/javascript">
	{literal}
    function render() {
        var d = location.href.replace( SITEROOT + '/', '' ).split( '/' )[0];
        $( "ul#nav a[href*='" + d + "']" ).parents( "li" ).addClass( "open" );
        $( "ul#nav a[href*='" + d + "']" ).parents( "li" ).addClass( "current" );

	    $( '.clearHere' ).html( '' );

        $( 'button[href]' ).click( function () {
            location.href = $( this ).attr( 'href' );
        } )
    }

	$('#quickWord').typeahead({
		remote: SITEROOT+'/xhr/words/%QUERY/0',
		name: 'quickWord',
		selected : function(item) {
			alert(item)
			/*this.$element[0].value = item;
			this.$element[0].form.submit();
			return item;*/
		}
	});

	$('#quickWord').bind('typeahead:selected', function(obj, datum) {
		if(datum.value.length>0)
			location.href=SITEROOT+'/words/'+datum.value;
	})


	render();
	{/literal}
</script>