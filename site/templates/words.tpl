<!DOCTYPE html>
<html lang="en">
<head>
	{include file="_top.tpl"}
	<script>
		{literal}
		$(document).ready(function(){
			"use strict";

			App.init(); // Init layout and core plugins
			Plugins.init(); // Init all plugins
			FormComponents.init(); // Init all form-specific plugins
		});
		{/literal}
	</script>

</head>

<body>

<!-- Header -->
<header class="header navbar navbar-fixed-top" role="banner">
	{include file="_header.tpl"}
</header>
<!-- /.header -->

<div id="container">
	<div id="sidebar" class="sidebar-fixed">
		<div id="sidebar-content">

			<!-- Search Input -->
			<form class="sidebar-search">
				<div class="input-box">
					<button type="submit" class="submit">
						<i class="icon-search"></i>
					</button>
				<span>
					<input type="text" name="quickWord" id="quickWord" placeholder="Search for word...">
				</span>
				</div>
			</form>


			<!--=== Navigation ===-->
			{include file="_nav.tpl"}
			<!-- /Navigation -->

		</div>
		<div id="divider" class="resizeable"></div>
	</div>
	<div id="content">
		<div class="container">
			<!-- Breadcrumbs line -->
			<div class="crumbs">
				<ul id="breadcrumbs" class="breadcrumb">
					<li>
						<i class="icon-home"></i>
						<a href="{$SITEROOT}/home">Panel</a>
					</li>
					<li class="current">
						<a href="{$SITEROOT}/words" title="">Words</a>
					</li>
				</ul>
				<ul class="crumb-buttons">
					<li class="first"><a href="{$SITEROOT}/addWord" title=""><i class="icon-plus"></i><span>New Word</span></a></li>
				</ul>
			</div>
			<div class="page-header"></div>
			<!-- /Breadcrumbs line -->
			{*<div class="row">
				<div class="col-md-12">
					<div class="widget box">
						<div class="widget-header">
							<h4><i class="icon-reorder"></i> Words</h4>
							<div class="toolbar no-padding">
								<div class="btn-group">
									<span class="btn btn-xs widget-collapse"><i class="icon-angle-down"></i></span>
								</div>
							</div>
						</div>
						<div class="widget-content no-padding">
							<table class="table table-hover table-striped table-bordered table-highlight-head">
								<thead>
								<tr>
									<th>Label</th>
									<th>Type</th>
									<th>Categories</th>
									*}{*<th>Sp. Type</th>*}{*
									<th class="col-md-2">Action</th>
								</tr>
								</thead>
								<tbody>
								{if $words->gotValue}
									{doWhile}
										{callUserFunc $words->loadType()}
										{callUserFunc $words->listCategories()}
										<tr id="row_{$words->id}">
											<td>{$words->label}</td>
											<td>{$words->type->name}</td>
											<td>{$words->catsListed}</td>
											*}{*<td>{$words->specialType}</td>*}{*
											<td>
												<button class="btn btn-xs" href="javascript:wordTab({$words->id})"><i class="icon-quote-left bs-tooltip" data-placement="top" data-original-title="Show Language Definitions"></i></button>
												<button class="btn btn-xs" href="{$SITEROOT}/addWord/{$words->id}"><i class="icon-cog"></i></button>
												<button class="btn btn-xs" href="javascript:goIfOk('{$SITEROOT}/delWord/{$words->id}')"><i class="icon-remove"></i></button>

											</td>
										</tr>
									{/doWhile ($words->populate() != false)}
								{/if}
								</tbody>
							</table>
						</div>
					</div>
				</div>


			</div>*}
			<div class="row">
				<div class="col-md-12">
					<div class="widget box">
						<div class="widget-header">
							<h4><i class="icon-reorder"></i> Words </h4>
							<div class="toolbar no-padding">
								<div class="btn-group">
									<span class="btn btn-xs widget-collapse"><i class="icon-angle-down"></i></span>
								</div>
							</div>
						</div>
						<div class="widget-content">
							<table class="table table-striped table-bordered table-hover table-checkable datatable" src="/xhr/wordList" id="tpp">
								<thead>
								<tr>
									<th>Label</th>
									<th>Type</th>
									<th class="noSort">Categories</th>
									{*<th>Sp. Type</th>*}
									<th class="noSort col-md-2">Action</th>
								</tr>
								</thead>
								<tfoot>
								<tr>
									<th tp="text" id="labelSearch">Search by Label</th>
									<th tp="text">Search by Type</th>
									<th tp="text">Search by Name</th>
									{*<th>Sp. Type</th>*}
									<th></th>
								</tr>
								</tfoot>
								<tbody>
								<tr>
									<td></td>
									<td></td>
									<td></td>
									{*<td></td>*}
									<td>{*<span class="label label-success">Approved</span>*}</td>
								</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="row clearHere" id="editRow">
			</div> <!-- /.row -->

			<!-- /.row -->
			<!-- /Page Content -->
		</div>
		<!-- /.container -->

	</div>
</div>
<!--noreplace-->
<script>
	var word = '{$word}';
    {literal}
    function rowCB( nRow, aData, iDisplayIndex ) {
	    var r = $(nRow ).find("td:last" );
	    r.append('<button class="btn btn-xs" href="javascript:wordTab('+aData.rowId+')"><i class="icon-quote-left bs-tooltip" data-placement="top" data-original-title="Show Language Definitions"></i></button>');
	    r.append('<button class="btn btn-xs" href="{$SITEROOT}/addWord/'+aData.rowId+'"><i class="icon-cog"></i></button>');
	    r.append('<button class="btn btn-xs" href="javascript:goIfOk(\'/delWord/'+aData.rowId+'\')"><i class="icon-remove"></i></button>')
    }

    function drawCB(){
	    render();
    }

    function wordTab( id, lang ) {
	    $( 'tr' ).removeClass( 'activeRow' );
	    $( '#row_' + id ).addClass( 'activeRow' );
        if ( !lang ) lang = 0;
        $( "#editRow" ).load( "/wordLang/" + id + '/' + lang, function ( response, status, xhr ) {
            if ( status == "error" ) {
                var msg = "Sorry but there was an error: ";
                alert( msg + xhr.status + " " + xhr.statusText );
            }

        } );
    }

	if(word.length>0)
		setTimeout(function(){$( '#labelSearch input' ).val( word );$( '#labelSearch input').keyup();},1000);

    {/literal}


</script>
<!--/noreplace-->
{include file="footer.tpl"}
</body>
</html>