<!DOCTYPE html>
<html lang="en">
<head>
	{include file="_top.tpl"}
	<script>
		{literal}
		$(document).ready(function(){
			"use strict";

			App.init(); // Init layout and core plugins
			Plugins.init(); // Init all plugins
			FormComponents.init(); // Init all form-specific plugins
		});
		{/literal}
	</script>

</head>

<body>

<!-- Header -->
<header class="header navbar navbar-fixed-top" role="banner">
	{include file="_header.tpl"}
</header>
<!-- /.header -->

<div id="container">
	<div id="sidebar" class="sidebar-fixed">
		<div id="sidebar-content">

			<!-- Search Input -->
			<form class="sidebar-search">
				<div class="input-box">
					<button type="submit" class="submit">
						<i class="icon-search"></i>
					</button>
				<span>
					<input type="text" name="quickWord" id="quickWord" placeholder="Search for word...">
				</span>
				</div>
			</form>


			<!--=== Navigation ===-->
			{include file="_nav.tpl"}
			<!-- /Navigation -->

		</div>
		<div id="divider" class="resizeable"></div>
	</div>
<div id="content">
	<div class="container">
		<!-- Breadcrumbs line -->
		<div class="crumbs">
			<ul id="breadcrumbs" class="breadcrumb">
				<li>
					<i class="icon-home"></i>
					<a href="{$SITEROOT}/home">Panel</a>
				</li>
				<li>
					<i class="icon-road"></i>
					<a href="{$SITEROOT}/users">Users</a>
				</li>
			</ul>

		</div>
		<!-- /Breadcrumbs line -->

		<!--=== Page Header ===-->
		<div class="page-header">
		</div>
		<!-- /Page Header -->

		<!--=== Page Content ===-->


		<div class="row">
			<div class="col-md-12">
				<div class="widget box">
					<div class="widget-header">
						<h4><i class="icon-reorder"></i>
							{if $user->gotValue}
								Edit
							{else}
								Add New
							{/if}
							User
						</h4>
					</div>
					<div class="widget-content">
						<form class="form-horizontal row-border" action="{$SITEROOT}/userAction" name="user" id="user" method="POST" target="ps">
							{if $user->gotValue}
								<input type="hidden" name="id" value="{$user->id}"/>
							{/if}
							{getFormToken f="user"}
							<div class="form-group">
								<label class="col-md-2 control-label" for="input17">Username</label>
								<div class="col-md-10">
									<input type="text" id="username" name="username" class="form-control" value="{$user->username}">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-2 control-label" for="input17">Email</label>
								<div class="col-md-10">
									<input type="text" id="email" name="email" class="form-control" value="{$user->email}">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-2 control-label" for="input17">Password (type to change)</label>
								<div class="col-md-10">
									<input type="password" id="password" name="password" class="form-control" value="">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-2 control-label" for="input17">Is Active?</label>
								<div class="col-md-10">
									<input type="checkbox" id="isActive" name="isActive" class="form-control uniform" value="1" {if $user->isActive==1}checked="checked"{/if}>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-2 control-label" for="input17">Is Root?</label>
								<div class="col-md-10">
									<input type="checkbox" id="isRoot" name="isRoot" class="form-control uniform" value="1" {if $user->isRoot==1}checked="checked"{/if}>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-2 control-label" for="input17">User's Site</label>
								<div class="col-md-10">
									<select class="form-control" name="site">
										<option value="">Select Site</option>
										{if $sites->gotValue}
										{doWhile}
										<option value="{$sites->id}" {if $user->site && $sites->id==$user->site->id}selected{/if}>{$sites->label}</option>
										{/doWhile ($sites->populate())}
										{/if}
									</select>
								</div>
							</div>
							<div class="form-group">
								<div class="widget box">
									<div class="widget-header">
										<h4><i class="icon-reorder"></i> Roles</h4>
									</div>
									<div class="widget-content clearfix">
										<!-- Left box -->
										<div class="left-box">
											<input type="text" id="box1Filter" class="form-control box-filter" placeholder="Filter entries..."><button type="button" id="box1Clear" class="filter">x</button>
											<select id="box1View" multiple="multiple" class="multiple">
												{if $roles->gotValue}
													{doWhile}
														<option value="{$roles->id}">{$roles->label}</option>
													{/doWhile ($roles->populate())}
												{/if}
											</select>
											<span id="box1Counter" class="count-label"></span>
											<select id="box1Storage"></select>
										</div>
										<!--left-box -->

										<!-- Control buttons -->
										<div class="dual-control">
											<button id="to2" type="button" class="btn">&nbsp;&gt;&nbsp;</button>
											<button id="allTo2" type="button" class="btn">&nbsp;&gt;&gt;&nbsp;</button><br>
											<button id="to1" type="button" class="btn">&nbsp;&lt;&nbsp;</button>
											<button id="allTo1" type="button" class="btn">&nbsp;&lt;&lt;&nbsp;</button>
										</div>
										<!--control buttons -->

										<!-- Right box -->
										<div class="right-box">
											<input type="text" id="box2Filter" class="form-control box-filter" placeholder="Filter entries..."><button type="button" id="box2Clear" class="filter">x</button>
											<select id="box2View" name="roles[]" multiple="multiple" class="multiple">
												{if $user->roles && $user->roles->gotValue}
													{doWhile}
														<option value="{$user->roles->id}">{$user->roles->label}</option>
													{/doWhile ($user->roles->populate())}
												{/if}
											</select>
											<span id="box2Counter" class="count-label"></span>
											<select id="box2Storage"></select>
										</div>
										<!--right box -->
									</div>
								</div>
							</div>
							<div class="form-actions">
								<button type="submit" class="submit btn btn-primary pull-right">
									Submit <i class="icon-angle-right"></i>
								</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<!-- /.row -->
		<!-- /Page Content -->
	</div>
	<!-- /.container -->

</div>
</div>

{include file="footer.tpl"}

</body>
</html>