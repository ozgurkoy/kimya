<!DOCTYPE html>
<html lang="en">
<head>
	{include file="_top.tpl"}
	<script>
		{literal}
		$(document).ready(function(){
			"use strict";

			App.init(); // Init layout and core plugins
			Plugins.init(); // Init all plugins
			FormComponents.init(); // Init all form-specific plugins
		});
		{/literal}
	</script>

</head>

<body>

<!-- Header -->
<header class="header navbar navbar-fixed-top" role="banner">
	{include file="_header.tpl"}
</header>
<!-- /.header -->

<div id="container">
	<div id="sidebar" class="sidebar-fixed">
			<div id="sidebar" class="sidebar-fixed">
		<div id="sidebar-content">

			<!-- Search Input -->
			<form class="sidebar-search">
				<div class="input-box">
					<button type="submit" class="submit">
						<i class="icon-search"></i>
					</button>
				<span>
					<input type="text" placeholder="Search a Word...">
				</span>
				</div>
			</form>

			<!--=== Navigation ===-->
			{include file="_nav.tpl"}
			<!-- /Navigation -->

		</div>
		<div id="divider" class="resizeable"></div>
	</div>
		<div id="divider" class="resizeable"></div>
	</div>
	<div id="content">
		<div class="container">
			<!-- Breadcrumbs line -->
			<div class="crumbs">
				<ul id="breadcrumbs" class="breadcrumb">
					<li>
						<i class="icon-home"></i>
						<a href="{$SITEROOT}/home">Panel</a>
					</li>
					<li>
						<i class="icon-road"></i>
						<a href="{$SITEROOT}/actions">Actions</a>
					</li>
					<li class="current">
						<a href="{$SITEROOT}/addAction" title="">New Action</a>
					</li>
				</ul>

			</div>
			<!-- /Breadcrumbs line -->

			<!--=== Page Header ===-->
			<div class="page-header">
			</div>
			<!-- /Page Header -->

			<!--=== Page Content ===-->


			<div class="row">
				<div class="col-md-12">
					<div class="widget box">
						<div class="widget-header">
							<h4><i class="icon-reorder"></i>
								{if $action->gotValue}
									Edit
								{else}
									Add New
								{/if}
								Action
							</h4>
						</div>
						<div class="widget-content">
							<form class="form-horizontal row-border" action="{$SITEROOT}/actionAction" name="action" id="action" method="POST" target="ps">
								{if $action->gotValue}
									<input type="hidden" name="id" value="{$action->id}"/>
								{/if}
								{getFormToken f="action"}
								<div class="form-group">
									<label class="col-md-2 control-label" for="input17">Action Name</label>
									<div class="col-md-10">
										<input type="text" name="label" class="form-control" value="{$action->label}">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2 control-label" for="input17">Action Tasks</label>
									<div class="col-md-10">
										<textarea name="tasks" class="form-control" rows="10">{$action->tasks}</textarea>
									</div>
								</div>
								<div class="form-actions">
									<button type="submit" class="submit btn btn-primary pull-right">
										Submit <i class="icon-angle-right"></i>
									</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
			<!-- /.row -->
			<!-- /Page Content -->
		</div>
		<!-- /.container -->

	</div>
</div>

{include file="footer.tpl"}
<script>
	{literal}
	$("ul#nav a[href='/actions']").parents("li").addClass("open");
	$("ul#nav a[href='/actions']").parents("li").addClass("current");
	{/literal}
</script>
</body>
</html>