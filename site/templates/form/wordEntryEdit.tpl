<form class="form-horizontal row-border" action="{$SITEROOT}/versionAction" name="wordEntryEdit" id="wordEntryEdit" method="POST" target="ps">
	<input type="hidden" name="id" value="{$entry->id}"/>
	<input type="hidden" name="wordId" value="{$entry->states->entries->id}"/>
	{getFormToken f="wordEntryEdit"}
	<div class="form-group">
		<label class="col-md-2 control-label" for="input17">Content</label>
		<div class="col-md-10">
			<textarea name="word_content" id="word_content" class="form-control">{$entry->content}</textarea>
			{if $entry->states->entries->type->requiresParameters==1}
				<span class="help-block">
					You must use these parameters: {$entry->states->entries->bodyParameters}
				</span>
			{/if}
		</div>
	</div>

	<div class="form-group{if $entry->states->entries->type->requiresBody!=1} non{/if}" >
		<label class="col-md-2 control-label" for="input17">Body</label>
		<div class="col-md-10">
			<textarea name="word_body" id="word_body" class="form-control">{$entry->body}</textarea>
			{if $entry->states->entries->type->requiresParameters==1}
				<span class="help-block">
					You must use these parameters: {$entry->states->entries->titleParameters}
				</span>
			{/if}
		</div>
	</div>
	<div class="form-actions">
		<button type="submit" class="submit btn btn-primary pull-right">
			Submit <i class="icon-angle-right"></i>
		</button>
		<a  href="javascript:wordTab('{$entry->states->entries->id}','{$entry->stLang->id}')">Cancel</a>
	</div>
</form>
