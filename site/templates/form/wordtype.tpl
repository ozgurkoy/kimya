<!DOCTYPE html>
<html lang="en">
<head>
	{include file="_top.tpl"}
	<script>
		{literal}
		$(document).ready(function(){
			"use strict";

			App.init(); // Init layout and core plugins
			Plugins.init(); // Init all plugins
			FormComponents.init(); // Init all form-specific plugins
		});
		{/literal}
	</script>

</head>

<body>

<!-- Header -->
<header class="header navbar navbar-fixed-top" role="banner">
	{include file="_header.tpl"}
</header>
<!-- /.header -->

<div id="container">
	<div id="sidebar" class="sidebar-fixed">
		<div id="sidebar-content">

			<!-- Search Input -->
			<form class="sidebar-search">
				<div class="input-box">
					<button type="submit" class="submit">
						<i class="icon-search"></i>
					</button>
				<span>
					<input type="text" name="quickWord" id="quickWord" placeholder="Search for word...">
				</span>
				</div>
			</form>


			<!--=== Navigation ===-->
			{include file="_nav.tpl"}
			<!-- /Navigation -->

		</div>
		<div id="divider" class="resizeable"></div>
	</div>
	<div id="content">
		<div class="container">
			<!-- Breadcrumbs line -->
			<div class="crumbs">
				<ul id="breadcrumbs" class="breadcrumb">
					<li>
						<i class="icon-home"></i>
						<a href="{$SITEROOT}/home">Panel</a>
					</li>
					<li>
						<i class="icon-road"></i>
						<a href="{$SITEROOT}/wordtypes">Word Types</a>
					</li>
				</ul>

			</div>
			<!-- /Breadcrumbs line -->

			<!--=== Page Header ===-->
			<div class="page-header">
			</div>
			<!-- /Page Header -->

			<!--=== Page Content ===-->


			<div class="row">
				<div class="col-md-12">
					<div class="widget box">
						<div class="widget-header">
							<h4><i class="icon-reorder"></i>
								{if $wordtype->gotValue}
									Edit
								{else}
									Add New
								{/if}
								Word Type
							</h4>
						</div>
						<div class="widget-content">
							<form class="form-horizontal row-border" action="{$SITEROOT}/wordtypeAction" name="wordtype" id="wordtype" method="POST" target="ps">
								{if $wordtype->gotValue}
									<input type="hidden" name="id" value="{$wordtype->id}"/>
								{/if}
								{getFormToken f="wordtype"}
								<div class="form-group">
									<label class="col-md-2 control-label" for="input17">Name</label>
									<div class="col-md-10">
										<input type="text" id="name" name="name" class="form-control" value="{$wordtype->name}">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2 control-label" for="input17">Is Active?</label>
									<div class="col-md-10">
										<input type="checkbox" id="isActive" name="isActive" class="form-control uniform" value="1" {if $wordtype->isActive==1}checked="checked"{/if}>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2 control-label" for="input17">Requires Body?</label>
									<div class="col-md-10">
										<input type="checkbox" id="requiresBody" name="requiresBody" class="form-control uniform" value="1" {if $wordtype->requiresBody==1}checked="checked"{/if}>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2 control-label" for="input17">Requires Parameters?</label>
									<div class="col-md-10">
										<input type="checkbox" id="requiresParameters" name="requiresParameters" class="form-control uniform" value="1" {if $wordtype->requiresParameters==1}checked="checked"{/if}>
									</div>
								</div>
								<div class="form-actions">
									<button type="submit" class="submit btn btn-primary pull-right">
										Submit <i class="icon-angle-right"></i>
									</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
			<!-- /.row -->
			<!-- /Page Content -->
		</div>
		<!-- /.container -->

	</div>
</div>

{include file="footer.tpl"}
</body>
</html>