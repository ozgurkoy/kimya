<!DOCTYPE html>
<html lang="en">
<head>
	{include file="_top.tpl"}
	<script>
		{literal}
		$(document).ready(function(){
			"use strict";

			App.init(); // Init layout and core plugins
			Plugins.init(); // Init all plugins
			FormComponents.init(); // Init all form-specific plugins
		});
		{/literal}
	</script>

</head>

<body>

<!-- Header -->
<header class="header navbar navbar-fixed-top" role="banner">
	{include file="_header.tpl"}
</header>
<!-- /.header -->

<div id="container">
	<div id="sidebar" class="sidebar-fixed">
		<div id="sidebar-content">

			<!-- Search Input -->
			<form class="sidebar-search">
				<div class="input-box">
					<button type="submit" class="submit">
						<i class="icon-search"></i>
					</button>
				<span>
					<input type="text" name="quickWord" id="quickWord" placeholder="Search for word...">
				</span>
				</div>
			</form>


			<!--=== Navigation ===-->
			{include file="_nav.tpl"}
			<!-- /Navigation -->

		</div>
		<div id="divider" class="resizeable"></div>
	</div>
	<div id="content">
		<div class="container">
			<!-- Breadcrumbs line -->
			<div class="crumbs">
				<ul id="breadcrumbs" class="breadcrumb">
					<li>
						<i class="icon-home"></i>
						<a href="{$SITEROOT}/home">Panel</a>
					</li>
					<li>
						<i class="icon-road"></i>
						<a href="{$SITEROOT}/languages">Languages</a>
					</li>
				</ul>

			</div>
			<!-- /Breadcrumbs line -->

			<!--=== Page Header ===-->
			<div class="page-header">
			</div>
			<!-- /Page Header -->

			<!--=== Page Content ===-->


			<div class="row">
				<div class="col-md-12">
					<div class="widget box">
						<div class="widget-header">
							<h4><i class="icon-reorder"></i>
								{if $language->gotValue}
									Edit
								{else}
									Add New
								{/if}
								Language
							</h4>
						</div>
						<div class="widget-content">
							<form class="form-horizontal row-border" action="{$SITEROOT}/languageAction" name="language" id="language" method="POST" target="ps">
								{if $language->gotValue}
									<input type="hidden" name="id" value="{$language->id}"/>
								{/if}
								{getFormToken f="language"}
								<div class="form-group">
									<label class="col-md-2 control-label" for="input17">Language Label</label>
									<div class="col-md-10">
										<input type="text" id="label" name="label" class="form-control" value="{$language->label}">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2 control-label" for="input17">Language Short</label>
									<div class="col-md-10">
										<input type="text" id="short" name="short" class="form-control" value="{$language->short}">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2 control-label" for="input17">Language Short Def</label>
									<div class="col-md-10">
										<input type="text" id="shortDef" name="shortDef" class="form-control" value="{$language->shortDef}">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2 control-label" for="input17">Language Alignment</label>
									<div class="col-md-10">
										<select name="alignment" id="alignment">
											<option value="left" {if $language->alignment=="left"}selected{/if}>Left</option>
											<option value="right" {if $language->alignment=="right"}selected{/if}>Right</option>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2 control-label" for="input17">Is Active?</label>
									<div class="col-md-10">
										<input type="checkbox" id="isActive" name="isActive" class="form-control uniform" value="1" {if $language->isActive==1}checked="checked"{/if}>
									</div>
								</div>
								{if $language->gotValue!=1}
                                <div class="form-group">
                                    <label class="col-md-2 control-label" for="input17">Choose one, if you want to clone it from some existing language</label>
                                    <div class="col-md-10">
                                        <select name="cloner" id="cloner">
                                            <option value="">Don't clone</option>
	                                        {if $langs->gotValue}
		                                        {doWhile}
		                                        <option value="{$langs->id}">{$langs->label}</option>
		                                        {/doWhile ($langs->populate())}
	                                        {/if}
                                        </select>
                                    </div>
                                </div>
								{/if}
								<div class="form-group non" id="catChanger">
									<label class="col-md-2 control-label" for="input18">Select Categories</label>
									<div class="col-md-10">
										<select id="cloneCats" name="cloneCats[]" class="select2-select-00 col-md-12 full-width-fix" multiple size="5">

										</select>
									</div>
								</div>

								{*								<div class="form-group">
																	<label class="col-md-2 control-label" for="input17">Default Enabled?</label>
																	<div class="col-md-10">
																		<input type="checkbox" id="defaultEnabled" name="defaultEnabled" class="form-control uniform" value="1" {if $language->defaultEnabled==1}checked="checked"{/if}>
																	</div>
																</div>*}
								<div class="form-actions">
									<button type="submit" class="submit btn btn-primary pull-right">
										Submit <i class="icon-angle-right"></i>
									</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
			<!-- /.row -->
			<!-- /Page Content -->
		</div>
		<!-- /.container -->

	</div>
</div>
<script type="text/javascript">
	{literal}
	$('#cloner' ).change(function(){
		$( '#cloneCats').find('option' ).remove();

		if($(this ).val()=='')  {
			$( '#catChanger' ).hide();
			return;
		}

		$.ajax({
			url: '/xhr/langCats/'+($(this).val()),
			beforeSend: function( xhr ) { xhr.overrideMimeType( "text/plain; charset=x-user-defined" ); },
			dataType:'json'
		}).done(function( data ) {
				for ( var ct in data ) {
					$( '#cloneCats').append('<option value="'+ct+'">'+data[ct]+'</option>')
				}
				$( '#catChanger' ).show();
		});
//		$.ajax({, success: function(val){ console.log(val); } , dataType: 'json' });
	})
	{/literal}
</script>
{include file="footer.tpl"}
</body>
</html>