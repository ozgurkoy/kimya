<!DOCTYPE html>
<html lang="en">
<head>
	{include file="_top.tpl"}
	<script>
		{literal}
		$(document).ready(function(){
			"use strict";

			App.init(); // Init layout and core plugins
			Plugins.init(); // Init all plugins
			FormComponents.init(); // Init all form-specific plugins
		});
		{/literal}
	</script>

</head>

<body>

<!-- Header -->
<header class="header navbar navbar-fixed-top" role="banner">
	{include file="_header.tpl"}
</header>
<!-- /.header -->

<div id="container">
	<div id="sidebar" class="sidebar-fixed">
		<div id="sidebar-content">

			<!-- Search Input -->
			<form class="sidebar-search">
				<div class="input-box">
					<button type="submit" class="submit">
						<i class="icon-search"></i>
					</button>
				<span>
					<input type="text" name="quickWord" id="quickWord" placeholder="Search for word...">
				</span>
				</div>
			</form>


			<!--=== Navigation ===-->
			{include file="_nav.tpl"}
			<!-- /Navigation -->

		</div>
		<div id="divider" class="resizeable"></div>
	</div>
	<div id="content">
		<div class="container">
			<!-- Breadcrumbs line -->
			<div class="crumbs">
				<ul id="breadcrumbs" class="breadcrumb">
					<li>
						<i class="icon-home"></i>
						<a href="{$SITEROOT}/home">Panel</a>
					</li>
					<li>
						<i class="icon-road"></i>
						<a href="{$SITEROOT}/products">Urunler</a>
					</li>
				</ul>

			</div>
			<!-- /Breadcrumbs line -->

			<!--=== Page Header ===-->
			<div class="page-header">
			</div>
			<!-- /Page Header -->

			<!--=== Page Content ===-->


			<div class="row">
				<div class="col-md-12">
					<div class="widget box">
						<div class="widget-header">
							<h4><i class="icon-reorder"></i>
								{if $product->gotValue}
									Urun Guncelle
								{else}
									Yeni Urun 
								{/if}

							</h4>
						</div>
						<div class="widget-content">
							<form class="form-horizontal row-border" action="{$SITEROOT}/productAction" name="product" id="product" method="POST" target="ps">
								{if $product->gotValue}
									<input type="hidden" name="id" value="{$product->id}"/>
								{/if}
								{getFormToken f="product"}

								{formBox name="name" label="Urun Adi" value=$product->name type="text"}
								{formBox name="stockCode" label="Urun Stok Kodu" value=$product->stockCode type="text"}
								{formBox name="stockShort" label="Urun Kisa Stok Kodu" value=$product->stockShort type="text"}
								{formBox name="unit" label="Urun Birimi" value=$product->unit type="arraySelect" options=$units}
								{formBox name="minimumStock" label="Urun Min. Stok" value=$product->minimumStock type="text"}
								{formBox name="obtainingPeriod" label="Urun Tedarik Suresi" value=$product->obtainingPeriod type="text"}
								{formBox name="gtip" label="GTIP" value=$product->gtip type="text"}
								{formBox name="secondUnit" label="Ikinci Birim" value=$product->secondUnit type="arraySelect" options=$secondUnits}
								{formBox name="firstUnitAmount" label="Birim Miktari" value=$product->firstUnitAmount type="text" info="Kg/lt/lbs bazinda, <b>rakamsal</b>, siparis icin."}
								{formBox name="firstUnitLabel" label="Birim Olcu Birimi" value=$product->firstUnitLabel type="text" info="Torba, varil , vs, siparis icin"}
								{formBox name="firstUnitPrice" label="Birim Tutari" value=$product->firstUnitPrice type="text" info="1.25, 2, 10 vs, <b>rakamsal</b>, siparis icin"}
								{formBox name="firstUnitCurrency" label="Birim Para Birimi" value=$product->firstUnitCurrency type="populateSelect" options=$currencies vkey="shortName" info="Siparis icin"}
								{formBox name="firstPackingLabel" label="Ambalaj Miktari" value=$product->firstPackingLabel type="text" info="80 torba gibi, siparis icin"}
								{formBox name="origin" label="Mensei" value=$product->origin type="text" info="Cin, Amerika vs."}
								{formBox name="supplier" label="Uretici" value=$product->supplier type="text"}
								{formBox name="note" label="Not" value=$product->note type="textarea"}
								{formBox name="productSector" label="Urun Sektorleri" value=$product->productSector type="transferSelect" options=$sectors vkey="name"}
								{formBox name="groups" label="Urun Gruplari" value=$product->groups type="transferSelect" options=$groups vkey="name"}


								<div class="form-actions">
									<button type="submit" class="submit btn btn-primary pull-right">
										Submit <i class="icon-angle-right"></i>
									</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
			<!-- /.row -->
			<!-- /Page Content -->
		</div>
		<!-- /.container -->

	</div>
</div>

{include file="footer.tpl"}
</body>
</html>