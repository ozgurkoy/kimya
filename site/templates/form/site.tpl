<!DOCTYPE html>
<html lang="en">
<head>
	{include file="_top.tpl"}
	<script>
		{literal}
		$(document).ready(function(){
			"use strict";

			App.init(); // Init layout and core plugins
			Plugins.init(); // Init all plugins
			FormComponents.init(); // Init all form-specific plugins
		});
		{/literal}
	</script>

</head>

<body>

<!-- Header -->
<header class="header navbar navbar-fixed-top" role="banner">
	{include file="_header.tpl"}
</header>
<!-- /.header -->

<div id="container">
	<div id="sidebar" class="sidebar-fixed">
		<div id="sidebar-content">

			<!-- Search Input -->
			<form class="sidebar-search">
				<div class="input-box">
					<button type="submit" class="submit">
						<i class="icon-search"></i>
					</button>
				<span>
					<input type="text" name="quickWord" id="quickWord" placeholder="Search for word...">
				</span>
				</div>
			</form>


			<!--=== Navigation ===-->
			{include file="_nav.tpl"}
			<!-- /Navigation -->

		</div>
		<div id="divider" class="resizeable"></div>
	</div>
	<div id="content">
		<div class="container">
			<!-- Breadcrumbs line -->
			<div class="crumbs">
				<ul id="breadcrumbs" class="breadcrumb">
					<li>
						<i class="icon-home"></i>
						<a href="{$SITEROOT}/home">Panel</a>
					</li>
					<li>
						<i class="icon-road"></i>
						<a href="{$SITEROOT}/sites">Sites</a>
					</li>
				</ul>

			</div>
			<!-- /Breadcrumbs line -->

			<!--=== Page Header ===-->
			<div class="page-header">
			</div>
			<!-- /Page Header -->

			<!--=== Page Content ===-->


			<div class="row">
				<div class="col-md-12">
					<div class="widget box">
						<div class="widget-header">
							<h4><i class="icon-reorder"></i>
								{if $site->gotValue}
									Edit
								{else}
									Add New
								{/if}
								Site
							</h4>
						</div>
						<div class="widget-content">
							<form class="form-horizontal row-border" action="{$SITEROOT}/siteAction" name="site" id="site" method="POST" target="ps">
								{if $site->gotValue}
									<input type="hidden" name="id" value="{$site->id}"/>
								{/if}
								{getFormToken f="site"}
								<div class="form-group">
									<label class="col-md-2 control-label" for="input17">Site Label</label>
									<div class="col-md-10">
										<input type="text" id="label" name="label" class="form-control" value="{$site->label}">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2 control-label" for="input17">URL</label>
									<div class="col-md-10">
										<input type="text" id="url" name="url" class="form-control" value="{$site->url}">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2 control-label" for="input17">Is Development?</label>
									<div class="col-md-10">
										<input type="checkbox" id="isDevelopment" name="isDevelopment" class="form-control uniform" value="1" {if $site->isDevelopment==1}checked="checked"{/if}>
									</div>
								</div>
								<div class="form-group">
									<div class="widget box">
										<div class="widget-header">
											<h4><i class="icon-reorder"></i> Languages</h4>
										</div>
										<div class="widget-content clearfix">
											<!-- Left box -->
											<div class="left-box">
												<input type="text" id="box1Filter" class="form-control box-filter" placeholder="Filter entries..."><button type="button" id="box1Clear" class="filter">x</button>
												<select id="box1View" multiple="multiple" class="multiple">
													{if $langs->gotValue}
														{doWhile}
															<option value="{$langs->id}">{$langs->label}</option>
														{/doWhile ($langs->populate())}
													{/if}
												</select>
												<span id="box1Counter" class="count-label"></span>
												<select id="box1Storage"></select>
											</div>
											<!--left-box -->

											<!-- Control buttons -->
											<div class="dual-control">
												<button id="to2" type="button" class="btn">&nbsp;&gt;&nbsp;</button>
												<button id="allTo2" type="button" class="btn">&nbsp;&gt;&gt;&nbsp;</button><br>
												<button id="to1" type="button" class="btn">&nbsp;&lt;&nbsp;</button>
												<button id="allTo1" type="button" class="btn">&nbsp;&lt;&lt;&nbsp;</button>
											</div>
											<!--control buttons -->

											<!-- Right box -->
											<div class="right-box">
												<input type="text" id="box2Filter" class="form-control box-filter" placeholder="Filter entries..."><button type="button" id="box2Clear" class="filter">x</button>
												<select id="box2View" name="langs[]" multiple="multiple" class="multiple">
													{if $site->langs && $site->langs->gotValue}
														{doWhile}
															<option value="{$site->langs->id}">{$site->langs->label}</option>
														{/doWhile ($site->langs->populate())}
													{/if}
												</select>
												<span id="box2Counter" class="count-label"></span>
												<select id="box2Storage"></select>
											</div>
											<!--right box -->
										</div>
									</div>
								</div>
								<div class="form-actions">
									<button type="submit" class="submit btn btn-primary pull-right">
										Submit <i class="icon-angle-right"></i>
									</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
			<!-- /.row -->
			<!-- /Page Content -->
		</div>
		<!-- /.container -->

	</div>
</div>

{include file="footer.tpl"}
</body>
</html>