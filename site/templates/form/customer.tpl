<!DOCTYPE html>
<html lang="en">
<head>
	{include file="_top.tpl"}
	<script>
		{literal}
		$(document).ready(function(){
			"use strict";

			App.init(); // Init layout and core plugins
			Plugins.init(); // Init all plugins
			FormComponents.init(); // Init all form-specific plugins
		});
		{/literal}
	</script>

</head>

<body>

<!-- Header -->
<header class="header navbar navbar-fixed-top" role="banner">
	{include file="_header.tpl"}
</header>
<!-- /.header -->

<div id="container">
	<div id="sidebar" class="sidebar-fixed">
		<div id="sidebar-content">

			<!-- Search Input -->
			<form class="sidebar-search">
				<div class="input-box">
					<button type="submit" class="submit">
						<i class="icon-search"></i>
					</button>
				<span>
					<input type="text" name="quickWord" id="quickWord" placeholder="Search for word...">
				</span>
				</div>
			</form>


			<!--=== Navigation ===-->
			{include file="_nav.tpl"}
			<!-- /Navigation -->

		</div>
		<div id="divider" class="resizeable"></div>
	</div>
	<div id="content">
		<div class="container">
			<!-- Breadcrumbs line -->
			<div class="crumbs">
				<ul id="breadcrumbs" class="breadcrumb">
					<li>
						<i class="icon-home"></i>
						<a href="{$SITEROOT}/home">Anasayfa</a>
					</li>
					<li>
						<i class="icon-road"></i>
						<a href="{$SITEROOT}/customers">Musteriler</a>
					</li>
				</ul>

			</div>
			<!-- /Breadcrumbs line -->

			<!--=== Page Header ===-->
			<div class="page-header">
			</div>
			<!-- /Page Header -->

			<!--=== Page Content ===-->


			<div class="row">
				<div class="col-md-12">
					<div class="widget box">
						<div class="widget-header">
							<h4><i class="icon-reorder"></i>
								{if $customer->gotValue}
									Musteri Guncelleme
								{else}
									Yeni Musteri
								{/if}

							</h4>
						</div>
						<div class="widget-content">
							<form class="form-horizontal row-border" action="{$SITEROOT}/customerAction" name="customer" id="customer" method="POST" target="ps">
								{if $customer->gotValue}
									<input type="hidden" name="id" value="{$customer->id}"/>
								{/if}
								{getFormToken f="customer"}
								<div class="form-group">
									<label class="col-md-2 control-label" for="input17">Firma Adi</label>
									<div class="col-md-10">
										<input type="text" id="name" name="name" class="form-control" value="{$customer->name}">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2 control-label" for="input17">Adres</label>
									<div class="col-md-10">
										<textarea row="10" name="address" id="address" class="form-control uniform">{$customer->address}</textarea>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2 control-label" for="input17">Posta Kodu</label>
									<div class="col-md-10">
										<input type="text" id="postalCode" name="postalCode" class="form-control uniform" value="{$customer->postalCode}">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2 control-label" for="input17">Sehir</label>
									<div class="col-md-10">
										<select name="city" id="city">
											<option value="">Seciniz</option>
											{if $cities->gotValue}
												{doWhile}
													<option value="{$cities->id}"  {if $customer->city==$cities->id}selected{/if}>{$cities->name}</option>
												{/doWhile ($cities->populate())}
											{/if}
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2 control-label" for="input17">Ulke</label>
									<div class="col-md-10">
										<select name="country" id="country">
											<option value="">Seciniz</option>
											{if $countries->gotValue}
												{doWhile}
													<option value="{$countries->id}" {if $customer->country==$countries->id}selected{/if}>{$countries->name}</option>
												{/doWhile ($countries->populate())}
											{/if}
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2 control-label" for="input17">Vergi Dairesi</label>
									<div class="col-md-10">
										<input type="text" id="taxOffice" name="taxOffice" class="form-control uniform" value="{$customer->taxOffice}">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2 control-label" for="input17">Vergi No</label>
									<div class="col-md-10">
										<input type="text" id="taxNo" name="taxNo" class="form-control uniform" value="{$customer->taxNo}">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2 control-label" for="input17">Valor Gunu</label>
									<div class="col-md-10">
										<input type="text" id="valorDay" name="valorDay" class="form-control uniform" value="{$customer->valorDay}">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2 control-label" for="input17">Odeme Sekli</label>
									<div class="col-md-10">
										<input type="text" id="paymentType" name="paymentType" class="form-control uniform" value="{$customer->paymentType}">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2 control-label" for="input17">Ciro</label>
									<div class="col-md-10">
										<input type="text" id="endorsement" name="endorsement" class="form-control uniform" value="{$customer->endorsement}">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2 control-label" for="input17">Kredi Limiti</label>
									<div class="col-md-10">
										<input type="text" id="creditLimit" name="creditLimit" class="form-control uniform" value="{$customer->creditLimit}">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2 control-label" for="input17">Ozel Iskonto</label>
									<div class="col-md-10">
										<input type="text" id="sale" name="sale" class="form-control uniform" value="{$customer->sale}">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2 control-label" for="input17">Fiyat Tipi</label>
									<div class="col-md-10">
										<input type="text" id="priceType" name="priceType" class="form-control uniform" value="{$customer->priceType}">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2 control-label" for="input17">Not</label>
									<div class="col-md-10">
										<textarea row="10" name="customerNote" class="form-control uniform">{$customer->customerNote}</textarea>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2 control-label" for="input17">Musteri Temsilcisi</label>
									<div class="col-md-10">
										<select name="contact" id="contact">
											<option value="">Seciniz</option>
											{if $users->gotValue}
												{doWhile}
													<option value="{$users->id}"  {if $customer->related==$users->id}selected{/if}>{$users->username}</option>
												{/doWhile ($users->populate())}
											{/if}
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2 control-label" for="input17">Musteri Durumu</label>
									<div class="col-md-10">
										<select name="customerState" id="customerState">
											<option value="">Seciniz</option>
											<option value="potential" {if $customer->customerState=="potential"}selected{/if}>Potansiyel</option>
											<option value="active" {if $customer->customerState=="active"}selected{/if}>Aktif</option>
											<option value="passive" {if $customer->customerState=="passive"}selected{/if}>Pasif</option>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2 control-label" for="input17">Sektor</label>
									<div class="col-md-10">
										<select name="sector" id="sector">
											<option value="">Seciniz</option>
											{if $sector->gotValue}
												{doWhile}
													<option value="{$sector->id}"  {if $customer->sector==$sector->id}selected{/if}>{$sector->name}</option>
												{/doWhile ($sector->populate())}
											{/if}
										</select>
									</div>
								</div>


								<div class="form-actions">
									<button type="submit" class="submit btn btn-primary pull-right">
										Submit <i class="icon-angle-right"></i>
									</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
			<!-- /.row -->
			<!-- /Page Content -->
		</div>
		<!-- /.container -->

	</div>
</div>

{include file="footer.tpl"}
</body>
</html>