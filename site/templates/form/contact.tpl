<!DOCTYPE html>
<html lang="en">
<head>
	{include file="_top.tpl"}
	<script>
		{literal}
		$(document).ready(function(){
			"use strict";

			App.init(); // Init layout and core plugins
			Plugins.init(); // Init all plugins
			FormComponents.init(); // Init all form-specific plugins
		});
		{/literal}
	</script>

</head>

<body>

<!-- Header -->
<header class="header navbar navbar-fixed-top" role="banner">
	{include file="_header.tpl"}
</header>
<!-- /.header -->

<div id="container">
	<div id="sidebar" class="sidebar-fixed">
		<div id="sidebar-content">

			<!-- Search Input -->
			<form class="sidebar-search">
				<div class="input-box">
					<button type="submit" class="submit">
						<i class="icon-search"></i>
					</button>
				<span>
					<input type="text" name="quickWord" id="quickWord" placeholder="Search for word...">
				</span>
				</div>
			</form>


			<!--=== Navigation ===-->
			{include file="_nav.tpl"}
			<!-- /Navigation -->

		</div>
		<div id="divider" class="resizeable"></div>
	</div>
	<div id="content">
		<div class="container">
			<!-- Breadcrumbs line -->
			<div class="crumbs">
				<ul id="breadcrumbs" class="breadcrumb">
					<li>
						<i class="icon-home"></i>
						<a href="{$SITEROOT}/home">Panel</a>
					</li>
					<li>
						<i class="icon-road"></i>
						<a href="{$SITEROOT}/customerContacts/{$customer->id}">{$customer->name}</a>
					</li>
				</ul>

			</div>
			<!-- /Breadcrumbs line -->

			<!--=== Page Header ===-->
			<div class="page-header">
			</div>
			<!-- /Page Header -->

			<!--=== Page Content ===-->


			<div class="row">
				<div class="col-md-12">
					<div class="widget box">
						<div class="widget-header">
							<h4><i class="icon-reorder"></i>
								{if $contact->gotValue}
									Kontak Guncelle
								{else}
									Yeni Kontak
								{/if}

							</h4>
						</div>
						<div class="widget-content">
							<form class="form-horizontal row-border" action="{$SITEROOT}/customerContactsAction" name="contact" id="contact" method="POST" target="ps">
								<input type="hidden" name="customerId" value="{$customer->id}"/>

								{if $contact->gotValue}
									<input type="hidden" name="id" value="{$contact->id}"/>
								{/if}
								{getFormToken f="contact"}
								<div class="form-group">
									<label class="col-md-2 control-label" for="input17">Adi</label>
									<div class="col-md-10">
										<input type="text" id="name" name="name" class="form-control" value="{$contact->name}">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2 control-label" for="input17">Surname</label>
									<div class="col-md-10">
										<input type="text" id="surname" name="surname" class="form-control" value="{$contact->surname}">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2 control-label" for="input17">Pozisyon</label>
									<div class="col-md-10">
										<input type="text" id="position" name="position" class="form-control" value="{$contact->position}">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2 control-label" for="input17">Faks</label>
									<div class="col-md-10">
										<input type="text" id="fax" name="fax" class="form-control" value="{$contact->fax}">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2 control-label" for="input17">Tel-1</label>
									<div class="col-md-10">
										<input type="text" id="tel1" name="tel1" class="form-control" value="{$contact->tel1}">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2 control-label" for="input17">Tel-2</label>
									<div class="col-md-10">
										<input type="text" id="tel2" name="tel2" class="form-control" value="{$contact->tel2}">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2 control-label" for="input17">Tel-3</label>
									<div class="col-md-10">
										<input type="text" id="tel3" name="tel3" class="form-control" value="{$contact->tel3}">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2 control-label" for="input17">Email</label>
									<div class="col-md-10">
										<input type="text" id="email" name="email" class="form-control" value="{$contact->email}">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2 control-label" for="input17">Not</label>
									<div class="col-md-10">
										<textarea id="note" name="note" class="form-control">{$contact->note}</textarea>
									</div>
								</div>
								<div class="form-actions">
									<button type="submit" class="submit btn btn-primary pull-right">
										Submit <i class="icon-angle-right"></i>
									</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
			<!-- /.row -->
			<!-- /Page Content -->
		</div>
		<!-- /.container -->

	</div>
</div>

{include file="footer.tpl"}
</body>
</html>