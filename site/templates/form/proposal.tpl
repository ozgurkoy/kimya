<!DOCTYPE html>
<html lang="en">
<head>
	<style>
		{literal}
		#step2, #step3, #propProductFormBox{
			display:none;
		}
		{/literal}
	</style>
	{include file="_top.tpl"}
	<script>
		{literal}
		$(document).ready(function(){
			"use strict";

			App.init(); // Init layout and core plugins
			Plugins.init(); // Init all plugins
			FormComponents.init(); // Init all form-specific plugins
		});
		{/literal}
	</script>

</head>

<body>

<!-- Header -->
<header class="header navbar navbar-fixed-top" role="banner">
	{include file="_header.tpl"}
</header>
<!-- /.header -->

<div id="container">
	<div id="sidebar" class="sidebar-fixed">
		<div id="sidebar-content">

			<!-- Search Input -->
			<form class="sidebar-search">
				<div class="input-box">
					<button type="submit" class="submit">
						<i class="icon-search"></i>
					</button>
				<span>
					<input type="text" name="quickWord" id="quickWord" placeholder="Search for word...">
				</span>
				</div>
			</form>


			<!--=== Navigation ===-->
			{include file="_nav.tpl"}
			<!-- /Navigation -->

		</div>
		<div id="divider" class="resizeable"></div>
	</div>
	<div id="content">
		<div class="container">
			<!-- Breadcrumbs line -->
			<div class="crumbs">
				<ul id="breadcrumbs" class="breadcrumb">
					<li>
						<i class="icon-home"></i>
						<a href="{$SITEROOT}/home">Panel</a>
					</li>
					<li>
						<i class="icon-road"></i>
						<a href="{$SITEROOT}/proposals">Yeni Teklif</a>
					</li>
				</ul>

			</div>
			<!-- /Breadcrumbs line -->

			<!--=== Page Header ===-->
			<div class="page-header">
			</div>
			<!-- /Page Header -->

			<!--=== Page Content ===-->


			<div class="row">
				<div class="col-md-12">
					<div class="widget box">
						<div class="widget-header">
							<h4><i class="icon-reorder"></i>
								{if $proposal->gotValue}
									Teklifi Guncelle
								{else}
									Yeni Teklif
								{/if}
								<span id="stepInfo">1. Adim - Teklif Bilgileri</span>
							</h4>
						</div>
						<div class="widget-content">
							<form class="form-horizontal row-border" action="javascript:void(0)" name="proposal" id="proposal" method="POST" target="ps">
								<input type="hidden" name="productsColl" id="productsColl" value="">
								<div id="step1" info="1. Adim - Teklif Bilgileri">
								{if $proposal->gotValue}
									<input type="hidden" name="id" id="id" value="{$proposal->id}"/>

								{/if}
								{getFormToken f="proposal"}
								{callUserFunc $proposal->loadProposalContact()}


								{formBox name="date" label="Tarih" value=$proposal->date type="date"}
								{formBox name="validTill" label="Gecerlilik Tarihi" value=$proposal->validTill type="date"}
								{formBox name="proposalCode" label="Teklif Kodu" value=$proposal->proposalCode type="text" info="Teklif kodu"}
								{formBox name="paymentInfo" label="Odeme Bilgisi" value=$proposal->paymentInfo type="text" info="Or:90 gun vadeli cek"}
								{formBox name="deliveryInfo" label="Teslimat Bilgisi" value=$proposal->deliveryInfo type="text" info="Or:Ankara fabrikasi teslim"}
								{formBox name="extraInfo" label="Extra Bilgi" value=$proposal->extraInfo type="text" info="Or:kdv vs"}
								{formBox name="customer" label="Musteri" value=null options=$customers type="populateSelect" vkey="name"}
								<div class="form-group non" id="contBox">
									<label class="col-md-2 control-label" for="input17">Kontak</label>
									<div class="col-md-10">
										<select name="contact" id="contact">
											<option value="">Seciniz</option>
										</select>
									</div>
								</div>

								<div class="form-actions">
									<button class="btn btn-primary pull-right moveButton" tg="1">
										Devam <i class="icon-angle-right"></i>
									</button>
								</div>
								</div>
								<div id="step2"  info="2. Adim - Teklif Urun Secimi">

									<div class="form-actions">
										<a href="javascript:productForm()" class="pull-right">Teklife Urun Ekle </a><br/><br/>
										<table class="table table-hover table-striped table-bordered table-highlight-head" id="productsAdded">
											<thead>
											<tr>
												<th>Urun</th>
												<th>Miktar</th>
												<th>Ambalaj Sayisi</th>
												<th>Birim Fiyat</th>
												<th class="col-md-2">Islem</th>
											</tr>
											</thead>
											<tbody id="tbC">

											</tbody>
										</table>

										<div class="widget-content" id="propProductFormBox">
											<div id="propProductForm">
												<input type="hidden" name="propProductFormId" value=""/>

												{formBox name="productId" label="Urun" value=null type="populateSelect" class="hids" options=$products vkey="name"}
												{formBox name="unitAmount" label="Birim Miktari" value=null type="text" class="hids" info="Bir birim urundeki miktar. Or:25(kg) - kg urun biriminden eklenecektir"}
												{formBox name="unitPrice" label="Birim Fiyati" value=null type="text" class="hids" info="Teklifteki bu urunun birim ucreti"}
												{formBox name="unitCurrency" label="Birim Para Birimi" value=null options=$currencies type="populateSelect" vkey="name" class="hids" }
												{formBox name="unitLabel" label="Ambalaj Birimi" value=null type="text" class="hids" info="Bir birim urunun ambalaji. Or:torba, varil"}
												{formBox name="packAmount" label="Ambalaj Sayisi" value=null type="text" class="hids" info="Teklifteki bu urunun satilan ambalaj sayisi(torba, varil vs). Or:80 (torba)"}
												{formBox name="amount" label="Urun Miktari" value=null type="text" class="hids" info="Teklifteki bu urunun satilan birim sayisi. Or:2000(kg)"}

												<div class="form-actions">
													<button type="submit" class="submit btn btn-primary pull-right" id="addPross">
														Ekle <i class="icon-angle-right"></i>
													</button>
												</div>
											</div>
										</div>


										<button class="submit btn btn-primary pull-right moveButton" tg="1" id="summaryButton">
											Teklif Ozeti <i class="icon-angle-right"></i>
										</button>
										<button class="submit btn btn-primary pull-left moveButton" tg="-1">
											<i class="icon-angle-left"></i> Geri
										</button>
									</div>

								</div>
								<div id="step3"  info="3. Adim - Detay ve onay">
								<div class="form-actions">

									<h3>Teklif Bilgileri</h3>
									<table class="table table-hover table-striped table-bordered table-highlight-head" id="proposalSummary">
										<tbody>
										<tr>
											<td>Tarih</td>
											<td id="sumF_date"></td>
										</tr>
										<tr>
											<td>Gecerlilik Tarih</td>
											<td id="sumF_validTill"></td>
										</tr>
										<tr>
											<td>Teklif Kodu</td>
											<td id="sumF_proposalCode"></td>
										</tr>
										<tr>
											<td>Odeme Bilgisi</td>
											<td id="sumF_paymentInfo"></td>
										</tr>
										<tr>
											<td>Teslimat Bilgisi</td>
											<td id="sumF_deliveryInfo"></td>
										</tr>
										<tr>
											<td>Extra Bilgi</td>
											<td id="sumF_extraInfo"></td>
										</tr>
										<tr>
											<td>Musteri</td>
											<td id="sumF_cstm"></td>
										</tr>
										<tr>
											<td>Kontak</td>
											<td id="sumF_cntc"></td>
										</tr>
										</tbody>
									</table>

									<h3>Urunler</h3>
									<table class="table table-hover table-striped table-bordered table-highlight-head" id="productsAddedSummary">
										<thead>
										<tr>
											<th>Urun</th>
											<th>Miktar</th>
											<th>Ambalaj Sayisi</th>
											<th>Birim Fiyat</th>
										</tr>
										</thead>
										<tbody id="tbCS">

										</tbody>
									</table>



									<button type="submit" class="submit btn btn-primary pull-right" id="approveButton">
										Teklifi Onayla <i class="icon-angle-right"></i>
									</button>
									<button class="submit btn btn-primary pull-left moveButton" tg="-1">
										<i class="icon-angle-left"></i> Geri
									</button>

								</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
			<div class="row clearHere" id="editRow">
			</div> <!-- /.row -->

			<!-- /.row -->
			<!-- /Page Content -->
		</div>
		<!-- /.container -->

	</div>
</div>
<script type="text/javascript">
	{if $proposal->proposalContact}
	{callUserFunc $proposal->proposalContact->loadCustomer()}
	var loadedCustId = {$proposal->proposalContact->contacts->id};
	{/if}
	{literal}
	var products = {};
	{/literal}
	{if $proposal->gotValue}
	{callUserFunc $proposal->loadProposalProducts()}
	{if $proposal->proposalProducts && $proposal->proposalProducts->gotValue}
	{doWhile}
	{callUserFunc $proposal->proposalProducts->loadPProduct()}
	{*
	{"2":{"productId":"2","productText":"birinci urun 2","unitAmount":"10","unitPrice":"10","unitCurrency":"1","unitLabel":"varil","packAmount":"33","amount":"444"}}
	*}
	products[{$proposal->proposalProducts->pProduct->id}] = new Object();

	products[{$proposal->proposalProducts->pProduct->id}]["productId"] = {$proposal->proposalProducts->pProduct->id};
	products[{$proposal->proposalProducts->pProduct->id}]["productText"] = '{$proposal->proposalProducts->pProduct->name}';
	products[{$proposal->proposalProducts->pProduct->id}]["unitAmount"] = {$proposal->proposalProducts->unitAmount};
	products[{$proposal->proposalProducts->pProduct->id}]["unitPrice"] = {$proposal->proposalProducts->unitPrice};
	products[{$proposal->proposalProducts->pProduct->id}]["unitCurrency"] = {$proposal->proposalProducts->unitCurrency};
	products[{$proposal->proposalProducts->pProduct->id}]["unitLabel"] = '{$proposal->proposalProducts->packingLabel}';
	products[{$proposal->proposalProducts->pProduct->id}]["packAmount"] = {$proposal->proposalProducts->packAmount};
	products[{$proposal->proposalProducts->pProduct->id}]["amount"] = {$proposal->proposalProducts->amount};


	{/doWhile ($proposal->proposalProducts->populate())}
	{/if}
	{/if}

	{literal}
	if(loadedCustId){
		$('#customer' ).val(loadedCustId);
		setTimeout(function(){$('#customer').change()},500);
	}
	var ap = 1;


    var customerName = '';
    var contactName = '';



    $( '#approveButton' ).click( function () {
	    if(confirm('Emin misiniz?')){
		    $( '#productsColl' ).val( JSON.stringify( products ) );


		    $.ajax( {
			    url        : '/proposalAction',
			    data       : $('#proposal' ).serialize(),
			    method     : 'post'
		    } ).done( function ( data ) {
					    console.log(data)
					    if(data.indexOf('good')!=-1){
						    message( 'good', $('#id' ).length>0?'Teklif guncellendi':'Teklif olusturuldu' );
						    setTimeout(function(){
							    location.href = '/proposals';
						    },1000)
					    }
				    }
		    );

	    }
    });

    $( '.moveButton' ).click( function () {
	    var ret = false;
        var stp = $( this ).attr( 'tg' );
		if(stp==1){
		    eval('ret = validateStep'+ap+'()');
		    if(ret===false) return false;
		}


	    $( '#step' + ap ).hide();
        ap = parseInt( ap + parseInt( stp ) );

	    $( '#step' + ap ).show();

	    $( "#stepInfo" ).html( $( '#step' + ap ).attr( "info" ) );

        return false;
    } );

    $('#summaryButton' ).click(function(){
	    $( '#tbCS tr' ).remove();

	    for(var ip in products){
		    if(products[ip]===null) continue;

		    $('#tbCS' ).append('<tr id="rs_'+ip+'"></tr>');
		    var tr = $( '#tbCS' ).find( 'tr#rs_' + ip );
		    tr.append('<td>'+products[ip].productText+'</td>')
		    tr.append('<td>'+products[ip].amount+'</td>')
		    tr.append('<td>'+products[ip].packAmount+'</td>')
		    tr.append('<td>'+products[ip].unitPrice+'</td>')
	    }

	    $('#proposalSummary' ).find('tr' ).find('td:last' ).each(function(ax,e){
		    var nid = $( e ).attr( 'id' ).replace( 'sumF_', '' );
		    if($('#'+nid ).length>0)
			    $( e ).html( $( '#' + nid ).val() );
	    });

	    $( '#sumF_cntc' ).html( contactName );
	    $( '#sumF_cstm' ).html( customerName );

    });

    function productForm( id ) {
        $( '#propProductFormBox' ).show();
    }

    function redrawPR(){
	    $( '#tbC tr' ).remove();
	    $( '#propProductFormBox' ).hide();

		for(var ip in products){
			if(products[ip]===null) continue;
			$('#tbC' ).append('<tr id="r_'+ip+'"></tr>');
			var tr = $( '#tbC' ).find( 'tr#r_' + ip );
			tr.append('<td>'+products[ip].productText+'</td>')
			tr.append('<td>'+products[ip].amount+'</td>')
			tr.append('<td>'+products[ip].packAmount+'</td>')
			tr.append('<td>'+products[ip].unitPrice+'</td>')
			tr.append('<td><a href="javascript:delRow('+products[ip].productId+')">Sil</a></td>')
		}
    }
	function delRow(ip){
		products[ip]=null;

		resetProductForm();
		redrawPR();
	}

    function resetProductForm(){
	    $( '#propProductForm input' ).val("");
	    $( '#propProductForm select' ).val("");
    }

    function validateStep1(){
		var fields = [
			'date',
			'validTill',
			'proposalCode',
			'paymentInfo',
			'deliveryInfo',
			'extraInfo',
			'customer'
		];
	    var errs = {},gotError = false;

        for ( var fs = 0; fs < fields.length; fs++ ) {
            if ( $( '#' + fields[fs] ).val().length == 0 ) {
	            gotError = true;
	            errs[fields[fs]] = 'Gerekli alan';
            }
        }
	    if(gotError){
		    fieldError( 'proposal', JSON.stringify(errs));
		    return false;
	    }
	    else{
		    customerName = $( "#customer" ).text();
		    contactName = $( "#contact" ).text();
		    redrawPR();
	        return true;}
    }

    function validateStep2(){
	    var pc=0;


	    for(var ip in products){
		    if(products[ip]===null) continue;
		    pc++;
	    }


	    if(pc==0) {
		    alert( 'Teklif icin urun secmelisiniz.' );
		    return false;
	    }

	    return true;
    }

    $( '#customer' ).change( function () {
        var vl = $( this ).val();

        if ( vl.length > 0 ) {
            $.ajax( {
                url        : '/xhr/contacts/' + ($( this ).val()),
                beforeSend : function ( xhr ) {
                    xhr.overrideMimeType( "text/plain; charset=x-user-defined" );
                }
            } ).done( function ( data ) {
                        var cons = JSON.parse( data );
                        $( '#contact' ).find( 'option' ).remove();

                        for ( var dd in cons ) {
                            $( '#contact' ).append( '<option value="' + dd + '">' + cons[dd] + '</option>' );
                        }

                        $( '#contBox' ).show();
                    }
            );
        }
        else
            $( '#contBox' ).hide();
    } );

    $('#addPross' ).click(function(){
	    if($('#productId' ).val()==''){
		    alert( 'Urun Seciniz' );
		    return;
	    }

	    var r0 = {
			productId : $('#productId' ).val(),
			productText : $('#productId' ).find('option:selected').text(),
			unitAmount : $('#unitAmount' ).val(),
			unitPrice : $('#unitPrice' ).val(),
			unitCurrency : $('#unitCurrency' ).val(),
			unitLabel : $('#unitLabel' ).val(),
			packAmount : $('#packAmount' ).val(),
			amount : $('#amount' ).val()
	    };

	    products[$('#productId' ).val()] = r0;
	    resetProductForm();
	    redrawPR();
    })

    $( '#productId' ).change( function () {
        var vl = $( this ).val();

        if ( vl.length > 0 ) {
            $.ajax( {
                url        : '/xhr/productInfo/' + ($( this ).val()),
                beforeSend : function ( xhr ) {
                    xhr.overrideMimeType( "text/plain; charset=x-user-defined" );
                }
            } ).done( function ( data ) {
                        var cons = JSON.parse( data );
                        $( '#unitAmount' ).val( cons.firstUnitAmount );
                        $( '#unitPrice' ).val( cons.firstUnitPrice );
                        $( '#unitCurrency' ).val( cons.firstUnitCurrency );
                        $( '#unitLabel' ).val( cons.firstUnitLabel );
                    }
            );
        }
//        else
//        alert('no')
//            $( '#contBox' ).hide();
    } )
    {/literal}

</script>
{include file="footer.tpl"}
</body>
</html>