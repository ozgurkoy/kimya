<!DOCTYPE html>
<html lang="en">
<head>
	{include file="_top.tpl"}
	<script>
		{literal}
		$(document).ready(function(){
			"use strict";

			App.init(); // Init layout and core plugins
			Plugins.init(); // Init all plugins
			FormComponents.init(); // Init all form-specific plugins
		});
		{/literal}
	</script>

</head>

<body>

<!-- Header -->
<header class="header navbar navbar-fixed-top" role="banner">
	{include file="_header.tpl"}
</header>
<!-- /.header -->

<div id="container">
<div id="sidebar" class="sidebar-fixed">
	<div id="sidebar-content">

		<!-- Search Input -->
		<form class="sidebar-search" action="javascript:alert(1)">
			<div class="input-box">
				<button type="submit" class="submit">
					<i class="icon-search"></i>
				</button>
				<span>
					<input type="text" name="quickWord" id="quickWord" placeholder="Search for word...">
				</span>
			</div>
		</form>


		<!--=== Navigation ===-->
		{include file="_nav.tpl"}
		<!-- /Navigation -->

	</div>
	<div id="divider" class="resizeable"></div>
</div>
	<div id="content">
		<div class="container">
			<!-- Breadcrumbs line -->
			<div class="crumbs">
				<ul id="breadcrumbs" class="breadcrumb">
					<li>
						<i class="icon-home"></i>
						<a href="{$SITEROOT}/home">Panel</a>
					</li>
					<li>
						<i class="icon-road"></i>
						<a href="{$SITEROOT}/words">Words</a>
					</li>
				</ul>

			</div>
			<!-- /Breadcrumbs line -->

			<!--=== Page Header ===-->
			<div class="page-header">
			</div>
			<!-- /Page Header -->

			<!--=== Page Content ===-->


			<div class="row">
				<div class="col-md-12">
					<div class="widget box">
						<div class="widget-header">
							<h4><i class="icon-reorder"></i>
								{if $word->gotValue}
									Edit
								{else}
									Add New
								{/if}
								Word
							</h4>
						</div>
						<div class="widget-content">
							<form class="form-horizontal row-border" action="{$SITEROOT}/wordAction" name="word" id="word" method="POST" target="ps">
								{if $word->gotValue}
									<input type="hidden" name="id" value="{$word->id}"/>
								{/if}
								{getFormToken f="word"}
								<div class="form-group">
									<label class="col-md-2 control-label" for="input17">Label</label>
									<div class="col-md-10">
										<input type="text" id="label" name="label" class="form-control" value="{$word->label}">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2 control-label" for="input17">Type</label>
									<div class="col-md-10">
										<select class="form-control" name="type" id="type">
											<option value="">Select Type</option>
											{if $types->gotValue}
												{doWhile}
													<option value="{$types->id}" requiresBody="{$types->requiresBody}" requiresParameters="{$types->requiresParameters}" {if $word->gotValue && $word->type && $word->type->id==$types->id}selected{/if}>{$types->name}</option>
												{/doWhile ($types->populate())}
											{/if}
										</select>
									</div>
								</div>
								<div class="form-group" id="titleTemp">
									<label class="col-md-2 control-label" for="input17">Content Template</label>
									<div class="col-md-10">
										<div class="col-md-10">
											<input type="text" name="titleTemplate" id="titleTemplate" class="form-control col-md-10" value="{$word->titleTemplate}" />
										</div>
									</div>
								</div>

{*
								<div class="form-group">
									<label class="col-md-2 control-label" for="input17">Special Type</label>
									<div class="col-md-10">
										<select class="form-control" name="specialType">
											<option value="NORMAL" {if $word->gotValue && $word->specialType=="NORMAL"}selected{/if}>Normal</option>
											<option value="MAIL" {if $word->gotValue && $word->specialType=="MAIL"}selected{/if}>Mail</option>
											<option value="DASHBOARD" {if $word->gotValue && $word->specialType=="DASHBOARD"}selected{/if}>Dashboard</option>
											<option value="PUSH_NOTIFICATION" {if $word->gotValue && $word->specialType=="PUSH_NOTIFICATION"}selected{/if}>Push Notification</option>
										</select>
									</div>
								</div>
*}

								<div class="form-group extrass" id="titleParams" >
									<label class="col-md-2 control-label" for="input17">Content Parameters</label>
									<div class="col-md-10">
										<div class="col-md-10"><input type="text" id="titleParameters" name="titleParameters" class="tags" value="{$word->titleParameters}"></div>
									</div>
								</div>
								<div class="form-group extrass" id="bodyTemp">
									<label class="col-md-2 control-label" for="input17">Body Template</label>
									<div class="col-md-10">
										<div class="col-md-10">
											<textarea name="bodyTemplate" id="bodyTemplate" class="col-md-10" rows="10">{$word->bodyTemplate}</textarea>
										</div>
									</div>
								</div>
								<div class="form-group extrass" id="bodyParams">
									<label class="col-md-2 control-label" for="input17">Body Parameters</label>
									<div class="col-md-10">
										<div class="col-md-10"><input type="text" name="bodyParameters" id="bodyParameters" class="tags" value="{$word->bodyParameters}"></div>
									</div>
								</div>
								<div class="form-group extrass"  id="footerLn">
									<label class="col-md-2 control-label bs-tooltip" for="input17" data-placement="bottom" data-original-title="Link to the word at the end of the body"><i class="icon-info-sign"></i> Footer Link</label>
									<div class="col-md-10">
										<div class="col-md-10">
											<input type="text" class="form-control" id="footerLink" name="footerLink" value="{if $word->word}{$word->word->label}{/if}">
										</div>
									</div>
								</div>
                                <div class="form-group extrass" id="gSendMail">
                                    <label class="col-md-2 control-label" for="sendMail">Send as Mail?</label>

                                    <div class="col-md-10">
                                        <input type="checkbox" id="sendMail" name="sendMail"
                                               class="form-control uniform"
                                               value="1" {if $word->sendMail==1}checked="checked"{/if}>
                                    </div>
                                </div>
                                <div class="form-group extrass" id="gSendPush">
                                    <label class="col-md-2 control-label" for="sendPush">Send as Notification?</label>

                                    <div class="col-md-10">
                                        <input type="checkbox" id="sendPush" name="sendPush"
                                               class="form-control uniform" value="1"
                                               {if $word->sendPush==1}checked="checked"{/if}>
                                    </div>
                                </div>
                                <div class="form-group extrass" id="gSendDashboard">
                                    <label class="col-md-2 control-label" for="sendDashboard">Send to Dashboard?</label>

                                    <div class="col-md-10">
                                        <input type="checkbox" id="sendDashboard" name="sendDashboard"
                                               class="form-control uniform" value="1"
                                               {if $word->sendDashboard==1}checked="checked"{/if}>
                                    </div>
                                </div>

								<div class="form-group">
									<div class="widget box">
										<div class="widget-header">
											<h4><i class="icon-reorder"></i> Categories</h4>
										</div>
										<div class="widget-content clearfix">
											<!-- Left box -->
											<div class="left-box">
												<input type="text" id="box1Filter" class="form-control box-filter" placeholder="Filter entries..."><button type="button" id="box1Clear" class="filter">x</button>
												<select id="box1View" multiple="multiple" class="multiple">
													{if $cats->gotValue}
														{doWhile}
															<option value="{$cats->id}">{$cats->name}</option>
														{/doWhile ($cats->populate())}
													{/if}
												</select>
												<span id="box1Counter" class="count-label"></span>
												<select id="box1Storage"></select>
											</div>
											<!--left-box -->

											<!-- Control buttons -->
											<div class="dual-control">
												<button id="to2" type="button" class="btn">&nbsp;&gt;&nbsp;</button>
												<button id="allTo2" type="button" class="btn">&nbsp;&gt;&gt;&nbsp;</button><br>
												<button id="to1" type="button" class="btn">&nbsp;&lt;&nbsp;</button>
												<button id="allTo1" type="button" class="btn">&nbsp;&lt;&lt;&nbsp;</button>
											</div>
											<!--control buttons -->

											<!-- Right box -->
											<div class="right-box">
												<input type="text" id="box2Filter" class="form-control box-filter" placeholder="Filter entries..."><button type="button" id="box2Clear" class="filter">x</button>
												<select id="box2View" name="cats[]" multiple="multiple" class="multiple">
													{if $word->categories && $word->categories->gotValue}
														{doWhile}
															<option value="{$word->categories->id}">{$word->categories->name}</option>
														{/doWhile ($word->categories->populate())}
													{/if}
												</select>
												<span id="box2Counter" class="count-label"></span>
												<select id="box2Storage"></select>
											</div>
											<!--right box -->
										</div>
									</div>
								</div>

								<div class="form-actions">
									<button type="submit" class="submit btn btn-primary pull-right">
										Submit <i class="icon-angle-right"></i>
									</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
			<!-- /.row -->
			<!-- /Page Content -->
		</div>
		<!-- /.container -->

	</div>
</div>
<script>
	var id = {if $word->gotValue}{$word->id}{else}-1{/if};
	{literal}

	$('#footerLink').typeahead({
		remote: '/xhr/words/%QUERY/'+id,
		name: 'footerLink',
	});


	$('#type').change( function () {
        $( '.extrass' ).hide();

        var requiresBody = $( this ).find('option[value='+$(this ).val()+']').attr( "requiresBody" )==1;
        var requiresParameters = $( this ).find('option[value='+$(this ).val()+']').attr( "requiresParameters" )==1;

        if ( requiresBody ){
            $( '#bodyTemp' ).show();
            $( '#footerLn' ).show();
	        $( '#gSendDashboard' ).show();
	        $( '#gSendPush' ).show();
	        $( '#gSendMail' ).show();
        }

        if ( requiresParameters ){
            $( '#titleParams' ).show();
            $( '#titleTemp' ).show();
        }

        if ( requiresParameters && requiresBody ) {
            $( '#bodyParams' ).show();

        }

    });

	setTimeout( function () {
		$( '#type' ).change()
	}, 200 );
	{/literal}
</script>
{include file="footer.tpl"}
</body>
</html>