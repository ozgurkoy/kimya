<!DOCTYPE html>
<html lang="en">
<head>
	{include file="_top.tpl"}
	<script>
		{literal}
		$(document).ready(function(){
			"use strict";

			App.init(); // Init layout and core plugins
			Plugins.init(); // Init all plugins
			FormComponents.init(); // Init all form-specific plugins
		});
		{/literal}
	</script>

</head>

<body>

<!-- Header -->
<header class="header navbar navbar-fixed-top" role="banner">
	{include file="_header.tpl"}
</header>
<!-- /.header -->

<div id="container">
	<div id="sidebar" class="sidebar-fixed">
		<div id="sidebar-content">

			<!-- Search Input -->
			<form class="sidebar-search">
				<div class="input-box">
					<button type="submit" class="submit">
						<i class="icon-search"></i>
					</button>
				<span>
					<input type="text" name="quickWord" id="quickWord" placeholder="Search for word...">
				</span>
				</div>
			</form>


			<!--=== Navigation ===-->
			{include file="_nav.tpl"}
			<!-- /Navigation -->

		</div>
		<div id="divider" class="resizeable"></div>
	</div>
	<div id="content">
		<div class="container">
			<!-- Breadcrumbs line -->
			<div class="crumbs">
				<ul id="breadcrumbs" class="breadcrumb">
					<li>
						<i class="icon-home"></i>
						<a href="{$SITEROOT}/home">Panel</a>
					</li>
					<li>
						<i class="icon-road"></i>
						<a href="{$SITEROOT}/activities">Aktiviteler</a>
					</li>
				</ul>

			</div>
			<!-- /Breadcrumbs line -->

			<!--=== Page Header ===-->
			<div class="page-header">
			</div>
			<!-- /Page Header -->

			<!--=== Page Content ===-->


			<div class="row">
				<div class="col-md-12">
					<div class="widget box">
						<div class="widget-header">
							<h4><i class="icon-reorder"></i>
								{if $activity->gotValue}
									Aktivite Guncelle
								{else}
									Yeni Aktivite
								{/if}

							</h4>
						</div>
						<div class="widget-content">
							<form class="form-horizontal row-border" action="{$SITEROOT}/activityAction" name="activity" id="activity" method="POST" target="ps">
								{if $activity->gotValue}
									<input type="hidden" name="id" value="{$activity->id}"/>
									{callUserFunc $activity->loadActivityContact()}
									{callUserFunc $activity->activityContact->loadCustomer()}
									{assign var="custVal" value=$activity->activityContact->contacts}
								{else}
									{assign var="custVal" value=null}
								{/if}

								{getFormToken f="activity"}
								{formBox name="type" label="Aktivite Turu" value=$activity->type options=$actTypes  type="arrayKVSelect"}
								{formBox name="customer" label="Musteri" value=$custVal options=$customers type="populateSelect" vkey="name"}
								<div class="form-group non" id="contBox">
									<label class="col-md-2 control-label" for="input17">Kontak</label>
									<div class="col-md-10">
										<select name="activityContact" id="activityContact">
										</select>
									</div>
								</div>
								{formBox name="date" label="Tarih" value=$activity->date type="date"}
								{formBox name="hourStart" label="Baslangic Saati" value=$activity->hourStart type="hour"}
								{formBox name="hourEnd" label="Bitis Saati" value=$activity->hourEnd type="hour"}
								{formBox name="place" label="Aktivite Mekani" value=$activity->place type="textarea" info="Ziyaret/konusmanin yer aldigi mekan"}
								{formBox name="info" label="Aktivite Bilgisi" value=$activity->info type="textarea" info="Ziyaret/konusmanin sonucuyla alakali bilgi"}


								<div class="form-actions">
									<button type="submit" class="submit btn btn-primary pull-right">
										Submit <i class="icon-angle-right"></i>
									</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
			<!-- /.row -->
			<!-- /Page Content -->
		</div>
		<!-- /.container -->

	</div>
</div>
<script type="text/javascript">

	{if $activity->gotValue}

	{literal}
	setTimeout(function(){$('#customer').change()},500);
	{/literal}
	{/if}
	{literal}
	$( '#customer' ).change( function () {
		var vl = $( this ).val();

		if ( vl.length > 0 ) {
			$.ajax( {
				url        : '/xhr/contacts/' + ($( this ).val()),
				beforeSend : function ( xhr ) {
					xhr.overrideMimeType( "text/plain; charset=x-user-defined" );
				}
			} ).done( function ( data ) {
						var cons = JSON.parse( data );
						$( '#activityContact' ).find( 'option' ).remove();

						for ( var dd in cons ) {
							$( '#activityContact' ).append( '<option value="' + dd + '">' + cons[dd] + '</option>' );
						}

						$( '#contBox' ).show();
					}
			);
		}
		else
			$( '#contBox' ).hide();
	} );

	{/literal}

</script>
{include file="footer.tpl"}
</body>
</html>