{getSessionParams}
<!-- Top Navigation Bar -->
<div class="container">

	<!-- Only visible on smartphones, menu toggle -->
	<ul class="nav navbar-nav">
		<li class="nav-toggle"><a href="javascript:void(0);" title=""><i class="icon-reorder"></i></a></li>
	</ul>

	<!-- Logo -->
	<a class="navbar-brand" href="{$SITEROOT}/home">
		<img src="{$SITEROOT}/site/layout/assets/img/logo.png" alt="logo"/>
		<strong>ME</strong>LON
	</a>
	<!-- /logo -->

	<!-- Sidebar Toggler -->
	<a href="#" class="toggle-sidebar bs-tooltip" data-placement="bottom" data-original-title="Toggle navigation">
		<i class="icon-reorder"></i>
	</a>
	<!-- /Sidebar Toggler -->
	{*<ul class="nav navbar-nav navbar-left hidden-xs hidden-sm">
		<li>
			<a href="{$SITEROOT}/words">
				Words
			</a>
		</li>

	</ul>*}

	<!-- Top Right Menu -->
	<ul class="nav navbar-nav navbar-right">
		<!-- Notifications -->
		<li class="dropdown">
			<a href="#" class="dropdown-toggle" data-toggle="dropdown">
				<i class="icon-warning-sign"></i>
				<span class="badge">5</span>
			</a>
			<ul class="dropdown-menu extended notification">
				<li class="title">
					<p>You have 5 new notifications</p>
				</li>
				<li>
					<a href="javascript:void(0);">
						<span class="label label-success"><i class="icon-plus"></i></span>
						<span class="message">New user registration.</span>
						<span class="time">1 mins</span>
					</a>
				</li>
				<li>
					<a href="javascript:void(0);">
						<span class="label label-danger"><i class="icon-warning-sign"></i></span>
						<span class="message">High CPU load on cluster #2.</span>
						<span class="time">5 mins</span>
					</a>
				</li>
				<li>
					<a href="javascript:void(0);">
						<span class="label label-success"><i class="icon-plus"></i></span>
						<span class="message">New user registration.</span>
						<span class="time">10 mins</span>
					</a>
				</li>
				<li>
					<a href="javascript:void(0);">
						<span class="label label-info"><i class="icon-bullhorn"></i></span>
						<span class="message">New items are in queue.</span>
						<span class="time">25 mins</span>
					</a>
				</li>
				<li>
					<a href="javascript:void(0);">
						<span class="label label-warning"><i class="icon-bolt"></i></span>
						<span class="message">Disk space to 85% full.</span>
						<span class="time">55 mins</span>
					</a>
				</li>
				<li class="footer">
					<a href="javascript:void(0);">View all notifications</a>
				</li>
			</ul>
		</li>


		<!-- User Login Dropdown -->
		<li class="dropdown user">
			<a href="#" class="dropdown-toggle" data-toggle="dropdown">
				<!--<img alt="" src="http://{$SITEROOT}/site/layout/assets/img/avatar1_small.jpg" />-->
				<i class="icon-male"></i>
				<span class="username">{$_session.username}</span>
				<i class="icon-caret-down small"></i>
			</a>
			<ul class="dropdown-menu">
				{*<li><a href="pages_user_profile.html"><i class="icon-user"></i> My Profile</a></li>*}
				{*<li><a href="pages_calendar.html"><i class="icon-calendar"></i> My Calendar</a></li>*}
				{*<li><a href="#"><i class="icon-tasks"></i> My Tasks</a></li>*}
				{*<li class="divider"></li>*}
				<li><a href="{$SITEROOT}/logout"><i class="icon-key"></i> Log Out</a></li>
			</ul>
		</li>
		<!-- /user login dropdown -->
	</ul>
	<!-- /Top Right Menu -->
</div>
<!-- /top navigation bar -->