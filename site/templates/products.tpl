<!DOCTYPE html>
<html lang="en">
<head>
	{include file="_top.tpl"}
	<script>
		{literal}
		$(document).ready(function(){
			"use strict";

			App.init(); // Init layout and core plugins
			Plugins.init(); // Init all plugins
			FormComponents.init(); // Init all form-specific plugins
		});
		{/literal}
	</script>

</head>

<body>

<!-- Header -->
<header class="header navbar navbar-fixed-top" role="banner">
	{include file="_header.tpl"}
</header>
<!-- /.header -->

<div id="container">
	<div id="sidebar" class="sidebar-fixed">
		<div id="sidebar-content">

			<!-- Search Input -->
			<form class="sidebar-search">
				<div class="input-box">
					<button type="submit" class="submit">
						<i class="icon-search"></i>
					</button>
				<span>
					<input type="text" name="quickWord" id="quickWord" placeholder="Search for word...">
				</span>
				</div>
			</form>


			<!--=== Navigation ===-->
			{include file="_nav.tpl"}
			<!-- /Navigation -->

		</div>
		<div id="divider" class="resizeable"></div>
	</div>
	<div id="content">
		<div class="container">
			<!-- Breadcrumbs line -->
			<div class="crumbs">
				<ul id="breadcrumbs" class="breadcrumb">
					<li>
						<i class="icon-home"></i>
						<a href="{$SITEROOT}/home">Panel</a>
					</li>
					<li class="current">
						<a href="{$SITEROOT}/products" title="">Urunler</a>
					</li>
				</ul>
				<ul class="crumb-buttons">
					<li class="first"><a href="{$SITEROOT}/addProduct" title=""><i class="icon-plus"></i><span>Yeni Urun</span></a></li>
				</ul>

			</div>
			<div class="page-header"></div>
			<!-- /Breadcrumbs line -->
			<div class="row">
				<div class="col-md-12">
					<div class="widget box">
						<div class="widget-header">
							<h4><i class="icon-reorder"></i> Urunler</h4>
							<div class="toolbar no-padding">
								<div class="btn-group">
									<span class="btn btn-xs widget-collapse"><i class="icon-angle-down"></i></span>
								</div>
							</div>
						</div>
						<div class="widget-content no-padding">
							<table class="table table-hover table-striped table-bordered table-highlight-head">
								<thead>
								<tr>
									<th>Urun</th>
									<th>Mensei</th>
									<th>Gruplari</th>
									<th>Sektorler</th>
									<th class="col-md-2">Islem</th>
								</tr>
								</thead>
								<tbody>
								{if $products->gotValue}
									{doWhile}
										{callUserFunc $products->listProductGroups()}
										{callUserFunc $products->listSectors()}
										<tr  id="row_{$products->id}">
											<td>{$products->name} - {$products->stockCode}</td>
											<td>{$products->origin}</td>
											<td>{$products->groupsListed}</td>
											<td>{$products->sectorsListed}</td>
											<td>
												<button class="btn btn-xs" href="{$SITEROOT}/addProduct/{$products->id}"><i class="icon-cog"></i></button>
												<button class="btn btn-xs" href="javascript:wordTab({$products->id})"><i class="icon-cloud  bs-tooltip" data-placement="top" data-original-title="Stok Hareketleri"></i></button>
												<button class="btn btn-xs" href="javascript:goIfOk('{$SITEROOT}/delProduct/{$products->id}')"><i class="icon-remove"></i></button>
											</td>
										</tr>
									{/doWhile ($products->populate() != false)}
								{/if}
								</tbody>
							</table>
						</div>
					</div>
				</div>


			</div>
			<div class="row clearHere" id="editRow">
			</div> <!-- /.row -->


			<!-- /.row -->
			<!-- /Page Content -->
		</div>
		<!-- /.container -->

	</div>
</div>

<script type="text/javascript">
	{literal}
	function wordTab( id, lang ) {
		$( 'tr' ).removeClass( 'activeRow' );
		$( '#row_' + id ).addClass( 'activeRow' );
		if ( !lang ) lang = 0;

		$( "#editRow" ).load( "/backend/productStocks/" + id + '/', function ( response, status, xhr ) {
			if ( status == "error" ) {
				var msg = "Sorry but there was an error: ";
				alert( msg + xhr.status + " " + xhr.statusText );
			}

		} );
	}
	{/literal}
</script>
{include file="footer.tpl"}
</body>
</html>