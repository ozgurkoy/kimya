<div class="form-group">
	<label class="col-md-2 control-label" for="input17">{$__label}</label>
	<div class="col-md-10">
		<select name="{$__name}" id="{$__name}" class="{$__class}">
			<option value="">Seciniz</option>
			{if $__options->gotValue}
				{doWhile}
					<option value="{$__options->id}"  {if !is_null($__value) && $__value->id==$__options->id}selected{/if}>{$__options->$__vkey}</option>
				{/doWhile ($__options->populate())}
			{/if}
		</select>
		{if isset($__info)}
			<span class="help-block">{$__info}</span>
		{/if}
	</div>
</div>
