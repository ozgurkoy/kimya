<div class="form-group">
	<label class="col-md-2 control-label" for="input17">{$__label}</label>
	<div class="col-md-10">
		<input type="text" name="{$__name}" id="{$__name}" class="form-control" value="{$__value}">
		{if isset($__info)}
		<span class="help-block">{$__info}</span>
		{/if}
	</div>
</div>
