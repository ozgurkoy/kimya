<div class="form-group">
	<label class="col-md-2 control-label" for="input17">{$__label}</label>
	<div class="col-md-10">
		<textarea name="{$__name}" rows="10" id="{$__name}" class="form-control">{$__value}</textarea>
		{if isset($__info)}
			<span class="help-block">{$__info}</span>
		{/if}
	</div>
</div>
