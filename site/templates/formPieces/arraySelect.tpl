<div class="form-group">
	<label class="col-md-2 control-label" for="input17">{$__label}</label>
	<div class="col-md-10">
		<select name="{$__name}" id="{$__name}">
			<option value="">Seciniz</option>
			{foreach from=$__options item=op}
			<option value="{$op}"  {if $__value==$op}selected{/if}>{$op}</option>
			{/foreach}
		</select>
		{if isset($__info)}
			<span class="help-block">{$__info}</span>
		{/if}
	</div>
</div>
