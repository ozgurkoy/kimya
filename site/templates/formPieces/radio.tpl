<div class="form-group">
	<label class="col-md-2 control-label" for="input17">{$__label}</label>

	<div class="col-md-10">
		{foreach from=$__options key=op item=opv}
			<input type="radio" class="uniform" name="{$__name}" value="{$op}" {if $__value==$op}checked{/if}>
			{$opv}
		{/foreach}
		{if isset($__info)}
			<span class="help-block">{$__info}</span>
		{/if}

	</div>

</div>
