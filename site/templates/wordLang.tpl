<div class="col-md-12">
	<div class="widget box">
		<div class="widget-header">
			<h4><i class="icon-reorder"></i> Language Definitions for : {$word->label} </h4>
		</div>
		<div class="widget-content">
			<div class="tabbable box-tabs">
				<ul class="nav nav-tabs">
					{if $word->entries && $word->entries->gotValue}
						{doWhile}
							{callUserFunc $word->entries->loadLangWord()}
							<li {if $lang == $word->entries->langWord->id}class="active"{/if}><a href="#box_tab_{$word->entries->id}" data-toggle="tab">{$word->entries->langWord->label}</a></li>
						{/doWhile ($word->entries->populate())}
					{/if}
					<li {if $lang==0}class="active"{/if}><a href="#box_tabAdd" data-toggle="tab">+</a></li>
				</ul>
				<div class="tab-content">
					<div class="tab-pane {if $lang == 0}active{/if}" id="box_tabAdd">
						<!--new lang-->
						{if is_null($langs)===false}

						<form class="form-horizontal row-border" action="{$SITEROOT}/wordLangAction" name="wordLangAction" id="wordLangAction" method="POST" target="ps">
							<input type="hidden" name="wordId" value="{$word->id}"/>
							{getFormToken f="wordLangAction"}
							<div class="form-group">
								<label class="col-md-2 control-label" for="input17">Language</label>
								<div class="col-md-10">
									<select name="lang" id="lang" class="form-control">
										<option value="">Select Language</option>
										{if $langs->gotValue}
											{doWhile}
												<option value="{$langs->id}">{$langs->label}</option>
											{/doWhile ($langs->populate())}
										{/if}
									</select>

								</div>
							</div>
							<div class="form-group">
								<label class="col-md-2 control-label" for="input17">Content</label>
								<div class="col-md-10">
									<textarea name="word_content" id="word_content" class="form-control">{if strlen($word->titleTemplate)}{$word->titleTemplate}{/if}</textarea>
									{if $word->type->requiresParameters==1}
									<span class="help-block">
										You must use these parameters: {$word->titleParameters}
									</span>
									{/if}
								</div>
							</div>

							<div class="form-group{if $word->type->requiresBody!=1} non{/if}" >
								<label class="col-md-2 control-label" for="input17">Body</label>
								<div class="col-md-10">
									<textarea name="word_body" id="word_body" class="form-control">{if strlen($word->bodyTemplate)}{$word->bodyTemplate}{/if}</textarea>
									{if $word->type->requiresParameters==1}
									<span class="help-block">
										You must use these parameters: {$word->bodyParameters}
									</span>
									{/if}
								</div>
							</div>

							<div class="form-actions">
								<button type="submit" class="submit btn btn-primary pull-right">
									Submit <i class="icon-angle-right"></i>
								</button>
							</div>
						</form>

						{else}
							Can't add anymore languages, edit the current ones if you need to change.
						{/if}
						<!--/new lang-->
					</div>
					{if $word->entries && $word->entries->gotValue}
					{callUserFunc $word->entries->resetIndex()}
					{doWhile}
						{callUserFunc $word->entries->loadLangWord()}
						{callUserFunc $word->loadType()}

						<div class="tab-pane {if $lang == $word->entries->langWord->id}active{/if}" id="box_tab_{$word->entries->id}">
							<div style="float:right;margin-bottom:10px">
								<button class="btn btn-sm btn-success editButton" stId="{$word->entries->id}" >Edit</button>&nbsp;
								{*<button class="btn btn-sm btn-warning versionButton" stId="{$word->entries->id}" >New Version</button>*}
							</div>
							<!--lang box-->
							<table class="table table-hover table-striped table-bordered table-highlight-head">
								<tbody>
								<tr>
									<td>Language</td>
									<td>{$word->entries->langWord->label}</td>
								</tr>
								<tr>
									<td>Content</td>
									<td>{$word->entries->content}</td>
								</tr>
								{if $word->type->requiresBody==1}
								<tr>
									<td>Body</td>
									<td>{$word->entries->body}</td>
								</tr>
								{/if}
								{callUserFunc $word->entries->loadStates(null,0,20,"jdate DESC")}
								{if $word->entries->states && $word->entries->states->gotValue && sizeof($word->entries->states->_ids)>1}
								<tr>
									<td>Match Versions</td>
									<td>
										<select class="versions" id="versions_{$word->entries->id}" stId="{$word->entries->id}">
											<option value="">Select version to compare</option>
										{doWhile}
											{callUserFunc $word->entries->states->loadAddedBy()}
											{if $word->entries->states->state != 'active'}
											<option value="{$word->entries->states->id}">{intToDate t=$word->entries->states->jdate type="d-M-Y H:i"} - by {$word->entries->states->addedBy->username}</option>
											{/if}
										{/doWhile ($word->entries->states->populate())}
										</select>
									</td>
								</tr>
								{/if}
							</table>
							<div id="versionBox_{$word->entries->id}">

							</div>

							<!--/lang box-->

						</div>
					{/doWhile ($word->entries->populate())}
					{/if}


				</div>
			</div> <!-- /.tabbable portlet-tabs -->
		</div> <!-- /.widget-content -->
	</div> <!-- /.widget .box -->
</div> <!-- /.col-md-12 -->

<script>
	{literal}
	$('.versions' ).change(function(){
		var tDiv = $( 'div#versionBox_' + $( this ).attr( "stId" ) );

		if($(this).val()==""){
			tDiv.html( "" );
			return;
		}
		tDiv.load( "/wordEntryVersion/"+$(this ).val(), function( response, status, xhr ) {
			if ( status == "error" ) {
				var msg = "Sorry but there was an error: ";
				alert( msg + xhr.status + " " + xhr.statusText );
			}
		});

	});

	$( '.editButton' ).click( function () {
		var tDiv = $( '#box_tab_' + $( this ).attr( "stId" ) );

		tDiv.load( "/editVersion/"+$( this ).attr( "stId" ), function( response, status, xhr ) {
			if ( status == "error" ) {
				var msg = "Sorry but there was an error: ";
				alert( msg + xhr.status + " " + xhr.statusText );
			}
		});

	} );

	{/literal}
</script>