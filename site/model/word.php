<?php
/**
 * word real class - firstly generated on 14-10-2013 22:44, add edit anyway you like wont be touched over , ever again.
 *
 * @package jeelet
 **/
include $__DP.'/site/model/base/word.php';

class Word extends Word_base
{
	/**
	 * constructor for the class
	 *
	 * @return void
	 **/
	public function __construct($id=null) {
		parent::__construct($id);
	}

	public function listCategories() {
		$this->loadCategories();
		$cats = array();

		if($this->categories && $this->categories->gotValue){
			do {
				$cats[ ] = $this->categories->name;
			} while ( $this->categories->populate() );

		}

		$this->catsListed = implode( ",", $cats );
	}
}

