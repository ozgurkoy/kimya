<?php
/**
 * role real class - firstly generated on 14-10-2013 22:44, add edit anyway you like wont be touched over , ever again.
 *
 * @package jeelet
 **/
include $__DP.'/site/model/base/role.php';

class Role extends Role_base
{
	/**
	 * constructor for the class
	 *
	 * @return void
	 **/
	public function __construct($id=null) {
		parent::__construct($id);
	}

	public function parsePerms() {
		$perm = array();
		$this->loadPermissions();
		if ( $this->permissions ) {
			do {
				$this->permissions->loadAction();
				if($this->permissions->action)
					$perm[ $this->permissions->action->id ] = $this->permissions->bits;
			} while ( $this->permissions->populate() );
		}

		$this->parsedPerms = $perm;
	}
}

