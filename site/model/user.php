<?php
/**
 * user real class - firstly generated on 14-10-2013 22:44, add edit anyway you like wont be touched over , ever again.
 *
 * @package jeelet
 **/
include $__DP.'/site/model/base/user.php';

class User extends User_base
{

	public $permissions = array();

	public $authLevel = 100;

	/**
	 * constructor for the class
	 *
	 * @return void
	 **/
	public function __construct( $id = null ) {
		parent::__construct( $id );
	}

	public function login( $username, $password ) {
		$this->load( array( "username" => $username, "password" => md5( $password ), "active" => 1 ) );

		return $this->gotValue;
	}

	//	TODO:cache role permissions on db
	public function loadPerms() {
		$roles = $this->loadRoles();
		if ( $roles ) {
			do {
				if ( $this->authLevel > $roles->authLevel )
					$this->authLevel = $roles->authLevel;

				$perms = $roles->loadPermissions();
				if ( $perms ) {
					do {
						$action = $perms->loadAction();
						if ( !isset( $this->permissions[ $action->label ] ) )
							$this->permissions[ $action->label ] = 0;

						$this->permissions[ $action->label ] |= $perms->bits;
					} while ( $perms->populate() );
				}
			} while ( $roles->populate() );
		}

		return $this->permissions;
	}

	public static function record($userId, $action){
		$h = new UserHistory();
		$h->action = $action;
		$h->save();
		$h->saveUser( $userId );

		unset( $h );
	}
}