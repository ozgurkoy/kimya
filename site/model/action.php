<?php
/**
 * action real class - firstly generated on 14-10-2013 22:44, add edit anyway you like wont be touched over , ever again.
 *
 * @package jeelet
 **/
include $__DP.'/site/model/base/action.php';

class Action extends Action_base
{
	public $parsedTasks = array();

	/**
	 * constructor for the class
	 *
	 * @return void
	 **/
	public function __construct($id=null) {
		parent::__construct($id);
	}

	public function parseTasks() {
		$this->parsedTasks = array();
		$t0 = explode( "\n",$this->tasks );
		foreach ( $t0 as $pi => $pt ) {
			if ( !( strlen( trim( $pt ) ) > 0 ) ) continue;

			$this->parsedTasks[ trim($pt) ] = pow( 2, $pi );
		}
		$this->parsedTasks = array_reverse($this->parsedTasks);
	}

}

