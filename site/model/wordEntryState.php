<?php
/**
 * wordEntryState real class - firstly generated on 19-10-2013 00:20, add edit anyway you like wont be touched over , ever again.
 *
 * @package jeelet
 **/
include $__DP.'/site/model/base/wordEntryState.php';

class WordEntryState extends WordEntryState_base
{
	/**
	 * constructor for the class
	 *
	 * @return void
	 **/
	public function __construct($id=null) {
		parent::__construct($id);
	}

	public function archive( WordEntry &$wordEntry, $userId, $newState = "discarded" ) {
		//archive the old version.
		$w     = new WordEntryState();
		$oldId = null;

		$w->load( array( "stLang" => $this->stLang, "isDeleted"=>0, "state" => "active", "states" => $wordEntry->id ) );

		if ( $w->gotValue ) {
			$oldId = $w->id;

			$w->state = $newState;
			$w->record( $userId, "new state : " . $newState );
			$w->save();
		}

		//update the word entry
		$wordEntry->content = $this->content;
		$wordEntry->body    = $this->body;
		$wordEntry->edited  = 1;
		$wordEntry->save();

		return $oldId;
	}

	public function cloneToNew( $isRoot = false ) {
		$w = new WordEntryState();

		$w->state     = $isRoot ? 'active' : 'candidate';
		$w->authLevel = $this->authLevel;
		$w->content   = $this->content;
		$w->body      = $this->body;
		$w->stLang    = $this->stLang;
		$w->addedBy   = $this->addedBy;
		$w->states    = $this->states;

		return $w;
	}

	public function record( $userId, $action ) {
		$h              = new WordEntryStateHistory();
		$h->action      = $action;
		$h->historyUser = $userId;
		$h->save();

		$this->saveHistory( $h->id );

		unset( $h );
	}
}

