<?php
/**
 * wordEntry real class - firstly generated on 14-10-2013 22:44, add edit anyway you like wont be touched over , ever again.
 *
 * @package jeelet
 **/
include $__DP.'/site/model/base/wordEntry.php';

class WordEntry extends WordEntry_base
{
	/**
	 * constructor for the class
	 *
	 * @return void
	 **/
	public function __construct($id=null) {
		parent::__construct($id);
	}

	public function record( $userId, $action, $newId = null, $oldId = null ) {
		$h                   = new WordEntryHistory();
		$h->action           = $action;
		$h->entryHistoryUser = $userId;
		$h->oldId            = $oldId;
		$h->newId            = $newId;

		$h->save();

		$this->saveHistory( $h->id );

		unset( $h );
	}

}
