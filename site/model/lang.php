<?php
/**
 * lang real class - firstly generated on 14-10-2013 22:44, add edit anyway you like wont be touched over , ever again.
 *
 * @package jeelet
 **/
include $__DP.'/site/model/base/lang.php';

class Lang extends Lang_base
{
	/**
	 * constructor for the class
	 *
	 * @return void
	 **/
	public function __construct( $id = null ) {
		parent::__construct( $id );
	}

	public function cloneFromAnother( $fromId, $userId, $categories ) {
		$w = new WordEntry();
		$w->nopop()->populateOnce( false )->load( array( "entries" => array( "categories" => array( "id" => $categories ) ), "langWord" => array( "id" => $fromId ) ) );
		while ( $w->populate() ) {
			$w->loadWord();
			//get the final state of the word thats being cloned
			$activeState = $w->loadStates( array( "stLang" => $fromId, "isDeleted" => 0, "state" => "active" ) );

			/*something might be wrong.*/
			if ( $activeState->gotValue === false )
				continue;

			//clone the entry first
			$e           = new WordEntry();
			$e->content  = $w->content;
			$e->body     = $w->body;
			$e->edited   = 0;
			$e->adder    = $userId;
			$e->langWord = $this->id;
			$e->status   = 'needTranslation';
			$e->save();

			//save the entry to the word
			$e->saveWord( $w->entries );

			//create a new state from the source
			$state            = new WordEntryState();
			$state->authLevel = 50;
			$state->content   = $activeState->content;
			$state->stLang    = $this->id;
			$state->addedBy   = $userId;
			$state->state     = "candidate";
			$state->states    = $e->id;
			$state->body      = $activeState->body;

			$state->save();

			//history stuff
			$state->record( $userId, "cloned from : " . $activeState->id );
			$e->record( $userId, "cloned entry ", $state->id, $activeState->id );

//			else
//				$e->record( $userId, "cloned entry", null, $activeState->id );

			//User::record( $userId, "cloned word entry. Lang:" . $this->id . " Entry:" . $state->id );
		}
		User::record( $userId, "cloned language from $fromId. New one :" . $this->id );

	}
}

