<?php
/**
 * product real class - firstly generated on 27-10-2013 14:38, add edit anyway you like wont be touched over , ever again.
 *
 * @package jeelet
 **/
include $__DP.'/site/model/base/product.php';

class Product extends Product_base
{

	public $groupsListed = array();
	public $sectorsListed = array();

	/**
	 * constructor for the class
	 *
	 * @return void
	 **/
	public function __construct( $id = null ) {
		parent::__construct( $id );
	}

	public function listProductgroups() {
		$this->loadGroups();
		$cats = array();

		if ( $this->groups && $this->groups->gotValue ) {
			do {
				$cats[ ] = $this->groups->name;
			} while ( $this->groups->populate() );

		}

		$this->groupsListed = implode( ",", $cats );
	}

	public function listSectors() {
		$this->loadProductSector();
		$cats = array();

		if ( $this->productSector && $this->productSector->gotValue ) {
			do {
				$cats[ ] = $this->productSector->name;
			} while ( $this->productSector->populate() );

		}

		$this->sectorsListed = implode( ",", $cats );
	}

	public function getCurrentStock() {
		$st = $this->loadStockMovement();
		$ret = 0;
		if($st && $st->gotValue){
			do {
				$ret += $st->amount;
			} while ( $st->populate() );

		}

		return $ret;
	}

}

