function fieldError( formName, fields ) {
	var validator = $( "#"+formName ).validate();

	validator.showErrors(
		JSON.parse(fields)
	);
}

function clearErrorMessage(){
	$( "label.error" ).remove();
	$( "label.has-error" ).remove();
	$( 'div.has-error' ).removeClass( 'has-error' );
}

function goIfOk(url){
	if(confirm('Emin misiniz, islemin geri donusu yoktur'))
		location.href = url;
}

function message(type,message){
	switch(type){
		default:
		case 'good':
			noty({
				text: '<strong>'+message+'</strong>',
				type: 'success',
				timeout: 2000
			});
			break;
	}
}