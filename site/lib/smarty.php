<?php
/**
* smarty functions class
*/
class UserSmarty
{
	public static function getValueFromArray( $params, &$smarty ) {
		if ( $params[ 'ar' ] == "SESSION" ) $params[ 'ar' ] = $_SESSION;
		$kr = $params[ 'ar' ][ $params[ 'key' ] ];
		if ( isset( $params[ 'func' ] ) ) {
			$prms = array();
			if ( isset( $params[ 'pr1' ] ) ) $prms[ ] = "'" . $params[ 'pr1' ] . "'";
			if ( isset( $params[ 'pr2' ] ) ) $prms[ ] = "'" . $params[ 'pr2' ] . "'";
			$prms = implode( ",", $prms );
			if ( strlen( $prms ) == 0 ) $prms = "\"\"";

			eval( "\$kr=" . $params[ 'func' ] . "('$kr',$prms);" );
		}

		return $kr;
	}

	public static function getSessionParams( $params, &$smarty ) {
		$smarty->assign( "_session", $_SESSION );
	}

	/**
	 * @param null $params
	 * @param null $smarty
	 * @return bool|void
	 */
	public static function readPerms( $params = null, &$smarty = null ) {
		if ( !isset( $_SESSION[ "userId" ] ) ) return false;

		/*role permissions TODO: READ FROM SESSION*/
		$u    = new User( $_SESSION[ "userId" ] );
		if($_SESSION["isRoot"]==1){
			$role = new Role();
			$role->load();
		}
		else
			$role = $u->loadRoles();

		$pers = array();

		if ( $role && $role->gotValue ) {
			do {
				$per = $role->loadPermissions();

				if ( $per && $per->gotValue ) {
					do {
						$per->loadAction();
						if ( !isset( $pers[ $per->action->label ] ) )
							$pers[ $per->action->label ] = $_SESSION["isRoot"]==1?2047:0;


						$pers[ $per->action->label ] |= $per->bits;
					} while ( $per->populate() );
				}
			} while ( $role->populate() );
		}

		/*general permissions*/
		$perms = array();

		$p = new Action();
		$p->nopop()->load();
		while ( $p->populate() ) {
			$p->parseTasks();

			$perms[ $p->label ] = $p->parsedTasks;
		}
		$smarty->assign( "userPerms", $pers );
		$smarty->assign( "perms", $perms );

//		print_r( $perms );exit;
		unset( $perms, $pers, $p, $role );
	}

	public static function haltt( $params = null, &$smarty = null ) {
		exit;
	}

	public static function niceTruncate( $params, &$smarty ) {
		$str = substr( strip_tags( $params[ "str" ] ), 0, $params[ "length" ] );
		if ( strlen( $str ) < strlen( $params[ "str" ] ) ) $str .= "..";

		return $str;
	}

	public static function randId( $params, &$smarty ) {
		return JUtil::getRandomInteger( 15200 );
	}

	public static function mdv( $params, &$smarty ) {
		return md5( $params[ "var" ] );
	}

	public static function urlenc( $params, &$smarty ) {
		return urlencode( $params[ "var" ] );
	}

	public static function getGlobal( $params, &$smarty ) {
		global $$params[ "var" ];

		return $$params[ "var" ];
	}

	public static function getDateFormatted( $params, &$smarty ) {
		$ret = JUtil::dateFormat( $params[ "d" ] );
		if ( $params[ "hibernate" ] == 1 ) $ret = str_replace( "/", "|", $ret );

		return $ret;

	}

	public static function reverseDate( $params, &$smarty ) {
		return JUtil::reverseDate( $params[ "d" ], isset( $params[ "s" ] ) ? $params[ "s" ] : "/" );
	}

	public static function stripSlashes( $params, &$smarty ) {
		return stripcslashes( $params[ "string" ] );

	}

	public static function formToken( $params, &$smarty ) {
		//to -do
	}

	public static function length( $params, &$smarty ) {
		return sizeof( $params[ 'array' ] );
	}

	public static function getSinceTime( $params, &$smarty ) {
		$dt = $ka = time() - $params[ "time" ];

		$rs    = array();
		$diffs = array(
			"yd"   => array( 31536000, "Yıl" ),
			"md"   => array( 2592000, "Ay" ),
			"wd"   => array( 604800, "Hafta" ),
			"dd"   => array( 86400, "Gün" ),
			"hd"   => array( 3600, "Saat" ),
			"mind" => array( 60, "Dakika" )
		);

		foreach ( $diffs as $d ) {
			if ( floor( $ka / $d[ 0 ] ) > 0 ) {
				$rs[ ] = floor( $ka / $d[ 0 ] ) . " " . $d[ 1 ];
				$ka    = $ka % $d[ 0 ];
			}
		}
		if ( $ka > 0 )
			$rs[ ] = $ka . " Saniye";

		return implode( " ", $rs );
	}

	public static function getsubstr( $params, &$smarty ) {
		if ( strpos( $params[ 'str' ], "img " ) == 1 ) return $params[ 'str' ];
		$t = substr( strip_tags( $params[ 'str' ] ), $params[ 'sta' ], $params[ 'len' ] );

		return $t . ( strlen( $params[ 'str' ] ) > $params[ 'len' ] ? "..." : "" );
	}

	public static function safeJs( $params, &$smarty ) {
		return JUtil::jsString2( $params[ "str" ] );
	}

	public static function getStoragePath( $params, &$smarty = null ) {
		$hrx = SiteCache::read( "storageServer." . $params[ "type" ] . "." . $params[ "srv" ] );
		$ret = $hrx[ $params[ "p" ] ];

		if ( $params[ "hibernate" ] == 1 )
			$ret = str_replace( "/", "|", $ret );

		return $ret;
	}

	public static function JDocPub( $params, &$smarty ) {
		global $__DP;
		//receive the jdoc array and reeturn the path
		// echo $params["d"]["realPath"];

		if ( !isset( $params[ "d" ][ "realPath" ] ) || !( strlen( $params[ "d" ][ "realPath" ] ) > 1 ) ) {
			$nim                         = JCache::read( "constants.siteAddress" ) . "site/layout/admin/images/default.jpg";
			$params[ "d" ]               = array();
			$params[ "d" ][ "realPath" ] = $nim;
			$missingImage                = true;
		}

		if ( isset( $params[ "w" ] ) && isset( $params[ "h" ] ) ) {
			if ( isset( $missingImage ) )
				$params[ "d" ][ "realPath" ] = JUtil::thumber( $__DP . "/site/layout/admin/images/default.jpg", $params[ "w" ], $params[ "h" ] );
			else
				$params[ "d" ][ "realPath" ] = JDoc::thumbById( $params[ "d" ][ "id" ], $params[ "d" ][ "type" ], $params[ "d" ][ "subtype" ], $params[ "w" ], $params[ "h" ] );
		}

		return $params[ "d" ][ "realPath" ];
	}

	/**
	 * jdoc by url
	 *
	 * @return void
	 * @author Özgür Köy
	 **/
	public static function JDocURL( $params, &$smarty ) {
		return JUtil::thumber( $params[ "i" ], $params[ "w" ], $params[ "h" ] );
	}

	/**
	 * low normalize
	 *
	 * @return void
	 * @author Özgür Köy
	 **/
	public static function normalize( $params, &$smarty ) {
		return JUtil::normalize( $params[ "s" ], true );
	}

	/**
	 * thumb by jdoc id
	 * DEPRECATED : SEE JDoc::loadById
	 *
	 * @return mixed
	 * @author Özgür Köy
	 **/
	public static function JDocPubId( $params, &$smarty ) {
		$params[ "d" ] = str_replace( "|", "", $params[ "d" ] );

		if ( is_numeric( $params[ "d" ] ) && $params[ "d" ] > 0 )
			$params[ "d" ] = JDoc::loadById( $params[ "d" ] );
		else return "";

		if ( isset( $params[ "d" ] ) )
			$params[ "d" ] = reset( $params[ "d" ] );

		// print_r($params["d"]);

		return self::JDocPub( $params, $smarty );
	}

	/**
	 * create link array for product listing
	 *
	 * @return string
	 * @author Özgür Köy
	 **/
	public static function getProductLink( $params, &$smarty ) {
		//type=size ar=linkarrays id=3
		$cid  = isset( $params[ "id" ] ) ? $params[ "id" ] : 0;
		$car  = $params[ "ar" ];
		$type = isset( $params[ "type" ] ) ? $params[ "type" ] : "";

		$links = array();
		foreach ( $car as $carkey => $carp ) {
			$carp = JUtil::killEmps( $carp );

			if ( $carkey != $type )
				$links[ ] = $carkey . ":" . implode( ",", $carp );
			else {
				$tempar = $carp;
				if ( in_array( $cid, $carp ) ) {
					unset( $tempar[ array_search( $cid, $tempar ) ] );
				}
				else {
					$tempar[ ] = $cid;
				}
				$links[ ] = $carkey . ":" . implode( ",", $tempar );
			}
		}

		return implode( ";", $links );


	}

	/**
	 * https image
	 *
	 * @return void
	 * @author Özgür Köy
	 **/
	public static function secureJdoc( $params, &$smarty ) {
		$url = str_replace( "http:", "https:", self::JDocPub( $params, $smarty ) );

		// $url = self::JDocPub($params, $smarty);
		return $url;
	}

	/**
	 * check if https
	 *
	 * @return void
	 * @author Özgür Köy
	 **/
	public static function isSecurePage( $params, &$smarty ) {
		if ( isset( $_SERVER[ 'HTTPS' ] ) && $_SERVER[ 'HTTPS' ] == "on" ) {
			$kf = $smarty->assign( "isSecurePage", 1 );

			return;
		}

		$kf = $smarty->assign( "isSecurePage", 0 );

	}

	/**
	 * check if online
	 *
	 * @return void
	 * @author Özgür Köy
	 **/
	public static function isOnline( $params, &$smarty ) {
		if ( isset( $_SESSION[ 'userId' ] ) ) {
			$kf = $smarty->assign( "isOnline", 1 );

			return;
		}

		$kf = $smarty->assign( "isOnline", 0 );

	}

	/**
	 * user top menu
	 *
	 * @return void
	 * @author Özgür Köy
	 **/
	public static function buildUserMenu( $params, &$smarty ) {
		if ( isset( $_SESSION[ 'userId' ] ) && $_SESSION[ 'userId' ] > 0 ) {
			$kf = $smarty->assign( "userOn", 1 );

			return;
			// return true;
		}

		$kf = $smarty->assign( "userOn", 0 );
		// return false;
	}

	public static function prePopulate( &$params, &$smarty ) {
//		$smarty->assign($params["var"], )
	}

	/**
	 * populate
	 *
	 * @return void
	 * @author Özgür Köy
	 **/
	public static function populate( &$params, &$smarty ) {
		$params[ "i" ]->populate();
	}

	public static function callUserFunction( $params, &$smarty ) {
//		print_r( $params );
//		$params[ "m" ] = '123';
		call_user_func( array( $params[ "obj" ], $params[ "m" ] ) );
	}

	/**
	 * slider build
	 *
	 * @return void
	 * @author Özgür Köy
	 **/
	public static function sliders( $params, &$smarty ) {
		$s = new Slider();
		$s->nopop()->orderBy( "jorder ASC" )->load();
		$sl = array();

		while ( $s->populate() ) {
			$s->loadImage();
			// print_r($s);
			// print_r($si->image);

			$sl[ $s->link ] = $s->image[ 0 ][ "realPath" ];
		}

		$k = $smarty->assign( "slider", $sl );
	}

	/**
	 * home page fetcher
	 *
	 * @return void
	 * @author Özgür Köy
	 **/
	public static function landingBg( $params, &$smarty ) {
		$h         = new HomebgBatch();
		$h->name   = $params[ "name" ];
		$h->active = 1;

		$bgs   = $h->load()->loadBgs();
		$bgimg = $bglink = $bgmap = array();

		do {
			$bgs->loadBg();
			$bgimg[ ]  = $bgs->bg[ 0 ][ "realPath" ];
			$bglink[ ] = strlen( $bgs->link ) > 0 ? $bgs->link : null;
			$bgmap     = strlen( $bgs->imageMap ) > 0 ? $bgs->imageMap : null;
		} while ( $bgs->populate() );


		$smarty->assign( "bgimg", $bgimg );
		$smarty->assign( "bglink", $bglink );
		$smarty->assign( "bgmap", $bgmap );

	}

	public static function niceDate2( $params, &$smarty ) {
		$t0 = explode( "-", $params[ "d" ] );

		return date( "d / m", mktime( 0, 0, 0, $t0[ 1 ], $t0[ 2 ], $t0[ 0 ] ) );
	}

	public static function intToDate( $params, &$smarty ) {
		if ( !isset( $params[ "type" ] ) ) $params[ "type" ] = "d-M-Y";

		return date( $params[ "type" ], $params[ "t" ] );
	}

	/**
	 * split
	 *
	 * @return void
	 * @author Özgür Köy
	 **/
	public static function spp( $params, &$smarty ) {
		$t0 = explode( "\n", $params[ "s" ] );
		$smarty->assign( "t0", $t0 );
	}


	/**
	 * load trends available
	 *
	 * @return void
	 * @author Özgür Köy
	 **/
	public static function loadTrends( $params, &$smarty ) {
		$t          = new Trend();
		$t->active  = 1;
		$t->orderBy = "title ASC";
		$t->load();
		$tr = array();

		do {
			$tr[ $t->id ] = $t->title;
		} while ( $t->populate() );

		$smarty->assign( "allTrends", $tr );
	}

	/**
	 * cities
	 *
	 * @return void
	 * @author Özgür Köy
	 **/
	public static function getCities( $params, &$smarty ) {
		global $GLBS;


		$smarty->assign( "cities", $GLBS[ "cities" ] );

	}

	/**
	 * to upper
	 *
	 * @return void
	 * @author Özgür Köy
	 **/
	public static function toUpper( $params, &$smarty ) {
		$str = $params[ "s" ];

		// if($_SESSION['lang'] != "TR") return strtoupper($str);


		$str = str_replace( "ğ", "Ğ", $str );
		$str = str_replace( "ü", "Ü", $str );
		$str = str_replace( "ş", "Ş", $str );
		$str = str_replace( "i", "İ", $str );
		$str = str_replace( "ı", "I", $str );
		$str = str_replace( "ö", "Ö", $str );
		$str = str_replace( "ç", "Ç", $str );

		return strtoupper( $str );

	}

	public static function tr_strtoupper( $metin ) {
		$metin = strtr( $metin, "ığüşiöç", "IĞÜŞİÖÇ" );

		return strtoupper( $metin );
	}

	/**
	 * ucwords tr
	 *
	 * @return void
	 * @author Özgür Köy
	 **/
	public static function ucwordsTR( $params, &$smarty ) {
		$str = $params[ "s" ];

		// $str[0] = strtoupper(strtr($str[0], "ığüşiöç", "IĞÜŞİÖÇ"));
		$str[ 0 ] = str_replace( "ş", "Ş", $str[ 0 ] );


		for ( $i = 0; $i < strlen( $str ); $i++ ) {
			if ( ( $str[ $i ] == " " || $str[ $i ] == "(" ) && isset( $str[ $i + 1 ] ) )
				$str[ $i + 1 ] = self::tr_strtoupper( $str[ $i + 1 ] );
		}

		return $str;
	}


	/**
	 * read word by key
	 *
	 * @return void
	 * @author Özgür Köy
	 **/
	public static function readByKey( $params, &$smarty ) {
		return $params[ "root" ][ $params[ "key" ] ];
	}

	/**
	 * check cart.
	 *
	 * @return void
	 * @author Özgür Köy
	 **/
	public function getLangFromDb( $params, &$smarty ) {
		// $_SESSION['lang'] = "TR";
		if ( !isset( $_SESSION[ 'lang' ] ) ) $_SESSION[ 'lang' ] = DEFAULT_LANGUAGE;


		$langs         = new Lang();
		$langs->active = 1;
		$langs->load();

		$langsArr = array();

		do {
			$langsArr[ $langs->langShort ] = $langs->langName;
		} while ( $langs->populate() );

		/*
$langs = array($_SESSION['lang']);

do {
	if($_SESSION['lang']==$lang->langShort){
		$smarty->assign("langSel",$lang->langName);
		$langs[0] = $lang->langName;
	}
	else
		$langs[]=$lang->langName;
} while ($lang->populate());
		*/


		$lang            = new Lang();
		$lang->langShort = $_SESSION[ 'lang' ];
		$lang->load();

		// $we = new WordEntry();
		// $we->load($lang);
		// $we
		$word = new Word();
		$word->load();

		$words = array();

		do {
			$e = $word->loadEntries();
			$e->filter( $lang );
			//
			// print_r($e);
			$words[ str_replace( " ", "", $word->name ) ] = $e->name;

		} while ( $word->populate() );

		unset( $word, $lang );
		// print_r($words);

		$smarty->assign( "lang", $_SESSION[ 'lang' ] );
		$smarty->assign( "langs", $langsArr );
		$smarty->assign( "words", $words );


	}

	/**
	 * check cart.
	 *
	 * @return void
	 * @author Özgür Köy
	 **/
	public static function getLang( $params, &$smarty ) {
		global $__DP;

		// $_SESSION['lang'] = "TR";
		if ( !isset( $_SESSION[ 'lang' ] ) ) $_SESSION[ 'lang' ] = DEFAULT_LANGUAGE;


		$lang         = new Lang();
		$lang->active = 1;
		$lang->load();

		$langsArr = array();

		do {
			$langsArr[ $lang->langShort ] = $lang->langName;
		} while ( $lang->populate() );


		//$langs = array($_SESSION['lang']);

		do {
			if ( $_SESSION[ 'lang' ] == $lang->langShort ) {
				$smarty->assign( "langSel", $lang->langName );
				$langs[ 0 ] = $lang->langName;
			}
			else
				$langs[ ] = $lang->langName;
		} while ( $lang->populate() );

		$words = JUtil::getLangWordFileContent();


		$smarty->assign( "lang", $_SESSION[ 'lang' ] );
		$smarty->assign( "langs", $langsArr );
		$smarty->assign( "words", $words );


	}

	/**
	 * remove ws
	 *
	 * @return void
	 * @author Özgür Köy
	 **/
	public static function cripple( $params, &$smarty ) {
		$smarty->assign( $params[ "var" ], str_replace( " ", "", $params[ "s" ] ) );
	}

	/**
	 * check sign in
	 *
	 * @return void
	 * @author Özgür Köy
	 **/
	public static function checkSignIn( $params, &$smarty ) {
		global $_S, $_cpath, $__DP;

		if ( isset( $_SESSION[ 'forceStep3' ] ) && $_SESSION[ 'forceStep3' ] == 1 && $_cpath != "sign-up-pub3" && $_cpath != "sPubProfileSave" && $_cpath != "editProfile" ) {
			$user = new User( $_SESSION[ 'adId' ] );
			$user->saveHistory( "FORCED TO STEP3" );
			unset( $user );

			echo '
			<script type="text/javascript" charset="utf-8">
				location.href="' . $_S . 'sign-up-pub3";
			</script>
			';
			exit;
		}

		$smarty->assign( "adLogged", isset( $_SESSION[ 'adLogged' ] ) );
		if ( isset( $_SESSION[ 'adLogged' ] ) ) {

			$smarty->assign( "adName", $_SESSION[ 'adName' ] );
			$smarty->assign( "adEmail", $_SESSION[ 'adEmail' ] );
			$smarty->assign( "adType", $_SESSION[ 'adType' ] );
			$smarty->assign( "adUName", $_SESSION[ 'adUName' ] );
			$smarty->assign( "avatar", "/site/layout/images/anonymous.jpg" );

			$smarty->assign( "adScreenname", $_SESSION[ 'adName' ] );
			if ( isset( $_SESSION[ 'timg' ] ) ) {
				$smarty->assign( "avatar", $_SESSION[ 'timg' ] );
				$smarty->assign( "adScreenname", $_SESSION[ 'adScreenname' ] );
			}
			elseif ( isset( $_SESSION[ 'fbimg' ] ) ) {
				$smarty->assign( "avatar", $_SESSION[ 'fbimg' ] );
				if ( isset( $_SESSION[ 'fbUsername' ] ) ) {
					$smarty->assign( "adScreenname", $_SESSION[ 'fbUsername' ] );
				}
				else {
					$smarty->assign( "adScreenname", $_SESSION[ 'fbNameSurname' ] );
				}
			}


		}
		// print_r($_SESSION);
	}


	/**
	 * check reg and redirect to reg page.
	 *
	 * @return void
	 * @author Özgür Köy
	 **/
	public static function checkReg() {
		global $_S;
		if ( ENABLE_INVITE == 1 && !isset( $_SESSION[ 'ivcode' ] ) && !isset( $_SESSION[ "IGNORE_ENABLE_INVITE_MODE" ] ) ) {
			// session_destroy();

			echo '
				<body>
				<script type="text/javascript" charset="utf-8">
					location.href="' . $_S . 'invite";
				</script>
				</body>
				</html>
			';
			die();

		}
		//INV HERE


		if ( isset( $_SESSION[ 'adLogged' ] ) && !isset( $_SESSION[ 'twitterAct' ] ) && ( !isset( $_SESSION[ 'adUName' ] ) || strlen( $_SESSION[ 'adUName' ] ) < 2 ) ) {
			session_destroy();

			echo '
				<script type="text/javascript" charset="utf-8">
					location.href="' . $_S . 'sign-up-pub";
				</script>
			';

		}
	}

	/**
	 * number format
	 *
	 * @return mixed
	 * @author Özgür Köy
	 **/
	public static function nformat( $params, &$smarty ) {
		return JUtil::NumberFormat( $params[ "n" ], isset( $params[ "d" ] ) ? $params[ "d" ] : 2 );
	}

	/**
	 * return admin username
	 *
	 * @return void
	 * @author Özgür Köy
	 **/
	public static function admUser( $params, &$smarty ) {
		return isset( $_SESSION[ 'admingleADMUS' ] ) ? $_SESSION[ 'admingleADMUS' ] : "";
	}

	/**
	 * get good error
	 *
	 * @return void
	 * @author Özgür Köy
	 **/
	public static function getGoodVal( $params, &$smarty ) {
		$ms = isset( $_SESSION[ 'goodVal' ] ) ? $_SESSION[ 'goodVal' ] : "";
		unset( $_SESSION[ 'goodVal' ] );

		$smarty->assign( "goodVal", $ms );
	}

	/**
	 * get the array value by step
	 *
	 * @return void
	 * @author Özgür Köy
	 **/
	public static function moveArrayStep( $params, &$smarty ) {
		$al = sizeof( $params[ "a" ] );
		if ( $al <= $params[ "key" ] ) $params[ "key" ] = $al;


		$r0 = reset( $params[ "a" ] );


		for ( $za = 1; $za < $params[ "key" ]; $za++ ) {
			$r0 = next( $params[ "a" ] );
		}

		$smarty->assign( "lastArrVal", $r0 );

	}

	/**
	 * get form token
	 *
	 * @return void
	 * @author Özgür Köy
	 **/
	public static function getFormToken( $params, &$smarty ) {
		$token = JFORM::getFormToken( $params[ "f" ] );

		return '<input type="hidden" name="t0ken" value="' . $token . '" id="t0ken">';
	}

	public static function timestampToDate( $params, &$smarty ) {
		if ( isset( $params[ 'l' ] ) ) {
			$l = strtolower( $params[ 'l' ] );
			if ( $l == 'tr' ) {
				return date( 'd/m/Y', $params[ 'd' ] );
			}
			elseif ( $l == 'en' ) {
				return date( 'm/d/Y', $params[ 'd' ] );
			}
		}
		else {
			return date( 'd/m/Y', $params[ 'd' ] );
		}
	}

	public static function timestampToDateWithTime( $params, &$smarty ) {
		if ( isset( $params[ 'l' ] ) ) {
			$l = strtolower( $params[ 'l' ] );
			if ( $l == 'tr' ) {
				return date( 'd/m/Y (H:i)', $params[ 'd' ] );
			}
			elseif ( $l == 'en' ) {
				return date( 'm/d/Y (H:i)', $params[ 'd' ] );
			}
		}
		else {
			return date( 'd/m/Y (H:i)', $params[ 'd' ] );
		}
	}

	public static function rndNumber( $params, &$smarty ) {
		extract( $params );

		$random_number = null;

		if ( isset( $step ) ) {
			$vals = array();
			for ( $i = $in; $i < $out; $i += $step ) {
				$vals[ ] = $i;
			}
			$random_number = $vals[ array_rand( $vals ) ];
		}
		else {
			srand( (double)microtime() * 1000000 );

			$random_number = rand( $in, $out );
		}
		if ( isset( $assign ) ) {
			$smarty->assign( $assign, $random_number );
		}
		else {
			return $random_number;
		}
	}

	/**
	 * Returns the formatted currency for current active country
	 *
	 * @param type $params
	 * @param type $smarty
	 * @return string
	 *
	 * @author Murat
	 */
	public static function getFormattedCurrency( $params, &$smarty ) {
		return JUtil::getFormattedCurrency( $params[ "a" ] );
	}


	/**
	 * currency format
	 *
	 * @return string
	 * @author Murat
	 **/
	public static function cformat( $params, &$smarty ) {
		return JUtil::getFormattedCurrency( JUtil::NumberFormat( $params[ "n" ], isset( $params[ "d" ] ) ? $params[ "d" ] : 2 ) );
	}


	/**
	 * DateTime Format
	 *
	 * @return string
	 * @author Murat
	 */
	public static function dformat( $params, &$smarty ) {

		$dateFormat = isset( $params[ "f" ] ) ? $params[ "f" ] : 'm/d/Y  H:i:s';

		return date( $dateFormat, $params[ "t" ] );
	}


	public static function formBox( $params, Smarty &$smarty ) {

		$smarty->assign('__label',$params["label"]);
		$smarty->assign('__name',$params["name"]);
		$smarty->assign('__value',$params["value"]);
		
		if(isset($params["info"]))
			$smarty->assign('__info',$params["info"]);
		else
			$smarty->assign('__info',null);
		
		if(isset($params["class"]))
			$smarty->assign('__class',$params["class"]);
		else
			$smarty->assign('__class',null);

		if(isset($params["vkey"]))
			$smarty->assign('__vkey',$params["vkey"]);
		else
			$smarty->assign('__vkey',null);

		if(isset($params["options"]))
			$smarty->assign('__options',$params["options"]);
		else
			$smarty->assign('__options',null);

		return $smarty->fetch('formPieces/'.$params["type"].'.tpl');
	}
}
