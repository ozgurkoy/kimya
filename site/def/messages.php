<?php
// "dummy"=>array("title"=>"","message"=>"");
/*
	_x -> will not post to user dashboard
	x_ -> will not post to user email
	x -> will post to user dashboard
*/

$_footerText['TR']['both']		 = '<p>Daha fazla bilgi almak için Sıkça Sorulan Sorular bölümünü ziyaret edebilir, her türlü soru, öneri ve şikayetinizle ilgili olarak <a href="mailto:' . $SYSTEM_EMAILS["yardim"] . '">' . $SYSTEM_EMAILS["yardim"] . '</a> e-Posta adresinden sorularınızı iletebilirsiniz.</p>';
$_footerText['TR']['publisher']  = '<p>Daha fazla bilgi almak için <a href="'.$_S.'publisher-faq/TR">Sıkça Sorulan Sorular</a> bölümünü ziyaret edebilir, her türlü soru, öneri ve şikayetinizle ilgili olarak <a href="mailto:' . $SYSTEM_EMAILS["yardim"] . '">' . $SYSTEM_EMAILS["yardim"] . '</a> e-Posta adresinden sorularınızı iletebilirsiniz.</p>';
$_footerText['TR']['advertiser'] = '<p>Daha fazla bilgi almak için <a href="'.$_S.'advertiser-faq/TR">Sıkça Sorulan Sorular</a> bölümünü ziyaret edebilir, her türlü soru, öneri ve şikayetinizle ilgili olarak <a href="mailto:' . $SYSTEM_EMAILS["yardim"] . '">' . $SYSTEM_EMAILS["yardim"] . '</a> e-Posta adresinden sorularınızı iletebilirsiniz.</p>';

$_footerText['EN']['both']		 = '<p>For further information, feel free to visit our Frequently Asked Questions section, all other questions, suggestions and complaints  <a href="mailto:' . $SYSTEM_EMAILS["help"] . '">' . $SYSTEM_EMAILS["help"] . '</a> can be e-mailed to our customer services.</p>';
$_footerText['EN']['publisher']  = '<p>For further information, feel free to visit our <a href="'.$_S.'publisher-faq/EN">Frequently Asked Questions</a> section, all other questions, suggestions and complaints  <a href="mailto:' . $SYSTEM_EMAILS["help"] . '">' . $SYSTEM_EMAILS["help"] . '</a> can be e-mailed to our customer services.</p>';
$_footerText['EN']['advertiser'] = '<p>For further information, feel free to visit our <a href="'.$_S.'advertiser-faq/EN">Frequently Asked Questions</a> section, all other questions, suggestions and complaints  <a href="mailto:' . $SYSTEM_EMAILS["help"] . '">' . $SYSTEM_EMAILS["help"] . '</a> can be e-mailed to our customer services.</p>';

$_sysMessages = array(
 	"TR"=> array(
		"generic_"=>array(
			"title"=>"%s",
			"message"=>"%s"
		),
		// admin
		"resetAdmPass"=>array(
		  	"title"=>"Admin Şifreniz oluşturuldu.",
			"message"=>"Sayın %s,<br />
			Aşağıdaki bilgileriniz ile Admin Panel'e giriş yapabilirsiniz.<br />
			Kullanıcı adı: %s<br />
			Şifre : %s<br />
			"),
		// end admin
		"_welcomeAdv"=>array(
			"title"=>"Hoş geldiniz!",
			"message"=>"Sayın %s (%s),
			<a href=\"%s\">%s</a> tıklayarak e-posta adresinizi onaylamanız gerekmektedir.<br />
			Siz e-posta adresinizi onayladıktan sonra üyeliğiniz adMingle tarafından incelenecek ve onaylandığında size haber verilecektir.<br />
			Kullanıcı adınız ve şifreniz ayrıca size gönderilecektir.<br />
			adMingle'a hoş geldiniz, yeni bir reklamveren olarak başarılar dileriz.<br />
			".$_footerText['TR']['advertiser']
			),
		"_welcomePub"=>array(
			"title"=>"Hoş geldiniz!",
			"message"=>"Sayın %s,<br />
			adMingle’ı tercih ettiğiniz için çok teşekkürler. Kaydınız başarı ile gerçekleştirildi.<br />
               <a href=\"%s\">adMingle panelinizden</a> profilinize ulaşabilir ve size sunulacak kampanyaları gözden geçirebilirsiniz.<br />
               Çeşitli e-posta servisleri bağlantıya tıklamanıza izin vermeyebilir. Bu tarz bir sıkıntı ile karşılaşıyorsanız bağlantıyı kopyaladıktan sonra adres satırına yapıştırmanız ve sayfaya girmeniz yeterli olacaktır.<br />
               ".$_footerText['TR']['publisher']
			),
		"welcomePubDash_"=>array(
			"title"=>"Hoşgeldiniz!",
			"message"=>"Sayın %s,<br />
			adMingle’ı tercih ettiğiniz için çok teşekkürler. Kaydınız başarı ile gerçekleştirildi."
			),
		"_usernamePasswordActivation"=>array(
		  	"title"=>"Üyeliğiniz onaylandı, hoşgeldiniz!"  ,
			"message"=>"Sayın %s,<br />
               Hesabınız adMingle tarafından onaylandı. Artık adMingle’ın avantajlarından faydalanmanızın tam zamanı! Aşağıda sistem tarafından size özel üretilmiş kullanıcı adı ve şifrenizi bulabilirsiniz.<br />
               Kullanıcı adı: %s<br />
               Şifre : %s<br />
               Size özel rastgele üretilmiş şifrenizi <a href=\"%s\">adMingle panelinizden</a> değiştirmeyi unutmayın.
               ".$_footerText['TR']['advertiser']
		),
		"_newAdvPass"=>array(
		  	"title"=>"Şifreniz değiştirildi."  ,
			"message"=>"Sayın %s,<br />
               İsteğiniz üzere şifreniz sıfırlanmıştır. Aşağıdaki yeni bilgileriniz ile giriş yapabilirsiniz.<br />
               Kullanıcı adı: %s<br />
               Şifre : %s<br />
               Size özel rastgele üretilmiş şifrenizi <a href=\"%s\">adMingle panelinizden</a> değiştirmeyi unutmayın.<br />
               ".$_footerText['TR']['advertiser']
		),
		"_resetAdvPass"=>array(
		  	"title"=>"Şifreniz oluşturuldu."  ,
			"message"=>"Sayın %s,<br />
               Şifreniz oluşturulmuştur. Aşağıda bulunan bilgileriniz ile giriş yapabilirsiniz.<br />
               Kullanıcı adı: %s<br />
               Şifre : %s<br />
               Size özel rastgele üretilmiş şifrenizi <a href=\"%s\">adMingle panelinizden</a> değiştirmeyi unutmayın.<br />
               ".$_footerText['TR']['advertiser']
		),
 		"campaignAccepted"=>array(
			"title"=>"%s kampanyanız adMingle tarafından kabul edildi",
			"message"=>"Sayın %s,<br />
               adMingle üzerinden oluşturduğunuz %s adlı kampanyanız adMingle tarafından onaylandı. Teklifiniz oluşturulacak ve sistem üzerinde aktif hale getirilecektir. Kampanyanızın mevcut durumunu panelinizden kontrol edebileceğiniz gibi, her durum değişikliği e-posta ile de bildirilmektedir.<br />
               ".$_footerText['TR']['advertiser']
			),
 		"newCampaign"=>array(
			"title"=>"%s kampanyanız oluşturuldu",
			"message"=>"Sayın %s,<br />
               adMingle üzerinden oluşturduğunuz %s adlı kampanyanız başarı ile oluşturuldu. Şu anda kontrol ve teklif oluşturulması amacı ile adMingle’a iletilmiş durumdadır. Kampanyanızın mevcut durumunu panelinizden kontrol edebileceğiniz gibi, her durum değişikliği e-posta ile de bildirilmektedir.<br />
               ".$_footerText['TR']['advertiser']
			),
 		"campaignDeclinedNoReason"=>array(
			"title"=>"%s kampanyanız adMingle tarafından reddedildi",
			"message"=>"Sayın %s,<br />
               adMingle üzerinden oluşturduğunuz %s adlı kampanyanız adMingle tarafından kabul edilmemiştir. Dilerseniz adMingle paneliniz üzerinden yeni bir kampanya oluşturabilirsiniz.<br />
               Yeni bir kampanya oluşturmak için <a href=\"%s\">tıklayın</a>.<br />
               ".$_footerText['TR']['advertiser']
			),
 		"campaignDeclined"=>array(
			"title"=>"%s kampanyanız adMingle tarafından reddedildi",
			"message"=>"Sayın %s,<br />
               adMingle üzerinden oluşturduğunuz %s adlı kampanyanız adMingle tarafından kabul edilmemiştir. Kabul edilmeme sebebini aşağıda bulabilirsiniz.<br />
               \"%s\"<br />
               Yeni bir kampanya oluşturmak için <a href=\"%s\">tıklayın</a>.<br />
               ".$_footerText['TR']['advertiser']
			),
 		"newOfferArrived"=>array(
			"title"=>"%s kampanyanız için Admingle size teklif gönderdi",
			"message"=>"Sayın %s,<br />
               adMingle üzerinden oluşturduğunuz %s adlı kampanyanızın teklifini aşağıda görebilirsiniz. Lütfen son kontrollerinizi yapın. Ardından vereceğiniz onay ve yapacağınız ödeme ile kampanyanız belirttiğiniz tarih aralığında otomatik olarak başlatılacaktır.<br />
               Kampanya detayları için <a href=\"%s\">tıklayın</a>.<br />
               ".$_footerText['TR']['advertiser']
			),
 		"offerAccepted"=>array(
			"title"=>"%s kampanyanız için teklifi onayladınız",
			"message"=>"Sayın %s,<br />
               adMingle üzerinden oluşturduğunuz %s adlı kampanyanızın teklifini onayladınız. Ödeme kontrollerinden sonra kampanyanız aktif hale getirilecektir.<br />
                Mevcut adMingle bakiyeniz %s " . CURRENCY_SYMBOL . ". Teklifini onayladığınız %s kampanyasının bütçesi %s " . CURRENCY_SYMBOL . "’dir.<br />
               Admingle hesabınızı görüntülemek için <a href=\"%s\">tıklayın</a>.<br />
               Admingle kasanıza para yatırmak için <a href=\"%s\">tıklayın</a>.<br />
               ".$_footerText['TR']['advertiser']
			),
 		"offerDeclined"=>array(
			"title"=>"%s kampanyanız için teklifi reddettiniz",
			"message"=>"Sayın %s,<br />
               adMingle üzerinden oluşturduğunuz %s adlı kampanyanızın teklifini reddettiniz. Reddettiğiniz teklif ile ilgili satış danışmanlarımız sizlere profilinizde belirttiğiniz telefonunuzdan ulaşacaktır.<br />
                Eğer herhangi bir soru veya sorununuz varsa <a href=\"mailto:" . $SYSTEM_EMAILS["satis"] . "\">" . $SYSTEM_EMAILS["satis"] . "</a> adresine e-posta gönderebilir ya da 0 (212) 223 23 90 numaralı telefonumuzdan müşteri temsilcimiz ile görüşebilirsiniz.<br />
               ".$_footerText['TR']['advertiser']
			),
 		"paymentApproved"=>array(
			"title"=>"Ödemeniz onaylandı",
			"message"=>"Sayın %s,<br />
                adMingle üzerinden gerçekleştirdiğiniz %s " . CURRENCY_SYMBOL . " para transferiniz onaylanmış ve adMingle bakiyeniz güncellenmiştir.<br />
               ".$_footerText['TR']['both']
			),
 		"needPayment"=>array(
			"title"=>"Ödeme Hatırlatması",
			"message"=>"Sayın %s,<br />
               adMingle üzerinden gerçekleştirdiğiniz %s adlı kampanyanızın başlayabilmesi için ödeme yapmanız gerekmektedir.<br />
               ".$_footerText['TR']['advertiser']
			),
 		"campaignReady"=>array(
			"title"=>"%s kampanyanız başlıyor!",
			"message"=>"Sayın %s,<br />
               adMingle üzerinden oluşturduğunuz %s adlı kampanyanız yayınlanmaya hazırdır.<br />
               %s - %s tarihleri arasında, onaylayan yayıncılar tarafından kampanyanız yayınlanacaktır.<br />
               ".$_footerText['TR']['advertiser']
			),
 		"campaignAvailable"=>array(
			"title"=>"Yeni Kampanya: %s!",
			"message"=>"Merhaba %s,<br />
               %s adlı kampanya %s itibari ile katılımınıza açılmıştır.<br />
               Kampanya detaylarını görebilmek için lütfen <a href=\"%s\" target=\"_blank\">buraya</a> tıklayınız.<br />
               ".$_footerText['TR']['publisher']
			),
 		"campaignStarted"=>array(
			"title"=>"%s kampanyanız başlıyor!",
			"message"=>"Sayın %s,<br />
               adMingle üzerinden oluşturduğunuz %s adlı kampanyanız %s itibari ile yayınlanmaya başlamıştır. %s tarihinde yayınlanma tamamlanacaktır.<br />
               ".$_footerText['TR']['advertiser']
			),
 		"campaignEnded"=>array(
			"title"=>"%s kampanyanız sonuçlandı!",
			"message"=>"Sayın %s,<br />
               adMingle üzerinden oluşturduğunuz %s adlı kampanyanız %s tarihinde tamamlanmıştır. Toplam ulaşılan kişi sayısı: %s<br />
               Detaylı raporları görmek için <a href=\"%s\">raporlar</a> sayfanıza bakabilirsiniz.<br />
               ".$_footerText['TR']['advertiser']
			),
 		"checkPendingCampaigns"=>array(
			"title"=>"Yeni kampanya teklifi!",
			"message"=>"Merhaba %s,<br />
               adMingle üzerinde sizi bekleyen yeni bir kampanya teklifi bulunuyor! Teklifinizi hemen kontrol etmek için <a href=\"%s\">tıklayın</a>.<br />
               ".$_footerText['TR']['advertiser']
			),
 		"acceptedCampaign"=>array(
			"title"=>"%s kampanya teklifini kabul ettiniz!",
			"message"=>"Merhaba %s,<br />
               %s adlı kampanyanın teklifini kabul ettiniz. %s tarihinde saat %s itibariyle mesajınız gönderilecektir.<br />
                Kampanya detaylarını görebilmek için lütfen <a href=\"%s\" target=\"_blank\">buraya</a> tıklayınız.<br />
               ".$_footerText['TR']['advertiser']
			),
 		"acceptedYoutubeCampaign"=>array(
			"title"=>"%s kampanya teklifini kabul ettiniz!",
			"message"=>"Merhaba %s,<br />
               %s adlı youtube kampanyasının teklifini kabul ettiniz. Kampanya sonuna kadar video yüklemesini gerçekleştirmelisiniz.<br />
               ".$_footerText['TR']['advertiser']
			),
 		"youtubeVideoAccepted"=>array(
			"title"=>"%s kampanyasına gönderdiğiniz video kabul edildi!",
			"message"=>"Merhaba %s,<br />
               %s adlı youtube kampanyasının gönderdiğiniz video kabul edildi.<br />
               ".$_footerText['TR']['advertiser']
			),
 		"youtubeVideoDenied"=>array(
			"title"=>"%s kampanyasına gönderdiğiniz video reddedildi!",
			"message"=>"Merhaba %s,<br />
               %s adlı youtube kampanyasının gönderdiğiniz video reddedildi.<br /> 
			Sebebi : %s<br />
               ".$_footerText['TR']['advertiser']
			),
 		"acceptedCampaignMultiple"=>array(
			"title"=>"%s kampanya teklifini kabul ettiniz!",
			"message"=>"Merhaba %s,<br />
               %s adlı kampanyanın teklifini kabul ettiniz. Mesajlarınız belirttiğiniz tarih ve saatlerde gönderilecektir.<br />
               ".$_footerText['TR']['advertiser']
			),
		"_forgotPass"=>array(
			"title"=>"Şifre değişikliği",
			"message"=>"Merhaba %s,<br />
               %s tarihinde şifre değişikliği talebinde bulundunuz. <a href=\"%s\">%s</a> adresine tıklayarak yeni şifrenizi belirleyebilirsiniz.<br />
               Eğer herhangi bir talepte bulunmadıysanız bu e-postayı gözardı edebilirsiniz.<br />
               ".$_footerText['TR']['both']
		),
		"_forgotPassNew"=>array(
			"title"=>"Şifre değişikliği",
			"message"=>"Merhaba %s,<br />
               %s tarihinde şifre değişikliğiniz gerçekleştirildi. Yeni şifreniz ile adMingle’a giriş yapabilirsiniz.<br />
			".$_footerText['TR']['both']
		),
		"tweetDeleted_"=>array(
			"title"=>"Kampanya mesajı silmeniz hakkında",
			"message"=>"Merhaba %s,<br />
			%s kampanyası için onayladığınız ve adMingle tarafından gönderilmiş %s tarihli kampanya mesajını 72 saat geçmeden sildiğiniz tespit edilmiştir. Bu durumda kazancınızın hesabınıza geçmeyeceğini hatırlatmak durumundayız.<br />
			<strong>Silinen mesaj :</strong><br />%s <br />
			".$_footerText['TR']['publisher']
		),
		"tweetNotDeleted_"=>array(
			"title"=>"Kampanya katılımı başarı ile tamamlandı!",
			"message"=>"Merhaba %s,<br />
                %s kampanyası katılımınız %s tarihinde başarı ile tamamlanmıştır. adMingle Bakiyenize %s " . CURRENCY_SYMBOL . " eklenmiştir.<br />
			".$_footerText['TR']['publisher']
		),
		"tweetSent"=>array(
			"title"=>"Kampanya mesajı gönderildi.",
			"message"=>"Merhaba %s,<br />
               %s kampanyası için onayladığınız kampanya mesajı, adMingle tarafından %s tarihinde hesabınıza gönderilmiştir. Kampanya katılımınızın başarılı olabilmesi için kampanya mesajının 72 saat boyunca yayında kalması gerekmektedir.<br />
			<strong>Gönderilen Kampanya Mesajınız :</strong><br />%s <br />
			".$_footerText['TR']['publisher']
		),
		"inviteAccepted"=>array(
			"title"=>"Davet Cevabı",
				"message"=>"%s adMingle a hoşgeldin. " . SERVER_NAME . " a girip twitter hesabın ile giriş yaparak hemen kazanmaya başlayabilirsin!"
		),
		"inviteAcceptedShort"=>array(
			"title"=>"Kısa Davet Cevabı",
			"message"=>"%s adMingle a hoşgeldin."
		),
		"inviteAcceptedTitle"=>array(
			"title"=>"adMingle Davetiyeniz Hakkında",
			"message"=>""
		),
		"_UserDeactivated"=>array(
			"title"=>"adMingle Üyeliğiniz Hakkında",
			"message"=>"Merhaba %s,<br />
				Twitter hesabınızda adMingle yetkilerini kaldırdığınız tespit edilmiştir. Bundan dolayı bugüne kadar ki kesinleşmemiz kazançlarınız iptal edilmiş olup, yeniden twitter ile giriş yapmanız durumunda yeni kampanyalardan yeniden kazanmaya başlayacağınızı bilgilerinize sunarız.
			<br /><br />
			".$_footerText['TR']['publisher']
		),
		"spamMailInformation_"=>array(
			"title"=>"E-Mail adresiniz hakkında",
			"message"=>"Sayın %s,<br />
			Size son göndermiş olduğumuz maili spam olarak işaretlediğinizi tespit ettik.<br/>
			Spam politikamız gereği mail adresinizi değiştirmediğiniz müddetçe artık size bildirim maillerini gönderemeyeceğimizi, bu tip mailleri tekrardan alabilmek için Profil Günceleme kısmından mail adresinizi değiştirmeniz gerektiğini bilgilerinize sunarız.
			Profil günceleme kısmından gidip değiştirmezseniz sistem bildirimlerimizi sadece adMingle Panelinizden takip edebilirsiniz. <br /><br /> ".$_footerText['TR']['publisher']
		),

 	),

 	"EN"=> array(
		"generic_"=>array(
			"title"=>"%s",
			"message"=>"%s"
		),
		// admin
		"resetAdmPass"=>array(
		  	"title"=>"Admin Password generated.",
			"message"=>"Dear %s,<br />
			You may log-in to the admin panel with the information below.<br />
			Username : %s<br />
			Password : %s<br />
			"),
		// end admin
		"_welcomeAdv"=>array(
			"title"=>"Welcome!",
			"message"=>"
				Dear %s (%s), <a href=\"%s\">%s</a> You must confirm your e-mail address by clicking. <br />
				After you confirm your e-mail address adMingle team will review your membership once approved we will notify you.
				Your username and password will be sent to you in a separate e-mail.<br />
				Welcome to Admingle, good luck, you just become our Publisher!<br />".$_footerText['EN']['publisher']
			),
		"_welcomePub"=>array(
			"title"=>"Welcome!",
			"message"=>"
				Dear %s,<br />
				Thank you very much for choosing adMingle. Your registration is successfully approved.<br />
				<a href=\"%s\">Using adMingle panel you</a> may access your profile and review the campaigns presented to you.<br />
				Various e-mail services may not allow you to click the link provided here in.
				In case you are experiencing a problem with opening this link than be kind to copy and paste the address link provided here to your browser.<br />
            ".$_footerText['EN']['publisher']
			),
		"welcomePubDash_"=>array(
			"title"=>"Welcome!",
			"message"=>"Dear %s,<br />
			Thank you for choosing adMingle. Your registration is successfully approved."
			),
		"_usernamePasswordActivation"=>array(
			"title"=>"Your account has been approved, Welcome!"  ,
			"message"=>"
				Dear %s,<br />
				Your account is been approved through adMingle. Now is the time to take advantage of adMingle! Here below you shall find a user name and password prepared especially for you.<br />
				User name : %s<br />
				Password : %s<br />
				The random password created for you may be accessed from the <a href=\"%s\">adMingle panel;</a> don't forget to change it.<br />
			".$_footerText['EN']['advertiser']
		),
		"_newAdvPass"=>array(
			"title"=>"Your password had been changed."  ,
			"message"=>"
				Dear %s,<br />
				upon your request we had reset your password. You may enter adMingle using your new password provided below.<br />
				User Name : %s<br />
				Password : %s<br />
				The random password created for you may be changed from your <a href=\"%s\">adMingle panel;</a> once you log-in don't forget to change it.<br />
            ".$_footerText['EN']['advertiser']
		),
		"_resetAdvPass"=>array(
			"title"=>"Your Password is created."  ,
			"message"=>"
				Dear %s,<br />
				Your password is created. You may enter adMingle using your new information provided below.<br />
				User Name: %s<br />
				Password : %s<br />
				The random password created for you may changed from your  <a href=\"%s\">adMingle panel</a> don't forget to change it.<br />
            ".$_footerText['EN']['advertiser']
		),
 		"campaignAccepted"=>array(
			"title"=>"%s Your campaign was approved by adMingle.",
			"message"=>"
				Dear %s,<br />
				Your %s named campaign was approved by adMingle. Your proposal will be created and activated using our system shortly. You may control your campaigns status through your adMingle panel. In the same time adMingle will notify you via e-mail each status update.<br />
            ".$_footerText['EN']['advertiser']
			),
 		"newCampaign"=>array(
			"title"=>"%s your campaign is created",
			"message"=>"
				Dear %s,<br />
				Your %s named campaign was created successfully. It was submitted to adMingle for further control and approval. You may control your campaigns status through your adMingle panel. You will e-mail you each status update.<br />
            ".$_footerText['EN']['advertiser']
			),
 		"campaignDeclinedNoReason"=>array(
			"title"=>"%s named campaign was rejected by adMingle",
			"message"=>"Dear %s,<br />
            Your %s named campaign was rejected by adMingle. Be kind to go back to your adMingle dashboard to create a new campaign.<br />
            To create a new campaign <a href=\"%s\">click here</a>.<br />
            ".$_footerText['EN']['advertiser']
			),
 		"campaignDeclined"=>array(
			"title"=>"%s named campaign was rejected by adMingle",
			"message"=>"Dear %s,<br />
            Your %s named campaign was rejected by adMingle. Please see below the reason for rejecting your campaign.
            \"%s\"<br />
            To create a new campaign <a href=\"%s\">click here</a>.<br />
            ".$_footerText['EN']['advertiser']
			),
 		"newOfferArrived"=>array(
			"title"=>"%s adMingle had sent you a proposal related to your campaign.",
			"message"=>"
				Dear %s,<br />
				Your %s named campaign's proposal is down here below. Please make sure to make your final control. After you provide us your campaign approval and payment your campaign will start automatically within the chosen dates range.<br />
				For your campaign details <a href=\"%s\">click here</a><br />
            ".$_footerText['EN']['advertiser']
			),
 		"offerAccepted"=>array(
			"title"=>"%s You approved the proposal for your campaign",
			"message"=>"
				Dear %s,<br />
				You approved your %s submitted campaign proposal. After your payment is confirmed your campaign will be activated.<br />
				Your Current adMingle Balance %s " . CURRENCY_SYMBOL . ". The approved proposal for your %s campaign has a budget of %s " . CURRENCY_SYMBOL . ".<br />
				To view your adMingle account <a href=\"%s\">click here</a><br />
				To deposit money to your adMingle account <a href=\"%s\">click here</a><br />
            ".$_footerText['EN']['advertiser']
			),
 		"offerDeclined"=>array(
			"title"=>"%s You had rejected the proposal for your campaign",
			"message"=>"
				Dear %s,<br />
				You rejected our proposal for the %s campaign you created. One of our sales representatives will contact you shortly regarding your rejected campaign to your provided contact information .<br />
				For further assistance , questions or problems feel free to e-mail us to <a href=\"mailto:" . $SYSTEM_EMAILS["sales"] . "\">" . $SYSTEM_EMAILS["sales"] . "</a> or call us on 0 (212) 223 23 90 to speak to one of our customer representatives.<br />
            ".$_footerText['EN']['advertiser']
			),
 		"paymentApproved"=>array(
			"title"=>"You payment is approved",
			"message"=>"Dear %s,<br />
            You transfer of %s " . CURRENCY_SYMBOL . " to adMingle was approved and your account was credited accordingly.<br />
            ".$_footerText['EN']['both']
			),
 		"needPayment"=>array(
			"title"=>"Payment Reminder",
			"message"=>"
				Dear %s,<br />
				in order to publish your %s named campaign using adMingle you will need to complete your payment.<br />
            ".$_footerText['EN']['advertiser']
			),
 		"campaignReady"=>array(
			"title"=>"%s Your campaign just took-off!",
			"message"=>"
				Dear %s,<br />
				The %s named campaign you created is ready to be published.<br />
				adMingle Publishers that approved your campaign will publish it during these dates range  %s - %s.<br />
            ".$_footerText['EN']['advertiser']
			),
		"campaignAvailable"=>array(
			"title"=>"New Campaign: %s!",
			"message"=>"
				Dear %s,
				The <br /> named Campaign %s is open to your participation by %s.<br />
				Click <a href=\"%s\" target=\"_blank\">here</a> to see more details.<br />
			".$_footerText['EN']['publisher']
			),
 		"campaignStarted"=>array(
			"title"=>"%s Your campaign just took-off!",
			"message"=>"
				Dear %s,<br />
				The %s named campaign you created on adMingle started to be published on %s. and shall end on %s.<br />
            ".$_footerText['EN']['advertiser']
			),
 		"campaignEnded"=>array(
			"title"=>"%s Your campaign was Published!",
			"message"=>"
				Dear %s,<br />
				The %s named campaign you created on adMingle was published and completed on %s. People engaged with your message: %s<br />
				For further detailed reports feel free to visit your report page on <a href=\"%s\"> reports page</a> <br />
            ".$_footerText['EN']['advertiser']
			),
 		"checkPendingCampaigns"=>array(
			"title"=>"New Campaign Offer!",
			"message"=>"
				Dear %s,<br />
				adMingle has a new campaign offer waiting for you! To quickly check-out your proposal  <a href=\"%s\">click here</a>.<br />
            ".$_footerText['EN']['advertiser']
			),
 		"acceptedCampaign"=>array(
			"title"=>"%s You had accepted the campaign offer!",
			"message"=>"
				Dear %s,<br />
				The %s named campaign proposal was accepted by you. Your campaign will be published on %s at %s .<br />
				Click <a href=\"%s\" target=\"_blank\">here</a> to see more details.<br />
            ".$_footerText['EN']['advertiser']
			),
 		"acceptedYoutubeCampaign"=>array(
			"title"=>"%s You had accepted the campaign offer!",
			"message"=>"
				Dear %s,<br />
				The %s named campaign proposal was accepted by you. You must upload your video before the campaign end date.<br />
            ".$_footerText['EN']['advertiser']
			),
 		"youtubeVideoAccepted"=>array(
			"title"=>"Your video is accepted for the %s campaign.",
			"message"=>"Hello %s,<br />
               Your video is accepted for the %s campaign.<br />
               ".$_footerText['EN']['advertiser']
			),
 		"youtubeVideoDenied"=>array(
			"title"=>"Your video for the %s campaign is denied .",
			"message"=>"Hello %s,<br />
			Your video is denied for the %s campaign.<br />
			Reason : %s<br />
               ".$_footerText['EN']['advertiser']
			),
 		"acceptedCampaignMultiple"=>array(
			"title"=>"%s You had accepted the campaign offer!",
			"message"=>"
				Dear %s,<br />
				The %s named campaign proposal was accepted by you. Your campaign will be published at your chosen date and time.<br />
            ".$_footerText['EN']['advertiser']
			),
		"_forgotPass"=>array(
			"title"=>"Password Change",
			"message"=>"
				Dear %s,<br />
				On %s you requested to change your password. <a href=\"%s\">%s</a> by clicking this address you may set your new password.<br />
				In case you did not request any password change than be kind to ignore this e-mail. <br />
			".$_footerText['EN']['both']
		),
		"_forgotPassNew"=>array(
			"title"=>"Password Change",
			"message"=>"
				Dear %s,<br />
				On %s your password change request was approved. You may sign-in to adMingle using your new password.<br />
			".$_footerText['EN']['both']
		),
		"tweetDeleted_"=>array(
			"title"=>"Information regarding the rejected campaign message",
			"message"=>"
				Dear %s,<br />
				Your %s campaign message sent using adMingle on %s was erased by you before it completed being available on your timeline for 72 hours. According to our mutual agreement you may not realize any profit and therefore we ought to remind you that no payment can be made to your bank account. <br />
				<strong>Once again please see the campaign message you had erased :</strong><br />%s <br />
			".$_footerText['EN']['publisher']
		),
		"tweetNotDeleted_"=>array(
			"title"=>"You participation in the campaign was successful!",
			"message"=>"
				Dear %s,<br />
				The %s campaign was successfully completed on %s. Your account was credited with  %s " . CURRENCY_SYMBOL . ".<br />
			".$_footerText['EN']['publisher']
		),
		"tweetSent"=>array(
			"title"=>"Your campaing messgae was sent",
			"message"=>"
				Dear %s,<br />
				The %s campaign message that you approved, was sent by adMingle on %s to your account. For your campaign to be successful you need to keep it published for 72 hours in your timeline.<br />
				<strong> The campaign message sent is: </strong><br />%s <br />
			".$_footerText['EN']['publisher']
		),
		"inviteAccepted"=>array(
			"title"=>"Invite Response",
			"message"=>"%s Welcome to adMingle. Go to " . SERVER_NAME . " log-in with your Twitter account and start to profit today!"
		),
		"inviteAcceptedShort"=>array(
			"title"=>"ShowInviteResponse",
			"message"=>"%s Welcome to adMingle"
		),
		"inviteAcceptedTitle"=>array(
			"title"=>"About Your adMingle Invitation",
			"message"=>""
		),
		"_UserDeactivated"=>array(
			"title"=>"About Your adMingle Membership",
			"message"=>"
				Dear %s,<br />
				You have revoked adMingle to access your Twitter account.
				Therefore, apart from the gains obtained so far, only after you log-in again to adMingle using your Twitter account you will be able to join new campaigns and make a profit. <br /><br />
			".$_footerText['TR']['publisher']
		),
		"spamMailInformation_"=>array(
			"title"=>"About your e-mail address",
			"message"=>"
				Dear %s,<br />
				We noticed that you had marked adMingle notification e-mails sent to you as spam.<br/>
				Our Spam policy is in line with international spam policies, therefore, as long as you do not go to your profile update and provide the adMingle system with a new e-mail address, you will not be able to receive any notification e-mails from adMingle, however, to continue and follow adMingle system notifications you may go to adMingle dashboard. <br /><br />
				" . $_footerText['EN']['publisher']
		),

 	)
);
