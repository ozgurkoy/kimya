<?php
/************************************
 *
 *	smarty cache dirs
 *	özgür köy
 *	to-do's:
 *	- use memcached if performance sucks
 *	do not edit the necessary ones!
 *
 ************************************/
$smartyCacheDirs = array(
/*
	necessary
*/
'_sysHalt'=>
	array(
	'realPath'=>'necessary/siteHalt.tpl',
	'cacheDir'=>'necessary|siteHalt'
	),
'_sysError'=>
	array(
		'realPath'=>'necessary/siteError.tpl',
		'cacheDir'=>'necessary|siteError'
	),
'_configProblem'=>
	array(
	'realPath'=>'necessary/configProblem.tpl',
	'cacheDir'=>'necessary|configProblem'
	),
'login'=>
	array(
	'realPath'=>'login.tpl',
	'cacheDir'=>'login'
	),
'home'=>
	array(
	'realPath'=>'home.tpl',
	'cacheDir'=>'home'
	),
'actions'=>
	array(
	'realPath'=>'actions.tpl',
	'cacheDir'=>'actions'
	),
'actionForm'=>
	array(
	'realPath'=>'form/action.tpl',
	'cacheDir'=>'factions'
	),
'languages'=>
	array(
	'realPath'=>'languages.tpl',
	'cacheDir'=>'languages'
	),
'languageForm'=>
	array(
	'realPath'=>'form/language.tpl',
	'cacheDir'=>'flanguages'
	),
'sites'=>
	array(
	'realPath'=>'sites.tpl',
	'cacheDir'=>'sitese'
	),
'siteForm'=>
	array(
	'realPath'=>'form/site.tpl',
	'cacheDir'=>'fsites'
	),
'users'=>
	array(
	'realPath'=>'users.tpl',
	'cacheDir'=>'userse'
	),
'userForm'=>
	array(
	'realPath'=>'form/user.tpl',
	'cacheDir'=>'fusers'
	),
'wordLang'=>
	array(
	'realPath'=>'wordLang.tpl',
	'cacheDir'=>'wordLang'
	),
'wordEntryVersion'=>
	array(
	'realPath'=>'wordEntryVersion.tpl',
	'cacheDir'=>'wordEntryVersion'
	),
'wordEntryEdit'=>
	array(
	'realPath'=>'form/wordEntryEdit.tpl',
	'cacheDir'=>'wordEntryEdit'
	),
'words'=>
	array(
	'realPath'=>'words.tpl',
	'cacheDir'=>'wordse'
	),
'wordForm'=>
	array(
	'realPath'=>'form/word.tpl',
	'cacheDir'=>'fwords'
	),
'customers'=>
	array(
	'realPath'=>'customers.tpl',
	'cacheDir'=>'customerse'
	),
'customerForm'=>
	array(
	'realPath'=>'form/customer.tpl',
	'cacheDir'=>'fcustomers'
	),
'proposals'=>
	array(
	'realPath'=>'proposals.tpl',
	'cacheDir'=>'proposalse'
	),
'proposalForm'=>
	array(
	'realPath'=>'form/proposal.tpl',
	'cacheDir'=>'fproposals'
	),
'proposalProductForm'=>
	array(
	'realPath'=>'form/proposalProduct.tpl',
	'cacheDir'=>'fproposalprs'
	),
'contacts'=>
	array(
	'realPath'=>'contacts.tpl',
	'cacheDir'=>'contactse'
	),
'allContacts'=>
	array(
	'realPath'=>'allContacts.tpl',
	'cacheDir'=>'allcontactse'
	),
'contactForm'=>
	array(
	'realPath'=>'form/contact.tpl',
	'cacheDir'=>'fcontacts'
	),
'sectors'=>
	array(
	'realPath'=>'sectors.tpl',
	'cacheDir'=>'sectorse'
	),
'sectorForm'=>
	array(
	'realPath'=>'form/sector.tpl',
	'cacheDir'=>'fsectors'
	),
'activities'=>
	array(
	'realPath'=>'activities.tpl',
	'cacheDir'=>'activitiese'
	),
'activityForm'=>
	array(
	'realPath'=>'form/activity.tpl',
	'cacheDir'=>'factivities'
	),
'currencies'=>
	array(
	'realPath'=>'currencies.tpl',
	'cacheDir'=>'currencyse'
	),
'currencyForm'=>
	array(
	'realPath'=>'form/currency.tpl',
	'cacheDir'=>'fcurrencys'
	),
'productGroups'=>
	array(
	'realPath'=>'productGroups.tpl',
	'cacheDir'=>'productGroupse'
	),
'productGroupForm'=>
	array(
	'realPath'=>'form/productGroup.tpl',
	'cacheDir'=>'fproductgroups'
	),
'products'=>
	array(
	'realPath'=>'products.tpl',
	'cacheDir'=>'productse'
	),
'productStocks'=>
	array(
	'realPath'=>'stocks.tpl',
	'cacheDir'=>'productstte'
	),
'productForm'=>
	array(
	'realPath'=>'form/product.tpl',
	'cacheDir'=>'fproducts'
	),
'wordcats'=>
	array(
	'realPath'=>'sectors.tpl',
	'cacheDir'=>'wordcatse'
	),
'wordcatForm'=>
	array(
	'realPath'=>'form/sector.tpl',
	'cacheDir'=>'fwordcats'
	),
'wordtypes'=>
	array(
	'realPath'=>'wordtypes.tpl',
	'cacheDir'=>'wordtypese'
	),
'wordtypeForm'=>
	array(
	'realPath'=>'form/wordtype.tpl',
	'cacheDir'=>'fwordtypes'
	),
'roles'=>
	array(
	'realPath'=>'roles.tpl',
	'cacheDir'=>'rolese'
	),
'roleForm'=>
	array(
	'realPath'=>'form/role.tpl',
	'cacheDir'=>'froles'
	),
'roleMod'=>
	array(
	'realPath'=>'form/roleMod.tpl',
	'cacheDir'=>'frolesmod'
	),

    
);

