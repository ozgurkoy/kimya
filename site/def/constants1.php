<?php
date_default_timezone_set('Europe/Istanbul');
define('SESSION_TIMEOUT', 600);

if (PHP_SAPI === 'cli') { // working from command line
	if (count($argv) < 2) {
		print_f($argv);
		die('Argument Error');
	}
	define('SERVER_NAME', 'n.admingle.com');
	define('DOCUMENT_ROOT', $argv[1].'/');
} else {
	define('SERVER_NAME', 'n.admingle.com');
	define('DOCUMENT_ROOT', $_SERVER['DOCUMENT_ROOT'].'/');
	define('WKHTML_ROOT', '/Applications/wkhtmltopdf.app/Contents/MacOS/');
}

if (PHP_SAPI != 'cli') {
	define('COOKIE_LIFE', 1209600) ; //14 days
	define('COOKIE_PREFIX', 'ADM_');
}

// session files will be saved to...
$sess_save_path = DOCUMENT_ROOT.'temp/session/';

// datasources
$GLBS['mysqlDSN'] = array(
	'MDB'	=>array( 'dsn'=>'mysql:host=127.0.0.1;dbname=devV15_main','user'=>'root', 'pass'=>'r123'),	// main
	'CAMP0'	=>array( 'dsn'=>'mysql:host=127.0.0.1;dbname=devV15_camp','user'=>'root', 'pass'=>'r123'),	// campaigns
	'SESS'	=>array( 'dsn'=>'mysql:host=127.0.0.1;dbname=devV15_main','user'=>'root', 'pass'=>'r123'),	// session
	'LOG'	=>array( 'dsn'=>'mysql:host=127.0.0.1;dbname=devV15_log', 'user'=>'root', 'pass'=>'r123'),	// log
);

// file location of the site
$__DP = DOCUMENT_ROOT;
$__site = 'ffefwrfwrfwfef3'; // cache key

// file upload targets
$GLBS['storageServer'] = array(
	'default'=>array(
		'p01'=>array(
			'host'	=> SERVER_NAME,
			'path'	=> $__DP.'files/prod/',
			'dpath'	=> 'files/prod/',
			'dsn'	=> 'MDB',
			'size'	=> 0
		),
	),
	'campaignUserYoutube-youtubeVideo'=>array(
		'p01'=>array(
			'host'	=> SERVER_NAME,
			'path'	=> $__DP.'files/video/youtube/',
			'dpath'	=> 'files/video/youtube/',
			'dsn'	=> 'MDB',
			'size'	=> 0
		),
	),
	'campaignUserYoutube-previewVideo'=>array(
		'p01'=>array(
			'host'	=> SERVER_NAME,
			'path'	=> $__DP.'files/video/preview/',
			'dpath'	=> 'files/video/preview/',
			'dsn'	=> 'MDB',
			'size'	=> 0
		),
	)
);

/*
 * RabbitMQ Configuration
 */
define('RMQ_HOST', 'localhost');
define('RMQ_PORT', 5672);
define('RMQ_USER', 'guest');
define('RMQ_PASS', 'guest');
define('RMQ_VHOST', '/');
define('RMQ_EXCHANGE', '');
define('RMQ_QUEUE_EMAIL', 'admingle_cron_jobs_email');
define('RMQ_QUEUE_TWITTER', 'admingle_cron_jobs_twitter');
define('RMQ_QUEUE_UPPOOL_TWITTER', 'admingle_cron_update_twitter');
define('RMQ_QUEUE_PUSH', 'admingle_cron_push');
define('RMQ_QUEUE_GET_TWIT', 'admingle_cron_get_twit');

/**
 * Memcache Configuration
 */
define('MEMCACHE_SERVER', 'localhost');
define('MEMCACHE_PORT', '11211');

$_newPanel  = "";

require_once $__DP.'site/def/localConfig.php';

define("NEW_PANEL", true);