<?php
/************************************
 *
 *    required form fields file
 *    özgür köy
 *    0 -> error message
 *    1 -> function to run error check ( defaults to matchEmpty )
 *    2 -> parameters, seperated with commas
 *
 *    multiple operations for some field can be added like
 *    password
 *    password@
 *    password@@
 *
 *    TODO ::: CATCH ERRORS AND SHOW EM AT CLIENT  SIDE ,AUTOMATICALLY
 *
 ************************************/
$GLBS[ "formFields" ] = array(
	/*registration form*/
	"registration"   => array(
		"email"       => array( "E-posta adresinizi belirtiniz." ),
		"email@"      => array( "Geçerli bir e-posta adresi yazınız.", "matchEmail" ),
		"password"    => array( "Şifrenizi yazınız." ),
		"password@"   => array( "Şifreler aynı değil. Kontrol ediniz.", "matchEqual", "password2" ),
		"password@@"  => array( "Şifreniz en az 6 karakterden oluşmalıdır.", "matchPassword" ),
		"password@@@" => array( "Şifreniz en fazla 15 karakterden oluşmalıdır.", "matchPasswordBig" ),
		"yob"         => array( "Doğum yılınızı seçiniz.", "matchBetween", "1910,2000" ),
		"mob"         => array( "Doğum ayınızı seçiniz.", "matchBetween", "1,12" ),
		"dob"         => array( "Doğum gününüzü seçiniz.", "matchBetween", "1,31" ),
		"country"     => array( "Ülke seçiniz." ),
		"city"        => array( "Şehir seçiniz.", "matchIfValueMatches", "country,1" ),
		"gender"      => array( "Cinsiyetinizi belirtiniz." ),
		"kepica"      => array( "Güvenlik kelimesini belirtiniz." ),
		"readSS"      => array( "Kullanım sözleşmesini okumalı ve kabul etmelisiniz." ),
	),
	"login"          => array(
		"username"  => array( "Enter username" ),
		"password"  => array( "Enter password" ),
		"password@" => array( "Login&Pass Wrong", "checkLogin" )
	),
	"action"         => array(
		"label" => array( "Enter label" ),
		"tasks" => array( "Enter one task per line")
	),
	"site"           => array(
		"label" => array( "Enter label" ),
		"url"   => array( "Enter url" )
	),
	"user"           => array(
		"username"  => array( "Enter username" ),
		"username@" => array( "Enter different username", "checkExistingUsername" ),
		"email"     => array( "Enter email" ),
		"email@"    => array( "Enter email", "matchEmail" ),
		"password"  => array( "Must type password for a new user", "checkEmptyPassForNewUser" ),
	),
	"role"           => array(
		"label" => array( "Enter label" )
	),
	"wordcat"        => array(
		"name" => array( "Enter name" )
	),
	"wordtype"       => array(
		"name" => array( "Enter name" )
	),
	"word"           => array(
		"label"           => array( "Enter label" ),
		"label@"          => array( "Choose a different name", "checkWordLabelUniqueness" ),
		"label@@"         => array( "No spaces in label are allowed", "matchWhiteSpace" ),
		"type"            => array( "Choose a type" ),
		"footerLink"      => array( "Invalid footer link, select from autocomplete box, or leave blank", "checkFooterLink" ),
		"titleParameters" => array( "Enter title parameters for this word type", "checkTitleParams" ),
		"bodyParameters"  => array( "Enter body parameters for this word type", "checkBodyParams" ),
	),
	"wordLangAction" => array(
		"lang"         => array( "Choose a language for definition" ),
		"word_content" => array( "You must use required parameters, ONLY ONCE", "checkContent" ),
		"word_body"    => array( "You must use required parameters, ONLY ONCE", "checkBody" )
	),
	"wordEntryEdit"  => array(
		"word_content" => array( "You must use required parameters, ONLY ONCE", "checkContent" ),
		"word_body"    => array( "You must use required parameters, ONLY ONCE", "checkBody" )
	),
	"sector"       => array(
		"name"    => array( "Sektor adini belirtiniz" ),
	),
	"customer"       => array(
		"name"     => array( "Firma adini belirtiniz" ),
		"city"     => array( "Sehir seciniz" ),
		"country" => array( "Ulke seciniz" ),
	),
	"contact"       => array(
		"name"     => array( "Kontak adini belirtiniz" ),
		"surname"     => array( "Kontak soyadini belirtiniz" ),
	),
	"product"       => array(
		"name"     => array( "Urun adi belirtiniz" ),
		"stockCode"     => array( "Urun stok kodu belirtiniz" ),
		"firstUnitCurrency"=>array( "Birim para birimi belirtiniz" ),

	),
	"stockAction"       => array(
		"amount"     => array( "Miktar belirtiniz" ),
		"opDate"     => array( "Tarih belirtiniz" ),
		"direction"=>array( "Hareket yonu belirtiniz" ),

	),
	"activity"       => array(
		"customer"     => array( "Kontak secmelisiniz" ),
		"date"     => array( "Tarih belirtiniz" ),
		"type"=>array( "Aktivite turunu belirtiniz" ),

	),
);

?>