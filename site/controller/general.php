<?php

class General_controller{
	public function languages() {
		if ( !isset( $_SESSION[ "userId" ] ) )
			JUtil::jredirect( "http://".SERVER_NAME."/home" );

		$a = new Lang();
		$a->load();

		JT::init();
		JT::assign( "langs", $a );
		echo JT::pfetch( "languages" );
	}

	public function addLanguage($id=null) {
		$a = new Lang( $id );
		$l = new Lang();
		$l->load( array( "isActive" => 1 ) );

		JT::init();
			JT::assign( "language", $a );
		JT::assign( "langs", $l );

		echo JT::pfetch( "languageForm" );
	}

	public function languageAction() {
		JFORM::formResponse( "language" );
		$a = new Lang( isset( $_POST[ "id" ] ) ? $_POST[ "id" ] : null );

		$a->label     = $_POST[ "label" ];
		$a->short     = $_POST[ "short" ];
		$a->shortDef  = $_POST[ "shortDef" ];
		$a->alignment = $_POST[ "alignment" ];
		$a->isActive  = isset( $_POST[ "isActive" ] ) ? $_POST[ "isActive" ] : 0;
//		$a->defaultEnabled = isset($_POST[ "defaultEnabled" ])?$_POST[ "defaultEnabled" ]:0;
		$a->save();

		if ( isset( $_POST[ "cloner" ] ) && $a->gotValue !== true ) {
			$a->cloneFromAnother( $_POST[ "cloner" ], $_SESSION[ "userId" ], $_POST[ "cloneCats" ] );
		}

		unset( $a );
		JUtil::jredirect( "http://".SERVER_NAME."/languages" );
	}

	public function delLanguage( $id ) {
		$a = new Lang( $id );
//		$a->active = 0;
		$a->delete();

		unset( $a );
		JUtil::jredirect( "http://".SERVER_NAME."/languages" );
	}

	/*currencies*/
	public function currencies() {
		if ( !isset( $_SESSION[ "userId" ] ) )
			JUtil::jredirect( "http://".SERVER_NAME."/home" );

		$a = new Currency();
		$a->load();

		JT::init();
		JT::assign( "currencies", $a );
		echo JT::pfetch( "currencies" );
	}

	public function addCurrency( $id = null ) {
		$a = new Currency( $id );

		JT::init();
		JT::assign( "currency", $a );

		echo JT::pfetch( "currencyForm" );
	}

	public function currencyAction() {
		JFORM::formResponse( "currency" );

		$a                = new Currency( isset( $_POST[ "id" ] ) ? $_POST[ "id" ] : null );

		foreach ( $a->_fields as $field ) {
			if ( isset( $_POST[ $field ] ) && in_array( $field, $a->_relatives ) === false )
				$a->$field = $_POST[ $field ];
		}

		$a->save();

		unset( $a );
		JUtil::jredirect( "http://".SERVER_NAME."/currencies" );
	}

	public function delCurrency( $id ) {
		$a = new Currency( $id );
//		$a->active = 0;
		$a->delete();

		unset( $a );
		JUtil::jredirect( "http://".SERVER_NAME."/currencies" );
	}

	public function logout(){
		session_destroy();

		JUtil::jredirect( "http://".SERVER_NAME."/" );
	}
	/*xhr*/
	public function xhr( $type, $query = null, $extra = null ) {
		switch ( $type ) {
			case "contacts":
				$w = new Contact();
				$w->nopop()->load( array("contacts"=>$query) );
				$ret = array();

				while ( $w->populate() ) {
					$ret[$w->id] = $w->name." ".$w->surname;
				}
				echo json_encode( $ret );
				break;
			case "productInfo":
				$p = new Product( $query );
				$ret = array();
				foreach ( $p->_fields as $f ) {
					$ret[ $f ] = $p->$f;
				}

				echo json_encode( $ret );

				break;
			case "wordList":
				/*
				 * Array
(
    [sEcho] => 1
    [iColumns] => 4
    [sColumns] =>
    [iDisplayStart] => 0
    [iDisplayLength] => 5
    [mDataProp_0] => 0
    [mDataProp_1] => 1
    [mDataProp_2] => 2
    [mDataProp_3] => 3
    [sSearch] =>
    [bRegex] => false
    [sSearch_0] =>
    [bRegex_0] => false
    [bSearchable_0] => true
    [sSearch_1] =>
    [bRegex_1] => false
    [bSearchable_1] => true
    [sSearch_2] =>
    [bRegex_2] => false
    [bSearchable_2] => true
    [sSearch_3] =>
    [bRegex_3] => false
    [bSearchable_3] => true
    [iSortCol_0] => 0
    [sSortDir_0] => asc
    [iSortingCols] => 1
    [bSortable_0] => false
    [bSortable_1] => true
    [bSortable_2] => true
    [bSortable_3] => true
    [_] => 1382530015448*/
				$sortings = array( "label", "type", "_category" );
				$s        = JUtil::getSearchParameter();
				$w        = new Word();
				if ( strlen( $s[ "sSearch" ] ) )
					$filter = array( "label" => "LIKE " . $s[ "sSearch" ] . "%" );
				else
					$filter = array();

				if(isset($s["sSearch_0"]) && strlen($s["sSearch_0"])>0)
					$filter["label"] = "LIKE ".$s["sSearch_0"]."%";

				if(isset($s["sSearch_1"]) && strlen($s["sSearch_1"])>0)
					$filter["type"] = array("name"=>"LIKE ".$s["sSearch_1"]."%");

				if(isset($s["sSearch_2"]) && strlen($s["sSearch_2"])>0)
					$filter["categories"] = array("name"=>"LIKE ".$s["sSearch_2"]."%");


				$w->count( "id", "cid", $filter );
				$count = $w->gotValue ? $w->cid : 0;


				$w->orderBy( $sortings[ $s[ "iSortCol_0" ] ] . " " . $s[ "sSortDir_0" ] )->limit( $s[ "iDisplayStart" ], $s[ "iDisplayLength" ] )->load( $filter );
				$response = array(
					"sEcho"                => $s[ "sEcho" ],
					"iTotalRecords"        => $count,
					"iTotalDisplayRecords" => $count,
					"aaData"               => array()

				);
				if ( $w->gotValue ) {
					do {
						$w->listCategories();
						$w->loadType();

						$response[ "aaData" ][ ] = array( $w->label, $w->type->name, $w->catsListed, null, "rowId"=>$w->id  );
					} while ( $w->populate() );

				}

				echo json_encode( $response );

				break;
			case "langCats":
				$cat = new WordCategory();
				$cat->load( array( "catLangs" => $query ) );
				$ret = array();
				if ( $cat->gotValue ) {
					do {
						$ret[ $cat->id ] = $cat->name;
					} while ( $cat->populate() );
				}
				echo json_encode( $ret );
				break;
		}
	}

	public function backend( $method, $id = -1, $idAlt = null ) {
		switch ( $method ) {
			case "productStocks":
				$stock = new StockMovement();
				$stock->load( array( "product" => $id ) );
				$product = new Product( $id );
				JT::init();
				JT::assign( "stocks", $stock );
				JT::assign( "today", date( "d-m-Y", time() ) );
				JT::assign( "directions", array( 1 => "Iceri", -1 => "Disari" ) );

				JT::assign( "product", $product );

				echo JT::pfetch( "productStocks" );
				break;
			case "stockAction":
				JFORM::formResponse( "stockAction" );

				$p           = new Product( $_POST[ "productId" ] );
				$cs          = $p->getCurrentStock();
				$s           = new StockMovement();
				$s->amount   = intval( $_POST[ "amount" ] ) * ( isset( $_POST[ "direction" ] ) ? intval( $_POST[ "direction" ] ) : 1 );
				$s->opDate   = JUtil::reverseDate( $_POST[ "opDate" ] );
				$s->product  = $_POST[ "productId" ];
				$s->operator = $_SESSION[ "userId" ];
				$s->beforeOp = $cs;
				$s->info     = $_POST[ "info" ];

				$s->afterOp = $cs + $s->amount;
				$s->save();

				JUtil::customCScript( "message", array( "good", "Yeni stok girisi yapildi" ), false );
				JUtil::customCScript( "wordTab", array( $_POST[ "productId" ] ), true, 2000 );

				break;
		}
	}
}