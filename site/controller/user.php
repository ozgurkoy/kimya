<?php
class User_controller
{
	public function checkLogin() {
//		if ( isset( $_SESSION[ "userId" ] ) )
//			JUtil::jredirect( "http://".SERVER_NAME."/home" );

		$u = new User();
		JFORM::formResponse( "login", $u );

		if ( $u->gotValue ) {
			$_SESSION[ "permissions" ] = $u->loadPerms();
			$_SESSION[ "userId" ]      = $u->id;
			$_SESSION[ "authLevel" ]   = $u->authLevel;
			$_SESSION[ "username" ]    = $u->username;
			$_SESSION[ "isRoot" ]      = $u->isRoot;


			JUtil::jredirect( "http://".SERVER_NAME."/home" );
		}
		else {
			JUtil::jredirect( "http://".SERVER_NAME."/" );
		}
	}

	public function actions() {
		if ( !isset( $_SESSION[ "userId" ] ) )
			JUtil::jredirect( "http://".SERVER_NAME."/home" );

		$a = new Action();
		$a->load();

		JT::init();
		JT::assign( "actions", $a );
		echo JT::pfetch( "actions" );
	}

	public function addAction( $id = null ) {
		$a = new Action( $id );

		JT::init();
		JT::assign( "action", $a );
		echo JT::pfetch( "actionForm" );
	}

	public function actionAction() {
		JFORM::formResponse( "action" );

		$a        = new Action( isset( $_POST[ "id" ] ) ? $_POST[ "id" ] : null );
		$a->label = $_POST[ "label" ];
		$a->tasks = $_POST[ "tasks" ];
		$a->save();
		unset( $a );
		JUtil::jredirect( "http://".SERVER_NAME."/actions" );
	}

	public function delAction( $id ) {
		$a = new Action( $id );
		$a->delete();

		unset( $a );
		JUtil::jredirect( "http://".SERVER_NAME."/actions" );
	}

	/*roles*/
	public function roles() {
		if ( !isset( $_SESSION[ "userId" ] ) )
			JUtil::jredirect( "http://".SERVER_NAME."/home" );

		$a = new Role();
		$a->load();

		JT::init();
		JT::assign( "roles", $a );

		echo JT::pfetch( "roles" );
	}

	public function addRole( $id = null ) {
		$a = new Role( $id );

		JT::init();
		JT::assign( "role", $a );
		echo JT::pfetch( "roleForm" );
	}

	public function roleAction() {
		JFORM::formResponse( "role" );

		$a                 = new Role( isset( $_POST[ "id" ] ) ? $_POST[ "id" ] : null );
		$a->label          = $_POST[ "label" ];
		$a->short          = $_POST[ "short" ];
		$a->isActive       = isset( $_POST[ "isActive" ] ) ? $_POST[ "isActive" ] : 0;
		$a->defaultEnabled = isset( $_POST[ "defaultEnabled" ] ) ? $_POST[ "defaultEnabled" ] : 0;
		$a->authLevel      = $_POST[ "authLevel" ];
		$a->save();

		unset( $a );
		JUtil::jredirect( "http://".SERVER_NAME."/roles" );
	}

	public function delRole( $id ) {
		$a = new Role( $id );
		$a->delete();
//		$a->active = 0;

		unset( $a );
		JUtil::jredirect( "http://".SERVER_NAME."/roles" );
	}

	public function roleMod( $id ) {
		$r = new Role( $id );
		$a = new Action();
		$a->load();

		JT::init();
		JT::assign( "actions", $a );
		JT::assign( "role", $r );

		echo JT::pfetch( "roleMod" );
	}

	public function roleModAction() {
		$r = new Role( $_POST[ "id" ] );
		$a = new Action();
		$a->load();
		if ( $r->gotValue ) {
			$r->loadPermissions();
		}
		if ( $a->gotValue ) {
			do {
				if ( isset( $_POST[ "d_" . $a->id ] ) ) {
					$p = new Permission();
					$p->load( array( "action" => $a->id, "permissions" => $r->id ) );

					$t0 = 0;
					foreach ( $_POST[ "d_" . $a->id ] as $bit ) {
						$t0 = $t0 | $bit;
					}
					$p->bits = $t0;
					$p->save();
					$p->saveAction( $a->id );

					$r->savePermissions( $p->id );
				}
			} while ( $a->populate() );
		}

		JUtil::jredirect( "http://".SERVER_NAME."/roles" );

//		print_r( $_POST );
	}

	/*users*/
	public function users() {
		if ( !isset( $_SESSION[ "userId" ] ) )
			JUtil::jredirect( "http://".SERVER_NAME."/home" );

		$a = new User();
		$a->load();

		JT::init();
		JT::assign( "users", $a );
		echo JT::pfetch( "users" );
	}

	public function addUser( $id = null ) {
		$a = new User( $id );
		$a->loadSite();
		$a->loadRoles();

		$filter = $a->roles && $a->roles->gotValue ? array( "id" => "NOT IN " . join( ",", $a->roles->_ids ) ) : null;

		$l = new Role();
		$l->load( $filter );

		$s = new Site();
		$s->load();

		JT::init();
		JT::assign( "roles", $l );
		JT::assign( "sites", $s );
		JT::assign( "user", $a );
		echo JT::pfetch( "userForm" );
	}

	public function userAction() {
		$a = new User( isset( $_POST[ "id" ] ) ? $_POST[ "id" ] : null );
		JFORM::formResponse( "user", $a );

		$a->username  = $_POST[ "username" ];
		$a->email     = $_POST[ "email" ];
		$a->isRoot    = isset( $_POST[ "isRoot" ] ) ? $_POST[ "isRoot" ] : 0;
		$a->isActive  = isset( $_POST[ "isActive" ] ) ? $_POST[ "isActive" ] : 0;
		$a->authLevel = $_POST[ "authLevel" ];

		if ( isset( $_POST[ "password" ] ) && strlen( $_POST[ "password" ] ) > 0 )
			$a->password = md5( $_POST[ "password" ] );

		$a->save();

		if ( isset( $_POST[ "site" ] ) && $_POST[ "site" ] > 0 )
			$a->saveSite( $_POST[ "site" ] );
		else
			$a->deleteSite();

		if ( $a->gotValue )
			$a->deleteAllRoles();

		$a->saveRoles( $_POST[ "roles" ] );

		unset( $a );
		JUtil::jredirect( "http://".SERVER_NAME."/users" );
	}

	public function delUser( $id ) {
		$a = new User( $id>1?$id:null );
//		$a->active = 0;
		$a->delete();

		unset( $a );
		JUtil::jredirect( "http://".SERVER_NAME."/users" );
	}

	/*activities*/
	public function activities() {
		if ( !isset( $_SESSION[ "userId" ] ) )
			JUtil::jredirect( "http://".SERVER_NAME."/home" );

		$a = new Activity();
		$a->load();

		JT::init();
		JT::assign("actTypes", array("meeting"=>"Toplanti","visit"=>"Ziyaret","phone"=>"Telefon"));

		JT::assign( "activities", $a );
		echo JT::pfetch( "activities" );
	}

	public function addActivity( $id = null ) {
		$a = new Activity( $id );
		if($a->gotValue)
			$a->date = JUtil::reverseDate( $a->date );
		$c = new Customer();
		$c->load();

		JT::init();
		JT::assign("actTypes", array("meeting"=>"Toplanti","visit"=>"Ziyaret","phone"=>"Telefon"));
		JT::assign( "customers", $c );
		JT::assign( "activity", $a );

		echo JT::pfetch( "activityForm" );
	}

	public function activityAction() {
		$a = new Activity( isset( $_POST[ "id" ] ) ? $_POST[ "id" ] : null );
		JFORM::formResponse( "activity", $a );

		$a->info            = $_POST[ "info" ];
		$a->type            = $_POST[ "type" ];
		$a->activityContact = $_POST[ "activityContact" ];
		$a->date            = JUtil::reverseDate( $_POST[ "date" ] );
		$a->hourStart       = $_POST[ "hourStart" ];
		$a->hourEnd         = $_POST[ "hourEnd" ];
		$a->place           = $_POST[ "place" ];
		$a->save();

		$a->saveUser( $_SESSION[ "userId" ] );

		unset( $a );
		JUtil::jredirect( "http://".SERVER_NAME."/activities" );
	}

	public function delActivity( $id ) {
		$a = new Activity( $id );
//		$a->active = 0;
		$a->delete();

		unset( $a );
		JUtil::jredirect( "http://".SERVER_NAME."/activities" );
	}


}
?>