<?php
class Home_controller
{
	public function display() {
		if ( !isset( $_SESSION[ "userId" ] ) )
			echo JT::pfetch( "login" );
		else
			JUtil::jredirect( "http://".SERVER_NAME."/home" );
	}

	public function home() {
		if ( !isset( $_SESSION[ "userId" ] ) )
			JUtil::jredirect( "http://".SERVER_NAME."/" );

		//print_r( $_SESSION );exit;

		echo JT::pfetch( "home" );
	}
}
?>