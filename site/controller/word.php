<?php

class Word_controller{
	/*cats*/
	public function wordcats() {
		if ( !isset( $_SESSION[ "userId" ] ) )
			JUtil::jredirect( "http://".SERVER_NAME."/home" );

		$a = new WordCategory();
		$a->load();

		JT::init();
		JT::assign( "wordcats", $a );
		echo JT::pfetch( "wordcats" );
	}

	public function addWordcat($id=null) {
		$a = new WordCategory( $id );
		$a->loadCatLangs();

		$filter = $a->catLangs && $a->catLangs->gotValue ? array( "isActive" => 1, "id" => "NOT IN " . join( ",", $a->catLangs->_ids ) ) : null;

		$l = new Lang();
		$l->load( $filter );

		JT::init();
		JT::assign( "wordcat", $a );
		JT::assign( "langs", $l );
		echo JT::pfetch( "wordcatForm" );
	}

	public function wordcatAction() {
		JFORM::formResponse( "wordcat" );

		$a = new WordCategory(isset($_POST["id"])?$_POST["id"]:null);
		$a->name = $_POST[ "name" ];
		$a->isActive = isset($_POST[ "isActive" ])?$_POST[ "isActive" ]:0;
		$a->save();

		if($a->gotValue)
			$a->deleteAllCatLangs();

		$a->saveCatLangs( $_POST[ "langs" ] );

		unset( $a );
		JUtil::jredirect( "http://".SERVER_NAME."/wordcats" );
	}

	public function delWordcat( $id ) {
		$a = new WordCategory( $id );
//		$a->active = 0;
		$a->delete();

		unset( $a );
		JUtil::jredirect( "http://".SERVER_NAME."/wordcats" );
	}

	/*types*/
	public function wordtypes() {
		if ( !isset( $_SESSION[ "userId" ] ) )
			JUtil::jredirect( "http://".SERVER_NAME."/home" );

		$a = new WordType();
		$a->load();

		JT::init();
		JT::assign( "wordtypes", $a );
		echo JT::pfetch( "wordtypes" );
	}

	public function addWordtype($id=null) {
		$a = new WordType( $id );

		JT::init();
		JT::assign( "wordtype", $a );
		echo JT::pfetch( "wordtypeForm" );
	}

	public function wordtypeAction() {
		JFORM::formResponse( "wordtype" );

		$a                     = new Wordtype( isset( $_POST[ "id" ] ) ? $_POST[ "id" ] : null );
		$a->name               = $_POST[ "name" ];
		$a->isActive           = isset( $_POST[ "isActive" ] ) ? $_POST[ "isActive" ] : 0;
		$a->requiresBody       = isset( $_POST[ "requiresBody" ] ) ? $_POST[ "requiresBody" ] : 0;
		$a->requiresParameters = isset( $_POST[ "requiresParameters" ] ) ? $_POST[ "requiresParameters" ] : 0;
		$a->save();

		unset( $a );
		JUtil::jredirect( "http://".SERVER_NAME."/wordtypes" );
	}

	public function delWordtype( $id ) {
		$a = new Wordtype( $id );
//		$a->active = 0;
		$a->delete();

		unset( $a );
		JUtil::jredirect( "http://".SERVER_NAME."/wordtypes" );
	}
	/*words*/

	public function wordEntryVersion( $id) {
		$w = new WordEntryState( $id );
		$w->loadStLang();
		$w->loadAddedBy();

		JT::init();
		JT::assign( "entry", $w );
		echo JT::pfetch( "wordEntryVersion" );
	}

	public function wordEntryVersionEdit( $id) {
		$wl = new WordEntry( $id );

		$w = $wl->loadStates(array("state"=>"active"));

		if ( $w->gotValue === false )
			$w = $wl->loadStates(array("state"=>"candidate"),0,1,"id DESC");

		$w->loadWordEntry()->loadWord()->loadType();

		$w->loadStLang();
		$w->loadAddedBy();

		JT::init();
		JT::assign( "entry", $w );
		echo JT::pfetch( "wordEntryEdit" );
	}

	public function wordEntryVersionAction() {
		JFORM::formResponse( "wordEntryEdit" );

		$w  = new WordEntryState( $_POST[ "id" ] );
		$we = $w->loadWordEntry();

		$we->loadWord();
		$newState          = $w->cloneToNew( $_SESSION[ "isRoot" ] );
		$newState->body    = $_POST[ "word_body" ];
		$newState->content = $_POST[ "word_content" ];
		$newState->addedBy = $_SESSION[ "userId" ];


		$oldStateId = $newState->archive( $we, $_SESSION[ "userId" ] );
		$newState->save();

		$we->loadLangWord();

		$we->status = 'needApproval';
		$we->save();

		$we->record( $_SESSION[ "userId" ], "Edited old entry", $w->id, $oldStateId );
		User::record( $_SESSION[ "userId" ], "Edited old entry :" . $_POST[ "id" ] );

		JUtil::customCScript( "message", array( "good", "Lang version added" ), false );
		JUtil::customCScript( "wordTab", array( $we->entries->id, $we->langWord->id ), true, 2000 );

	}

	public function wordChangeVersion( $id ) {
		$w        = new WordEntryState( $id );
		$we       = $w->loadWordEntry();
		$we->loadWord();
		$newState = $w->cloneToNew( $_SESSION[ "isRoot" ] );

		$oldStateId = $newState->archive( $we, $_SESSION[ "userId" ] );
		$newState->save();

		$we->loadLangWord();

		$we->status = 'needApproval';
		$we->save();

		$we->record( $_SESSION[ "userId" ], "Resurrected old entry", $w->id, $oldStateId );
		User::record( $_SESSION[ "userId" ], "Resurrected old entry :" . $id );

		JUtil::customCScript( "message", array( "good", "Lang version switched" ), false );
		JUtil::customCScript( "wordTab", array( $we->entries->id, $we->langWord->id ), true, 2000 );

	}

	public function wordLang( $id, $lang=0 ) {
		if ( !isset( $_SESSION[ "userId" ] ) )
			JUtil::jredirect( "http://".SERVER_NAME."/home" );

		$a = new Word($id);
		$a->loadType();
		$a->loadCategories();
		$a->loadEntries();

		$l1 = new Lang();
		$l1->load( array("langWord"=>array("entries"=>$id)) );

		$loadeds = $l1->gotValue ? $l1->_ids : array();

		$l1 = new Lang();
		$l1->load( array("catLangs"=>array("categories"=>array("id"=>$id))) );
		$loadeds2 = $l1->gotValue ? $l1->_ids : array();

		$loadeds = array_diff( $loadeds2, $loadeds );

//		$loadeds = $loadeds + $loadeds2;
		if(sizeof($loadeds)>0){
			$l = new Lang();
			$l->load( array( "isActive" => 1, "id"=>$loadeds ) );
		}
		else
			$l = null;

		JT::init();
		JT::assign( "word", $a );
		JT::assign( "lang", $lang );
		JT::assign( "langs", $l );
		echo JT::pfetch( "wordLang" );
	}

	public function wordLangAction() {

		if ( !isset( $_SESSION[ "userId" ] ) )
			JUtil::jredirect( "http://".SERVER_NAME."/home" );

		JFORM::formResponse( "wordLangAction" );
//		JPDO::beginTransaction();
//		JPDO::rollback();exit;

		$w = new Word( $_POST[ "wordId" ] );
		$w->loadType();
		$we = $w->loadEntries( array( "langWord" => $_POST[ "lang" ] ) );
		if ( !( $we && $we->gotValue ) ) {
			$we           = new WordEntry();
			$we->langWord = $_POST[ "lang" ];
			$we->adder    = $_SESSION[ "userId" ];
			$we->save();
			$we->saveWord( $w );
			User::record( $_SESSION[ "userId" ], "added word entry. Lang:" . $_POST[ "lang" ] . " Entry:" . serialize( $_POST ) );

		}

		//creating new word entry
		$state            = new WordEntryState();
		$state->authLevel = $_SESSION[ "authLevel" ];
		$state->content   = $_POST[ "word_content" ];
		$state->stLang    = $_POST[ "lang" ];
		$state->addedBy   = $_SESSION[ "userId" ];
		$state->state     = $_SESSION[ "isRoot" ] == 1 ? "active" : "candidate";
		$state->states    = $we->id;
		if ( $w->type->requiresBody )
			$state->body = $_POST[ "word_body" ];


		$oldStateId = $state->archive( $we, $_SESSION[ "userId" ] );
		$state->save();

		$we->status = 'needApproval';
		$we->save();

//		JPDO::commit();
		/*todo : add to batch here?*/
		$we->record( $_SESSION[ "userId" ], "updated entry", $state->id, $oldStateId );
		JUtil::customCScript( "message", array( "good", "Lang added" ), false );
		JUtil::customCScript( "wordTab", array( $_POST["wordId"], $_POST["lang"] ), true, 2000 );

	}

	public function words($word=null) {
		if ( !isset( $_SESSION[ "userId" ] ) )
			JUtil::jredirect( "http://".SERVER_NAME."/home" );

//		$a = new Word();
//		$a->limit( $page * 3, 3 )->orderBy( "label asc" )->load();
		JT::init();
		JT::assign( "word", $word );
		echo JT::pfetch( "words" );
	}

	public function addWord( $id = null ) {
		$a = new Word( $id );
		$a->loadWord();

		$a->loadCategories();
		$a->loadType();

		$w           = new WordType();
		$w->isActive = 1;
		$w->load();

		$wc     = new WordCategory();
		$filter = $a->categories && $a->categories->gotValue ? array( "isActive" => 1, "id" => "NOT IN " . join( ",", $a->categories->_ids ) ) : null;

		$wc->load( $filter );

		JT::init();
		JT::assign( "word", $a );
		JT::assign( "types", $w );
		JT::assign( "cats", $wc );
		echo JT::pfetch( "wordForm" );
	}

	public function wordAction() {
		JFORM::formResponse( "word" );

		$a                  = new Word( isset( $_POST[ "id" ] ) ? $_POST[ "id" ] : null );
		$a->label           = $_POST[ "label" ];
		$a->type            = $_POST[ "type" ];
		$a->titleParameters = $_POST[ "titleParameters" ];
		$a->bodyParameters  = $_POST[ "bodyParameters" ];
		$a->titleTemplate   = $_POST[ "titleTemplate" ];
		$a->bodyTemplate    = $_POST[ "bodyTemplate" ];
		$a->sendDashboard   = isset( $_POST[ "sendDashboard" ] ) ? $_POST[ "sendDashboard" ] : 0;
		$a->sendMail        = isset( $_POST[ "sendMail" ] ) ? $_POST[ "sendMail" ] : 0;
		$a->sendPush        = isset( $_POST[ "sendPush" ] ) ? $_POST[ "sendPush" ] : 0;

		if ( !empty( $_POST[ "footerLink" ] ) ) {
			$wa = new Word();
			$wa->load( array( "label" => $_POST[ "footerLink" ] ) );
			if ( $wa->gotValue )
				$a->footerLink = $wa->id;

			unset( $wa );
		}

		$a->save();

		if ( $a->gotValue )
			$a->deleteAllCategories();

		$a->saveCategories( $_POST[ "cats" ] );


		unset( $a );
		JUtil::jredirect( "http://".SERVER_NAME."/words" );
	}

	public function delWord( $id ) {
		$a = new Word( $id );
//		$a->active = 0;
		$a->delete();

		unset( $a );
		JUtil::jredirect( "http://".SERVER_NAME."/words" );
	}
}
