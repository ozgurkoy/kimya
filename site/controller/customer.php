<?php
class Customer_controller
{
	public function checkLogin() {
		if ( isset( $_SESSION[ "userId" ] ) )
			JUtil::jredirect( "http://".SERVER_NAME."/home" );

		$u = new Customer();
		JFORM::formResponse( "login", $u );

		if ( $u->gotValue ) {
			$_SESSION[ "permissions" ] = $u->loadPerms();
			$_SESSION[ "userId" ]      = $u->id;
			$_SESSION[ "authLevel" ]   = $u->authLevel;
			$_SESSION[ "customername" ]    = $u->customername;
			$_SESSION[ "isRoot" ]      = $u->isRoot;


			JUtil::jredirect( "http://".SERVER_NAME."/home" );
		}
		else {
			JUtil::jredirect( "http://".SERVER_NAME."/" );
		}
	}

	public function actions() {
		if ( !isset( $_SESSION[ "userId" ] ) )
			JUtil::jredirect( "http://".SERVER_NAME."/home" );

		$a = new Action();
		$a->load();

		JT::init();
		JT::assign( "actions", $a );
		echo JT::pfetch( "actions" );
	}

	public function addAction( $id = null ) {
		$a = new Action( $id );

		JT::init();
		JT::assign( "action", $a );
		echo JT::pfetch( "actionForm" );
	}

	public function actionAction() {
		JFORM::formResponse( "action" );

		$a        = new Action( isset( $_POST[ "id" ] ) ? $_POST[ "id" ] : null );
		$a->label = $_POST[ "label" ];
		$a->tasks = $_POST[ "tasks" ];
		$a->save();
		unset( $a );
		JUtil::jredirect( "http://".SERVER_NAME."/actions" );
	}

	public function delAction( $id ) {
		$a = new Action( $id );
		$a->delete();

		unset( $a );
		JUtil::jredirect( "http://".SERVER_NAME."/actions" );
	}

	/*roles*/
	public function roles() {
		if ( !isset( $_SESSION[ "userId" ] ) )
			JUtil::jredirect( "http://".SERVER_NAME."/home" );

		$a = new Role();
		$a->load();

		JT::init();
		JT::assign( "roles", $a );

		echo JT::pfetch( "roles" );
	}

	public function addRole( $id = null ) {
		$a = new Role( $id );

		JT::init();
		JT::assign( "role", $a );
		echo JT::pfetch( "roleForm" );
	}

	public function roleAction() {
		JFORM::formResponse( "role" );

		$a                 = new Role( isset( $_POST[ "id" ] ) ? $_POST[ "id" ] : null );
		$a->label          = $_POST[ "label" ];
		$a->short          = $_POST[ "short" ];
		$a->isActive       = isset( $_POST[ "isActive" ] ) ? $_POST[ "isActive" ] : 0;
		$a->defaultEnabled = isset( $_POST[ "defaultEnabled" ] ) ? $_POST[ "defaultEnabled" ] : 0;
		$a->authLevel      = $_POST[ "authLevel" ];
		$a->save();

		unset( $a );
		JUtil::jredirect( "http://".SERVER_NAME."/roles" );
	}

	public function delRole( $id ) {
		$a = new Role( $id );
		$a->delete();
//		$a->active = 0;

		unset( $a );
		JUtil::jredirect( "http://".SERVER_NAME."/roles" );
	}

	public function roleMod( $id ) {
		$r = new Role( $id );
		$a = new Action();
		$a->load();

		JT::init();
		JT::assign( "actions", $a );
		JT::assign( "role", $r );

		echo JT::pfetch( "roleMod" );
	}

	public function roleModAction() {
		$r = new Role( $_POST[ "id" ] );
		$a = new Action();
		$a->load();
		if ( $r->gotValue ) {
			$r->loadPermissions();
		}
		if ( $a->gotValue ) {
			do {
				if ( isset( $_POST[ "d_" . $a->id ] ) ) {
					$p = new Permission();
					$p->load( array( "action" => $a->id, "permissions" => $r->id ) );

					$t0 = 0;
					foreach ( $_POST[ "d_" . $a->id ] as $bit ) {
						$t0 = $t0 | $bit;
					}
					$p->bits = $t0;
					$p->save();
					$p->saveAction( $a->id );

					$r->savePermissions( $p->id );
				}
			} while ( $a->populate() );
		}

		JUtil::jredirect( "http://".SERVER_NAME."/roles" );

//		print_r( $_POST );
	}

	/*customers*/
	public function customers() {
		if ( !isset( $_SESSION[ "userId" ] ) )
			JUtil::jredirect( "http://".SERVER_NAME."/home" );

		$a = new Customer();
		$a->load();

		JT::init();
		JT::assign( "customers", $a );
		echo JT::pfetch( "customers" );
	}

	public function addCustomer( $id = null ) {
		$a       = new Customer( $id );
		$u       = new User();
		$s       = new Sector();
		$country = new Country();
		$city    = new City();

		$country->load();
		$city->load();
		$s->load();
		$u->load();

		JT::init();
		JT::assign( "sector", $s );
		JT::assign( "cities", $city );
		JT::assign( "countries", $country );
		JT::assign( "customer", $a );
		JT::assign( "users", $u );
		echo JT::pfetch( "customerForm" );
	}

	public function customerAction() {
		$a = new Customer( isset( $_POST[ "id" ] ) ? $_POST[ "id" ] : null );
		JFORM::formResponse( "customer" );
		foreach ( $a->_fields as $field ) {
			if ( isset( $_POST[ $field ] ) && in_array( $field, $a->_relatives ) === false )
				$a->$field = $_POST[ $field ];
		}

		if($_POST[ "contact" ]>0)
			$a->related = $_POST[ "contact" ];
		if($_POST[ "city" ]>0)
			$a->city    = $_POST[ "city" ];
		if($_POST[ "country" ]>0)
			$a->country = $_POST[ "country" ];
		if($_POST[ "sector" ]>0)
			$a->sector  = $_POST[ "sector" ];

		$a->save();

		unset( $a );
		JUtil::jredirect( "http://".SERVER_NAME."/customers" );
	}

	public function delCustomer( $id ) {
		$a = new Customer( $id>1?$id:null );
//		$a->active = 0;
		$a->delete();

		unset( $a );
		JUtil::jredirect( "http://".SERVER_NAME."/customers" );
	}

	public function addCustomerContact( $id, $cid = null ) {
		$a = new Customer( $id );
		$b = new Contact( $cid );
		JT::init();
		JT::assign( "customer", $a );
		JT::assign( "contact", $b );
		echo JT::pfetch( "contactForm" );
	}

	public function customerContactsAction() {
		if ( !isset( $_SESSION[ "userId" ] ) )
			JUtil::jredirect( "http://".SERVER_NAME."/home" );

		JFORM::formResponse( "contact" );

		$c           = new Contact( isset( $_POST[ "id" ] ) ? $_POST[ "id" ] : null );
		$c->name     = $_POST[ "name" ];
		$c->surname  = $_POST[ "surname" ];
		$c->tel1     = $_POST[ "tel1" ];
		$c->tel2     = $_POST[ "tel2" ];
		$c->tel3     = $_POST[ "tel3" ];
		$c->fax      = $_POST[ "fax" ];
		$c->position = $_POST[ "position" ];
		$c->email    = $_POST[ "email" ];
		$c->note     = $_POST[ "note" ];

		$c->save();

		if ( !isset( $_POST[ "id" ] ) ) {
			$cs = new Customer( $_POST[ "customerId" ] );
			$cs->saveContacts( $c->id );
		}

		JUtil::jredirect( "http://".SERVER_NAME."/customerContacts/" . $c->id );

	}

	public function customerContacts($id) {
		if ( !isset( $_SESSION[ "userId" ] ) )
			JUtil::jredirect( "http://".SERVER_NAME."/home" );

		$a = new Customer($id);

		$a->loadContacts();

		JT::init();
		JT::assign( "id", $id );
		JT::assign( "customer", $a );
		echo JT::pfetch( "contacts" );
	}
	//all contacts
	public function contacts() {
		if ( !isset( $_SESSION[ "userId" ] ) )
			JUtil::jredirect( "http://".SERVER_NAME."/home" );

		$c = new Contact();
		$c->load();
		JT::init();
		JT::assign( "contacts", $c );
		echo JT::pfetch( "allContacts" );
	}

	/*sectors*/
	public function sectors() {
		if ( !isset( $_SESSION[ "userId" ] ) )
			JUtil::jredirect( "http://".SERVER_NAME."/home" );

		$a = new Sector();
		$a->load();

		JT::init();
		JT::assign( "sectors", $a );
		echo JT::pfetch( "sectors" );
	}

	public function addSector( $id = null ) {
		$a = new Sector( $id );

		JT::init();
		JT::assign( "sector", $a );
		echo JT::pfetch( "sectorForm" );
	}

	public function sectorAction() {
		JFORM::formResponse( "sector" );

		$a        = new Sector( isset( $_POST[ "id" ] ) ? $_POST[ "id" ] : null );
		$a->name = $_POST[ "name" ];
		$a->save();
		unset( $a );
		JUtil::jredirect( "http://".SERVER_NAME."/sectors" );
	}

	public function delSector( $id ) {
		$a = new Sector( $id );
		$a->delete();

		unset( $a );
		JUtil::jredirect( "http://".SERVER_NAME."/sectors" );
	}
	/*orders*/
	public function orders() {
		if ( !isset( $_SESSION[ "userId" ] ) )
			JUtil::jredirect( "http://".SERVER_NAME."/home" );

		$a = new Border();
		$a->load();

		JT::init();
		JT::assign( "orders", $a );
		echo JT::pfetch( "orders" );
	}

	public function addOrder( $id = null ) {
		$a = new Border( $id );

		JT::init();
		JT::assign( "order", $a );
		echo JT::pfetch( "orderForm" );
	}

	public function orderAction() {
		JFORM::formResponse( "order" );

		$a        = new Border( isset( $_POST[ "id" ] ) ? $_POST[ "id" ] : null );
		$a->name = $_POST[ "name" ];
		$a->save();
		unset( $a );
		JUtil::jredirect( "http://".SERVER_NAME."/orders" );
	}

	public function delOrder( $id ) {
		$a = new Border( $id );
		$a->delete();

		unset( $a );
		JUtil::jredirect( "http://".SERVER_NAME."/orders" );
	}

	/*proposals*/
	public function proposals() {
		if ( !isset( $_SESSION[ "userId" ] ) )
			JUtil::jredirect( "http://".SERVER_NAME."/home" );

		$a = new Proposal();
		$a->load();

		JT::init();
		JT::assign( "proposals", $a );
		echo JT::pfetch( "proposals" );
	}

	public function addProposal( $id = null ) {
		$a = new Proposal( $id );
		if($a->gotValue){
			$a->validTill = JUtil::reverseDate( $a->validTill );
			$a->date= JUtil::reverseDate( $a->date);
		}
		$c = new Customer();
		$c->load();

		$p = new Product();
		$p->load();

		$cr = new Currency();
		$cr->load();

		JT::init();
		JT::assign( "customers", $c );
		JT::assign( "currencies", $cr );
		JT::assign( "products", $p );
		JT::assign( "proposal", $a );
		echo JT::pfetch( "proposalForm" );
	}

	public function proposalAction() {

		/*
		 * [productsColl] => {"2":{"productId":"2","productText":"birinci urun 2","unitAmount":"10","unitPrice":"10","unitCurrency":"1","unitLabel":"varil","packAmount":"33","amount":"444"}}
    [t0ken] => hTKoi8w1FkmukFPjylpPxsdfY_vH9IljyAv94q07JEmTlL4dLgmZkIiaiui4OQwGa1d0JLDoSVwlfIdP6h5NqZ9insUakzL5h6t_9YR1xo9mzohi-A6gRH9XhSfTI6na
    [date] => 11/11/1111
    [validTill] => 11/11/1111
    [proposalCode] => TK123
    [paymentInfo] => cek
    [deliveryInfo] => merkez
    [extraInfo] => 18 kdv
    [customer] => 1
    [contact] => 1
		 * */

		JFORM::formResponse( "proposal" );

		$a                  = new Proposal( isset( $_POST[ "id" ] ) ? $_POST[ "id" ] : null );
		$a->extraInfo       = $_POST[ "extraInfo" ];
		$a->paymentInfo     = $_POST[ "paymentInfo" ];
		$a->proposalCode    = $_POST[ "proposalCode" ];
		$a->deliveryInfo    = $_POST[ "deliveryInfo" ];
		$a->proposalContact = $_POST[ "contact" ];
		$a->date            = JUtil::reverseDate( $_POST[ "date" ] );
		$a->validTill       = JUtil::reverseDate( $_POST[ "validTill" ] );
		$a->proposalUser    = $_SESSION[ "userId" ];
		$a->status          = 'pending';
		$a->save();

		$prs = json_decode( $_POST[ "productsColl" ], true );

		if ( $a->gotValue )
			$a->deleteAllProposalProducts();

		foreach ( $prs as $pr ) {
			$pp               = new ProposalProduct();
			$pp->unitAmount   = $pr[ "unitAmount" ];
			$pp->unitPrice    = $pr[ "unitPrice" ];
			$pp->unitCurrency = $pr[ "unitCurrency" ];
			$pp->packAmount   = $pr[ "packAmount" ];
			$pp->amount       = $pr[ "amount" ];
			$pp->packingLabel = $pr[ "unitLabel" ];
			$pp->pProduct     = $pr[ "productId" ];
			$pp->save();

			$a->saveProposalProducts( $pp->id );
		}

		$ph                 = new ProposalHistory();
		$ph->action         = isset( $_POST[ "id" ] )?"Teklif guncellendi":"Teklif olusturuldu";
		$ph->type           = "positive";
		$ph->historyUser    = $_SESSION[ "userId" ];
		$ph->relatedContact = $_POST[ "contact" ];
		$ph->history        = $a->id;
		$ph->save();

		unset( $a, $pp );
		echo "good";
		exit;
	}

	public function delProposal( $id ) {
		$a = new Proposal( $id );
		$a->delete();

		unset( $a );
		JUtil::jredirect( "http://".SERVER_NAME."/proposals" );
	}

}
?>