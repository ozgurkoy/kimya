<?php

class Product_controller{
	/*product groups*/
	public function pgroups() {
		if ( !isset( $_SESSION[ "userId" ] ) )
			JUtil::jredirect( "http://".SERVER_NAME."/home" );

		$a = new ProductGroup();
		$a->load();

		JT::init();
		JT::assign( "productGroups", $a );
		echo JT::pfetch( "productGroups" );
	}

	public function addPgroup( $id = null ) {
		$a = new ProductGroup( $id );

		JT::init();
		JT::assign( "group", $a );
		echo JT::pfetch( "productGroupForm" );
	}

	public function pgroupAction() {
		JFORM::formResponse( "pgroup" );

		$a                = new ProductGroup( isset( $_POST[ "id" ] ) ? $_POST[ "id" ] : null );

		$a->name    = $_POST[ "name" ];
		$a->save();


		unset( $a );
		JUtil::jredirect( "http://".SERVER_NAME."/pgroups" );
	}

	public function delPgroup( $id ) {
		$a = new ProductGroup( $id );
		$a->delete();

		unset( $a );
		JUtil::jredirect( "http://".SERVER_NAME."/pgroups" );

	}

	/*products*/
	public function products() {
		if ( !isset( $_SESSION[ "userId" ] ) )
			JUtil::jredirect( "http://".SERVER_NAME."/home" );

		$a = new Product();
		$a->load();

		JT::init();
		JT::assign( "products", $a );
		echo JT::pfetch( "products" );
	}

	public function addProduct( $id = null ) {
		$a = new Product( $id );

		$a->loadFirstUnitCurrency();
		$a->loadGroups();
		$a->loadProductSector();

		$s = new Sector();
		$filter = $a->productSector && $a->productSector->gotValue ? array( "id" => "NOT IN " . join( ",", $a->productSector->_ids ) ) : null;
		$s->load($filter);

		$g = new ProductGroup();
		$filter = $a->groups && $a->groups->gotValue ? array( "id" => "NOT IN " . join( ",", $a->groups->_ids ) ) : null;
		$g->load($filter);

		$cr = new Currency();
		$cr->load();

		JT::init();

		JT::assign( "sectors", $s );
		JT::assign( "currencies", $cr );
		JT::assign( "groups", $g );
		JT::assign( "units", array( "kg", "lt", "lbs" ) );
		JT::assign( "secondUnits", array( "drum", "IBC" ) );
		JT::assign( "product", $a );
		echo JT::pfetch( "productForm" );
	}

	public function productAction() {
		JFORM::formResponse( "product" );

		$a = new Product( isset( $_POST[ "id" ] ) ? $_POST[ "id" ] : null );

		foreach ( $a->_fields as $field ) {
			if ( isset( $_POST[ $field ] ) && in_array( $field, $a->_relatives ) === false )
				$a->$field = $_POST[ $field ];
		}
		$a->firstUnitCurrency = $_POST[ "firstUnitCurrency" ];
		$a->save();

		if($a->gotValue){
			$a->deleteAllGroups();
			$a->deleteAllProductSector();
		}

		if(isset($_POST["groups"]))
			$a->saveGroups( $_POST[ "groups" ] );

		if(isset($_POST["productSector"]))
			$a->saveProductSector( $_POST[ "productSector" ] );

		unset( $a );
		JUtil::jredirect( "http://".SERVER_NAME."/products" );
	}

	public function delProduct( $id ) {
		$a = new Product( $id );
		$a->delete();

		unset( $a );
		JUtil::jredirect( "http://".SERVER_NAME."/products" );
	}	
}