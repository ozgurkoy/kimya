<?php
/************************************
 *
 *		utility class
 *		özgür köy
 *
 *
 ************************************/
class JUtil
{
	public static $cipher		= NULL;
	public static $cipherMode	= NULL;
	public static $cipherKey	= NULL;
	public static $ivSize		= NULL;

	static $instance;
	public static $IV = NULL;

	static function singleton(){
		self::$instance ||
		self::$instance = new JUtil();
		return self::$instance;
	}

	public function __construct() {
	}

	public static function resetIV() {
		self::$IV = NULL;
		self::deleteCookie('IV');
		return self::getIV();
	}

	public static function getIV() {
		self::$cipher		= MCRYPT_RIJNDAEL_128;
		self::$cipherMode	= MCRYPT_MODE_CBC;
		self::$cipherKey	= 'adminglesever2012monikidinlemez.'; // This should be a random string, recommended 32 bytes
		self::$cipherKey	= hash('tiger128,3', self::$cipherKey, FALSE);
		self::$ivSize		= mcrypt_get_iv_size(self::$cipher, self::$cipherMode);

		if(NULL === self::$IV) {
			if(!isset($_COOKIE['IV'])) {
				$iv = mcrypt_create_iv(self::$ivSize, MCRYPT_RAND);
				self::setCookie('IV', self::base64_url_encode($iv), false);
			} else {
				$iv = self::base64_url_decode(self::readCookie('IV', false));
			}
			self::$IV = $iv;
		}

		// fix for pre-generated keys in cookies
		if(strlen(self::$IV) > self::$ivSize) {
			JLog::log('gen', 'IV Size Problem:'.strlen(self::$IV));
			self::$IV = substr(self::$IV, 0, self::$ivSize);
			self::setCookie('IV', self::base64_url_encode(self::$IV), false);
		}

		return self::$IV;
	}

	public static function base64_url_encode($input) {
		return strtr(base64_encode($input), '+/=', '-_,');
	}

	public static function base64_url_decode($input) {
		return base64_decode(strtr($input, '-_,', '+/='));
	}

	public static function getEncrypted($text, $b64=true) {
		$iv = self::getIV();

		$mcc = mcrypt_encrypt(self::$cipher, self::$cipherKey, $text, self::$cipherMode, $iv);

		if($b64) {
			$mcc = self::base64_url_encode($mcc);
		} else {
			$mcc = urlencode($mcc);
			$mcc = str_replace('%', 'zZz', $mcc);
		}

		return $mcc;
	}

	public static function getDecrypted($text, $b64=true) {
		$iv = self::getIV();

		if($b64) {
			$premcc = self::base64_url_decode($text);
		} else {
			$text1 = str_replace('zZz', '%', $text);
			$premcc = urldecode($text1);
		}

		$mcc = mcrypt_decrypt(self::$cipher, self::$cipherKey, $premcc, self::$cipherMode, $iv);
		//JLog::log('gen', 'Included Files:' . print_r(get_included_files(), true).' TOKENN:'. $text . ' URL Decoded:'.$premcc . ' Decrypted:'.$mcc);

		return $mcc;
	}

	public static function customCScript( $script, $parameters=null,$forceExit=true,$tm=1 ){
		if(!is_null($parameters)){
			$params = array();
			foreach($parameters as $p=>$ar){
				if( strlen(trim($ar))>0 ){
					$params[$p] = in_array($ar, array("true","false"))? $ar : self::jsString( $ar );
				}
			}
			$parameters = implode(",",$params);
		}
		if (!headers_sent())
			header('Content-Type: text/html; charset=utf-8');

		echo self::scriptize("
			window.parent.setTimeout(function(){window.parent.".$script."(".$parameters.");},$tm);
		");
		if($forceExit)
			exit;
	}

	public static function pageClearErrorMessage( $messages="" ){
		$messages = (is_array($messages)?implode("<BR>",$messages):$messages);

		echo self::scriptize("
			window.parent.clearErrorMessage();
		");
	}

	public static function jsString( $st,$wq=true ){
		$st = str_replace("'","\'",$st);
		$st = preg_replace('/\\r?\\n?/', '', $st);

		if($wq) $st = "'".$st."'";

		return $st;
	}

	public static function jsString2( $st,$wq=true ){
		$st = str_replace("'","\'",$st);
		$st = preg_replace('/\\r?\\n?/', '\\\\n', $st);

		return $st;
	}
	/**
	 * generate random string
	 *
	 * @return string
	 **/
	public static function randomString($length = 32, $chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890',$minLength=null)
	{
		if(!is_null($minLength)){
			$length = rand($minLength,$length);
		}

		// Length of character list
		$chars_length = (strlen($chars) - 1);

		// Start our string
		$string = $chars{rand(0, $chars_length)};

		// Generate random string
		for ($i = 1; $i < $length; $i = strlen($string))
		{
			// Grab a random character from our list
			$r = $chars{rand(0, $chars_length)};

			// Make sure the same two characters don't appear next to each other
			if ($r != $string{$i - 1}) $string .=  $r;
		}

		// Return the string
		return $string;
	}

	public static function entity( $st ){
		return htmlentities($st ,ENT_NOQUOTES,"UTF-8");
	}

	public static function scriptize($sc){
		return "<script>$sc</script>";
	}

	public static function getRandomInteger($q=1000){
		return md5(uniqid(rand(1,$q)));
	}

	public static function dateFormat( $d ){
		if(!($d>1000)) return "-";
		return date("d/m/Y H:i", $d);
	}

	public static function dropFirst( &$ar ){
		reset($ar);
		$key = key($ar);
		unset( $ar[$key] );
	}

	public static function getDateParams($dstr){
		$xpl = strpos($dstr,"-")>0?"-":"/";

		$x = explode($xpl,$dstr);
		return $x;

	}

	public static function reverseDate($dstr,$ds="/"){
		$x = self::getDateParams($dstr);
		if(sizeof($x)!=3) return $dstr;

		// return $x[2].$ds.$x[0].$ds.$x[1];
		return $x[2].$ds.$x[1].$ds.$x[0];


	}

	public static function publishDirect( $cn ){
		global $__DP,$time_start;
		echo $cn ;
		//require_once "$__DP/engine/class/_kill.php";
		exit;
	}

	public static function getMailServer(){
		$hrx = JCache::read("mailServers");
		$hrf = array_keys($hrx);

		$cnt = rand(0, sizeof($hrx)-1);

		//update here.. RANDOM for now..
		return $hrx[$hrf[$cnt]];
	}

	public static function isProduction(){
		global $__DP;
		return (is_file("$__DP/site/def/state/production-1") && PHP_SAPI !== 'cli' && !in_array(SERVER_NAME , array("s1th.admingle.com","t.admingle.com")));
	}

	public static function checkSiteHalt(){
		global $__DP;
		return is_file("$__DP/site/def/state/halt-1");
	}

	public static function siteHalt($from=""){
		JT::init();
		JT::assign("time",date("d-m-Y H:i",time()));
		JT::assign("from",$from);

		if(JUtil::isProduction()) {
			self::publishDirect(JT::pfetch("_sysHalt"));
		}
	}

	public static function siteError($from=""){
		JT::init();
		JT::assign("time",date("d-m-Y H:i",time()));
		JT::assign("from",$from);

		if(JUtil::isProduction()) {
			self::publishDirect(JT::pfetch("_sysError"));
		}
	}

	public static function configProblem($problem){
		JT::assign("date",date("d-m-Y H:i",time()));
		JT::assign("problem",$problem);

		JLog::log("911",$problem);

		self::publishDirect(JT::pfetch("_configProblem"));
	}

	/**
	 * Removes the directory and all its contents.
	 *
	 * @param string the directory name to remove
	 * @param boolean whether to just empty the given directory, without deleting the given directory.
	 * @return boolean True/False whether the directory was deleted.
	 */
	public static function deleteDirectory($dirname,$only_empty=false) {
		if (!is_dir($dirname))
			return false;
		$dscan = array(realpath($dirname));
		$darr = array();
		while (!empty($dscan)) {
			$dcur = array_pop($dscan);
			$darr[] = $dcur;
			if ($d=opendir($dcur)) {
				while ($f=readdir($d)) {
					if ($f=='.' || $f=='..')
						continue;
					$f=$dcur.'/'.$f;
					if (is_dir($f))
						$dscan[] = $f;
					else
						unlink($f);
				}
				closedir($d);
			}
		}
		$i_until = ($only_empty)? 1 : 0;
		for ($i=count($darr)-1; $i>=$i_until; $i--) {
			rmdir($darr[$i]);
		}
		return (($only_empty)? (count(scandir)<=2) : (!is_dir($dirname)));
	}

	public static function maill($to, $subject, $content,$type="html",$from=null,$att=null){
		global $__DP;

		if (!in_array(substr($to,0,strpos($to,"@")),array("postmaster","admin","administrator","support","sales","satis","destek","webmaster"))){
			if (!class_exists('EuroMessage', false)) require_once($__DP."/site/lib/euro.message.php");
			$em = new EuroMessage();
			$r = $em->SendTMail($to, $subject, $content);
			unset($em);
			return $r;
		}


		// return;
		// "MS1"=>array( "ip"=>"incele.me","user"=>"akuma@incele.me","pass"=>"14411441","port"=>25,"secure"=>true)
		$ms = self::getMailServer();
		// self::incLib("PHPMailer_v5.1/class.phpmailer");
		require_once($__DP."/site/lib/PHPMailer_v5.1/class.phpmailer.php");

		if(!isset($ms["ip"])) return false;

		if(is_null($from)) $from = $ms["from"];

		$mail             = new PHPMailer();
		// $body             = file_get_contents('contents.html');
		// $content             = eregi_replace("[\]",'',$content);
		$mail->CharSet 		 = "utf-8";

		$mail->IsHTML($type=="html"?true:false);
		$mail->IsSMTP(); // telling the class to use SMTP
		$mail->Hostname   = $ms["hostname"];  // Message-ID header content
		$mail->Host       = $ms["ip"]; 	// SMTP server
		// enables SMTP debug information (for testing)
		// 1 = errors and messages
		// 2 = messages only
		$mail->SMTPAuth   = $ms["secure"];                  		// enable SMTP authentication
		$mail->Port       = $ms["port"];                    	// set the SMTP port for the GMAIL server
		$mail->Username   = $ms["user"]; 		// SMTP account username
		$mail->Password   = $ms["pass"];        		// SMTP account password
		$mail->SetFrom($from, (isset($ms["fromLabel"]) ? $ms["fromLabel"] : $from));

		$mail->ClearReplyTos();
		$mail->AddReplyTo($ms['replyTo'], (isset($ms['replyToLabel']) ? $ms['replyToLabel'] : $ms['replyTo']));

		$mail->Subject    = $subject;

		//$mail->AltBody    = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test
		$mail->AltBody = strip_tags($content);

		$mail->MsgHTML($content);

		//$mail->AddAddress($to, $to);
		$mail->AddAddress($to);
		$mail->SMTPDebug =false;

		if(!is_null($att)){
			$mail->AddAttachment($att);// attachment
		}
		// $mail->AddAttachment("images/phpmailer_mini.gif"); // attachment
		try {
			$mail->Send();
		} catch (Exception $e) {
			echo $e;
		}
		return "SendWithLocalMailServer";
		/**/
	}

	public static function readCookie($title ,$enc=false){
		if(!isset($_COOKIE[$title])) return null;

		$ck = $_COOKIE[$title];
		if(strlen($ck)==0) return null;

		if($enc) {
			return self::getDecrypted( $ck,true );
		}
		else {
			return $ck;
		}
	}

	public static function setCookie( $title, $str , $enc=false ){
		setcookie ( $title, ($enc?self::getEncrypted($str,true):$str), time() + COOKIE_LIFE , "/"/*, ".".JCache::read("constants.cookieDomain"), 1*/);
	}

	public static function deleteCookie($title){
		setcookie ( $title, "", time() - 3600, "/"/*, ".".JCache::read("constants.cookieDomain"), 1*/);
	}

	public static function quotize( &$ar){
		foreach($ar as $q=>$q1){
			if( substr($q1,0,1)=="'" ) continue; //cooked before
			$ar[$q] = "'".$q1."'";
		}
	}

	public static function refineRequest( &$req ){
		foreach($req as $r=>$q)
			$req[$r] = htmlentities( urldecode($q) , ENT_NOQUOTES );
	}

	public static function textPrepare(&$str){
		$str = preg_replace('/\\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|]/i', '<a href="\\0" target="_blank">\\0</a>', $str);
		$str = nl2br( $str );
	}

	public static function allToLower($str){
		$str = str_replace("Ğ","ğ",$str);
		$str = str_replace("Ü","ü",$str);
		$str = str_replace("Ş","ş",$str);
		$str = str_replace("İ","i",$str);
		$str = str_replace("Ö","ö",$str);
		$str = str_replace("Ç","ç",$str);

		return strtolower($str);
	}

	public static function normalize($string,$low=false){
		$string = str_replace(" ", "-",$string);
		$string = strtr($string,array("ş"=>"s","ı"=>"i","ğ"=>"g","ü"=>"u","ö"=>"o","Ş"=>"s","İ"=>"i","Ğ"=>"g","Ü"=>"u","Ö"=>"o","Ç"=>"c","ç"=>"c","&"=>"-ve-"));
		$string = preg_replace("/[^a-zA-Z0-9-.]/", "", $string);
		if($low)
			$string = strtolower($string);
		else
			$string = strtoupper($string);

		return utf8_encode($string);
	}

	public static function standartize( $tag ){
		$mx	= strlen(urldecode($tag));
		$ret = array();
		for( $i=0;$i<$mx;$i++ ){
			$cc = substr($tag, $i, 1 );
			if( !preg_match('/([A-Za-z0-9-_şıüğçöŞİÜĞÖÇ\\s])/',	$cc ) )
				$cc= " ";

			$ret[] = $cc;
		}
		$ret = trim(implode("",$ret));

		return self::allToLower($ret);
	}

	public static function aposArray($ar,$ap="'"){
		if(!is_array($ar)||!sizeof($ar)>0) return false;
		foreach ($ar as $key => $value) {
			if(substr($value, 0, 1)==$ap) continue; //already patched!
			$ar[$key] = $ap.$value.$ap;
		}

		return $ar;
	}

	public static function multistr($str,$c,$sep=","){
		$r = array();
		for ($i=0; $i < $c; $i++) {
			$r[] = $str;
		}

		return implode($sep,$r);

	}

	public static function redirectHome($error=true){
		if($error)
			JLog::log("user", "wrong page red(get:".(serialize($_GET)."post:".(serialize($_POST)))."server:".(serialize($_SERVER)).")");

		echo "<script>
			if(window.parent)
				window.parent.location.href='".JCache::read("constants.siteAddress")."';</script>
			else
				location.href='".JCache::read("constants.siteAddress")."';
		</script>
		";
		exit;
		// die("going home");

	}


	public static function jredirect($col=""){
		echo "<script>window.parent.location.href='".JCache::read("constants.siteAddress")."$col';</script>";
		exit;
	}

	/**
	 * kill empty values
	 *
	 * @return array
	 **/
	public static function killEmps($arr)
	{
		$arr = array_unique($arr);
		foreach ($arr as $pr => $pro) {
			if(strlen(trim($pro))==0) unset($arr[$pr]);
		}

		return $arr;
	}

	/**
	 * get search parameter with starting ? / ?s=asdfsaf+Ara
	 *
	 * @return array
	 **/
	public static function getSearchParameter($returnAsURI=false)
	{
		$t0 = explode("?",$_SERVER['REQUEST_URI']); $t0 = end($t0);

		if(strpos($t0,"&")!==false)
			$t1 = explode("&",$t0);
		else
			$t1 = explode("/",$t0);

		// print_r($t1);
		$ret= array();

		foreach ($t1 as $tsearch) {

			if(strpos($tsearch,"=")===false){
				$ret[] = urldecode($tsearch);
				continue;
			}
			if($returnAsURI){
				$ret[] = $tsearch;
			}
			else{
				$t3 = explode("=",$tsearch);
				$ret[$t3[0]] = mysql_escape_string(urldecode($t3[1]));
			}
		}
		return $ret;
	}

	/**
	 * split search param rom abc=123&def=456
	 *
	 * @return void
	 **/
	public static function splitSearchParameters($param)
	{
		$t1 = explode("&",$param);
		$ret = array();
		foreach ($t1 as $tsearch) {
			if(strpos($tsearch,"=")===false){
				$ret[] = urldecode($tsearch);
				continue;
			}
			else{
				$t3 = explode("=",$tsearch);
				$ret[$t3[0]] = mysql_escape_string(urldecode($t3[1]));
			}

		}
		return $ret;
	}

	/**
	 * create real table name
	 *
	 * @return string
	 **/
	public static function tableName($tableName)
	{
		return JDef::$prefix.$tableName;
	}


	/**
	 * create pager array upon variables
	 *
	 * @return array
	 **/
	public static function makePager( $recordCount, $perPage, $currentPage=0)
	{
		$lastPage     = intval( $recordCount / $perPage ) + intval( $recordCount % $perPage > 0 );

		$currentCount = $perPage * $currentPage;
		$fpages = array();

		$dlim = (($currentPage-2>0)?($currentPage-2):0);
		$ulim = (($currentPage+3<$lastPage)?($currentPage+3):$lastPage);

		$firstDot = false;
		$secDot   = false;

		for( $pa=0; $pa<$lastPage; $pa++ ){
			if( !(
				($pa==0) ||
				( ($pa >= $dlim) && ($pa<$ulim) ) ||
				($pa==$lastPage-1)
			)) {
				if( !$firstDot && ($pa<$currentPage) ) {$fpages[$pa]="...";$firstDot=true;}
				if( !$secDot && ($pa>$currentPage) ) {$fpages[$pa]="...";$secDot=true;}
				continue;
			}

			$slin = $pa+1;

			$fpages[$pa] = $slin;
		}

		return $fpages;
	}



	/**
	 * load hier
	 *
	 * @return void
	 **/
	public static function findHi($type,$id,$ar=array(),$suff=null)
	{
		if($suff==null) $suff="parent".ucfirst($type);

		$ids = JPDO::executeQuery("SELECT id FROM `".self::tableName($type)."` WHERE $suff=?",$id);
		// var_dump($ids);
		if(is_array($ids)){
			foreach ($ids as $idk) {
				if(!in_array($idk["id"],$ar)) $ar[] = $idk["id"];
				$ar = array_merge($ar, self::findHi($type,$idk["id"],$ar,$suff));
			}
		}

		return array_unique($ar);

	}

	/**
	 * time end label
	 *
	 * @return string
	 **/
	public static function scriptTime($label="")
	{
		global $JTS;

		$time_end = microtime(true);
		$time = $time_end - $JTS;

		echo "
		<BR>$label : SCRIPT TIME: $time seconds\n<BR>
		";
	}


	/**
	 * lower case first
	 *
	 * @return string
	 **/
	public static function lcfirst($str)
	{
		$str{0} = strtolower($str{0});

		return $str;
	}


	/**
	 * fetch page
	 *
	 * @return void
	 **/
	public static function fetchPage($page)
	{
		$ch = curl_init ($page);
		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
		$result = curl_exec ($ch);
		curl_close ($ch);

		return $result;
	}

	/**
	 * get lang from langbase, TEMPORARY
	 *
	 * @return string
	 **/
	public static function getLangWord($word)
	{
		$words = JUtil::getLangWordFileContent();
		if (isset($words[$word])){
			return $words[$word];
		} else {
			return null;
		}
	}

	/**
	 * get lang file content from proper lang file
	 *
	 * @return string
	 **/
	public static function getLangWordFileContent()
	{
		//global $__DP, $SYSTEM_EMAILS;

		if (!isset($_SESSION['lang'])) $_SESSION['lang'] = DEFAULT_LANGUAGE;
		$words = LangWords::getLangWords("word",$_SESSION['lang']);
		/*
				$f = file_get_contents($__DP."site/langFiles/lang".$_SESSION['lang'].".txt");
				$f = str_replace("{CURRENCY_SYMBOL}",CURRENCY_SYMBOL,$f);
				$f = str_replace("{ROOT_DOMAIN}",ROOT_DOMAIN,$f);
				foreach ($SYSTEM_EMAILS as $key=>$email){
					$f = str_replace("{mail.$key}",$email,$f);
				}
				//die("<pre>" . $f);
				   $t0 = explode("\n",$f);
				$words = array();
				foreach ($t0 as $t1) {
					if(!(strlen($t1)>0) || strpos($t1,":")===false)
						continue;

					$t2 = explode(":",$t1);
					$t3 = array_shift($t2);
					$words[$t3] = implode(":",$t2);
				}
		 */
		return $words;
	}

	/**
	 * val url
	 *
	 * @return void
	 **/
	public static function validateURL($url)
	{
		return preg_match('|^http(s)?\://[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,4}(/\S*)?$|i', $url);
		// return preg_match('|^http(s)?://[a-z0-9-]+(.[a-z0-9-]+)*(:[0-9]+)?(/.*)?$|i', $url);

	}

	/**
	 * Validate phone number
	 *
	 * @return boolean
	 */
	public static function validatePhone($phone,$mandatory=false){
		$validPhone = true;

		if (strlen($phone)>0 && $phone != "-"){
			if (strpos($phone,"-") === false){
				$validPhone = false;
			} else {
				$gsmNumber = explode("-", $phone);
				if (count($gsmNumber) != 2){
					$validPhone = false;
				}else{
					if (COUNTRY_CODE == 213) //Turkey
					{
						if(!is_numeric($gsmNumber[0])) {
							$validPhone = false;
						} elseif(!is_numeric($gsmNumber[1])) {
							$validPhone = false;
						}

						if (strlen($gsmNumber[0])<3){
							$validPhone = false;
						} elseif(strlen($gsmNumber[1])<7) {
							$validPhone = false;
						} elseif(strlen($gsmNumber[0]) > 4 || strlen($gsmNumber[1]) > 7)  {
							$validPhone = false;
						}
					}
					elseif (COUNTRY_CODE == 220) // United Kingdom
					{
						$phone = str_replace("-", "", $phone);
						$intError = null;
						$strError = null;
						$validPhone = self::checkUKTelephone($phone,$intError,$strError);
					}
					elseif (COUNTRY_CODE == 80) // Germany
					{
						if(!is_numeric($gsmNumber[0])) {
							$validPhone = false;
						} elseif(!is_numeric($gsmNumber[1])) {
							$validPhone = false;
						}

						if (strlen($gsmNumber[0])<3){
							$validPhone = false;
						} elseif(strlen($gsmNumber[1])<6) {
							$validPhone = false;
						} elseif(strlen($gsmNumber[0]) > 4 || strlen($gsmNumber[1]) > 7)  {
							$validPhone = false;
						}
					}
				}
			}
		} elseif ($mandatory && (strlen($phone) == 0 && $phone == "-")){
			$validPhone = false;
		}

		return $validPhone;
	}

	/**
	 * Indents a flat JSON string to make it more human-readable.
	 *
	 * @param string $json The original JSON string to process.
	 *
	 * @return string Indented version of the original JSON string.
	 */
	public static function jsonIndent($json) {

		$result      = '';
		$pos         = 0;
		$strLen      = strlen($json);
		$indentStr   = '  ';
		$newLine     = "\n";
		$prevChar    = '';
		$outOfQuotes = true;

		for ($i=0; $i<=$strLen; $i++) {

			// Grab the next character in the string.
			$char = substr($json, $i, 1);

			// Are we inside a quoted string?
			if ($char == '"' && $prevChar != '\\') {
				$outOfQuotes = !$outOfQuotes;

				// If this character is the end of an element,
				// output a new line and indent the next line.
			} else if(($char == '}' || $char == ']') && $outOfQuotes) {
				$result .= $newLine;
				$pos --;
				for ($j=0; $j<$pos; $j++) {
					$result .= $indentStr;
				}
			}

			// Add the character to the result string.
			$result .= $char;

			// If the last character was the beginning of an element,
			// output a new line and indent the next line.
			if (($char == ',' || $char == '{' || $char == '[') && $outOfQuotes) {
				$result .= $newLine;
				if ($char == '{' || $char == '[') {
					$pos ++;
				}

				for ($j = 0; $j < $pos; $j++) {
					$result .= $indentStr;
				}
			}

			$prevChar = $char;
		}

		return $result;
	}

	/**
	 * Set last URL to session for login redirect
	 *
	 * @return void
	 * @author Murat
	 */
	public static function setLastLocation(){
		if(session_id() == '') {
			session_start();
		}

		$pageURL = 'http';
		if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
		$pageURL .= "://";
		if ($_SERVER["SERVER_PORT"] != "80") {
			$pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
		} else {
			$pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
		}

		$_SESSION["LAST_URL"] = $pageURL;
	}

	/**
	 * Random number
	 *
	 * @return int
	 * @author Murat hosver
	 */
	public static function randomNumber($in,$out,$step=null){
		if (!is_null($step)){
			$vals = array();
			for ($i=$in;$i<$out;$i+=$step){
				$vals[] = $i;
			}
			return $vals[array_rand($vals)];
		} else {
			srand((double) microtime() * 1000000);

			return rand($in, $out);
		}
	}

	/**
	 * Referrer parser
	 *
	 * @return array
	 * @author Murat
	 */
	public static function parseReferrer($data){
		$domains = array();

		$domains = array();
		$total = 0;
		if(!isset($data->error) && count($data)) {
			foreach ($data as $value) {
				$parsed = @parse_url($value->_id);
				$parsedHost = isset($parsed['host']) ? $parsed['host'] : '';
				if(strlen($parsedHost)) {
					$parsedHost = str_replace('www.', '', $parsedHost);
					if(!isset($domains[$parsedHost])) {
						$domains[$parsedHost] = 0;
					}
					$domains[$parsedHost]+=$value->count;
					$total+=$value->count;
				}
			}
		}
		if(!count($domains)) {
			$domains[SERVER_NAME] = 1;
			$total = 1;
		}
		$domains['total'] = $total;

		return $domains;
	}

	public  function __destruct() {}


/**
 * Replace global variables in word texts
 *
 * @return string
 * @author Murat
 */
public static function replaceGlobals($t){
	global $SYSTEM_EMAILS;

	$t = str_replace("{CURRENCY_SYMBOL}",CURRENCY_SYMBOL,$t);
	$t = str_replace("{ROOT_DOMAIN}",ROOT_DOMAIN,$t);
	$t = str_replace("{SERVER_NAME}",SERVER_NAME,$t);
	$t = str_replace("{COUNTRY_NAME}",COUNTRY_NAME,$t);
	foreach ($SYSTEM_EMAILS as $key=>$email){
		$t = str_replace("{mail.$key}",$email,$t);
	}
	$t = preg_replace("/{([^}]+)\|cformat}/i",self::getFormattedCurrency('$1'),$t);
	return $t;
}

	/**
	 * check the country code and format the given currency
	 *
	 * @return void
	 * @author Murat
	 */
	public static function getFormattedCurrency($amount,$countryCode=COUNTRY_CODE,$formatNumber=false,$formatDecimal=2){
		if ($formatNumber && is_numeric($amount)) $amount = self::NumberFormat($amount,$formatDecimal);
		if ($countryCode==220)
			return CURRENCY_SYMBOL.$amount;
		elseif ($countryCode==80)
			return CURRENCY_SYMBOL.$amount;
		else
			return $amount." ".CURRENCY_SYMBOL;
	}


	/**
	 * Custom number formatting
	 *
	 * @return float
	 * @author Murat Hosver
	 */
	public static function NumberFormat($n,$d=2){
		return number_format($n,$d,NUMBER_FORMAT_DECIMAL_SEPERATOR,NUMBER_FORMAT_THOUSAND_SEPERATOR);
	}

}