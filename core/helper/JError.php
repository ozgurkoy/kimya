<?php
/************************************
 *
 *		error handler class
 *
 *		se/cl-side errors parsed ,processed and showed here..
 *
 *		error types	:
 *		1 ->	client side/form errors, print to screen
 *		911 ->	fatal errors
 *
 *		TODO :: ADD LOGGING
 *		özgür köy
 *
 ************************************/
class JError extends Exception
{
	const MEMCACHED_ZERO	= "no memcached servers connected";
	const PDO_ZERO			= "PDO connection error";

	public function __construct( $message, $code=0)
	{
		JLog::log("user",$message,0);

		parent::__construct($message, $code);
	}

	// custom string representation of object
	public function __toString() {
			return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
	}

	public function csResponse($method, $message="")
	{
		global $__DP;
		//check for user's client response library
		if(is_file($__DP."/site/lib/clientResponse.php")){
			include $__DP."/site/lib/clientResponse.php";

			return call_user_func(array("ClientResponse", $method, $message));
		}
	}

	public function __destruct() {

	}
}
