<?php
/************************************
 *
 *		Jcache - jeelet
 *		özgür köy
 *
 ************************************/

//memcached stuff
require_once "$__DP/core/lib/memcached.php";

$hasMC = (bool)function_exists("memcache_pconnect");

if($hasMC){
	//init memcache
	MC::singleton();

	//try memcached
	$hasMD = sizeof(MC::$mc_servers_count)>0;
}

//try apc
$hasAPC = (bool)function_exists("apc_fetch");


class JCache{
	const STATIK		= 1;
	const RETURNCHAIN 	= true;

	public static function read($key){
		global $GLBS,$hasAPC,$__site,$hasMC;

		$key = $__site."|".$key;

		//try memcached
		$keychain = self::parseKey( $key, null, self::RETURNCHAIN );

		if($hasMC)
			$ret = MC::get($keychain);
		else
			$ret = false;

		if( (is_array($ret) && sizeof($ret)>0) || (trim(strlen($ret))>0)) {
			return self::parseKey($key,$ret);
		}

		if($hasAPC){
			$ret = apc_fetch($keychain);

			if((is_array($ret) && sizeof($ret)>0) || (trim(strlen($ret))>0)) return self::parseKey($key,$ret);
		}

		//try global variable if set...
		// echo $keychain."
		// ";
		list($t0,$key)= explode("|",$key);
		$keychain = self::parseKey( $key, null, self::RETURNCHAIN );
		if( isset( $GLBS[$keychain] ) ){
			$ret = $GLBS[$keychain];

			return self::parseKey($key,$ret);
		}

		//nothing.. sorry..
		return false;
	}

	public static function readStatik($key){
		global $hasAPC,$hasMC,$__site;

		$key = $__site."|".$key;

		//try memcached
		$keychain = self::parseKey( $key, null, self::RETURNCHAIN );
		$ret = $hasMC && MC::get($keychain);

		if( (is_array($ret) && sizeof($ret)>0) || (trim(strlen($ret))>0)) {
			return self::parseKey($key,$ret);
		}
		//try apc
		if($hasAPC){
			$ret = apc_fetch($keychain);
			if(trim(strlen($ret))>0) return self::parseKey($key,$ret);
		}


		return false;
	}

	public static function write($key, $value, $ttl=0){
		global $GLBS,$hasAPC,$__site,$hasMC;
		$key = $__site."|".$key;

		//write to memcached
		$hasMC && MC::set($key, $value,1,$ttl);
		//write to APC
		if($hasAPC)
			apc_store($key,$value,$ttl);

		//set global (fr what??)
		$GLBS[$key] = $value;
	}

	public static function delete($key){
		global $GLBS,$hasAPC,$__site,$hasMC;
		$key = $__site."|".$key;

		//write to memcached
		$hasMC && MC::delete($key);

		//write to APC
		if($hasAPC)
			apc_delete($key);

		//set global (fr what??)
		unset($GLBS[$key]);
	}

	public static function addToSiteVariables($key,$value){
		global $GLBS,$hasAPC,$__site;
		$key = $__site."|".$key;

		if($hasAPC){
			apc_store($key,$value,0);
		}
		else
			$GLBS[$key] = $var;
	}

	private static function parseKey($key,$ret=null,$returnRootChainOnly=false){
		$keyparts = array();

		if(strpos($key,".")>0) {
			$keyparts = explode( ".",$key );
			$key = $keyparts[0];
		}

		if($returnRootChainOnly) return $key;

		if( sizeof( $keyparts ) > 1 ){
			for( $i=1; $i<sizeof( $keyparts ); $i++){
				if(!isset($ret[ $keyparts[$i] ])) return false;
				$ret = $ret[ $keyparts[$i] ];
			}
		}

		return $ret;
	}

}
