<?php
class JFORM
{
    static $instance;
    static $formkey = 'A1B2C3D4E5f6g7h8i9j0A1B2C3D4E5f6';
    static $delimiter = '@@@';

    public static function getFormToken( $forForm ) {
		if(!array_key_exists('formKeys', $_SESSION)) {
			$_SESSION['formKeys'] = array();
		}
		$_SESSION['formKeys'][$forForm] = JUtil::randomString(32);
		return JUtil::getEncrypted(self::$formkey . self::$delimiter . $forForm . self::$delimiter . time() . self::$delimiter . $_SESSION['formKeys'][$forForm]. self::$delimiter . 'a');
    }

	public static function parseFormToken( $token ) {
		$tokenen = JUtil::getDecrypted($token);
		$tokenPieces = explode(self::$delimiter, $tokenen);
		return $tokenPieces;
	}

	public static function formCheck( $formName, &$ref=null ) {

		//token control
		if ( !isset( $_REQUEST[ 't0ken' ] ) ) {
			JLog::log( 'gen', 'Token: Not received!' );

			return false;
		}

		if ( !isset( $_SESSION[ 'formKeys' ][ $formName ] ) ) {
			JLog::log( 'gen', 'Token: Session key missing in session' );

			return false;
		}

		$tokenPieces = self::parseFormToken( $_REQUEST[ 't0ken' ] );

		if ( sizeof( $tokenPieces ) != 5 ) {
			JLog::log( 'gen', 'Token: Invalid size:' . sizeof( $tokenPieces ) . ' Pieces:' . print_r( $tokenPieces, true ) );

			return false;
		}

		if ( $tokenPieces[ 0 ] != self::$formkey ) {
			JLog::log( 'gen', 'Token: Wrong key:' . $tokenPieces[ 0 ] . ' IV:' . JUtil::$IV . ' IV Encoded:' . JUtil::base64_url_encode( JUtil::$IV ) );

			return false;
		}

		if ( $tokenPieces[ 1 ] != $formName ) {
			JLog::log( 'gen', 'Token: Wrong Form Name: Expected:' . $formName . ' Incoming:' . $tokenPieces[ 1 ] );

			return false;
		}

		$timeDiff = ( $tokenPieces[ 2 ] - time() ) / 60;
		if ( $timeDiff > 30 ) {
			JLog::log( 'gen', 'Token: Timeout: created:' . date( 'Y-m-d H:i:s', $tokenPieces[ 2 ] ) . ' and now:' . date( 'Y-m-d H:i:s', time() ) . ' diff:' . $timeDiff );

			return false;
		}

		if ( $tokenPieces[ 3 ] != $_SESSION[ 'formKeys' ][ $formName ] ) {
			JLog::log( 'gen', 'Token: Wrong From Random Key: Expected:' . $_SESSION[ 'formKeys' ][ $formName ] . ' Incoming:' . $tokenPieces[ 3 ] );

			return false;
		}
//		echo 123;exit;

		JUtil::pageClearErrorMessage();
		$formFields = JCache::read( "formFields" );

		//fields control
		$errors = array();
		if ( array_key_exists( $formName, $formFields ) && sizeof( $formFields[ $formName ] ) > 0 ) {
			foreach ( $formFields[ $formName ] as $field => $info ) {

				$field        = str_replace( "@", "", $field );
				if ( !isset( $_REQUEST[ $field ] ) ) {
					$_REQUEST[ $field ] = null;
					$function           = "matchEmpty";
				}
				else
					$function = ( isset( $info[ 1 ] ) ? $info[ 1 ] : "matchEmpty" );

				$value        = $_REQUEST[ $field ];
				$errorMessage = $info[ 0 ];
				$parameters   = (string)( isset( $info[ 2 ] ) ? $info[ 2 ] : "" );
				$k            = call_user_func( array( "JFORM", $function ), $value, $parameters, $ref );
				if ( !$k && !isset( $errors[ $field ] ) ) $errors[ $field ] = $errorMessage;
			}
		}
		if ( sizeof( $errors ) > 0 )
			return $errors;


		return true;
	}

	/**
	 * After successful form process session key has to be removed
	 */
	public static function removeSessionKey($formName) {
		unset($_SESSION['formKeys'][$formName]);
	}

	/**
	 * kill form response
	 *
	 * @param $formName
	 * @return void
	 */
	public static function formResponse( $formName, &$ref=null ) {
		$fc = self::formCheck( $formName, $ref );
		if ( $fc !== true ) {
			if ( !isset( $_SESSION[ 'formKeys' ][ $formName ] ) ) {
				JUtil::redirectHome();
			}
			else {
				JUtil::customCScript( "fieldError", array( $formName, json_encode( $fc ) ) );
			}
			exit;
		}

	}

	public static function matchEmpty( $vl ){
		if ( strlen( trim( (string)$vl ) ) > 0 ) return true;
		else return false;
	}

	public static function matchWhiteSpace( $vl ){
		if (preg_match('/\s/', $vl))
			return false;

		return true;
	}

	public static function matchPassword( $vl ){
		if(strlen(trim($vl))<6) return false;
		else return true;
	}



	public static function checkLogin( $password, $params=null, User &$userObject ) {

		return $userObject->login( $_POST[ "username" ], $_POST[ "password" ] );
	}

	public static function checkForCategoriesToClone( $cloner ) {
		if ( strlen( $cloner ) == 0 || ( strlen( $cloner ) > 0 && isset($_POST["cloneCats"]) ) ) return true;

		return false;
	}

	public static function checkFooterLink( $footerLink, $params = null ) {
		if(empty($footerLink)) return true;
		$w = new Word();
		$w->load(array("label"=>$footerLink));
		$ret = $w->gotValue;

		unset( $w );
		return $ret;
	}

	public static function checkTitleParams( $param, $params = null ) {
		$type = new WordType( $_POST[ "type" ] );
		if($type->gotValue!==true) return false;

		$r = $type->requiresParameters==1 && empty( $param ) ? false : true;

		unset( $type );

		return $r;
	}

	public static function checkContent( $content, $params = null ) {
		$word = new Word( $_POST[ "wordId" ] );
		$type = $word->loadType();

		if($type->requiresParameters==1){
			$cparams = explode( ",", $word->titleParameters );
			foreach ( $cparams as $param ) {
				$rt = preg_match_all('|\{'.trim($param).'\}|',$content, $matches );
				if($rt != 1 )
					return false;
			}
		}

		return true;
	}

	public static function checkBody( $content, $params = null ) {
		$word = new Word( $_POST[ "wordId" ] );
		$type = $word->loadType();

		if ( $type->requiresBody == 1 && $type->requiresParameters == 1 ) {

			$cparams = explode( ",", $word->bodyParameters );

			foreach ( $cparams as $param ) {
				$rt = preg_match_all( '|\{' . trim( $param ) . '\}|', $content, $matches );
				if ( $rt != 1 )
					return false;
			}
		}

		return true;
	}

	public static function checkWordLabelUniqueness( $label, $params = null ) {
		$type   = new Word();
		$filter = array( "label" => $label );

		if ( isset( $_POST[ "id" ] ) )
			$filter[ "id" ] = "NOT IN " . $_POST[ "id" ];

		$type->load( $filter );
		$r = $type->gotValue;

		unset( $type );

		return !$r;
	}

	public static function checkBodyParams( $param, $params = null ) {
		$type = new WordType( $_POST[ "type" ] );
		if($type->gotValue!==true) return false;

		$r = $type->requiresParameters==1 && $type->requiresBody==1 && empty( $param ) ? false : true;

		unset( $type );

		return $r;
	}

	public static function checkEmptyPassForNewUser( $password, $params=null, User &$userObject ) {
		return $userObject->gotValue === true || !( empty( $password ) );
	}

	public static function checkExistingUsername( $username, $params=null  ) {
		$u = new User();
		$filter = array( "username" => "LIKE $username" );
		if(isset($_POST["id"]))
			$filter[ "id" ] = "NOT IN " . $_POST[ "id" ];

		$u->load( $filter );
		$ret = !$u->gotValue;
		unset( $u );
		return $ret;
	}

	public static function matchPasswordBig( $vl ){
		if(strlen(trim($vl))>15) return false;
		else return true;
	}

	public static function matchEqual( $vl1,$vl2 ){
		if( trim($vl1)==trim($_REQUEST[$vl2]) ) return true;
		else return false;
	}

	public static function matchEmail( $vl ){
		if (preg_match('/^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$/', $vl))
			return true;

		return false;
	}

	public static function matchMaxValue( $vl1,$vl2 ){
		if(strlen($vl1)>$vl2) return false;

		return true;
	}

	public static function matchBetween( $vl1,$vl2 ){
		$vl2 = explode(",",$vl2);

		if((intval($vl1)>=$vl2[0])&&(intval($vl1)<=$vl2[1])) return true;

		return false;
	}

	public static function matchMinValue( $vl1,$vl2 ){
		if(strlen($vl1)<$vl2) return false;

		return true;
	}



	public static function matchIfValueMatches( $vl1,$params ){
		$p0 = explode(",",$params);
		$vl2 = $p0[0];
		$match = $p0[1];
		if( ($_REQUEST[$vl2]==$match) && ($vl1=='') ) return false;
		else return true;
	}


	public static function matchTag($vl1){
		if(strlen(trim($vl1))==0) return true;
		if (!preg_match('/^([A-Za-z0-9-_şıüğçöŞİÜĞÖÇ,\\s]+)$/', $vl1) || !(strlen(trim($vl1))>2) )
			return false;

		return true;
	}

	public static function matchUsernameStyle($vl1){
		//if (preg_match('/[^\\$#A-Za-z0-9\\^_\\)\\(!@%\\[\\]{}\\.\\|~éß\\*şüğçöıŞÜĞÇÖİ£€\\-\\+]+/s', $vl1))
		//if (preg_match('/[^\\$#A-Za-z0-9\\^_\\)\\(!@%\\[\\]{}\\.\\|~éß\\*£€\\-\\+]+/s', $vl1))
//       if (preg_match('/[^A-Za-z0-9_-\.]+/s', $vl1))
		if (preg_match('/^([\\w-\._]+)$/', $vl1))
			return true;

		return false;
	}

	public static function matchNonMinus( $vl1 ){
		return !( (trim($vl1) == -1 ) || !(self::matchEmpty($vl1) )  );
	}


	public static function clearScript( $vl1 ){
		$result = preg_replace('/<script.*?>/sim', '', $vl1);
		return $result;
	}
	public static function matchScript($vl1){
		if (preg_match('/<script.*?>/sim', $vl1)) {
			return false;
		} else {
			return true;
		}

	}



	// END class FORM
}
