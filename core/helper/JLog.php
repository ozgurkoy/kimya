<?php
/**
* JLog class for Jeelet
*/

require_once $__DP.'core/def/logTypes.php';

class JLog
{

	private static $eventId;
	private static $level;
	public static $writeToFile = false;

	public static function startEvent($eventName)
	{
		if(!(self::$eventId>0) && JPDO::connect( JCache::read('mysqlDSN.LOG'),true )){
			JPDO::executeQuery("INSERT INTO `".JDef::$logeventtable."`(`eventName`,`startDate`) VALUES(?,NOW())",$eventName);
			self::$eventId = JPDO::lastId();
			JPDO::revertDsn();
			return true;
		}
		else return false;
	}

	public static function endEvent()
	{
		if((self::$eventId>0) && JPDO::connect( JCache::read('mysqlDSN.LOG'),true )){
			JPDO::executeQuery("UPDATE `".JDef::$logeventtable."` SET `endDate`=NOW() WHERE id=?",self::$eventId);
			self::$eventId = 0;
			JPDO::revertDsn();
			return true;
		}
		else return false;
	}

	public static function log($logType, $log, $level=0)
	{
		global $_logTypes;

		if(!array_key_exists($logType, $_logTypes)){
			return false;
		}
		
		if(self::$writeToFile){
		// try to log to filesystem
			try {
				$myFile = DOCUMENT_ROOT.'errors/log_'.date('Ymd_His').'_'.$logType.'_'.$level.'_'.JUtil::randomString(3, 'qwertyuopasdfghjklzxcvbnm1234567890').'.log';
				$fh = fopen($myFile, 'c+');

				$fContent = 'SERVER:' . print_r($_SERVER, true) . PHP_EOL;
				$fContent.= 'REQUEST:' . print_r($_REQUEST, true) . PHP_EOL;
				$fContent.= 'LOG:' . $log . PHP_EOL;
				$fContent.= 'BackTrace:' . print_r(debug_backtrace(), true) . PHP_EOL;

				fwrite($fh, '\xEF\xBB\xBF' . utf8_encode($fContent));
				fclose($fh);
			} 
			catch (Exception $e) {
				// continue;
			}
		}
		
		$c = JPDO::connect( JCache::read('mysqlDSN.LOG'),true );
		
		if($c){
			JPDO::executeQuery("
				INSERT INTO `".JDef::$logtable."` (
					`log`,
					`type`,
					`level`,
					`eventId`,
					`dateAdded`
				) VALUES(
					?,
					?,
					?,
					?,
					NOW()
				)
				", $log, $logType, $level, (self::$eventId > 0 ? self::$eventId : 0));
			JPDO::revertDsn();
		}
		else {
			JPDO::revertDsn();
			return false;
		}

	}

	public static function readByEvent($eventId)
	{
		if(JPDO::connect( JCache::read('mysqlDSN.LOG'),true )){
			$ks = JPDO::executeQuery("SELECT * FROM `".JDef::$logtable."` WHERE `eventId`=?",$eventId);
			JPDO::revertDsn();
			return $ks;
		}
		else{
			JPDO::revertDsn();
			return false;
		}
	}

	public static function readByDate($sd=null,$ed=null)
	{
		if(JPDO::connect( JCache::read('mysqlDSN.LOG'),true )){
			$q = array();
			if(!is_null($sd))
				$q[] = "`dateAdded` >= '$sd'";

			if(!is_null($sd))
				$q[] = "`dateAdded` <= '$ed'";

			$ks = JPDO::executeQuery("SELECT * FROM `".JDef::$logtable."` WHERE ".implode(" AND ",$q));
			JPDO::revertDsn();

			return $ks;

		}
		else{
			return false;
		}
	}

}
