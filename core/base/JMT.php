<?php
/**
 * base class for Jeelet models, the all extend this one.
 *
 * @package jeelet
 * @author Özgür Köy
 **/
class JMT
{
	/**
	 * field comparisons
	 *
	 * @var array
	 **/
	public static $_comparingClauses = array(
		'&lt'=>"<",
		'&le'=>"<=",
		'&ge'=>">=",
		'&gt'=>">"
	);

	/**
	 * constructor, which is called by the class that extends this oneu
	 *
	 * @return bool
	 **/
	public function __construct($id=null)
	{
		if(!is_null($id)){
			$this->load($id);

			if($this->id>0)
				return true;
		}
		return false;
	}
	
	/**
	 * take in relatives to load with
	 *
	 * @return Jmodel|bool
	 * @author Özgür Köy
	 */
	public static function with(JModel &$ins, $options)
	{
		if(!sizeof($ins->relativeMethods)>0) return false;
		
		foreach ($options as $relative) {
			if(array_key_exists($relative, $ins->relativeMethods)){
				$ins->_loadingRelatives[$relative] = $ins->relativeMethods[$relative];
			}
		}
		
		return $ins;
	}

	/**
	 * @param JModel $ins
	 * @param array  $clauses
	 * @param bool   $returnRelations
	 * @param bool   $indirect
	 * @return array|JModel
	 */
	public static function load(JModel &$ins, $clauses=array(), $returnRelations=false, $indirect=false)
	{
		$ins->loadParams      = $clauses;
		$ins->_clauseValues   = array();
		$ins->populated       = false;

		if ( $ins->groupOperation !== false )
			$ins->_originalClause = array();

		//prepare self fields.
		foreach ($ins->_fields as $sff)
			self::addIfNotInArray( $ins->_fieldsToLoad, $ins->selfname . "." . $sff );


        if( sizeof($clauses) ==0 && sizeof($ins->_originalClause)==0 ){
            $ins->buildQuery();


			// just put something
	        if(is_null($ins->customClause))
		        $ins->_originalClause[] = 1;
        }
        else if(is_null($ins->customClause)){
            self::clearSelf($ins);
            $ins->customClause = null;

            // numeric id, numeric or numerical array
            if(is_numeric($clauses))
                $clauses = array("id"=>$clauses);


            //prepare fields, clauses , joins
            foreach ($clauses as $field => $value) {
                $found     = true;
                $processed = false;
				//echo $field . PHP_EOL;
                if(isset($ins->_remoteConnections[$field])){
                    // relations are hard.
                    $remo      = $ins->_remoteConnections[$field];
					//print_r($remo);
                    // if(isset($remo["clause"]) && strlen($remo["clause"])>0)
                    // 	$processed = array( array($remo["clause"]."=?", $value) );
                    // else
                    // echo $remo["table"].PHP_EOL;
                    $processed = self::processValue($ins, $remo["table"], $field, $value, $remo["clause"]);
                    // print_r($processed);
                    if(sizeof($processed)>0){
                        if(strlen($remo["defClause"])>0)
                            self::addIfNotInArray($ins->_clauses, $remo["defClause"]);

                        self::addIfNotInArray($ins->_joins, $remo["join"]);
                    }
                    else
                        $found = false;
                }
                elseif(in_array($field,$ins->_fields)){
                    // check for local fields.
                    $processed = self::processValue($ins, $ins->selfname, $field, $value);
                }
                elseif($field=="_custom"){
	                $processed = array( array( $value, null ) );
                }
                else{
                    $found = false;
                }

                if($found){
					//echo "FIELD:{$field}" . PHP_EOL."PROCESSED:".PHP_EOL; print_r( $processed );
                    foreach ($processed as $proc) {
						//echo ">>" . PHP_EOL; var_dump( $proc[ 0 ] ); echo "<<" . PHP_EOL;
                        if($proc[0]!==false)
                            self::addIfNotInArray($ins->_clauses, $proc[0]);

                        // echo "PROC1".PHP_EOL;
                        // print_r($proc);

                        foreach((array)$proc[1] as $clauz) {
                            // echo ">>>".$clauz.PHP_EOL;
//							var_dump( $clauz );
	                        if ( !is_null( $clauz ) && $clauz !==false ) {
								//var_dump( $clauz );
		                        $ins->_clauseValues[] = $clauz;
	                        }
							//else echo 66;
                        }
                        // print_r($ins->_clauseValues);

                        // extra joins
                        if(isset($proc[2]) && $proc[2]!==false){
                            foreach((array)$proc[2] as $joinz)
                                self::addIfNotInArray($ins->_joins, $joinz);
                        }
                    }
                }
            }
            // for recursive calls from processValue
            if($returnRelations)
                return array("clauses"=>$ins->_clauses, "clauseValues"=>$ins->_clauseValues, "joins"=>$ins->_joins);

        }

		// prepare the query.
		$q = self::prepare($ins);
		//echo PHP_EOL.PHP_EOL.$q.PHP_EOL.PHP_EOL;
		JPDO::connect( JCache::read('mysqlDSN.'.$ins->dsn) );
		//echo "======================" . PHP_EOL; print_r( $ins->_clauseValues );
		array_unshift($ins->_clauseValues, null);
		$r0 = JPDO::executeQuery($q, $ins->_clauseValues);
		// echo ">>>>>>>>>>>>>>>>>>>>";
		// print_r($r0);
		// echo "<<<<<<<<<<<<<<<<<<<<<<<";
		$popd = self::populateData($ins, $r0);


		// var_dump($popd);


		//after load, populate..
		if($ins->nonPopulate==false)
			self::populate($ins);

		//we are done here, since groupOperation is a single result operation.
		if($ins->groupOperation === true){
			$ins->groupOperation = false;
			$ins->_extraFields = array();
		}


		return $ins;
	}
	
	/**
	 * process incoming value for fields
	 *
	 * @param mixed $value
	 * @param string $forceType
	 * @return array
	 * @author Özgür Köy
	 */
	private static function processValue(&$ins, &$relatedTable, &$field, &$value, &$passedClause=null)
	{
		//0->clause , 1->value
		$ret = array();
		
		// echo PHP_EOL."@@$relatedTable@@ @@$field @@ @@$value @@".PHP_EOL;
		
		if(is_string($value) || is_numeric($value)){
			if(!is_null($passedClause)/* && $relatedTable == $field*/){
				// echo "GOOD".PHP_EOL;
				return array( array($passedClause."=?", $value) );
			}
			else{
				// echo "@@".($passedClause)."@@".PHP_EOL;
				// echo PHP_EOL."@@$relatedTable@@ @@$field @@".PHP_EOL;
				return array( self::processString($field, $value, $relatedTable) );
			}
		}
		elseif(is_array($value) && self::isRelative($ins, $value, $field)){
			// relative array with like "owner"=>array("name"=>"\abc%")
			//echo PHP_EOL."@@$relatedTable@@ @@$field @@".PHP_EOL; //print_r( $value );
			$tm0 = array();
			//$cnt = 1 ;
			foreach ($value as $key => $val) {
				//echo "CNT : ".($cnt++) . PHP_EOL;
				$relationField = $ins->_relatives[$field];
				$claz          = ucfirst($relationField);
				$tmp           = new $claz();
				
				if(is_array($val)){
					// omg another relationship??
					//echo "CLAZ:$claz($relationField $field)".PHP_EOL."FROM REMOTE".PHP_EOL;
					$fromRemote = (array)self::load($tmp, array($key=>$val), true);
					//echo $field ."--". $key."--".print_r($value,true).PHP_EOL;
//					print_r($fromRemote);
//					exit;
					if(sizeof($fromRemote)>0){
						$cnMax = max( sizeof( $fromRemote[ "clauses" ] ), sizeof( $fromRemote[ "clauseValues" ] ), sizeof( $fromRemote[ "joins" ] ) );
//						foreach ($fromRemote["clauses"] as $tmIndex=>$tmRemote) {
						for($tmIndex=0 ; $tmIndex<$cnMax; $tmIndex++){
//							 echo $tmIndex.'<><>'.(isset($fromRemote["clauseValues"][$tmIndex])?$fromRemote["clauseValues"][$tmIndex]:false).PHP_EOL;
							//echo "REMOTE:" . PHP_EOL; echo ( isset($fromRemote["joins"][$tmIndex])?str_replace("`$relationField`.","`$field`.",$fromRemote["joins"][$tmIndex]):"---" ).PHP_EOL;
							//echo "TO REPLACE : `$relationField` WITH `$field`" . PHP_EOL;
							$tm0[] = array(
								isset($fromRemote["clauses"][$tmIndex])?str_replace("`$relationField`.","`$field`.",$fromRemote["clauses"][$tmIndex]):false,
								isset($fromRemote["clauseValues"][$tmIndex])?$fromRemote["clauseValues"][$tmIndex]:false,
								isset($fromRemote["joins"][$tmIndex])?str_replace("`$relationField`.","`$field`.",$fromRemote["joins"][$tmIndex]):false,
								// ^^^ replacing the relation name with the tag.
							);
						}
					}
					//print_r( $tm0 ); exit;
				}
				else{
					//echo PHP_EOL.$relatedTable."<>".$key."<>".$val.PHP_EOL;
					$tm1 = self::processValue($tmp, $relatedTable, $key, $val/*,$passedClause*/);
					$tm0[] = reset($tm1); // returning value will be array too.
				}
			}
			
			return $tm0;
		}
		elseif(is_object($value) && self::isRelative($ins, $value) && is_numeric($value->id) && $value->id>0){
			// relative object
			return array( array($passedClause."=?", $value->id) );
		}
		elseif(is_array($value) && sizeof($value)>0){
			//must be numeric id array
			return array( array("`".$relatedTable."`.`".$field."` IN (".(JUtil::multistr("?", sizeof($value))).")", $value) );
		}
		else
			return array();
	}

	/**
	 * process clause string
	 *
	 * @param string $field 
	 * @param mixed $value 
	 * @param string $relatedTable 
	 * @param bool $onlyClause 
	 * @return string|array
	 * @author Özgür Köy
	 */
	private static function processString(&$field, &$value, &$relatedTable, $onlyClause=false)
	{
		$ret = array();

		if(strpos($value, "LIKE ")!==false){
			// echo "MATCH % --:".substr($value, 5).PHP_EOL;
			$ret   = array("`$relatedTable`.`$field` LIKE ?", substr($value, 5));
		}
		elseif(preg_match('/.+?&or.+?/i', $value)==1){
			// 'abc' OR 'def'
			$tm0   = explode(' &or ', $value);
			$tmStr = $tmVal = array();

			foreach ($tm0 as $tm1){
				$tm1 = self::processString($field, $tm1, $relatedTable);

				$tmStr[] = $tm1[0];
				if(is_array($tm1[1])){
					foreach ( $tm1[ 1 ] as $tm2 ) {
						$tmVal[] = $tm2;
					}
				}
				else
					$tmVal[] = $tm1[1];
			}
//			print_r( $tmVal );
			$tmStr = "(".implode(" OR ", $tmStr).")";

			$ret = array($tmStr, $tmVal);
		}
		elseif(strpos($value, "IS NOT NULL")!==false){
			// echo "MATCH %:".$value.PHP_EOL;
			$ret   = array("`$relatedTable`.`$field` IS NOT NULL", array());
		}
		elseif(strpos($value, "IS NULL")!==false){
			// echo "MATCH %:".$value.PHP_EOL;
			$ret   = array("`$relatedTable`.`$field` IS NULL", array());
		}
		elseif(strpos($value,"BETWEEN")!==false){
			// BETWEEN 4,5
			$value = substr($value, 8);
			$value = explode( ",", $value );
			$ret   = array("(".$relatedTable.".".$field." BETWEEN ? AND ?)", $value);
		}
		elseif(strpos($value,"NOT IN")!==false){
			// BETWEEN 4,5
			$value = substr($value, 7);
			$value = explode( ",", $value );
			$ret   = array("(".$relatedTable.".".$field." NOT IN (".(JUtil::multistr("?", sizeof($value))).") )", $value);
//			print_r( $ret );
		}
		elseif(
			strpos($value,'&lt')!==false ||
			strpos($value,'&gt')!==false ||
			strpos($value,'&ge')!==false ||
			strpos($value,'&le')!==false
		){
			$tm1 = self::$_comparingClauses[ (substr($value, 0, 3)) ];
			$ret = array( "`$relatedTable`.`$field` $tm1 ?", strval(substr($value, 4, 10)) );
		}
		else{
			// echo "123>".$field;
			$ret = array($relatedTable.".".$field."=?", strval($value));
		}


		return $onlyClause?$ret[0]:$ret;
			
		
	}

	/**
	 * prepare query for execution
	 *
	 * @param object $ins 
	 * @return array
	 * @author Özgür Köy
	 */
	private static function prepare(&$ins)
	{
		// king
		$str   = array("SELECT ".(sizeof($ins->_joins)?"DISTINCT ":""));

		if ( $ins->groupOperation !== true ) {
			$str[] = implode(",", $ins->_fieldsToLoad);
		}

		// extra fields
		$str[] = sizeof( $ins->_extraFields ) > 0 ? ( ( $ins->groupOperation === true ? "" : "," ) . implode( ",", $ins->_extraFields ) ) : "";
		// main table
		$str[] = "FROM ".$ins->dbName.".".$ins->selfname." AS ".$ins->selfname;
		// join join join
		$str[] = implode(PHP_EOL,$ins->_joins);
		// clauses
		if ( !is_null( $ins->customClause ) ) {
			$str[] = "WHERE ".$ins->customClause;
		}
		elseif(sizeof($ins->_clauses)>0)
			$str[] = "WHERE ".implode(" AND ",$ins->_clauses);
		// group by?
		$str[] = strlen($ins->_groupBy)>0?"GROUP BY ".$ins->_groupBy:"";
		// having clauses
		if($ins->_having)
			$str[] = "HAVING ".$ins->_havingClause;
		// order by?
		$str[] = strlen($ins->_orderBy)>0?$ins->_orderBy:"ORDER BY {$ins->selfname}.id ASC";
		// limit
		if ( $ins->groupOperation !== true ) {
			$str[] = "LIMIT {$ins->limitLow},{$ins->limitCount}";
		}
		// echo implode("\n",$str);
		return implode("\n",$str);
	}

	/**
	 * add group by field field
	 *
	 * @param JModel $ins
	 * @param string $field 
	 * @param string $label 
	 * @return object
	 * @author Özgür Köy
	 */
	public static function groupByField(JModel &$ins, $type, $field, $label=null)
	{
		if ( is_null( $label ) ) $label = $field;

		if ( $type == "COUNT" )
			$ins->_extraFields[ $label ] = "COUNT(`{$ins->selfname}`.$field) `$label`";
		elseif ( $type == "SUM" )
			$ins->_extraFields[ $label ] = "SUM(`{$ins->selfname}`.$field) `$label`";

		return $ins;
	}


	/**
	 * place data from query to instance
	 *
	 * @param object $ins
	 * @param string $data 
	 * @return bool
	 * @author Özgür Köy
	 */
	private static function populateData(&$ins, &$data)
	{
		$ins->dataIndex = -1;
		$ins->_ids      = array();
		$ins->gotValue  = false;

		if(!is_array($data) || !(sizeof($data)>0)){
			// $ins->data
			$ins->dataIndex = null;
			return false;
		}

		$ins->data      = $data;
		if ( $ins->groupOperation !== true ) {
			foreach ( $data as $dInd => $data1 ) {
				//load ids
				$ins->_ids[ ] = $data1[ "id" ];
			}
		}

		return true;
		
	}

	/**
	 * populate data of the object
	 *
	 * @param bool $full load all relatives too
	 * @param bool $back backwards populating.
	 * @return bool|Jmodel
	 * @author Özgür Köy
	 */
	public static function populate(JModel &$ins, $full=false, $back=false)
	{
		if(is_null($ins->dataIndex)) {
			return false;
		}
		
		// echo 666;
		/*
		if( (!$back && $ins->dataIndex == sizeof($ins->_ids)-1) || 	// max index reached
			($back && $ins->dataIndex==0)) 					  		// index at minimum
			return false;
		*/


		if (
			(
				( !$back && $ins->dataIndex == sizeof( $ins->_ids ) - 1 ) || // max index reached
				( $back && $ins->dataIndex == 0 )
			)
			&&
			$ins->groupOperation === false
		) { // index at minimum

//			echo "POPPP";
			// self::clearSelf($ins);

			// echo "Loading more..".PHP_EOL;
			// return false;
			// $str[] = "LIMIT {$ins->limitLow},{$ins->limitCount}";

			/*doing extra population*/
			if ( $ins->_populateOnce === false && $ins->groupOperation !== true ) {
				if ( !$ins->gotValue ) // not populated return false
					return false;

				$ins->limitLow = $ins->limitLow + $ins->limitCount;
//				print_r( $ins->loadParams );

//				$ins->loadParams[ "id" ] = "IS NOT NULL"; //fix
				self::load( $ins, $ins->loadParams, false, true );

				if ( $ins->nonPopulate )
					self::populate( $ins ); // < required for reloads, other wise it will have the latest data

				if ( is_null( $ins->dataIndex ) )
					return false;

				return $ins;
			}
			else
				return false;


		}

		// else
		// 	return false;


		self::clearSelf($ins);

		JPDO::connect( JCache::read('mysqlDSN.'.$ins->dsn) );
		$ins->gotValue = true;
		$ins->dataIndex += ( $back && $ins->dataIndex != 0 ? -1 : 1 );


		/*rendering data below*/
		foreach ($ins->_fields as $_f) {
			if(isset( $ins->data[$ins->dataIndex][$_f] )){
				$ins->$_f = $ins->data[$ins->dataIndex][$_f];
			}
		}

		if ( sizeof( $ins->_extraFields ) > 0 ) {
			$extras = array_keys( $ins->_extraFields );
			foreach ( $extras as $_f ) {
				if ( isset( $ins->data[ $ins->dataIndex ][ $_f ] ) ) {
					$ins->$_f = $ins->data[ $ins->dataIndex ][ $_f ];
				}
			}
			unset( $extras );
		}

		if(property_exists($ins,"_relatives")){

			foreach ($ins->_relatives as $_f=>$_ff0) {
				if(isset( $ins->data[$ins->dataIndex][$_f] ))
					$ins->$_f = $ins->data[$ins->dataIndex][$_f];
			}

		}
		// if full , load connecteds
		if($full==true && property_exists($ins,"_relatives")){
			foreach ($ins->_relatives as $ps => $prel) {
				$ps = "load".ucfirst($ps);

				if( method_exists($ins, $ps) ){
					$ins->$ps();
				}
			}
		}
		
		if(sizeof($ins->_loadingRelatives)>0 && property_exists($ins,"_relatives")){
			foreach($ins->_loadingRelatives as $rfunction){
				call_user_func(array($ins, $rfunction));
			}
		}
		$ins->populated = true;

		return $ins;
	}

	/**
	 * to clear the variables and relatives before populate
	 *
	 * @return bool|void
	 **/
	public static function clearSelf(&$ins)
	{
		// return;
		if(!isset($ins->_fields) || !sizeof($ins->_fields)>0) return false;

		foreach ($ins->_fields as $_f) {
			$ins->$_f = null;
		}

		//$ins->groupOperation = false;

		if(property_exists($ins,"_relatives")){
			foreach ($ins->_relatives as $_f=>$_ff0) {
				$ins->$_f = null;
				// unset($ins->$_f);
			}
		}
	}

	/**
	 * @param JModel $ins
	 * @return JModel
	 */
	public static function resetIndex(JModel &$ins) {
		$ins->dataIndex = -1;
		self::clearSelf($ins);
		$ins->populate();
		return $ins;
	}

	/**
	 * count , can only be called if load is run
	 *
	 * @param JModel $ins
	 * @param string $groupBy
	 * @return object
	 */
	public static function count(JModel &$ins, $groupBy="id")
	{
		global $__DP;

		if(is_null($ins->customClause))
			$ins->buildQuery();
		$options = array(
			"docount" 	=> true,
			"groupby" 	=> null, //$groupBy;->wont work:)
			"clauses" 	=> $ins->customClause
		);

		include($__DP."/".JDef::$queryFolder.$ins->selfname.".php");
	  	$q = $ins->modelQuery($ins->customClause);

		JPDO::connect( JCache::read('mysqlDSN.'.$ins->dsn) );
		$r0 = JPDO::executeQuery($q);


		return (int)$r0[0]["cid"];
	}

	/**
	 * delete the current id only, or if an id or an array of ids
	 *
	 * @return bool
	 **/
	public static function delete(&$ins, $id=null)
	{
		global $__DP;
		if(is_null($id) && (!($ins->id>0) || is_null($ins->id))) return false;

		JPDO::connect( JCache::read('mysqlDSN.'.$ins->dsn) );
		
		if(is_array($id)){
			$ids = $id;
			$q   = $ins->modelQuery("deleteMulti");
			$q  .= " IN (".(JUtil::multistr("?", sizeof($id))).")";
		}
		else{
			$ids = $ins->id;
			$q   = $ins->modelQuery("delete");
		}
		// JPDO::debugQuery();
		
		JPDO::executeQuery($q, $ids);
		// JPDO::debugQuery(false);
		
		return true;
	}

	/**
	 * load 1-1 remote connection
	 *
	 * @param object $ins object instance
	 * @param string $objectType  the type of the relative object 
	 * @param string $tag the label of connection between two object types 
	 * @param bool $reverse if 1-N connection and this one is the N
	 * @return mixed
	 * @author Özgür Köy
	 */
	public static function loadRemote($ins, $objectType, $tag, $reverse=false)
	{
		if($reverse){
			if(!($ins->id>0) || is_null($ins->id)) {
				return false;
			}
		
			$cn = ucfirst($objectType);
			// echo print_r(array($tag=>array("id"=>$ins->id)), true);
			$ins->$tag = new $cn(array($tag=>array("id"=>$ins->id)));
			return $ins->$tag;
		}

		if(!isset($ins->$tag) ){
			// echo PHP_EOL.$tag;
			// print_r($ins);
			return false;
		}
		
		//already an object. return away
		if(is_object($ins->$tag))
			return $ins->$tag;

		if(is_numeric($ins->$tag) && $ins->$tag>0){
			$cn = ucfirst($objectType);
			$ins->$tag = new $cn($ins->$tag);
		}
		return $ins->$tag;
	}
	
	/**
	 * save remote 1-1 and 1-N
	 *
	 * @param object $ins object instance
	 * @param mixed $id id of the relative object, might be of many types
	 * @param string $objectType the type of the relative object
	 * @param string $tag the tag. basically the label of connection between two object types 
	 * @return object
	 * @author Özgür Köy
	 */
	public static function saveRemote(&$ins, $id, $objectType, $tag, $reverse=false)
	{
		global $__DP;
		if(!($ins->id>0) || is_null($ins->id)) {return false;}

		if(is_numeric($id)) $id = array( $id );

		if(is_array($id)) {
			//well , it's a reverse
			$q = $ins->modelQuery("save_".$tag);

			JPDO::connect( JCache::read('mysqlDSN.'.$ins->dsn) );
			foreach ($id as $idk) {
				if ( $reverse == false )
					JPDO::executeQuery( $q, $idk, $ins->id );
				else
					JPDO::executeQuery( $q, $ins->id, $idk );
			}
			JPDO::revertDsn();
		}
		elseif(is_null($id)){
			//id is null, we are using the already loaded object to save.
			if(!is_object($ins->$tag)) return false; // not defined
			
			$q = $ins->modelQuery("save_".$tag);
			
			if(!$reverse){
				JPDO::connect( JCache::read('mysqlDSN.'.$ins->dsn) );
				JPDO::executeQuery($q, $ins->$tag->id, $ins->id);
			}
			else{
				JPDO::connect( JCache::read('mysqlDSN.'.$ins->$tag->dsn) );
				JPDO::executeQuery($q, $ins->id, $ins->$tag->id);
			}
			JPDO::revertDsn();
		}
		elseif(is_object($id) && isset($id->selfname)){
			//an object is passed
			// echo 123;
			if(!($id->id>0)) return false;

			$q = $ins->modelQuery("save_".$tag);
			// JPDO::debugQuery();
			
			if(!$reverse){
				JPDO::connect( JCache::read('mysqlDSN.'.$ins->dsn) );
				JPDO::executeQuery($q, $id->id, $ins->id);
			}
			else{
				JPDO::connect( JCache::read('mysqlDSN.'.$id->dsn) );
				JPDO::executeQuery($q, $ins->id, $id->id);
			}
			JPDO::revertDsn();

			$ins->$tag = $id;
		}

		return $ins;
	}

	/**
	 * save the self fields, only for the current id, important.
	 *
	 * @return JModel|bool
	 **/
	public static function save(JModel &$ins)
	{
		global $__DP;

		if($ins->groupOperation === true)
			return $ins;

		JPDO::connect( JCache::read('mysqlDSN.' . $ins->dsn) );
		$good = false;

		if(is_array($ins->id)) {
			$ins->id = reset($ins->id);
		}

		if(($ins->id > 0) && isset($ins->_fields)){
			//editing
			$q          = $ins->modelQuery("edit");
			$fieldArray = self::buildFieldArray($ins, array("id"=>$ins->id));
			$k          = JPDO::executeQuery($q, array_values($fieldArray));

			$good = true;
		} 
		elseif(($ins->id == null) || ($ins->id == 0)) {
			//inserting
			$q          = $ins->modelQuery("save");
			$fieldArray = self::buildFieldArray($ins);
			$k          = JPDO::executeQuery($q, array_values($fieldArray));

			if(JPDO::lastId() > 0) {
				//just effect the active id. nice.
				$ins->id   = JPDO::lastId();
				$ins->_ids = array($ins->id);
				$good      = true;

			}
		}

		if($good == false) {
			//something went wrong
			JLog::log("user","insert failure:".$ins->selfname);
	 		return false;
		} 
		else 
			return $ins;
	}
	
	/**
	 * build field array
	 *
	 * @param object $ins 
	 * @return array
	 * @author Özgür Köy
	 */
	private static function buildFieldArray(&$ins, $append=array())
	{
		$c0 = array(null);
		if(!isset($ins->_fields) || !sizeof($ins->_fields)>0) {
			return array();
		}

		//read fields and fill an array
		for ($fi=2; $fi < sizeof($ins->_fields); $fi++) { 
			$_field = $ins->_fields[$fi];
			
			if(strlen($_field)==0)
				$_field = null;
			else
				$_field = $ins->_fields[$fi];
			if(is_array($ins->$_field))
				$c0[$_field] =  isset($ins->$_field[0]) ? $ins->$_field[0] : '';
			elseif(is_object($ins->$_field))
				$c0[$_field] =  isset($ins->$_field->id) ? $ins->$_field->id : '';
			else
				$c0[$_field] = $ins->$_field;
		}
		
		foreach($append as $ap=>$ed)
			$c0[$ap] = $ed; 
		
		return $c0;
	}

	/**
	 * save N-N's, prevent uniqueness for connector below
	 *
	 * @param object $ins related object
	 * @param mixed $id passed id, array or int
	 * @param string $type table name
	 * @param string $tag relation label
	 * @param bool $reverse reverse connection, changes the dsn.
	 * @return mixed
	 * @author Özgür Köy
	 */
	public static function saveMany(&$ins, $id, $type, $tag, $reverse=false)
	{
		if(!($ins->id>0) || is_null($ins->id)) return false;
		
		if($reverse)
			$dsn = is_object($ins->$tag) ? $ins->$tag->dsn : ($ins->getRelativeDsn($tag)) ;
		else
			$dsn = $ins->dsn;

		if(is_null($id) || !isset($id)){
			//need something
			return false;
		}
		elseif(is_numeric($id)){
			//some id is passed
			$cn = ucfirst($type);
			if(!is_null( $ins->$tag = new $cn($id) )){
				$q = $ins->modelQuery("save_".$tag);

				JPDO::$hideError = true;
				JPDO::connect( JCache::read('mysqlDSN.'.$dsn) ); // connector table is here..
				if($reverse)
					JPDO::executeQuery($q, $id, $ins->id, $id.".".$ins->id.".".$tag);
				else
					JPDO::executeQuery($q, $ins->id, $id, $ins->id.".".$id.".".$tag);
				
				JPDO::$hideError = false;
			}
			else return false;
		}
		elseif(is_array($id)){
			$q = $ins->modelQuery("save_".$tag);
			
			foreach ($id as $id0) {
				if(!is_numeric($id0)) continue;

				JPDO::connect( JCache::read('mysqlDSN.'.$dsn) ); // connector table is here..
				JPDO::$hideError = true;
//				JPDO::$debugQuery=true;
				if($reverse)
					JPDO::executeQuery($q, $id0, $ins->id, $id0.".".$ins->id.".".$tag);
				else
					JPDO::executeQuery($q, $ins->id, $id0, $ins->id.".".$id0.".".$tag);

//				JPDO::$debugQuery=false;
				JPDO::$hideError = false;
			}
		}
		elseif(is_object($id) && property_exists($id,"selfname") && $id->selfname==$type){
			//an object is passed
			if(!($id->id>0)) return false;
		 	$q = $ins->modelQuery("save_".$tag);

			foreach ($id->_ids as $id0){
				if(!is_numeric($id0)) continue;

				JPDO::connect( JCache::read('mysqlDSN.'.$dsn) ); // connector table is here..
				JPDO::$hideError = true;
				if($reverse)
					JPDO::executeQuery($q, $id0, $ins->id, $id0.".".$ins->id.".".$tag);
				else
					JPDO::executeQuery($q, $ins->id, $id0, $ins->id.".".$id0.".".$tag);
				
				JPDO::$hideError = false;
			}

			$ins->$tag = $id;
		}
		else 
			return false;
			//echo 55;
		
		return $ins;
	}

	/**
	 * delete 1-1, and 1-N. be careful, no validation->speed!
	 *
	 * @param object $ins   main object
	 * @param string $tag   relation tag
	 * @param bool   $reverse
	 * @param mixed  $id    optional id.
	 * @param bool   $one2N one2N ops.
	 * @return bool
	 * @author Özgür Köy
	 */
	public static function deleteOne(&$ins, $tag, $reverse=false, $id=null, $one2N=false)
	{
		global $__DP;

		if(is_array($ins->id)) $ins->id = reset($ins->id);

		if(!($ins->id>0) || is_null($ins->id)) return false;
		
		if($reverse){
			$q         = $ins->modelQuery("delete_".$tag);
			if($id=="all"){
				$q         = $ins->modelQuery("deleteAll_".$tag);
				$remoteId  = array($ins->id);
			}
			elseif(is_array($id)){
				$q	 	  .= " IN (".(JUtil::multistr("?", sizeof($id))).")";
				array_unshift($id, $ins->id);
				
				$remoteId  = $id;
			}
			elseif(is_numeric($id)){
				$q		  .= "=?";
				$remoteId  = array($ins->id, $id);
			}
			elseif(is_object($id)){
				$q		  .= "=?";
				$remoteId  = array($ins->id, $id->id);
			}
			elseif(is_null($id)){
				//supposed to have a loaded relative
				if($one2N){
					$q		  .= "=?";
					$remoteId  = array($ins->id, (is_object($ins->$tag) ? $ins->$tag->id  : $ins->$tag));
				}
				else
					$remoteId  = array(is_object($ins->$tag) ? $ins->$tag->id  : $ins->$tag);
			}
			
			$dsn = is_object($ins->$tag) ? $ins->$tag->dsn : ($ins->getRelativeDsn($tag)) ;
		}
		else{ 
			$q        = $ins->modelQuery("delete_".$tag);
			$remoteId = array( is_null($id) ? $ins->id : (is_object($id)?$id->id:$id) );
			$dsn      = $ins->dsn;
		}
		
		// jpdo execution fix
		array_unshift($remoteId,0);
		// JPDO::debugQuery();
		
		// echo $dsn;print_r($remoteId);echo $q;exit;
		// print_r( JCache::read('mysqlDSN.'.($reverse?$ins->dsn:( is_object($ins->$tag) ? $ins->$tag->dsn : "MDB" )) ) );
		JPDO::connect( JCache::read('mysqlDSN.'.$dsn ) );
		JPDO::executeQuery($q, $remoteId);
		// JPDO::debugQuery(false);
		
		// echo $q." ".$remoteId.PHP_EOL;
		
		$ins->$tag = null;
		
		JPDO::revertDsn();
		
		return true;
	}

	/**
	 * delete N-N
	 *
	 * @param object $ins main object
	 * @param string $tag relation tag
	 * @param mixed $id 
	 * @return bool
	 * @author Özgür Köy
	 */
	public static function deleteMany(&$ins, $tag, $id=null, $reverse=false)
	{
		if(!($ins->id>0) || is_null($ins->id) || !is_numeric($ins->id)) return false;
		
		$id       = is_null($id) ? array() : array($id);

		$q        = $ins->modelQuery("delete_".$tag,array("deleteAll"=>sizeof($id)==0));
		$_prevDsn = JCache::read('mysqlDSN.'.$ins->dsn);
		
		if($reverse)
			$dsn = is_object($ins->$tag) ? $ins->$tag->dsn : ($ins->getRelativeDsn($tag)) ;
		else
			$dsn = $ins->dsn;
		
		JPDO::connect( JCache::read('mysqlDSN.'.$dsn) );
		// JPDO::debugQuery();
		
		if(sizeof($id)>0){ 
			//delete one or more
			foreach ($id as $i0v) {
				if(is_object($i0v) && isset($i0v->id) && $i0v->id>0)
					$i0v = $i0v->id;
				
				if(!is_numeric($i0v)) continue;
				
				// if(!$reverse)
					JPDO::executeQuery($q, $ins->id, $i0v);
				// else
					// JPDO::executeQuery($q, $i0v, $ins->id);
			}
		}
		else{
			//delete all!
			// if($reverse)
				JPDO::executeQuery($q, $ins->id);
			// else
				// JPDO::executeQuery($q, $ins->id);
		}
		// JPDO::debugQuery(false);

		if(is_null($id))
			$ins->$tag = null;

		JPDO::connect($_prevDsn);
		return true;
	}

	/**
	 * count 1-Ns
	 *
	 * @return int
	 **/
	public static function __countMany(&$ins, $tag)
	{
		global $__DP;

		$docount[$tag] 	= true;
		if(!($ins->id>0)) return 0;

		$clauses[$ins->selfname] = "`".$ins->selfname."`=".$ins->id;

		include($__DP."/".JDef::$queryFolder.$ins->selfname.".php");
//		$q = $foreign_queries[$tag];

		JPDO::connect( JCache::read('mysqlDSN.'.$ins->dsn) );
		$r0 = JPDO::executeQuery($q);

		return (int)$r0[0]["cid"];
	}

	/**
	 * check if the object is relatiev
	 *
	 * @return bool
	 **/
	private static function isRelative(&$ins, &$obj, &$tag=null)
	{
		if( is_object($obj) && ((isset($obj->selfname) && in_array($obj->selfname, $ins->_relatives)) || method_exists($obj, "selfname")) ){
			if(isset($obj->selfname))
				$oself = $obj->selfname;
			else
				$oself = call_user_func(array($obj, 'selfname'));


			foreach($ins->_relatives as $rk=>$rv){
				if($rv==$oself)
					return $rk;
			}
		}
		elseif(is_array($obj) && isset($ins->_relatives[$tag])){
			return true;
		}
		return false;
	}

	/**
	 * just prepare the fields with specified tag for query
	 *
	 * @param string $fields 
	 * @param string $table 
	 * @return void
	 * @author Özgür Köy
	 */
	private static function prepareFieldsForQuery(&$fields,$tag)
	{
		foreach ($fields as $fk=>$fv) 
			$fields[$fk] = $tag.".".$fv;
	}
	
	/**
	 * add not present in array, incredibly life saving world changing method
	 *
	 * @param array $array 
	 * @param string $value 
	 * @return void
	 * @author Özgür Köy
	 */
	private static function addIfNotInArray(&$array, $value)
	{
		if(!in_array($value,$array))
			array_push($array,$value);
	}

} // END class JModel
?>