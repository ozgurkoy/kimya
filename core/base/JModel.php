<?php
/**
 * base class for Jeelet models, the all extend this one. only variables
 * todo : remove load params or _originalClause
 * @package jeelet
 * @author Özgür Köy
 **/
class JModel
{
	/**
	 * order
	 *
	 * @var string
	 **/
	public $_order;

	/**
	 * dsn
	 *
	 * @var string
	 **/
	public $dsn;
	
	/**
	 * load parameters
	 *
	 * @var array
	 **/
	public $loadParams=array();
	
	/**
	 * custom WHERE clause
	 *
	 * @var string
	 **/
	public $customClause = null;
	
	/**
	 * if it s returned any values
	 *
	 * @var bool
	 **/
	public $gotValue = false;

	/**
	 * original clause for further population
	 *
	 * @var null
	 */
	public $_originalClause = null;

	/**
	 * do not populate again.
	 *
	 * @var bool
	 */
	public $_populateOnce = true;

	/**
	 * ids object currently holding
	 *
	 * @var array
	 **/
	public $_ids=array();

	/**
	 * data variable which holds the recordsets
	 *
	 * @var array
	 **/
	public $data;

	/**
	 * date of adding the record
	 *
	 * @var string
	 **/
	public $jdate;

	/**
	 * current id
	 *
	 * @var int
	 **/
	public $id=null;

	/**
	 * selfname
	 *
	 * @var string
	 **/
	public $selfname=null;

	/**
	 * index of the data array
	 *
	 * @var int
	 **/
	public $dataIndex=null;

	/**
	 * variable to control populate upon load
	 *
	 * @var string
	 **/
	public $nonPopulate=false;

	/**
	 * real order integer
	 *
	 * @var string
	 **/
	private $_rorder=null;

	/**
	 *  relatives
	 *
	 * @var array
	 **/
	public $_relatives=array();

	/**
	 *  fields
	 *
	 * @var array
	 **/
	public $_fields=array();

	/**
	 *  fields
	 *
	 * @var array
	 **/
	public $_remoteConnections=array();

	/**
	 *  relativeMethods
	 *
	 * @var array
	 **/
	public $relativeMethods=array();

	/**
	 * loading relatives
	 *
	 * @var array
	 **/
	public $_loadingRelatives=array();
	
	/**
	 * fields to load 
	 *
	 * @var array
	 **/
	public $_fieldsToLoad=array();
	
	/**
	 * clauses to load 
	 *
	 * @var array
	 **/
	public $_clauses=array();

	/**
	 * values for clause
	 *
	 * @var array
	 **/
	public $_clauseValues=array(null);
	
	/**
	 * extra fields array
	 *
	 * @var array
	 **/
	public $_extraFields=array();
	
	/**
	 * joins to load 
	 *
	 * @var array
	 **/
	public $_joins=array();

	/**
	 * having or where
	 *
	 * @var bool
	 **/
	public $_having=false;

	/**
	 * group by probability
	 *
	 * @var string
	 **/
	public $_groupBy="";

	/**
	 * order by probability
	 *
	 * @var string
	 **/
	public $_orderBy="";
	
	/**
	 * having clause, if used with count
	 *
	 * @var string
	 **/
	public $_havingClause="";
	
	/**
	 * limit low
	 *
	 * @var int
	 **/
	public $limitLow=0;
	
	/**
	 * ever populated
	 *
	 * @var bool
	 **/
	public $populated=false;
	
	/**
	 * undocumented class variable
	 *
	 * @var string
	 **/
	public $limitCount=10;

	/**
	 * count and sum operations.
	 *
	 * @var bool
	 */
	public $groupOperation=false;

	/**
	 * constructor, which is called by the class that extends this one
	 *
	 * @param null $id
	 * @return \JModel
	 */
	public function __construct($id=null)
	{
		// echo "@".$this->selfname."@";

		if(!is_null($id)){
			JMT::load($this, $id);

			if($this->id>0)
				return true;
		}
		return false;
	}
	
	/**
	 * takes in array of arguements
	 *
	 * @param array $options
	 * @return JModel|bool
	 * @author Özgür Köy
	 */
	public function load($options=array())
	{
		$this->resetObject();
		return JMT::load($this, $options);
	}
	
	/**
	 * add count field, beware, no validation
	 *
	 * @param string $field 
	 * @param string|null $label label for the field
	 * @param array $options options like load.
	 * @return object
	 * @author Özgür Köy
	 */
	public function count($field, $label=null, $options=array())
	{
		$this->groupOperation  = true;
		JMT::groupByField($this, "COUNT", $field, $label);
		return JMT::load($this, $options);
	}

	/**
	 * add sum field, beware, no validation
	 *
	 * @param string $field
	 * @param string $label label for the field
	 * @param array  $options
	 * @return object
	 * @author Özgür Köy
	 */
	public function sum($field, $label=null, $options=array())
	{
		$this->groupOperation = true;
		JMT::groupByField($this, "SUM", $field, $label);
		return JMT::load($this, $options);
	}
	
	/**
	 * with function takes in strings as relative names.
	 *
	 * @return object
	 * @author Özgür Köy
	 */
	public function with()
	{
		$_f0 = func_get_args();
		
		JMT::with($this,$_f0);

		return $this;
	}
	
	/**
	 * save the object
	 *
	 * @return bool|JModel
	 * @author Özgür Köy
	 */
	public function save()
	{
		return JMT::save($this);
	}


    /**
     * build query for loading
     *
     * @return object
     * @author Özgür Köy
     **/
    public function buildQuery()
    {
        $c0 = array();

        if(!is_null($this->customClause)) return $this;

        //read fields
        if(!isset($this->_fields) || !sizeof($this->_fields)>0) return false;


        foreach ($this->_fields as $_field) {
//            echo $_field." ".$this->$_field.PHP_EOL;
            if(!is_null($this->$_field) && ((is_array($this->$_field)&&sizeof($this->$_field)>0) || (!is_array($this->$_field)&&strlen($this->$_field)>0)) ){

	            if($_field != "id" && $this->populated==false )
		             $this->_originalClause[$_field] = $this->$_field;

                $c0[] = "`".JUtil::tableName($this->selfname).
                    "`.$_field ".
                    (is_array($this->$_field)?"IN(".implode(",",JUtil::aposArray($this->$_field)).")":"='".addslashes($this->$_field)."'");
            }


        }
        if(sizeof($c0)>0){
            $this->customClause = implode(" AND ",$c0);
        }
        return $this;
    }


    /**
	 * having with grouped, be careful to use it with group
	 *
	 * @param string $clause 
	 * @return JModel
	 * @author Özgür Köy
	 */
	public function having($clause=null)
	{
		$this->_having = true;
		$this->_havingClause = $clause;

		return $this;
	}
	
	/**
	 * group by
	 *
	 * @param string $group 
	 * @return object
	 * @author Özgür Köy
	 */
	public function groupBy($group)
	{
		$this->_groupBy = $group;
		
		return $this;
	}
	
	/**
	 * order by
	 *
	 * @param string $order 
	 * @return object
	 * @author Özgür Köy
	 */
	public function orderBy($order=null)
	{
		$this->_orderBy = is_null($order)?"":"ORDER BY $order";
		
		return $this;
	}
	
	/**
	 * add extra fields, beware, no validation
	 *
	 * @param mixed $extra add extra fields to load
	 * @return JModel
	 * @author Özgür Köy
	 */
	public function extraField($extra)
	{
		if(is_array($extra)){
			foreach ($extra as $ex) {
				array_push($this->_extraFields, $ex);
			}
		}
		else
			array_push($this->_extraFields, $extra);
		
		return $this;
	}
	
	/**
	 * set fields of object
	 *
	 * @param array $fields 
	 * @return object
	 * @author Özgür Köy
	 */
	public function set($fields)
	{
		foreach ($fields as $ff => $fv) 
			$this->$ff = $fv;
		
		return $this;
	}
	
	/**
	 * create object with data only
	 *
	 * @param array $fields 
	 * @return JModel
	 * @author Özgür Köy
	 */
	public function __inject($fields)
	{
		foreach ($fields as $key => $value) {
			$this->$key = $value;
		}
		
		return $this;
	}
	
	/**
	 * limiter
	 *
	 * @return JModel
	 **/
	public function limit($low,$count=null)
	{
		$this->limitLow   = $low;
		$this->limitCount = is_null($count)?$this->limitCount:$count;
		
		return $this;
	}

	/**
	 * delete the current instance.
	 *
	 * @param null $id
	 * @return bool
	 * @author Özgür Köy
	 */
	public function delete($id=null)
	{
		return JMT::delete($this, $id);
	}

	/**
	 * disable onload population
	 *
	 * @return JModel
	 **/
	public function nopop()
	{
		$this->nonPopulate = true;
		return $this;
	}

	/**
	 * populus!
	 *
	 * @param bool $full
	 * @param bool $back
	 * @return Jmodel|bool
	 * @author Özgür Köy
	 */
	public function populate($full=false, $back=false)
	{
		return JMT::populate($this, $full, $back);
	}

	/**
	 * reset index.
	 *
	 * @return JModel
	 */
	public function resetIndex() {
		return JMT::resetIndex( $this );
	}

	/**
	 * reset index
	 *
	 * @return object
	 **/
	public function resetObject()
	{
		JMT::clearSelf($this);
		$this->dataIndex     = -1;
		$this->customClause  = null;
		$this->loadParams    = array();
		$this->_clauseValues = array();
		$this->_clauses      = array();
		//$this->limitCount    = 10;
		//$this->limitLow      = 0;

		return $this;
	}
	
	/**
	 * get relative's dsn
	 *
	 * @param string $tag 
	 * @return mixed
	 * @author Özgür Köy
	 */
	public function getRelativeDsn($tag)
	{
		if(isset($this->_relatives[$tag])){
			$d0  = ucfirst($this->_relatives[$tag]);
			return $d0::getDsn();
		}
		
		return false;
	}

	/**
	 * enable or disable populate more than once
	 *
	 * @param boolean $good
	 * @return JModel
	 */
	public function populateOnce( $good )
	{
		$this->_populateOnce = $good;

		return $this;
	}
	
	/**
	 * add custom clause
	 *
	 * @param string $customClause 
	 * @return JModel
	 * @author Özgür Köy
	 */
	public function customClause($customClause)
	{
		$this->customClause = $customClause;
		
		return $this;
	}

	/**
	 * abstract method
	 * @return string
	 */
	public function modelQuery( $type, $options=null ){ }


}
