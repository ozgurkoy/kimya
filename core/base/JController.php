<?php
/************************************
 *
 *		base controller class
 *		özgür köy
 *
 *		functions may return any other value by
 *			return array( "kontent"=>$kontent );
 *
 ************************************/


class JController
{
	//public $smarty ;
	public $kontent;
	public $set;

	//public function __construct( &$smarty , &$kontent )
	public function __construct( &$kontent )
	{
		$this->set = 1;
		$this->kontent =& $kontent;
	}

	public function setGlobal( $name, $val ){
		$_GLOBALS[ $name ] = $val;
	}


	public function __destruct() {

	}
}
