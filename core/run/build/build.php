<?php
require_once '../../../site/def/constants.php';
@rename($__DP.'site/def/state/building-0', $__DP.'site/def/state/building-1');
require_once $__DP.'core/run/exec.php';
require_once $__DP.'core/build/main.php';
?><!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>Building...</title>
</head>
<body>
<?php
	JLog::$writeToFile = false;
	$j = new JBuild();
	$j->buildMain(isset($_GET["doindex"]) ? $_GET["doindex"] : false);
	JLog::$writeToFile = true;
	
	// Tool::echoLog("Configuration file generated successfully<br /><hr />\n");
	// Tool::echoLog("All build operation completed succesfully");
	rename($__DP.'site/def/state/building-1', $__DP.'site/def/state/building-0');
	?>
</body>
</html>