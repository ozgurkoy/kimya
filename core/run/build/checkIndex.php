<?php
require_once '../../../site/def/constants.php';
require_once $__DP.'core/run/exec.php';
require_once $__DP.'core/lib/spyc/spyc.php';
if(isset($_POST['class'])){
	$indexArray 		 = Spyc::YAMLLoad(file_get_contents($__DP."site/def/config/index.yaml"));
	foreach ($indexArray as $ik => $iv) {
		if($ik!=$_POST['class']) unset($indexArray[$ik]);

	}

	if(sizeof($indexArray)==1){
		JIndex::_rebuildAllIndex( $indexArray );

		$pro = true;
	}
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN"
   "http://www.w3.org/TR/html4/strict.dtd">

<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>Building...</title>
	<meta name="generator" content="TextMate http://macromates.com/">
</head>
<body>
<?php

	$indexArray 		 = Spyc::YAMLLoad(file_get_contents($__DP."site/def/config/index.yaml"));
	$t0 = array_keys($indexArray);


	?>
	<h2>Site Admin - Rebuild index for class</h2>
	<form action="checkIndex.php" method="POST" accept-charset="utf-8">
		<select name="class" id="class">
			<?php foreach ($t0 as $t1 => $cls): ?>
				<option value="<?php echo $cls ?>"><?php echo $cls ?></option>

			<?php endforeach ?>

		</select>

		<?php if(isset($pro))
			echo "Index rebuilt for class";

		 ?>

		<p><input type="submit" value="Continue &rarr;"></p>
	</form>

</body>
</html>