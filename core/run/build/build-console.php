<?php
// $argv[1] : site document root olmak zorundadır
if (count($argv) < 2) {
	echo 'Hatalı kullanım! Gelen parametre sayısı:'.count($argv);
	die();
}
if (PHP_SAPI !== 'cli') {
	die('Hatalı kullanım! Komut satırından gelmelisin ;)');
}

$__DP = $argv[1].'/';
$debugLevel = isset($argv[2]) ? intval($argv[2]) : 0;

require_once $__DP.'site/cron/consumerConfig.php';
require_once $__DP.DOCUMENT_ROOT_PROD.'site/def/constants.php';
require_once $__DP.'core/run/exec.php';
require_once $__DP.'core/build/main.php';

Tool::echoLog("build started");
$j = new JBuild();
$j->buildMain(isset($_GET["doindex"]) ? $_GET["doindex"] : false);
Tool::compareLangWordsFromFile();
Tool::generateConfigfile();
Tool::echoLog("Configuration file generated successfully");
	
rename($__DP.'site/def/state/building-1', $__DP.'site/def/state/building-0');
Tool::echoLog("build finished");
?>