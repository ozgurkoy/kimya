<?php
/************************************
*
*    executioner
*
************************************/
require_once $__DP . 'core/run/inc.php';

ini_set('include_path', get_include_path().':/'.DOCUMENT_ROOT);

ini_set('memory_limit', '256M');
ini_set('gd.jpeg_ignore_warning', 1);

//init core defs
$_jd = new JDef();

if ( JCache::readStatik("siteVarsInited") == false ) {
	foreach ( $GLBS as $key=>$value )
		JCache::write($key,$value);
	JCache::write('siteVarsInited',1);
}

$_S = JCache::read('constants.siteAddress');

//die("Server Name:" . $_SERVER["SERVER_NAME"] . " S: " . SERVER_NAME);
//if(PHP_SAPI !== 'cli' && strpos($_S, "www")!==false && strpos($_SERVER["HTTP_HOST"], "www")===false){
//	header("Location:".$_S.$_SERVER['REQUEST_URI']);
//	exit;
//}

/*if(PHP_SAPI !== 'cli' && $_SERVER["SERVER_NAME"] != SERVER_NAME ){
	$loc = $_S.$_SERVER['REQUEST_URI'];
	$loc = str_replace("//", "/", $loc);
	$loc = str_replace("http:/".SERVER_NAME, "http://".SERVER_NAME, $loc);
	$loc = str_replace("https:/".SERVER_NAME, "https://".SERVER_NAME, $loc);
	header("Location:$loc");
	exit;
}*/

$JTS = microtime(true);

//cache init complete
//init smarty
JT::singleton();

// register autoload function
function __autoload($name) {
    global $__DP;
	if(strpos($name,"Zend_")===false)
		require_once $__DP.'site/model/'.JUtil::lcfirst($name).'.php';
}

$__LOADEDOB = array();

//session stuff
require_once $__DP.'core/lib/session/session.php';




//  Check Country BY IP

if(PHP_SAPI !== 'cli'){
    
//Check if this is the global site
if(SERVER_NAME == 'www.admingle.com' ){
    
    if (isset($_GET["r"]) && $_GET["r"] == '0')
    {    
      setcookie("nredeir", 1, time()+3600);  // expire in 1 week 
    }else{    

        //Check for Cookike
        if (!isset($_COOKIE["nredeir"])){

        // if cookie doesn't exist Check 

            //$ip = $_SERVER['REMOTE_ADDR'];

            $country = $_SERVER["GEOIP_COUNTRY_CODE"];

            switch ($country) {
            case 'GB':
                //redirect to UK
                header("Location: https://www.admingle.co.uk/?r=1");
                break;

            default:
                //Set Cookie 
                setcookie("nredeir", 1, time()+604800);  // expire in 1 week 
                break;
            }
        }

        }
    }
}
