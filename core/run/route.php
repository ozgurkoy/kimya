<?php
/*
	router
*/

if(strpos($_SERVER['QUERY_STRING'],"pushh=1")!==false) $_SESSION['pushh']=1;


if(JUtil::checkSiteHalt()==true &&  !isset($_SESSION['pushh'])){
	//$r01 = rand(3,15); sleep($r01);
	JUtil::siteHalt("upd01202");
}

if(!isset($_REQUEST["deff"])) $_REQUEST["deff"] = "";
if(!isset($_REQUEST["action"])) $_REQUEST["action"] = "";
//clean deff
if(strpos($_REQUEST["deff"],"/")==(strlen($_REQUEST["deff"])-1)) $_REQUEST["deff"] = substr($_REQUEST["deff"],0,strlen($_REQUEST["deff"])-1 );

//identify..
$_globParams = array();
// print_r($_REQUEST['deff']); die();

if( isset($_REQUEST["deff"]) && strlen($_REQUEST["deff"])>0 ){
	$action0		 = explode( "/",$_REQUEST["deff"] );
	$_cpath  = trim($action0[0]);

	if( isset($_routings[ trim($action0[0]) ]) ){
		$actionBody	= $_routings[ $action0[0] ];
	}
	else{
            
		JUtil::redirectHome();
	}
	array_shift($action0);
	$_globParams = $action0;

	foreach($_globParams as $glo=>$lob)
		$_globParams[$glo] = trim($lob);
}
else{
	//check here out
	$actionBody	= ($_REQUEST['action']?$_REQUEST['action']:"home_display");
	$mainPage		 = 1;
}

if(!(strpos($actionBody,"_")>0)) $actionBody .= "_";

$actionBody 	= explode(	"_", $actionBody );
$_globAction 	= $actionBody[0];
$_globJob		= $actionBody[1];

require_once $__DP.'core/base/JController.php';
if(!is_file($_routeBase.$_globAction.".php") ){
	if(!JUtil::isProduction())
		JUtil::configProblem( "controller $_globAction is missing." );
	else
		JUtil::siteHalt("RT");

	JLog::log("911","Missing controller class : ".$_routeBase.$_globAction.".php");

}
else
	require_once $_routeBase.$_globAction.".php";

$interfaceON	= ucFirst($_globAction)."_controller";
$kontent		= "";

if( !class_exists( $interfaceON ) /* CHECK USERNAME HERE !!!*/ ){
	JUtil::redirectHome();
}
else{

	$interfaceObj = new $interfaceON( $kontent );
	if(!method_exists($interfaceObj, $_globJob ) /*&& !isset($_SESSION['pushh'])*/ ){
		if(!JUtil::isProduction()){
			JUtil::configProblem( "controller $_globAction is missing." );
		}
		else{
			JUtil::siteHalt("RT");
			}
		JLog::log("911","Missing controller method : ".$interfaceON.">".$_globJob);
	}
	$return = (array) call_user_func_array( array( $interfaceObj, $_globJob ), $_globParams	);

	foreach ($return as $ret=>$urn)
		$$ret = $urn;
}
