<?php

include $__DP."/core/def/buildDef.php";

/**
 * Main Building function which inst. other sub classes and complete the process
 *
 * @package jeelet
 * @author Özgür Köy
 **/
class JBuild extends BuildDef
{

	/**
	 * siteArray holds the structure of the site, read&filled from a config file
	 *
	 * @var string
	 **/
	private $siteArray;

	/**
	 * index array
	 *
	 * @var string
	 **/
	public $indexArray=array();
	
	/**
	 * id data type
	 *
	 * @var string
	 */
	public $idDT = "int(11)";

	/**
	 * dsns
	 *
	 * @var string
	 **/
	private $dsnArray;

	/**
	 * constructor
	 *
	 * @return void
	 * @author Özgür Köy
	 **/
	public function __construct()
	{

	}

	/**
	 * main builder function_exists
	 *
	 * @return bool
	 * @author Özgür Köy
	 **/
	public function buildMain($doindex=false)
	{
		global $__DP;
		ini_set("max_execution_time", 3600);
		echo "<PRE>";

		$this->readYamls();

		//build site
		$this->checkNecessaryTables();

		JLog::startEvent("Building Site");

		$toRunConnections 	= array();
		$allFields			= array();
		$enums				= array();
		$allIndexes			= array();
		$altersToRun		= array();
		$indexToRun			= array();
		$fieldsToRemove		= array();
		$tableFields		= array();
		$dsns				= array();
		
		// check structure
		$this->validateYaml();
		// exit;
		foreach ($this->siteArray as $table => $tableConf) {
			if(!isset($tableFields[$table]))
				$tableFields[$table] = array();
			
			$tableName    = JUtil::tableName($table);
			$tableDSN     = $this->getDsn($table);
			$dsns[$table] = array($tableDSN, $this->getDSNDetails($tableDSN));
			
			$ordering          = false;
			
			if(!isset($allFields[$table]))
				$allFields[$table] = array("id");
			else
				$allFields[$table][] = "id";

			JPDO::connect( JCache::read('mysqlDSN.'.$tableDSN) );
			
			$tableExists = JPDO::tableExists( $tableName ) ;

			//create table if not exists
			if(!$tableExists) {
				$tableQuery= $this->sql('createTable',$tableName);

				JPDO::executeQuery($tableQuery);
				JLog::log("build", "created table $tableName", 1);
				$this->debugLog("created table $tableName");
				
				$prvFields 	= array();
				$indexes	= array();
			} 
			else{
				$indexes	= $this->getTableIndexes($tableDSN, $table);
				JPDO::connect( JCache::read('mysqlDSN.'.$tableDSN) );
				
				$prvFields 	= JPDO::listFields($tableName, true);
			}
			
			$allIndexes[$tableName] = $indexes;
			
			//check for ordering
			if(isset($tableConf["ordering"])){
				$ordering = true;
				unset($tableConf["ordering"]);
			}

			$this->createOnSiteArray($table, $tableName);
			
			//extra features
			$this->site[$table]["ordering"] = $ordering?1:0;
			$this->site[$table]["self"]		= $tableName;
			$this->site[$table]["dsn"]		= isset($this->dsnArray[$table]) && array_key_exists($table, $this->dsnArray) ? $this->dsnArray[$table] : "MDB";

												
			foreach ($tableConf as $field) {
				$fieldSql  			= null;
			 	$fieldType 			= isset($this->fieldTypes[$field["type"]]) ? $this->fieldTypes[$field["type"]] : null;
				$fieldDetails		= array();
				$defaultValue 		= isset($field["default"])?$field["default"]:null;
				$isPrepared 		= $this->prepareField($field, $table, $toRunConnections, $fieldType, $fieldDetails, $defaultValue);
				
				if($isPrepared){
					if($field["type"]=="1toN" || ($field["type"]=="1to1" && isset($field["owns"]))){
						//prepare field changes..
						$remoteDsn        = $this->getDsn($field["chain"]);

						JPDO::connect( JCache::read('mysqlDSN.'.$remoteDsn) );
						JPDO::$hideError = true;
						$eprvFields = JPDO::listFields($field["chain"], true);
						JPDO::$hideError = false;
						JPDO::revertDsn();

						if(!is_array($eprvFields) || !array_key_exists($field["tag"], $eprvFields)) // new field
							$altersToRun[] = array("addField", $field["chain"], $field["tag"], $fieldType, "", "");
						elseif(strtolower($eprvFields[$field['tag']]) != strtolower($fieldType)) // field type changed
							$altersToRun[] = array("editField", $field["chain"], $field["tag"], $field["tag"], $fieldType, "", "");

						// echo PHP_EOL.$field["tag"]."==".$field["chain"].PHP_EOL;
						
						if(!in_array($field["chain"], $tableFields[$table]))
							$tableFields[$field["chain"]][]          = $field["tag"];
						
						if(!in_array($field["chain"], $allFields[$table]))
							$allFields[$field["chain"]][]            = $field["tag"];
						
						$altTableName    = JUtil::tableName($field["chain"]);
						$this->createOnSiteArray($field["chain"]);

						$this->site[$field["chain"]]["fields"][] = $field["tag"];
						
						continue;
					}
					if(!in_array($field["tag"], $tableFields[$table]))
						$tableFields[$table][]        = $field["tag"];
					if(!in_array($field["tag"], $allFields[$table]))
						$allFields[$table][]          = $field["tag"];
					
					if(sizeof($fieldDetails)>0){
						$this->site[$table]["enums"][$field["tag"]] = $fieldDetails;
					}
					
					//prepare field changes..
					$collSql 		= (in_array($fieldType, $this->noCollationFieldTypes)===false)?($this->sql("collation")):"";
					$defValSql 		= is_null($defaultValue)?"":"DEFAULT '$defaultValue'";
					
					//check for new-previous field situation
					if(!is_array($prvFields) || !array_key_exists($field["tag"], $prvFields)) // new field
						$altersToRun[] = array("addField", $tableName, $field["tag"], $fieldType, $collSql, $defValSql);
					elseif(strtolower($prvFields[$field['tag']]) != strtolower($fieldType)){ // field type changed
						$altersToRun[] = array("editField", $tableName, $field["tag"], $field["tag"], $fieldType, $collSql, $defValSql);
					}
					else{
						// get default value
						$tableDSN = $this->getDsn($tableName);
						JPDO::connect( JCache::read('mysqlDSN.'.$tableDSN) );
						$defSql = $this->sql("getFieldDefault", $field["tag"], $tableName);
						$def0 = JPDO::executeQuery( $defSql );
						if($defaultValue != $def0[0]["default"]){
							if(strlen($defaultValue)>0)
								$altersToRun[] = array("editField", $tableName, $field["tag"], $field["tag"], $fieldType, $collSql, $defValSql);
							elseif(is_null($defaultValue) && strlen($def0[0]["default"])>0) // this one won't be used very often 
								$altersToRun[] = array("editField", $tableName, $field["tag"], $field["tag"], $fieldType, $collSql, "DEFAULT NULL");
							
						}
					}
					//set, to add to unique index.
					$indexToRun[] = array($field, $table, $toRunConnections, $allIndexes[$table]);
					
					//add to main fields
					$this->site[$table]["main"][] = $field["tag"];
					
					if( 
						!isset($field["chain"]) || 
						(isset($field["chain"]) && !isset($field["reverse"]) && $field["type"]=="1to1")
					) 
						// if(!in_array($field["tag"], $this->site[$table]["fields"][]))
							$this->site[$table]["fields"][] = $field["tag"];
				}
			}
			
			//clear previous fields
			$fieldsToRemove[$table]  = array($prvFields, $table, $tableDSN);
			$this->siteArray[$table] = $tableConf;
		}

		foreach ($altersToRun as $alt) {
			$tableDSN = $this->getDsn($alt[1]);
			$fieldSql = $this->sql($alt[0], $alt[1], $alt[2], $alt[3], $alt[4], $alt[5], isset($alt[6])?$alt[6]:null);
			
			// echo $fieldSql.PHP_EOL;
			if ( $alt[ 0 ] == "editField" && isset( $allIndexes[ $alt[ 1 ] ] ) ) {
				$db0 = $allIndexes[ $alt[ 1 ] ][ $alt[ 2 ] ];
				foreach($db0 as $db1){
					if(isset($db1["gotFK"])){
						JPDO::executeQuery( $this->sql( "dropColumnFK", $db1["db"], JUtil::tableName( $alt[ 1 ] ), $db1["label"] ) );

						break;
					}
				}
			}

			JPDO::connect( JCache::read('mysqlDSN.'.$tableDSN) );
			JPDO::executeQuery($fieldSql);
		}
		
		foreach ($indexToRun as $itr)
			$this->processFieldIndex($itr[0], $itr[1], $itr[2], $itr[3]);
		
		/*
		// remove prv fields
		foreach ($fieldsToRemove as $remoteTable=>$ftr) 
			$this->removePreviousFields($ftr[0], $ftr[1], $ftr[2], $tableFields[$remoteTable], $allIndexes[$remoteTable]);
		*/
		
		$classPile = array();

		include "$__DP/core/build/model.php";

		//run connections
		echo "<PRE>";
		$this->buildConnections($toRunConnections, $allFields, $allIndexes, $enums);
		
		//nice site indexing.
		// $this->buildSiteIndex($classPile);
		$jm = new JBuildModel();
		$jm->buildModel($this->site, $classPile, $dsns);
		
		// exit;

		// if($doindex==true)
		// 	JIndex::_rebuildAllIndex( $this->indexArray, array_keys($this->siteArray) );

		$this->debugLog("done! <br/> <hr />");
	}
	
	/**
	 * process field index
	 *
	 * @param string $field 
	 * @return bool
	 * @author Özgür Köy
	 */
	public function processFieldIndex(&$field, &$table, &$toRunConnections, $indexes)
	{
		$gotIndex = false;
		$fk       = array();
		if(!isset($indexes[$field["tag"]])) 
			return false;
		
		$indexes    = $indexes[$field["tag"]];
		$tableDSN   = $this->getDsn($table);
		$dsnDetails = $this->getDSNDetails($tableDSN);
		
		//do not create u. index for 1-1
		if( isset($field["index"]) &&
			in_array($field["index"], $this->indexes) &&
			!isset($toRunConnections[$table.".".$field["tag"]]) && 
			!in_array($field["tag"], $indexes)
		){
			if(sizeof($indexes)>0){
				foreach($indexes as $index){
					if(isset($index["gotIndex"]))
						$gotIndex = true;
					
					if(isset($index["isUnique"]) && $index["isUnique"]==1)
						$gotUnique = true;
					
					if(isset($index["gotFK"]))
						continue;
				}
			}
			
			//situazion
			if($field["index"]=="unique" && !isset($gotUnique)){ //< do not add unique to already active unique index. but you can add unique to non unique indexed fields
				$sql = $this->sql("uniqueIndex", 
									$dsnDetails["db"],
									$table, 
									$field["tag"], 
									$field["tag"]
								);
			}
			elseif($field["index"]=="index" && !isset($gotIndex)){
				$sql = $this->sql("addIndex" , 
									$dsnDetails["db"], 
									$table, 
									$field["tag"], 
									$field["tag"]
								);
			}
			else
				return false;
			

			$tableDSN 		= $this->getDsn($table);
			// print_r($tableDSN);
			JPDO::connect( JCache::read('mysqlDSN.'.$tableDSN) );
			JPDO::executeQuery($sql);
			
			return true;
		}
		
		$ret = array();
		
		if(isset($this->indexes[$field["tag"]])){
			
		}
		
		return null;
	}

	/**
	 * validate site.yaml
	 *
	 * @return void
	 * @author Özgür Köy
	 **/
	private function validateYaml()
	{
		$oppositeConnections = array();
		$owns                = array();
		$owning				 = array();
		$remoteConn			 = array();
		$conns				 = array();
		$gotError 			 = false;
		
		foreach ($this->siteArray as $table=>$tableConf) {
			if(in_array(strtoupper($table), $this->reservedKeywords)===true){
				JLog::log('cri', 'reserved field used :'.$table, 0);
				echo '<br />table structure is wrong: reserved keyword used as object name : '.$table;
				die();
			}
			
			foreach ($tableConf as $t0=>$field) {
				if(is_array($field)) {
					if(!array_key_exists('tag', $field)) {
//						print_r( $t0 );
						JLog::log('cri', 'Wrong site YAML structure; tag of table :'.$table, 0);
						echo '<br />'.$table.' structure is wrong: cannot read tag';
						die();
					}

					if(in_array(strtoupper($field["tag"]), $this->reservedKeywords)===true){
						JLog::log('cri', 'reserved field used :'.$field["tag"], 0);
						echo '<br />'.$table.' structure is wrong: reserved keyword used : '.$field["tag"];
						die();
					}
					
					if(isset($field["owns"]) && strpos($field["type"],"has lots")!==false){
						JLog::log('cri', 'Wrong site YAML structure; owns runs for 1-1 and 1-N only ('.$field["tag"].') :'.$table."-".$field["type"], 0);
						echo '<br />'.$table.'Wrong site YAML structure; owns runs for 1-1 and 1-N only ('.$field["tag"].') :'.$field["type"];
						die();
					}

					if(!array_key_exists('type', $field)) {
						JLog::log('cri', 'Wrong site YAML structure; type of table :'.$table, 0);
						echo '<br />'.$table.' structure is wrong: cannot read type of '.$field['tag'];
						die();
					}
					
					if(in_array(strtolower($field["tag"]), $this->reservedFields)===true){
						JLog::log('cri', 'reserved field used :'.$table, 0);
						print_r($field["tag"]);
						echo '<br />'.$table.' structure is wrong: reserved field used : '.$field['tag'];
						die();
					}
					
					if(!isset($field["type"])) {
						echo "field type missing {$tableName} {$field["tag"]} <br />";
						JLog::log("cri","field type missing {$tableName} {$field["tag"]} ", 1);
						die();
					}
				
					if($field["tag"]==$table) {
						echo "field tag({$field["tag"]}) can't be the same with the table <br />";
						JLog::log("cri","field tag({$field["tag"]}) can't be the same with the table", 1);
						die();
					}
				
					if(!is_string($field["type"])){
						echo("wrong configuration(check yaml file):field type problem ({$table}.{$field["tag"]})");
						exit;
					}
					
					if($field["type"]=="text" && isset($field["index"])){
						echo("wrong configuration(check yaml file):can't add index to text field({$table}.{$field["tag"]}, add it manually(index length and all).)");
						exit;
					}
					
					if($field["type"]=="text" && isset($field["default"])){
						echo("wrong configuration(check yaml file):text fields can't have default values.({$table}.{$field["tag"]}))");
						exit;
					}
					
					if(in_array($field["type"], array("select" ,"enum")) && !array_key_exists("options",$field)){
						echo("wrong configuration(check yaml file): must add `options` to select and enum. ({$table}.{$field["tag"]})");
						exit;
					}
					
					if(in_array($field["type"], array("int","float","bigint","tinyint1")) && isset($field["default"]) && !is_numeric($field["default"])){
						echo("wrong configuration(check yaml file): integer ,floats and boolean values can have integer only default values. ({$table}.{$field["tag"]})");
						exit;
					}
					
					if($field["type"]=="enum"){
						if(isset($field["default"]) && !in_array($field["default"], $field["options"])){
							echo("wrong default value for enum. ({$table}.{$field["tag"]})");
							exit;
						}
					}

					if($field["type"]=="select"){
						if(isset($field["default"]) && !array_key_exists($field["default"], $field["options"])){
							echo("wrong default value for select. ({$table}.{$field["tag"]})");
							exit;
						}
					}

					// validate connections..
					if(!is_null(($chain=$this->detectFieldConnection($field["type"])))){
						// echo $field["tag"]." ".$table ;print_r($chain);
						
						if(!isset($conns[$field["tag"]]))
							$conns[$field["tag"]] = array();
						if(isset($conns[$field["tag"]][$chain["chain"]])){
							echo "<strong>".$field["tag"]."</strong> is already connected to <strong>".$conns[$field["tag"]][$chain["chain"]]."</strong> by label <strong>".$field["tag"]."</strong>, choose another label to connect to the <strong>".$table."</strong><BR>";
							$gotError = true;
						}
						else
							$conns[$field["tag"]][$chain["chain"]] = $table;

						if($field["tag"]==$chain["chain"] && strpos($field["type"],"has lots")!==false){
							echo "N-N connections tag must be different from the target table, table:".$table." tag:".$field["tag"]."  target:".$chain["chain"]." <BR>";
							$gotError = true;
						}

						
						if(isset($field["unique"])){
							echo "connection fields can not be unique labeled.";
							die();
						}
						
						if(array_key_exists($chain["chain"], $owns) && isset($field["owns"])){
							echo "<h3>WARNING : ".$chain["chain"]." is already owned by ".$owns[$chain["chain"]].", {$table} is trying to own it again.</h3>";
							// die();
						}
						
						if(array_key_exists($chain["chain"].".".$field["tag"], $owning) && isset($field["owns"])){
							echo "Error : ".$chain["chain"]." is owned by another object({$owning[$chain["chain"].".".$field["tag"]]}) with the same label : {$field["tag"]}. one of them must be different yo.";
							die();
						}
						
						if(isset($oppositeConnections[$chain["chain"].".".$table]) && $oppositeConnections[$chain["chain"].".".$table]==$field["tag"]){
							echo "same labels({$field["tag"]}) can't be used for both tables({$chain["chain"]},$table)<br />";
							JLog::log("cri","same labels({$field["tag"]}) can't be used for both tables({$chain["chain"]},$table)<br />", 1);
							die();
						}
						
						if(isset($field["owns"])){
							$owns[ $chain["chain"] ]                     = $table;
							$owning[ $chain["chain"].".".$field["tag"] ] = $table;
							
							
							if($chain["type"]=="1to1"){
								if(!isset($remoteConn[ $chain["chain"] ]))
									$remoteConn[ $chain["chain"] ] = array();
								if(array_key_exists($field["tag"], $remoteConn[ $chain["chain"] ])){
									echo "<h3>Error : ".reset($remoteConn[ $chain["chain"] ])." is already connected to {$chain["chain"]} with {$field["tag"]} but <u>{$table}</u> is still trying to connect to the same object <br>with the same label. sorry, you must change it.</h3>";
									die();
								}
								else
									$remoteConn[ $chain["chain"] ][$field["tag"]]                = $table;
							}
							// echo $field["tag"]."___".$chain["chain"]."___".$table.PHP_EOL;
						}
						
						if($chain["type"]=="1toN"){
							if(!isset($remoteConn[ $chain["chain"] ]))
								$remoteConn[ $chain["chain"] ] = array();
							
							if(array_key_exists($field["tag"], $remoteConn[ $chain["chain"] ])){
								echo "<h3>Error : ".reset($remoteConn[ $chain["chain"] ])." is already connected to {$chain["chain"]} with {$field["tag"]} but <u>{$table}</u> is still trying to connect to the same object <br>with the same label. sorry, you must change it.</h3>";
								die();
							}
							else
								$remoteConn[ $chain["chain"] ][$field["tag"]]                  	 = $table;
						}
						
						
						$oppositeConnections[$table.".".$chain["chain"]] = $field["tag"];
					}
					elseif(!array_key_exists($field["type"], $this->fieldTypes)){
						echo '<br />'.$field["type"].' what kind of a field type is this? ';
						die();
					}
				}
				else{
					print_r($field);
					echo '<br />'.$table.' structure is wrong: invalid field definition';
				}
			}
		}
		if($gotError)
			die();
		
		// print_r($remoteConn);
	}
	/**
	 * check necessary functions , just at the start of the building
	 *
	 * @return bool
	 * @author Özgür Köy
	 **/
	public function checkNecessaryTables()
	{
		$this->debugLog("checking necessary tables...<BR>\n");

		$this->logTableSQLs = array(
			"logTable"=>$this->sql("logTable",self::$logtable),
			"logEventTable"=>$this->sql("logEventTable",self::$logeventtable)
		);
		$this->tableSQLs = array(
			"sessionsTable"=>$this->sql("sessionsTable",self::$sessionstable)
		);
		
		JPDO::connect( JCache::read('mysqlDSN.MDB') );
		foreach ($this->tableSQLs as $sql) {
			JPDO::executeQuery($sql);
		}
		
		JPDO::connect( JCache::read('mysqlDSN.LOG') );
		foreach ($this->logTableSQLs as $sql) {
			JPDO::executeQuery($sql);
		}

	}	
	
	/**
	 * table indexes
	 *
	 * @param string $db 
	 * @param string $table 
	 * @return void
	 * @author Özgür Köy
	 */
	public function getTableIndexes($db, $table)
	{
		$dsnDetails  = $this->getDSNDetails($db);
		
		JPDO::connect( JCache::read('mysqlDSN.'.$db) );
		$coin = JPDO::listIndexes(JUtil::tableName($table));
		// print_r($coin);
		$inr = $inl  = array();
		
		foreach ($coin as $coco) {
			if(!isset($inl[$coco["Column_name"]]))
				$inl[$coco["Column_name"]] = array();
			
			$i0 = array(
				"gotIndex" => true,
				"isUnique" => $coco["Non_unique"]==0,
				"db"       => $dsnDetails["db"],
				"label"    => $coco["Key_name"]
			);
			
			$inl[$coco["Column_name"]][] = $i0;
		}
		
		$sql = $this->sql("getAllFK", $dsnDetails["db"], $table);
		$ins = (array)JPDO::executeQuery($sql);
		foreach ($ins as $in) {
			$ins = array(
				"gotFK"   => true,
				"label"   => $in["CONSTRAINT_NAME"],
				"db"	  => $dsnDetails["db"],
				"rtable"  => $in["REFERENCED_TABLE_NAME"],
				"rcolumn" => $in["REFERENCED_COLUMN_NAME"]
			);
			if(!isset($inl[$in["COLUMN_NAME"]]))
				$inl[$in["COLUMN_NAME"]] = array();
			
			$inl[$in["COLUMN_NAME"]][] = $ins;
		}
		// echo $table.PHP_EOL;print_r($inl);
		return $inl;
	}
	
	/**
	 * get host and db name from the dsn info
	 *
	 * @param string $dsn 
	 * @return array
	 * @author Özgür Köy
	 */
	public function getDSNDetails($dsn)
	{
		$t0 = JCache::read('mysqlDSN.'.$dsn);

		if(preg_match_all('|.+:host=(.+?);dbname=(.+)|i', $t0["dsn"], $match, PREG_SET_ORDER)){	
			return array("host"=>$match[0][1], "db"=>$match[0][2]);
		}
		
		return array();
	}
	
	/**
	 * read yaml files
	 *
	 * @return void
	 * @author Özgür Köy
	 **/
	public function readYamls()
	{
		global $__DP;
		
		include "$__DP/core/lib/spyc/spyc.php";

		//read site config
		$tempFileContent = file_get_contents($__DP.'/site/def/config/site.yaml');
		//echo $tempFileContent;
		$this->siteArray = Spyc::YAMLLoad($tempFileContent);
		// echo '<pre>'; print_r($this->siteArray); echo '</pre>'; die();

		if(is_file("$__DP/site/def/config/dsn.yaml")) {
			$this->dsnArray = Spyc::YAMLLoad(file_get_contents("$__DP/site/def/config/dsn.yaml"));
		}
	}
	
	/**
	 * prepare field and values
	 *
	 * @param array $field 
	 * @param array $toRunConnections 
	 * @param string $fieldType 
	 * @return bool
	 * @author Özgür Köy
	 */
	public function prepareField(&$field, &$table, &$toRunConnections, &$fieldType, &$fieldDetails, &$defaultValue)
	{
		$tableDSN 		= $this->getDsn($table);
		// $defaultValue 	= isset($field["default"])?$field["default"]:null;
		
		//the IF
		if(!is_null(($chain=$this->detectFieldConnection($field["type"])))){
			$field["type"] 	= $chain["type"];
			$field["chain"]	= $chain["chain"];
			$fieldType 		= $chain["fieldType"];
			$reverse 		= $chain["reverse"];
			$owns			= isset($field["owns"]);
			
			
			$toRunConnections[$table.".".$field["tag"]] = array(
				$table.".".$field["chain"],
				$field["chain"].".id",
				$field["tag"],
				$field["type"],
				(isset($field["ordering"])&&$field["ordering"]==1?true:false),
				$tableDSN,
				$table,
				$owns
			);
			
			//no need to change any fields for this.
			if(is_null($fieldType))
				return false;
		}
		elseif($field["type"]=="select"){
			$fieldType = array();
			foreach ($field["options"] as $key => $value) {
				if(is_null($defaultValue)) $defaultValue = $key;
				$fieldType[]="'".$key."'";
			}

			$fieldType = "enum(".implode(",",$fieldType).")";
		}
		elseif($field["type"]=="enum"){
			$fieldType = array();

			foreach ($field["options"] as $key => $value) {
				if(is_null($defaultValue)) $defaultValue = $value;
				$fieldDetails[$key] = $value;
			}
			$fieldType = "int(3)";
		}
		elseif($field["type"]=="bool"){
			$fieldType = "tinyint(1)";
			if(is_null($defaultValue)) $defaultValue = 0;
		}
		return true;
	}

	/**
	 * detect and return field connection details
	 *
	 * @param string $fieldType 
	 * @return mixed
	 * @author Özgür Köy
	 */
	public function detectFieldConnection($fieldType)
	{
		foreach ($this->connectionStrings as $keyf => $keyft) {
			if(strpos($fieldType,$keyf." ")!==false) { // thin validation
				$chain = trim(str_replace($keyf.' ', '', $fieldType));
				
				return array(
					"chain"     => $chain, 
					"type"      => $keyft["label"], 
					"fieldType" => $keyft["fieldType"], 
					"reverse"   => isset($keyft["reverse"])
				);
			}
		}
		
		return null;
			
	}
	
	/**
	 * remove previous(not used) fields from table.
	 *
	 * @param array $prvFields 
	 * @param array $table 
	 * @return void
	 * @author Özgür Köy
	 */
	public function removePreviousFields($prvFields, $table, $tableDSN, $tableFields, $indexes)
	{
		$prvFields 	= array_keys($prvFields);
		
		JPDO::connect( JCache::read('mysqlDSN.'.$tableDSN) );
		
		$dsnDetails  = $this->getDSNDetails($tableDSN);
		
		// JPDO::connect( JCache::read('mysqlDSN.'.$dsnDetails["db"]) );
		$ins = (array)JPDO::executeQuery( $this->sql("findTargetFK", $dsnDetails["db"], $table) );
		
		//strict operation needed
		foreach ($prvFields as $pf) {
			JPDO::$hideError = true;
			$tIndex          = isset($indexes[$pf])?$indexes[$pf]:array();

			if(!in_array($pf,$this->reservedFields) && !in_array($pf, $tableFields)){
				foreach ($tIndex as $tin) {
					// remove owning FK
					if(isset($tin["gotFK"])){
						$sql = $this->sql("dropColumnFK", $tin["db"], $table, $tin["label"]);
						JPDO::executeQuery($sql);
					}
					// remove index
					elseif(isset($tin["gotIndex"])){
						$sql = $this->sql("dropIndex", $tin["db"], $table, $tin["label"]);
						JPDO::executeQuery($sql);
					}
					
				}
				
				//find targetted FK's
				foreach ($ins as $in) {
					$sql = $this->sql("dropColumnFK", $in["TABLE_SCHEMA"], $in["TABLE_NAME"], $in["CONSTRAINT_NAME"]);
					JPDO::executeQuery($sql);
				}
				
				$sql = $this->sql("dropField", $table, $pf);
				JPDO::executeQuery($sql);
			}

			JPDO::$hideError = false;
		}
	}

	/**
	 * get table fields
	 *
	 * @param array $allFields 
	 * @param string $table
	 * @return array
	 * @author Özgür Köy
	 */
	private function getFields($allFields, $table, $tag=null, $suf="")
	{
		// $suf = ($table == $tag ? "_t" : "");
		// create fields
		foreach ($allFields[$table] as $ff)
			$fieldsStr[] = is_null($tag)?$ff:("`$tag.$suf`.`$ff` `$tag".ucfirst($ff)."`");
		
		
		return $fieldsStr;
	}

	/**
	 * build process : build connection for the 2 objects
	 *
	 * @return bool
	 * @author Özgür Köy
	 **/
	public function buildConnections(&$toRunConnections, &$allFields, &$allIndexes, &$enums)
	{
		foreach ($toRunConnections as $con) {
			$source           = $con[0];
			$dest             = $con[1];
			$tag              = $con[2];
			$type             = $con[3]?$con[3]:"1to1";
			$ordering         = $con[4]?$con[4]:false;
			$dsn              = $con[5];
			$tableName        = $con[6];
			$owns             = $con[7];
			
			$tm0              = explode(".",$source);$sourceTable=$tm0[0];$sourceField=$tm0[1];
			$tm0              = explode(".",$dest);$destTable=$tm0[0];$destField=$tm0[1];
			$indexes          = $allIndexes[$sourceTable];
			$dsnDetails       = $this->getDSNDetails($dsn);
			$dsnRemote        = $this->getDsn($destTable);
			$dsnRemoteDetails = $this->getDSNDetails($dsnRemote);
			
			if($type=="1to1"){
				// echo PHP_EOL.$sourceTable."<<<<".PHP_EOL;
				
				if($owns){
					$tm0         = $sourceTable;
					$sourceTable = $destTable;
					$destTable   = $tm0;
					//re-build
					$indexes          = $allIndexes[$sourceTable];
					$dsnDetails       = $this->getDSNDetails($dsn);
					$dsnRemote        = $this->getDsn($destTable);
					$dsnRemoteDetails = $this->getDSNDetails($dsnRemote);
					
				}
				
				if( !isset($this->siteArray[$destTable]) && $destTable!="JDoc" ) die("Wrong dest table : ".$destTable);

				//add to relatives
				$this->site[$sourceTable]["relatives"][$tag] = $destTable;
				$this->site[$destTable]["relatives"][$tag]   = $sourceTable;

				//if it's the same table connection
				$suff                                        = $sourceTable==$destTable ? "_top" : "";
				$suffTableField                              = $tag==$sourceTable ? "_t" : "";
				// echo $sourceTable."<<<<".PHP_EOL;
				
				$fieldsStr                                   = $this->getFields($allFields, $sourceTable, $tag, $suffTableField);
				$remFields                                   = $this->getFields($allFields, $destTable, $tag);
				$remoteDsn                                   = $this->getDsn($destTable);
				$remoteDsnDetails                            = $this->getDSNDetails($remoteDsn);
				// echo $sourceTable;
				// print_r($fieldsStr);
														
				$this->site[$sourceTable]["joins"][$tag]     = array(
																"table"       => $sourceTable,
																"targetTable" => $destTable,
																"tag"         => $tag,
																"type"        => "1to1",
																"reverse"     => false,
																"11Reverse"   => $owns?true:false
																);

				$this->site[$sourceTable]["remote"][$tag]    = array(
																"join"      => $this->sql(
																	"innerJoin",
																	$dsnRemoteDetails["db"],
																	JUtil::tableName($destTable), 
																	$tag.$suffTableField,
																	JUtil::tableName($sourceTable), 
																	$tag,
																	$tag.$suffTableField,
																	"id"
																),
																"type"      => "1to1",
																"defClause" => "",
																"clause"    => $tag.$suffTableField.".id",
																"fields"    => implode(",",$fieldsStr),
																"table"     => $tag.$suffTableField,
																);

				if($destTable=="JDoc") return;
				
				//ADD TO REMOTE TABLE
				$this->site[$destTable]["joins"][$tag] 		 = array(
																"table"       => $sourceTable,
																"targetTable" => $sourceTable,
																"tag"         => $tag,
																"reverse"     => true,
																"type"        => "1to1"
																);
																// print_r($this->site[$destTable]["joins"][$tag]);
				$this->site[$destTable]["remote"][$tag] 	 = array(
																"join"      => $this->sql(
																	"innerJoin", 
																	$dsnDetails["db"],
																	JUtil::tableName($sourceTable), 
																	$tag.$suffTableField,
																	JUtil::tableName($destTable), 
																	"id", 
																	$tag.$suffTableField,
																	$tag 
																),
																"type"      => "1to1",
																"defClause" => "",
																"clause"    => $tag.$suffTableField.".".$tag,
																"table"     => $tag.$suffTableField,
																"fields"    => implode(",",$remFields)
																);

				
				// indexing and fk
				$hasIndex   = false;
				$hasFK      = false;
				$fkLabel    = $this->getFKLabel($sourceTable, $tag, $destTable/*, $owns*/);
				
				JPDO::connect( JCache::read('mysqlDSN.'.$dsn) );
				if(sizeof($indexes)>0 && isset($indexes[$tag])){
					foreach($indexes[$tag] as $index){
						if(isset($index["gotFK"])){
							
							// has a FK , check if it's ours..
							if($index["label"]==$fkLabel)
								$hasFK = true;
							else{
								// not ours, old school maybe. so? gotta remove it.
								JPDO::connect( JCache::read('mysqlDSN.'.$dsn) );
								JPDO::executeQuery( $this->sql("dropColumnFK", $index["db"], JUtil::tableName($sourceTable), $index["label"]) );
								
								JPDO::connect( JCache::read('mysqlDSN.'.$remoteDsn) );
								JPDO::executeQuery( $this->sql("dropColumnFK", $remoteDsnDetails["db"], JUtil::tableName($destTable), $index["label"]."_121reverse") );
								
								JPDO::connect( JCache::read('mysqlDSN.'.$dsn) );
							}
							// break;
						}
						if(isset($index["gotIndex"]))
							$hasIndex = true;
					}
				}
				
				if(!$hasIndex){
					$sql = $this->sql("addIndex", $dsnDetails["db"], JUtil::tableName($sourceTable), $tag, $tag);
					JPDO::executeQuery($sql);
				}
				if(!$hasFK){          /*($owns?"deleteFK":"updateFK")*/
					$sql = $this->sql(($owns?"deleteFK":"updateFK"), $dsnDetails["db"], JUtil::tableName($sourceTable), $fkLabel, $tag, $remoteDsnDetails["db"], JUtil::tableName($destTable), "id" );
					JPDO::executeQuery($sql);
				}
				JPDO::$hideError = true;
				JPDO::connect( JCache::read('mysqlDSN.'.$remoteDsn) );
				// if($owns){
				// 	echo $sourceTable;
				// 	echo $sql = $this->sql("deleteFK", $remoteDsnDetails["db"], JUtil::tableName($destTable), $fkLabel."_121reverse", "id", $dsnDetails["db"], JUtil::tableName($sourceTable), $tag );
				// 	JPDO::executeQuery($sql);
				// }
				// else{
				// 	echo $sourceTable;
				// 	echo $sql = $this->sql("dropColumnFK", $remoteDsnDetails["db"], JUtil::tableName($destTable), $fkLabel."_121reverse");
				// 	JPDO::executeQuery( $sql );
				// 	
				// 	JPDO::connect( JCache::read('mysqlDSN.'.$dsn) );
				// }
				JPDO::$hideError = false;
			}
			elseif($type=="1toN"){
				if( !isset($this->siteArray[$destTable]) && $destTable!="JDoc" ) die("Wrong dest table : ".$destTable);

				//add to relatives
				$this->site[$sourceTable]["relatives"][$tag] = $destTable;
				$this->site[$destTable]["relatives"][$tag]   = $sourceTable;
				$suffTableField                              = $tag==$sourceTable ? "_t" : "";

				$fieldsStr                                   = $this->getFields($allFields, $sourceTable, $tag, $suffTableField);
				$remFields                                   = $this->getFields($allFields, $destTable, $tag);


				//if it's the same table connection
				$suff                                        = $sourceTable==$destTable?"_top":"";
				$suffTableField                              = $tag==$destTable ? "_t" : "";
				
				$this->site[$sourceTable]["joins"][$tag]     = array(
																"table"       =>$sourceTable,
																"targetTable" =>$destTable,
																"tag"         =>$tag,
																"type"        =>"1toN"
																);
															
				$this->site[$sourceTable]["remote"][$tag] 	 = array(
																"join"		=> $this->sql(
																						"innerJoin",
																						$dsnRemoteDetails["db"],
																						JUtil::tableName($destTable),
																						$tag.$suffTableField,
																						JUtil::tableName($sourceTable),
																						"id",
																						$tag.$suffTableField,
																						$tag
																				),
																"type"      => "1toN",
																"defClause" => "",
																"clause"    => $tag.$suffTableField.".id",
																"fields"    => implode(",",$remFields),
																"table"     => $tag.$suffTableField,
																);
														
				if($destTable=="JDoc") return;
				
				//ADD TO REMOTE TABLE
				$this->site[$destTable]["joins"][$tag]  	 = array(
																"table"       => $destTable,
																"targetTable" => $sourceTable,
																"tag"         => $tag,
																"reverse"     => true,
																"1NReverse"   => true,
																"type"        => "1to1"
																);
															
				$this->site[$destTable]["remote"][$tag] 	 = array(
																"join"		=> $this->sql(
																						"innerJoin", 
																						$dsnDetails["db"],
																						JUtil::tableName($sourceTable), 
																						$tag.$suffTableField,
																						JUtil::tableName($destTable), 
																						$tag,
																						$tag.$suffTableField, 
																						"id" 
																			   ),
																"type"      => "1to1",
																"defClause" => "",
																"clause"    => $tag.$suffTableField.".id",
																"table"     => $tag.$suffTableField,
																"fields"    => implode(",",$fieldsStr)
																);

				// $this->site[$destTable]["fields"][] 		 = $tag;

				$indexes  = $allIndexes[$destTable];
				
				// indexing and fk
				$hasIndex = false;
				$hasFK    = false;
				$fkLabel  = $this->getFKLabel($destTable, $tag, $sourceTable, $owns);
				
				$remoteDsn        = $this->getDsn($destTable);
				$remoteDsnDetails = $this->getDSNDetails($remoteDsn);
				
				if(sizeof($indexes)>0 && isset($indexes[$tag])){

					JPDO::connect( JCache::read('mysqlDSN.'.$remoteDsn) );
					
					foreach($indexes[$tag] as $index){
						if(isset($index["gotFK"])){
							// has a FK , check if it's ours..
							if($index["label"]==$fkLabel){
								$hasFK = true;
							}
							else{
								// not ours, old school maybe. so? gotta remove it.
								echo $index["label"]." ".$fkLabel.PHP_EOL;
								echo $sql = $this->sql("dropColumnFK", $index["db"], JUtil::tableName($destTable), $index["label"]);
								JPDO::executeQuery( $sql );
							}
							// break;
						}
						if(isset($index["gotIndex"]))
							$hasIndex = true;
					}
				}
				
				if(!$hasIndex){
					$sql = $this->sql("addIndex",$remoteDsnDetails["db"], JUtil::tableName($destTable), $tag, $tag);
					JPDO::executeQuery($sql);
				}
				if(!$hasFK){
					$sql = $this->sql(($owns?"deleteFK":"updateFK"), $remoteDsnDetails["db"], JUtil::tableName($destTable), $fkLabel, $tag, $dsnDetails["db"], JUtil::tableName($sourceTable), "id" );
					JPDO::executeQuery($sql);
				}
				
			}
			elseif($type=="NtoN"){
				if(!isset($this->siteArray[$destTable]) && $destTable!="JDoc") die("Wrong dest table(something is really wrong) : ".$destTable);
				
				//add to relatives
				$this->site[$sourceTable]["relatives"][$tag] = $destTable;
				$this->site[$destTable]["relatives"][$tag]   = $sourceTable;

				// create fields
				// echo "FETCHING FIELDS FOR TABLE $tableName".PHP_EOL;
				
				$fieldsStr                                   = $this->getFields($allFields, $sourceTable, $tag);
				$remFields                                   = $this->getFields($allFields, $destTable, $tag);


				$joTable   = self::$prefix."C_".$sourceTable."_".$destTable;
				
				$joinSql   = $this->sql(
										"innerJoin", 
										$dsnDetails["db"],
										$joTable,
										$joTable."_".$tag,
										JUtil::tableName($sourceTable), 
										"id", 
										$joTable."_".$tag,
										JUtil::tableName($sourceTable)
									).
							" ".
							$this->sql(
										"innerJoin", 
										$dsnRemoteDetails["db"],
										JUtil::tableName($destTable), 
										$tag,
										$joTable."_".$tag,
										$sourceField, 
										$tag,
										"id"
									);
									
				$this->site[$sourceTable]["remote"][$tag] 		= array(
																	"join"=>$joinSql,
																	"defClause"=>"{$joTable}_{$tag}.tag='$tag'",
																	"clause"=>"$tag.id",
																	"type"=>"NtoN",
																	"fields"=>implode(",",$remFields),
																	"table"=>$tag
																);

				$this->site[$sourceTable]["connections"][$tag] 	= array(
																	"joinTable"=>$joTable,
																	"sourceTable"=>$sourceTable,
																	"targetTable"=>$destTable,
																	"ordering"=>$ordering,
																	"tag"=>$tag
																);
				//ADD TO REMOTE TABLE
				$joinSql	= $this->sql(
										"innerJoin", 
										$dsnDetails["db"],
										$joTable, 
										$joTable."_".$tag,
										JUtil::tableName($destTable), 
										"id", 
										$joTable."_".$tag,
										JUtil::tableName($destTable)
									).
							" ".
							$this->sql(
										"innerJoin", 
										$dsnDetails["db"],
										JUtil::tableName($sourceTable), 
										$tag,
										$joTable."_".$tag,
										$sourceTable, 
										$tag,
										"id"
									);
				
				$this->site[$destTable]["remote"][$tag] 		= array(
																	"join"=>$joinSql,
																	"defClause"=>"{$joTable}_{$tag}.tag='$tag'",
																	"clause"=>"$tag.id",
																	"type"=>"NtoN",
																	"fields"=>implode(",",$fieldsStr),
																	"table"=>$tag
																  );

				$this->site[$destTable]["connections"][$tag] 	= array(
																	"joinTable"=>$joTable,
																	"sourceTable"=>$sourceTable,
																	"targetTable"=>$destTable,
																	"ordering"=>$ordering,
																	"reverse"=>true,
																	"tag"=>$tag
																  );



				
				// stop for jdoc NOW!
				if($destTable=="JDoc") return;

				//no need to connect to self dsn again.
				JPDO::connect( JCache::read('mysqlDSN.'.$dsn) );
				$tableExists = JPDO::tableExists( $joTable ) ;
				
				if(!$tableExists){
					$remoteDsn	 = $this->getDsn($destTable);
					$remoteDsnDetails  = $this->getDSNDetails($remoteDsn);
					
					$destField = ($destTable.($sourceTable==$destTable?"_":""));
					$csqls = array(
						$this->sql("createJOTable", $joTable, $sourceTable, $destField),
						$this->sql("addIndex", $dsnDetails["db"], $joTable, $sourceTable, $sourceTable),
						$this->sql("addIndex", $dsnDetails["db"], $joTable, $destField, $destField),
						$this->sql("addIndex", $dsnDetails["db"], $joTable, "", "jdate"),
						$this->sql("deleteFK", $dsnDetails["db"], $joTable, $this->getFKLabel($sourceTable, $tag, $destTable, true), $destField, $remoteDsnDetails["db"], JUtil::tableName($destTable), "id" ),
						$this->sql("deleteFK", $dsnDetails["db"], $joTable, $this->getFKLabel($destTable, $sourceTable, $sourceTable, true), $sourceTable, $dsnDetails["db"], JUtil::tableName($sourceTable), "id" )
						
					);
					
					foreach ($csqls as $csql) {
						JPDO::connect( JCache::read('mysqlDSN.'.$dsn) );
						JPDO::executeQuery($csql);
					}
				}
			}
		}
	}
	
	/**
	 * build index from index.yaml
	 *
	 * @return void
	 * @author Özgür Köy
	 * TODO : VALIDATE INDEX
	 **/
	private function buildSiteIndex(&$classPile)
	{
		global $__DP;
		
		if(is_file("$__DP/site/def/config/index.yaml")  ){
			$this->indexArray = Spyc::YAMLLoad(file_get_contents("$__DP/site/def/config/index.yaml"));

			foreach ($this->indexArray as $ip => $indexe) {
				if(!array_key_exists($ip,$this->site)){
					JUtil::configProblem("bad class name : $ip");
				}
				// $classPile[$ip]["final"] = $classPile[$ip]["bridge"]  = array();

				foreach ($indexe as $indexName=>$iloot) {
					$bigBridge[$indexName] = $bigFinal[$indexName] = array();

					foreach ($iloot as $loot) {
						if(!in_array("fields",array_keys($loot))||!in_array("type",array_keys($loot))||!in_array("name",array_keys($loot))){
							JUtil::configProblem("missing definition for <strong>$indexName</strong>, fields, type and name keys are required");
						}

						foreach ($loot["fields"] as $ifindex=>$ifield) {
							// echo $ifindex;

							if(strpos($ifield,".")!==false){
								$i0 = explode(".",$ifield);
								$tempfclass = $ip;

								foreach ($i0 as $if0) {
									if(!isset($classPile[$tempfclass]["final"])) $classPile[$tempfclass]["final"] = array();
									if(!isset($classPile[$tempfclass]["bridge"])) $classPile[$tempfclass]["bridge"] = array();

									if(isset($this->site[$tempfclass]) && isset($this->site[$tempfclass]["relatives"][$if0])){
										//digging deeper.
										// if($tempfclass!=$ip)
											if(substr($ifield, strlen($ifield)-1, 1)=="!")
												$ifield = substr($ifield,0,strlen($ifield)-1);


											$classPile[$tempfclass]["bridge"][] 	= array("index"=>$ifindex,
																							"ifield"=>$ifield,
																							"action"=>$if0,
																							"type"=>$loot["type"],
																							"field"=>$loot["name"],
																							"indexName"=>$indexName,
																							"top"=>$ip
																							);

										$tempfclass = $this->site[$tempfclass]["relatives"][$if0];
									}
									elseif(in_array($if0, $this->site[$tempfclass]["fields"])||in_array($if0,array("id","jdate"))){
										//dug enough, found the field.
										$classPile[$tempfclass]["final"][] 	= array("index"=>$ifindex,
																					"ifield"=>$ifield,
																					"indexName"=>$indexName,
																					"type"=>$loot["type"],
																					"top"=>$ip,
																					"field"=>$loot["name"]
																					);

									}
									else{
										//problem, if not forced (!)
										if(substr($if0, strlen($if0)-1, 1)!="!"){
											JUtil::configProblem("Wrong relativity for class $tempfclass -> $if0 . <BR>
																  if you want to define a custom field, use a ! at the end of the field");
									    }
										else{
											//dug enough, found the field.
											$classPile[$tempfclass]["final"][] 	= array("index"=>$ifindex,
																						"ifield"=>(substr($ifield, strlen($ifield)-1, 1)=="!"?substr($ifield,0,  strlen($ifield)-1):$ifield),
																						"indexName"=>$indexName,
																						"type"=>$loot["type"],
																						"field"=>$loot["name"],
																						"top"=>$ip
																						);

											// $classPile[$tempfclass]["bridge"] 	= $bridges;


										}

									}

								}
							}
							elseif(in_array($ifield, $this->site[$ip]["fields"]) || in_array($ifield,array("id","jdate"))){
								$classPile[$ip]["final"][] 	= array("index"=>$ifindex,
																	"ifield"=>$ifield,
																	"indexName"=>$indexName,
																	"type"=>$loot["type"],
																	"top"=>$ip,
																	"field"=>$loot["name"]
																	);

							}
							elseif(isset($this->site[$ip]["relatives"][$ifield])){
									if(substr($ifield, strlen($ifield)-1, 1)=="!")
										$ifield = substr($ifield,0,strlen($ifield)-1);

									$classPile[$ip]["bridge"][] 	= array("index"=>$ifindex,
																					"ifield"=>$ifield,
																					"action"=>$ifield,
																					"type"=>$loot["type"],
																					"field"=>$loot["name"],
																					"indexName"=>$indexName,
																					"top"=>$ip
																					);

							}

							else{
								if(substr($ifield, strlen($ifield)-1, 1)=="!"){
									$classPile[$ip]["final"][] 	= array("index"=>$ifindex,
																		"ifield"=>substr($ifield,0, strlen($ifield)-1),
																		"indexName"=>$indexName,
																		"type"=>$loot["type"],
																		"top"=>$ip,
																		"field"=>$loot["name"]
																		);

							    }
								else{
									JUtil::configProblem("Wrong field definition : $indexName -> $ifield");
								}
							}
						}
					}
				}
				JIndex::buildIndexTable($ip,$indexe,$this->site[$ip]["dsn"]);
			}



			// print_r($this->indexArray);

		}
		
	}

	/**
	 * get dsn of taable
	 *
	 * @return string
	 * @author Özgür Köy
	 **/
	private function getDsn($table) {
		if(isset($this->dsnArray[$table]) && array_key_exists($table, $this->dsnArray)) {
			return $this->dsnArray[$table];
		} 
		else {
			return 'MDB';
		}
	}
	
	private function getFKLabel($source, $field, $remote, $cascade=false)
	{
		return substr(($cascade?"jfc_":"jfnc_").$source."_".$remote."_".$field,0,60);
	}
	
	private function createOnSiteArray($table)
	{
		if(isset($this->site[$table])) return false;
		
		$this->site[$table] = array(
								"main"        => array(),
								"fields"      => array(),
								"uniques"     => array(),
								"label"       => "",
								"joins"       => array(),
								"enums"       => array(),
								"connections" => array(),
								"relatives"   => array()
							  );
		
	}
	
	/**
	 * debug to screen
	 *
	 * @return void
	 * @author Özgür Köy
	 **/
	private function debugLog($log)
	{
		echo date("Y.m.d H:i:s") . " >>> " . $log . PHP_EOL;
	}
	
} // END class Jbuild