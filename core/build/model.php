<?php
/**
 * Builds the model and the base model files
 *
 * @package jeelet
 * @author Özgür Köy
 **/
class JBuildModel extends JDef
{
	/**
	 * site array
	 *
	 * @var array
	 **/
	private $site;

	/**
	 * index array
	 *
	 * @var array
	 **/
	private $index;
	
	/**
	 * relative array to collect.
	 *
	 * @var array
	 **/
	private $relatives=array();

	/**
	 * builder function
	 *
	 * @return bool
	 * @author Özgür Köy
	 **/
	public function buildModel($site, $index=null, $dsns)
	{
		//now building model
		$this->debugLog("building model...<BR>");
		
		global $__DP;
		
		//now building static queries
		$this->site  = $site;
		$this->index = $index;
		
		foreach ($this->site as $f => $inf) {
			$dsnDetails    = $dsns[$f];
			$modelBaseFile = $__DP.self::$mdlFolder."base/".$f.".php";
			$modelFile     = $__DP.self::$mdlFolder.$f.".php";
			$dsn           = $inf["dsn"];
			
			if($f=="JDoc") continue;

			ob_start();
echo '<?php
/**
 * '.$f.' generated on '.date('d-m-Y H:i',time()).'
 *
 * @package jeelet
 **/

class '.ucfirst($f).'_base extends JModel 
{
';
			$classHeader = ob_get_contents();
			ob_end_clean();

			$classFooter =
"

}
";

			//standart content
			ob_start();

if(sizeof($inf["enums"])>0){
	foreach ($inf["enums"] as $ekey=>$enu) {
		foreach ($enu as $ek=>$ev) {
echo 
'
	/**
	 * ENUM '.$ev.' of '.$ek.' 
	 *
	 **/
	const ENUM_'.strtoupper($ekey).'_'.strtoupper($ek).' = "'.$ev.'";
';
		}
	}
	
}

echo
'
	/**
	 * definition of the model
	 *
	 * @var string
	 **/
	public $selfname=\''.$f.'\';

	/**
	 * database name
	 *
	 * @var string
	 **/
	public $dbName=\''.$dsns[$f][1]["db"].'\';
';
echo
'
	/**
	 * dsn of the object
	 *
	 * @var string
	 **/
	public $dsn=\''.$inf["dsn"].'\';
';

if($inf["ordering"]==1){
	echo
'
	/**
	 * order of the current index
	 *
	 * @var string
	 **/
	public $_order=0;
';

}
// add 1-1
$added1s=array();

if(isset($inf["remote"]) && sizeof($inf["remote"]>0)){
		echo '	/**
	 * all remote info
	 * include remote table fields here, because it`s much faster than remote static method. might change in the future though
	 * @var array
	 **/
	public $_remoteConnections = array(';

	foreach($inf["remote"] as $tag=>$remote){
		// manually typing the values, for loop no likes
		echo '
		"'.$tag.'"=>array(
					"join"      =>"'.$remote["join"].'",
					"table"     =>"'.$remote["table"].'",
					"type"      =>"'.$remote["type"].'",
					"defClause" =>"'.$remote["defClause"].'",
					"clause"    =>"'.$remote["clause"].'"
					),';

			}
			echo PHP_EOL.'
	);';

}

if(sizeof($inf["joins"])>0){
	foreach ($inf["joins"] as $njoin) {
		$added1s[] = $njoin["tag"];

		echo
'
	/**
	 * '.$njoin["targetTable"].', 1-1 Connection object and an id when saving
	 *
	 * @var '.ucfirst($njoin["targetTable"]).'
	 **/
	public $'.$njoin['tag'].';
';
	}
}
// add N-N
if(sizeof($inf["connections"])>0){
	foreach ($inf["connections"] as $njoin) {
		echo
'
	/**
	 * '.$njoin['targetTable'].' '.$njoin['sourceTable'].', N-N Connection object
	 *
	 * @var '.ucfirst($njoin["targetTable"]).'
	 **/
	public $'.($njoin['tag']).';
';
	}
}

// add fields
if(sizeof($inf["fields"])>0){
	foreach ($inf["fields"] as $field) {
		if(in_array($field, $added1s)){
			continue;
		}

		echo
'
	/**
	 * '.$f.' field : '.$field.'
	 *
	 * @var string
	 **/
	public $'.($field).'=null;
';
	}
}
	// add main fields
	// if(sizeof($inf["fields"])>0){
			echo
	'
	/**
	 * fields of the object
	 *
	 * @var array
	 **/
	public $_fields = array(\'id\', \'jdate\', '.(sizeof($inf['fields'])?(implode(', ',JUtil::aposArray($inf['fields']))):'').',);
	';
	// }
// }

// add relatives
if(sizeof($inf["relatives"])>0){
	echo
'
	/**
	 * relatives of the object
	 *
	 * @var array
	 **/
	public $_relatives = array(';
	foreach ($inf["relatives"] as $rtag => $rval) {
		echo "'{$rtag}'=>'{$rval}', ";

	}
	echo');';

}

if(isset($this->index[$f])){
	//index entries
	echo
'

	/**
	 * index finals.
	 *
	 * @var array
	 **/
	public $indexFinal=array(
';

	foreach ($this->index[$f]["final"] as $finale) {
		echo
'		array(
			"class"=>"'.$finale["top"].'",
			"index"=>'.$finale["index"].',
			"indexDef"=>"'.$finale["ifield"].'",
			"indexField"=>"'.$finale["field"].'",
			"indexLabel"=>"'.$finale["indexName"].'",
			"type"=>"'.$finale["type"].'",
		),
';

	}
	echo
'	);
';

	echo
'

	/**
	 * index bridges.
	 *
	 * @var array
	 **/
	public $indexBridge=array(
';

	foreach ($this->index[$f]["bridge"] as $finale) {
		echo
'		array(
			"class"=>"'.$finale["top"].'",
			"action"=>"'.$finale["action"].'",
			"index"=>'.$finale["index"].',
			"indexDef"=>"'.$finale["ifield"].'",
			"indexField"=>"'.$finale["field"].'",
			"indexLabel"=>"'.$finale["indexName"].'",
			"type"=>"'.$finale["type"].'",
		),
';

	}
	echo
'	);
';
}

echo
'

	/**
	 * constructor of the class
	 *
	 * @return JModel
	 **/
	public function __construct($id=null)
	{
		parent::__construct($id);
	}
	
	/**
	 * get dsn of the object
	 *
	 * @author Özgür Köy
	 */
	// get type`s dsn info without initiating
	public static function getDsn()
	{
		return \''.$inf["dsn"].'\';
	}
	
	/**
	 * get sql of the object
	 *
	 * @param object $type 
	 * @param array $options 
	 * @return string
	 * @author Özgür Köy
	 */
	public function modelQuery($type, $options=null){
		if(is_null($options))
			$options=array();
			
		$deleteAll		= isset($options["deleteAll"])&&$options["deleteAll"]==true;
		
		$myQueries = array(
			"save"=>"';
				if($inf["ordering"] == 1) {
					echo 'INSERT INTO `'
				. JUtil::tableName($f)
				. '`(`id`,`jdate`,`jorder`'
				. (count($inf['fields']) > 0 ? ',' . implode(',', JUtil::aposArray($inf['fields'], '`')) : '')
				. ') SELECT NULL,UNIX_TIMESTAMP(),(SELECT max(`jorder`)+1 FROM  `'
				. JUtil::tableName($f)
				. '`),'
				. (count($inf['fields']) > 0 ? ',' . JUtil::multistr('?', sizeof($inf['fields'])) : '');
				} else {
					echo '
				INSERT INTO `' . JUtil::tableName($f) . '`(`id`,`jdate`'
				. (sizeof($inf['fields']) > 0 ? ',' . implode(',', JUtil::aposArray($inf['fields'], '`')) : '')
				. ') VALUES (NULL,UNIX_TIMESTAMP()'
				. (count($inf['fields']) > 0 ? str_repeat(',?', count($inf['fields'])) : '')
				. ')",';

				}
				echo '
					
			"delete"=>"DELETE FROM `'.JUtil::tableName($f).'` WHERE id=?",
				
			"deleteMulti"=>"DELETE FROM `'.JUtil::tableName($f).'` WHERE id ",//" IN(%s)",
				
			"edit"=>"UPDATE `'.JUtil::tableName($f).'` SET ';
				$t0 = array();
				foreach ($inf['fields'] as $mmain) {
					$t0[] = "`$mmain`=?";
				}
				echo implode(",", $t0) . ' WHERE `id`=?",'.PHP_EOL;
				
				/*
					1-1S
				*/
				if(sizeof($inf["joins"])>0){

					//save query
					foreach ($inf["joins"] as $nj) {
						if($nj["type"]=="1to1"){
							echo '
			"save_'.$nj["tag"].'"=>"UPDATE `'.$nj["table"].'` SET `'.$nj["tag"].'`=? WHERE id=?",'.PHP_EOL;
							echo '
			"delete_'.$nj["tag"].'"=>"UPDATE `'.$nj["table"].'` SET `'.$nj["tag"].'`=NULL WHERE id=?",'.PHP_EOL;
						}
						elseif($nj["type"]=="1toN"){
							echo '
			"save_'.$nj["tag"].'"=>"UPDATE `'.$nj["targetTable"].'` SET `'.$nj["tag"].'`=? WHERE id=?",'.PHP_EOL;
							echo '
			"deleteAll_'.$nj["tag"].'" => "UPDATE `'.$nj["targetTable"].'` SET `'.$nj["tag"].'`=NULL WHERE `'.$nj["tag"].'`=?",'.PHP_EOL;
							echo '
			"delete_'.$nj["tag"].'" => "UPDATE `'.$nj["targetTable"].'` SET `'.$nj["tag"].'`=NULL WHERE `'.$nj["tag"].'`=?  AND id ",'.PHP_EOL;
						}
					}
				}
				
				/*
					N-NS
				*/
				if(sizeof($inf["connections"])>0){
					foreach ($inf["connections"] as $nr) {
						echo
			'"save_'.$nr["tag"].'"=>';
						if($nr["ordering"]==1){
							echo
			'"INSERT INTO `'.$nr["joinTable"].'`(`'.$nr["sourceTable"].'`,`'.$nr["targetTable"].($nr["sourceTable"]==$nr["targetTable"]?"_":"").'`,`tag`,`jorder`,`jdate`,`uniqueID`)
				SELECT ?,?,"'.$nr["tag"].'",(SELECT max(`jorder`)+1 FROM  `".$nr["joinTable"]."`),UNIX_TIMESTAMP(), ?",
			';
						} 
						else {
							echo
			'"INSERT INTO `'.$nr["joinTable"].'`(`'.$nr["sourceTable"].'`,`'.$nr["targetTable"].($nr["sourceTable"]==$nr["targetTable"]?"_":"").'`,`tag`,`jdate`,`uniqueID`)
												VALUES(?,?,\''.$nr["tag"].'\',UNIX_TIMESTAMP(), ?)",'.PHP_EOL;
						}
					
						echo '
			"delete_'.$nr["tag"].'"=>"DELETE FROM `'.$nr["joinTable"].'` WHERE `tag`=\''.$nr["tag"].'\' AND `'.$nr[isset($nr["reverse"])?"targetTable":"sourceTable"].'`=?".($deleteAll?"":" AND `'.$nr[isset($nr["reverse"])?"sourceTable":"targetTable"].($nr["sourceTable"]==$nr["targetTable"]?'_':'').'`=?"),';
					}
				}
				echo'
		);
	
		return $myQueries[$type];
	}

	';
	/*
		1-1
	*/
	if(sizeof($inf["joins"])>0){
		
		foreach ($inf["joins"] as $njoin) {
			
			/*
			
			Previous one, used to use tags.
			
			$isReversed = isset($njoin["reverse"]);
			$methodPrefix = $isReversed ? (ucfirst($njoin["targetTable"])."of") : "";
			echo
'
			public function load'.$methodPrefix.ucfirst($njoin["tag"]).'(){
				
			*/
			//check for reverse loading, the case of being owned
			$isReversed                     = isset($njoin["reverse"]) && $njoin["reverse"]==true;
			$is1Reversed                    = isset($njoin["11Reverse"]) && $njoin["11Reverse"]==true;
			$methodName                     = $isReversed || $is1Reversed ? (ucfirst($njoin["targetTable"])) : ucfirst($njoin["tag"]);
			$this->relatives[$njoin["tag"]] = 'load'.$methodName;
			
			echo
'
	/**
	 * load '.$njoin["targetTable"].'
	 *
	 * @return '.$njoin["targetTable"].'
	 * @author Özgür Köy
	 */
	public function load'.$methodName.'('.($njoin["type"]=="1to1"?"":($njoin["targetTable"]!="JDoc"?'$id=null, $start=0, $count=50, $orderBy=null':'')).'){
		';
		if($njoin["targetTable"]!="JDoc"){
			if($njoin["type"]=="1to1"){
				echo
				'
			return JMT::loadRemote($this, "'.$njoin["targetTable"].'", "'.$njoin["tag"].'", '.($isReversed?'true':'false').');
				';
			}
			else{
				$targetClass = ucfirst($njoin["targetTable"]);
				echo
				'
		if(!($this->id>0)) return false;
		$t0 = new '.$targetClass.'();
		if(!is_null($id))
			$id["'.$njoin["tag"].'"] = $this->id;
		else
			$id = array("'.$njoin["tag"].'"=>$this->id);
			
		$this->'.$njoin["tag"].' = $t0->orderBy($orderBy)->limit($start,$count)->load($id);
		unset($t0);
		return $this->'.$njoin["tag"].';
				';
			}
		}
		else{
			echo
			'
		$this->'.$njoin["tag"].' = JDoc::load("'.$f.'", $this->id, "'.$njoin["tag"].'");
		return $this;
			';
		}
		echo
		'
	}
	
	/**
	 * save '.$njoin["targetTable"].'
	 *
	 * @param mixed $id 
	 * @return JModel|bool
	 * @author Özgür Köy
	 */
	public function save'.$methodName.'($id=null){
		';
		if($njoin["targetTable"]!="JDoc"){
		echo
		'
		// '.$njoin["type"].'
		return JMT::saveRemote($this, $id, "'.$njoin["targetTable"].'", "'.$njoin["tag"].'", '.((($isReversed||$njoin["type"]=="1toN")&&!isset($njoin["1NReverse"]))?'true':'false').');
		';
		}
		else{
		echo
		'
		if(is_null($id) || !isset($id["name"]) || (isset($id["name"]) && (is_array($id["name"])&&strlen($id["name"][0])<2)||(!is_array($id["name"])&&strlen($id["name"])<2))  ) return;

		//1-1, so delete previous :)
		$this->delete'.ucfirst($njoin["tag"]).'();
		JDoc::add("'.$f.'",$this->id,$id,"'.$njoin["tag"].'");
		$this->checkIndex("'.$njoin["tag"].'");
		return $this;
		';
		}
		echo
		'
	}
	';
	if($njoin["type"]=="1toN"){
	echo 
	'
	/**
	 * delete all '.$njoin["targetTable"].'
	 *
	 * @author Özgür Köy
	 */	
	public function deleteAll'.$methodName.'(){
		';
		// $njoin["type"]!="1toN"
		if($njoin["targetTable"]!="JDoc"){
			if($isReversed){
				echo
		'
		if(!(is_object($this->'.$njoin["tag"].') || strlen($this->'.$njoin["tag"].')>0))
			JMT::loadRemote($this, "'.$njoin["targetTable"].'", "'.$njoin["tag"].'",'.($isReversed?'true':'false').');';
			}
		echo
		'
		return JMT::deleteOne($this, "'.$njoin["tag"].'", '.((($isReversed||$njoin["type"]=="1toN")&&!isset($njoin["1NReverse"]))?'true':'false').', "all");
		';
		}
		else{
		echo
		'
		JDoc::clear("'.$f.'",$this->id,"'.$njoin["tag"].'");
		$this->checkIndex("'.$njoin["tag"].'");
		return $this;
		';
		}
		echo
		'
	}
	';
	}
	echo 
	'
	/**
	 * delete '.$njoin["targetTable"].' 
	 *
	 * @author Özgür Köy
	 */
	public function delete'.$methodName.'('.($njoin["type"]=="1toN"?'$id=null':'').'){
		';
		// $njoin["type"]!="1toN"
		if($njoin["targetTable"]!="JDoc"){
			if($isReversed){
				echo
		'
		if(!(is_object($this->'.$njoin["tag"].') || strlen($this->'.$njoin["tag"].')>0))
			JMT::loadRemote($this, "'.$njoin["targetTable"].'", "'.$njoin["tag"].'",'.($isReversed?'true':'false').');';
			}
		echo
		'
		return JMT::deleteOne($this, "'.$njoin["tag"].'", '.((($isReversed||$njoin["type"]=="1toN")&&!isset($njoin["1NReverse"]))?'true':'false').' '.($njoin["type"]=="1toN"?', $id, true':'').');
		';
		}
		else{
		echo
		'
		JDoc::clear("'.$f.'",$this->id,"'.$njoin["tag"].'");
		$this->checkIndex("'.$njoin["tag"].'");
		return $this;
		';
		}
		echo
		'
	}
';
		if( $njoin["type"]!="1toN" ){
	
		}
	}
}
/*
	N-NS
*/
	if(+sizeof($inf["connections"])>0){
		foreach ($inf["connections"] as $nr) {
			$isReversed                     = isset($nr["reverse"]);
			$methodPrefix                   = $isReversed ? (ucfirst($nr["sourceTable"])."of") : "";
			$targetClass                    = ucfirst($nr[$isReversed ? "sourceTable": "targetTable"]);
			$this->relatives[$nr["tag"]] 	= 'load'.$methodPrefix.ucfirst($nr["tag"]);
			
		echo
		'
	/**
	 * load n-n connection
	 *
	 * @param mixed $id 
	 * @param number $start 
	 * @param number $count
	 * @param string $orderBy
	 * @return '.$methodPrefix.$targetClass.'
	 * @author Özgür Köy
	 */
	public function load'.$methodPrefix.ucfirst($nr["tag"]).'('.($nr["targetTable"]!="JDoc"?'$id=null, $start=0, $count=50, $orderBy=null':'').'){
		';
		if($nr["targetTable"]!="JDoc"){
		echo
		'
		if(!($this->id>0)) return false;
		
		if(!is_null($id))
			$id["'.$nr["tag"].'"] = $this->id;
		else
			$id = array("'.$nr["tag"].'"=>$this->id);
		
		$t0 = new '.$targetClass.'();
		$t0 = $this->'.$nr["tag"].' = $t0->orderBy($orderBy)->limit($start,$count)->load($id);
		unset($t0);
		return $this->'.$nr["tag"].';
		';
		}
		else{
		echo
		'
		$this->'.$nr["tag"].' = JDoc::load("'.$f.'",$this->id,"'.$nr["tag"].'");
		return $this;
		';
		}
		echo
		'
	}
	
	/**
	 * count the object
	 *
	 * @param object $type 
	 * @param array $options 
	 * @return string
	 * @author Özgür Köy
	 */
	public function count'.$methodPrefix.ucfirst($nr["tag"]).'(){
		';
		if($nr["targetTable"]!="JDoc"){
		echo
		'
		return JMT::countMany($this, "'.$nr["tag"].'");
		';
		}
		else{
		echo
		'
		return JDoc::count("'.$f.'",$this->id,"'.$nr["tag"].'");
		';
		}
		echo
		'
	}
	
	/**
	 * save '.$njoin["targetTable"].'
	 *
	 * @param mixed $id 
	 * @return JModel|bool
	 * @author Özgür Köy
	 */
	public function save'.$methodPrefix.ucfirst($nr["tag"]).'($id=null){
		';
		if($nr["targetTable"]!="JDoc"){
			echo
		'
		return JMT::saveMany($this, $id, "'.$nr[$isReversed?"sourceTable":"targetTable"].'", "'.$nr["tag"].'", '.($isReversed?'true':'false').');
		';
		}
		else{
			echo
		'
		JDoc::add("'.$f.'",$this->id,$id,"'.$nr["tag"].'",'.($nr["ordering"]==1?"true":"false").');
		$this->checkIndex("'.$nr["tag"].'");
		return $this;
		';
		}
			echo
		'
	}

	/**
	 * delete '.$njoin["targetTable"].'
	 *
	 * @param mixed $id 
	 * @return bool
	 * @author Özgür Köy
	 */
	public function delete'.$methodPrefix.ucfirst($nr["tag"]).'($id){
		if(!isset($id) || is_null($id)) return false;
		';
		if($nr["targetTable"]!="JDoc"){
		echo
		'
		return JMT::deleteMany($this, "'.$nr["tag"].'", $id, '.($isReversed?'true':'false').');
		';
		}
		else{
		echo
		'
		JDoc::clear("'.$f.'",$this->id,"'.$nr["tag"].'", $id);
		$this->checkIndex("'.$nr["tag"].'");
		return $this;
		';
		}
		echo
		'
	}
	
	/**
	 * delete all '.$njoin["targetTable"].'
	 *
	 * @return bool
	 * @author Özgür Köy
	 */
	public function deleteAll'.$methodPrefix.ucfirst($nr["tag"]).'(){
		';
		if($nr["targetTable"]!="JDoc"){
		echo
		'
		return JMT::deleteMany($this, "'.$nr["tag"].'", null, '.($isReversed?'true':'false').');
		';
		}
		else{
		echo
		'
		JDoc::clear("'.$f.'",$this->id,"'.$nr["tag"].'");
		$this->checkIndex("'.$nr["tag"].'");
		return $this;
		';
		}
		echo
	//clause , docount, groupby, orderby, limit
			'
	}

';
			}
		}

	$classBody = ob_get_contents();
	ob_end_clean();


	if(!is_writable($__DP.self::$mdlFolder."base/")){
		JUtil::configProblem("can't write to model dir:".$modelBaseFile);
	}

	if(is_file($modelBaseFile) && !is_writable($modelBaseFile)){
		JUtil::configProblem("can't write to model file:".$modelBaseFile);
	}

	$fp = fopen($modelBaseFile, 'w+');
	
	if(sizeof($this->relatives)>0){
		$tf = 
	'
	/**
	 * relative methods 
	 *
	 * @var array
	 **/
	public $relativeMethods = array(';
	foreach ($this->relatives as $rtag => $rval) {
		$tf.="'{$rtag}'=>'{$rval}', ";

	}
	$tf .=');
	';
		
		$classFooter = $tf.$classFooter;
	}
	
	fwrite($fp, $classHeader.$classBody.$classFooter);
	fclose($fp);

	//chmod($modelBaseFile, 0777);
	if(!is_file($modelFile)){
		//extender file
		$ex =
	'<?php
/**
 * '.$f.' real class - firstly generated on '.date('d-m-Y H:i',time()).', add edit anyway you like wont be touched over , ever again.
 *
 * @package jeelet
 **/
include $__DP.\'/site/model/base/'.$f.'.php\';

class '.ucfirst($f).' extends '.ucfirst($f).'_base
{
	/**
	 * constructor for the class
	 *
	 * @return void
	 **/
	public function __construct($id=null) {
		parent::__construct($id);
	}

}

';
				if(!is_writable($__DP.self::$mdlFolder)){
					JUtil::configProblem("can't write to model dir:".$modelFile);
				}

				if(is_file($modelFile) && !is_writable($modelFile)){
					JUtil::configProblem("can't write to query file:".$modelFile);

				}

				$fp = fopen($modelFile, 'w+');
				fwrite($fp, $ex);
				fclose($fp);

				chmod($modelFile, 0777);

		}

		}

	}
	
	
	/**
	 * debug to screen
	 *
	 * @return void
	 * @author Özgür Köy
	 **/
	private function debugLog($log)
	{
		echo date("Y.m.d H:i:s") . " >>> " . $log . PHP_EOL;
	}
	
}