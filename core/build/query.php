<?php
/**
 * Fill the query files, inst. by the JBuild
 *
 * @package jeelet
 * @author Özgür Köy
 * @author tiger
 **/
class JBuildQuery extends JDef
{
	/**
	 * site array
	 *
	 * @var array
	 **/
	private $site;
	/**
	 * building queries
	 *
	 * @return void
	 **/
	public function buildQueries($site)
	{
		global $__DP;
		//now building static queries
		$this->site = $site;
		echo "<hr />\n";
		$this->debugLog('generating queries for: ');
		JLog::log('build', 'generating queries', 1);

		foreach ($this->site as $f => $inf) {
			$queryFile = $__DP . self::$queryFolder . $f . '.php';
			if($f == 'JDoc') {
				continue;
			}
			$fss = "";
			
ob_start();
echo "<?php //generated queries

\$main_queries['".$f."']=\"
SELECT \".(isset(\$fieldsToLoad['".$f."'])? (\"`jorder`,\".implode(',',\$fieldsToLoad['".$f."'])) : ((isset(\$docount['".$f."'])?\"count(*) cid\":\" id,`jdate`,`jorder`".
(sizeof($inf["main"])>0? (",".implode(", ", JUtil::aposArray($inf["main"],"`"))):"").
" \"))).\"
\".(isset(\$extraFields['".$f."']) && is_array(\$extraFields['".$f."'])?\",\".implode(',',\$extraFields['".$f."']):\"\").\"
FROM `".JUtil::tableName($f)."`
\".(isset(\$clauses['".$f."'])?\" WHERE \".\$clauses['".$f."']:\"\").\"
\".(isset(\$groupby['".$f."'])?\"GROUP BY \".\$groupby['".$f."']:\"\").\"
ORDER BY \".(isset(\$orders['".$f."'])?\$orders['".$f."']:\"`".JUtil::tableName($f)."`.id DESC\").\"
\".(isset(\$limit['".$f."'])?\"LIMIT \".\$limit['".$f."']:\"\");

\$save_queries['".$f."']='";
	$tempParentArray = array();
	$tempInsertForParents = '';
	if($inf["ordering"] == 1) {
		echo 'INSERT INTO `'
		. JUtil::tableName($f)
		. '`(`id`,`status`,`jdate`,`jorder`'
		. (count($inf['fields']) > 0 ? ',' . implode(',', JUtil::aposArray($inf['fields'], '`')) : '')
		. ') SELECT NULL,10,UNIX_TIMESTAMP(),(SELECT max(`jorder`)+1 FROM  `'
		. JUtil::tableName($f)
		. '`)'
		. (count($inf['fields']) > 0 ? ',' . JUtil::multistr('?', sizeof($inf['fields'])) : '');
	} else {
		if(count($inf['joins']) > 0) {
			foreach($inf["joins"] as $tempParent) {
				$tempParentArray[] = $tempParent['tag'];
			}
			$tempInsertForParents = ',' . implode(',', JUtil::aposArray($tempParentArray, '`'));
			$fss.= $tempInsertForParents;
		}

		echo 'INSERT INTO `' . JUtil::tableName($f) . '`(`id`,`status`,`jdate`'
		. (sizeof($inf['fields']) > 0 ? ',' . implode(',', JUtil::aposArray($inf['fields'], '`')) : '')
		. $tempInsertForParents
		. ') VALUES (NULL,10,UNIX_TIMESTAMP()'
		. (count($inf['fields']) > 0 ? str_repeat(',?', count($inf['fields'])) : '')
		. (count($tempParentArray) > 0 ? str_repeat(',?', count($tempParentArray)) : '')
		. ')\';'
		. PHP_EOL;
	}

	echo "\$delete_self_query['".$f."']='DELETE FROM `".JUtil::tableName($f)."` WHERE id=?';" . PHP_EOL;

	echo "\$edit_queries['".$f."']='UPDATE `".JUtil::tableName($f)."` SET ";
	$t0 = array();
	foreach ($inf['fields'] as $mmain) {
		$t0[] = "`{$mmain}`=?";
	}
	if(count($tempParentArray)) {
		foreach ($tempParentArray as $tempParent) {
			$t0[] = "`{$tempParent}`=?";
		}
	}
	echo implode(',', $t0) . ' WHERE `id`=?\';' . PHP_EOL;

	if(sizeof($inf["joins"])>0){
		//no load query from now on

		//save query
		foreach ($inf["joins"] as $nj) {
			echo "\$save_queries['".$nj["tag"]."']=\"UPDATE `".JUtil::tableName($f)."` SET `".$nj["tag"]."`=? WHERE id=?\";" . PHP_EOL;
			echo "\$delete_queries['".$nj["tag"]."']=\"UPDATE `".JUtil::tableName($f)."` SET `".$nj["tag"]."`='' WHERE id=?\";" . PHP_EOL;
		}
	}

	if(sizeof($inf["connections"])>0){
		foreach ($inf["connections"] as $nr) {
			echo "\$foreign_queries['".$nr["tag"]."']=\"SELECT \".(isset(\$docount['".$nr["tag"]."'])?\"count(*) cid\":\"".($nr["ordering"]==1?"`jorder`,":"")."`".$nr["sourceTable"]."`,`".$nr["targetTable"].($nr["sourceTable"]==$nr["targetTable"]?"_":"")."`\").\"
									FROM `".$nr["joinTable"]."`
									WHERE `tag`='".$nr["tag"]."'
									\".(isset(\$clauses['".$f."'])?\" AND \".\$clauses['".$f."']:\"\").\"
									ORDER BY \".(isset(\$orders['".$f."'])?\$orders['".$f."']:\"`jdate` DESC\").\"
									\".(isset(\$limit['".$f."'])?\"LIMIT \".\$limit['".$f."']:\"\").\";\" ;
";
			echo
"\$save_queries['".$nr["tag"]."']=";
			if($nr["ordering"]==1){
				echo
"\"INSERT INTO `".$nr["joinTable"]."`(`".$nr["sourceTable"]."`,`".$nr["targetTable"].($nr["sourceTable"]==$nr["targetTable"]?"_":"")."`,`tag`,`jorder`,`jdate`)
	SELECT ?,?,'".$nr["tag"]."',(SELECT max(`jorder`)+1 FROM  `".$nr["joinTable"]."`),UNIX_TIMESTAMP()\";
";
			} else {
				echo
"\"INSERT INTO `".$nr["joinTable"]."`(`".$nr["sourceTable"]."`,`".$nr["targetTable"].($nr["sourceTable"]==$nr["targetTable"]?"_":"")."`,`tag`,`jdate`)
									VALUES(?,?,'".$nr["tag"]."',UNIX_TIMESTAMP())\";" . PHP_EOL;
			}
			echo "\$delete_queries['".$nr["tag"]."']=\"DELETE FROM `".$nr["joinTable"]."` WHERE `tag`='".$nr["tag"]."' AND `".$nr["sourceTable"]."`=?\".(isset(\$deleteAll['".$f."'])?\"\":\" AND `".$nr["targetTable"].($nr["sourceTable"]==$nr["targetTable"]?"_":"")."`=?\");" . PHP_EOL;
		}
	}
$cons = ob_get_contents();
ob_end_clean();
echo $fss;
			if(!is_writable($__DP.self::$queryFolder)){
				JUtil::configProblem("can't write to query dir:".$queryFile);

			}
			if(is_file($queryFile) && !is_writable($queryFile)){
				JUtil::configProblem("can't write to query file:".$queryFile);

			}

			$fp = fopen($queryFile, 'w+');
			fwrite($fp, $cons);
			fclose($fp);

			//chmod($queryFile, 0777);

		}
	echo "<br><hr>\n";


	}
	
	/**
	 * debug to screen
	 *
	 * @return void
	 * @author Özgür Köy
	 **/
	private function debugLog($log)
	{
		echo date("Y.m.d H:i:s") . " >>> " . $log . PHP_EOL;
	}

} // END class JBuildQuery
