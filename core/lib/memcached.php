<?php
/************************************
 *
 *		memcached class
 *		inspired and modified from memcached class by Grigori Kochanov
 *		özgür köy
 *
 ************************************/		 


//load config
require_once "$__DP/site/def/memcached.php";

/**
 *
 */
class MC {
	/**
	 * Resources of the opend memcached connections
	 * @var array [memcache objects]
	 */
	protected static $mc_servers = array();
	/**
	 * Quantity of servers used
	 * @var int
	 */
	public static $mc_servers_count;
	
	static $instance;
	
	/**
	 * Singleton to call from all other functions
	 */
	public static function singleton( $servers=null ){
		global $_mcservers;

		//Write here where from to get the servers list from, like
		// if($servers == null ) $servers = $_mcservers;

		//$servers = _Config::$memcache_servers;
		 if (!isset(self::$instance)) {
			$c = __CLASS__;
			self::$instance = new $c;
		 }

		return self::$instance;
	}

	/**
	 * Accepts the 2-d array with details of memcached servers
	 *
	 * @param array $servers
	 */
	private function __construct($servers=null){

		global $_mcservers;
		
		$servers = $_mcservers;
		// if ( !$_mcservers )
			// throw new JError( JError::MEMCACHED_ZERO );
			
		for ($i = 0, $n = count($servers); $i < $n; ++$i){
				( $con = @memcache_pconnect(key($servers[$i]), current($servers[$i])) )&& 
						self::$mc_servers[] = $con;
						
		}
		
		self::$mc_servers_count = (int)count(self::$mc_servers);

	}
	
	/**
	 * Returns the resource for the memcache connection
	 *
	 * @param string $key
	 * @return object memcache
	 */
	protected static function getMemcacheLink($key){
			if ( self::$mc_servers_count <2 ){
					//no servers choice
					return self::$mc_servers[0];
			}
			return self::$mc_servers[(crc32($key) & 0x7fffffff)%self::$mc_servers_count];
	}
	
	/**
	 * Clear the cache
	 *
	 * @return void
	 */
	public static function flush() {
			$x = self::$mc_servers_count;
			for ($i = 0; $i < $x; ++$i){
					$a = self::$mc_servers[$i];
					self::$mc_servers[$i]->flush();
			}
	}
	
	/**
	 * Returns the value stored in the memory by it's key
	 *
	 * @param string $key
	 * @return mix
	 */
	public static function get($key) {
			global $GLBS;
			$ret	= null;
			$keyparts = array();
			
			if(strpos($key,".")>0) {
				$keyparts = explode( ".",$key );
				$key = $keyparts[0];
			}
			
			if( (int)self::$mc_servers_count > 0 ){
				$link = @self::getMemcacheLink($key); 
				$ret	= $link->get($key); 
			}
			
			/*
			if( ($ret==null) &&
					isset( $GLBS[$key]) )
				$ret = $GLBS[$key]; 
			*/
			
			if( ($ret!=null) && (sizeof( $keyparts ) > 1) ){
				for( $i=1; $i<sizeof( $keyparts ); $i++){
					$ret = $ret[ $keyparts[$i] ];				
				}
			}

			return $ret;
	}
	
	/**
	 * Store the value in the memcache memory (overwrite if key exists)
	 *
	 * @param string $key
	 * @param mix $var
	 * @param bool $compress
	 * @param int $expire (seconds before item expires)
	 * @return bool
	 */
	public static function set($key, $var, $compress=0, $expire=0 ) {
			global $GLBS;
			$ret = false;
			if( intval(self::$mc_servers_count) > 0 ){
				$ret = self::getMemcacheLink($key)->set($key, $var, $compress?MEMCACHE_COMPRESSED:null, $expire);
			}

			$GLBS[$key] = $var;

			return $ret;
	}
	/**
	 * Set the value in memcache if the value does not exist; returns FALSE if value exists
	 *
	 * @param sting $key
	 * @param mix $var
	 * @param bool $compress
	 * @param int $expire
	 * @return bool
	 */
	public static function add($key, $var, $compress=0, $expire=0) {
			return self::getMemcacheLink($key)->add($key, $var, $compress?MEMCACHE_COMPRESSED:null, $expire);
	}
	
	/**
	 * Replace an existing value
	 *
	 * @param string $key
	 * @param mix $var
	 * @param bool $compress
	 * @param int $expire
	 * @return bool
	 */
	public static function replace($key, $var, $compress=0, $expire=0) {
			return self::getMemcacheLink($key)->replace($key, $var, $compress?MEMCACHE_COMPRESSED:null, $expire);
	}
	/**
	 * Delete a record or set a timeout
	 *
	 * @param string $key
	 * @param int $timeout
	 * @return bool
	 */
	public static function delete($key=null, $timeout=0) {
			if(self::getMemcacheLink($key))
				self::getMemcacheLink($key)->delete($key, $timeout);
	}
	/**
	 * Increment an existing integer value
	 *
	 * @param string $key
	 * @param mix $value
	 * @return bool
	 */
	public static function increment($key, $value=1) {
			return self::getMemcacheLink($key)->increment($key, $value);
	}
	
	public static function countConnectedServers(){
		return self::$mc_servers_count;
	}
	/**
	 * Decrement an existing value
	 *
	 * @param string $key
	 * @param mix $value
	 * @return bool
	 */
	public static function decrement($key, $value=1) {
			return self::getMemcacheLink($key)->decrement($key, $value);
	}
	
	public static function getStats(){
		$x = self::$mc_servers_count;
		for ($i = 0; $i < $x; ++$i){
				$a = self::$mc_servers[$i];
				echo "<PRE>";
				print_r(self::$mc_servers[$i]->getStats());
				
		}
			
	}
	//class end
}

?>