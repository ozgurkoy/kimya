<?php
/**
*		pdo class - jeelet
*		özgür köy
*		inspired and modified from connection.class by Adler Brediks Medrado
*		09-04-2013 added multi db transaction
*/

class JPDO {


	public static $connection;
	public static $dsn;
	public static $prevDsn=null;
	public static $username;
	public static $password;
	public static $effected ;
	public static $queryCount=0;
	public static $utfExecuted = false;
	public static $lastExecutionTime = 0;
	public static $isTransAction = 0;
	public static $hideError = false;
	public static $debugQuery = false;

	private static $instance;
	private static $connections = array();
	
	private function __construct() {}

	public static function singleton(){
	        if (!isset(self::$instance)) {
			$c = __CLASS__;
			self::$instance = new $c;
	        }

	        return self::$instance;
	}

	

	public static function debugQuery($set=true) {
		return self::$debugQuery = $set;
	}
	

	public static function getConnection() {
		return self::$connection;
	}

	/**
	 * Make a connection with a database using PDO Object.
	 *
	 */
	public static function connect($conf,$logging=false,$forced=false) {
		if(!is_array($conf)){
			
			if($logging==false) {
		    		JLog::log("cri",'Buggy dsn?: ' . serialize($conf));
			}
			// JUtil::siteHalt("PDOCA");
			
			return false;
		}
		else
			
		if(isset($conf["dsn"])){
			// forced connection
			if(!$forced && self::$dsn == $conf["dsn"]){
				//global $debugLevel;
				//if ((time()-self::$lastExecutionTime)>=60){
				//	try{
						//Eğer aşağıdaki sorgunun sonucunda exception almıyorsa SQL connection LIVE demektir.
				//		self::getConnection()->query("SELECT 1");
				//		return true;
				//	} catch (PDOException $e) {
				//	}
				//}else{
					self::$connection = self::$connections[self::$dsn]["connection"];
					return true;
				//}
			}
			
			// same connection
			if(array_key_exists( $conf["dsn"], self::$connections )){
				self::$connection = self::$connections[$conf["dsn"]]["connection"];
				self::$dsn        = $conf['dsn'];
				self::$username   = $conf['user'];
				self::$password   = $conf['pass'];
				
				return true;
			}
			
			// all new
			self::$prevDsn = array("dsn"=>self::$dsn,"user"=>self::$username,"pass"=>self::$password);

			self::$dsn 		= $conf['dsn'];
			self::$username = $conf['user'];
			self::$password	= $conf['pass'];
			
		}
		// elseif(!JUtil::isProduction()) 
		// 	die("buggy query");
		
		try {
			//echo print_r($conf,true) . PHP_EOL;
			//echo "connecting..." . PHP_EOL;
			$pdoConnect = new PDO(self::$dsn, self::$username, self::$password);
			
			$pdoConnect->query('SET NAMES UTF8');
			// $pdoConnect->query('SET FOREIGN_KEY_CHECKS=0'); // < THE HELL???
					
			self::$connection = $pdoConnect;
			self::$connections[self::$dsn] = array( "connection"=>$pdoConnect, "transaction"=>0);
			
			self::transactConnection();
			return true;
		} catch (PDOException $e) {
			if(!JUtil::isProduction()) {
				echo $e->getMessage().'<br />';
				die();
			}
		    JLog::log("cri",'Connection failed: ' . $e->getMessage());
			JUtil::siteHalt("PDOC");
			// exit;
			return false;
		}

	}

	/**
	 * revert to previous dsn
	 *
	 * @return void
	 * @author Özgür Köy
	 **/
	public static function revertDsn()
	{
		if(!is_null(self::$prevDsn)) {
			self::connect(self::$prevDsn);
		}
		self::$prevDsn = null;
	}
	
	/**
	 * begin transaction for the active connection, must be called after assignment
	 *
	 * @return void
	 * @author Özgür Köy
	 **/
	private static function transactConnection()
	{
		if(self::$isTransAction && self::$connections[self::$dsn]["transaction"]==0){
			self::startTransaction();
			self::$connections[self::$dsn]["transaction"] = 1;
		}
	}

	/**
	 * Execute a DML
	 *
	 * @param String $query
	 */

	public static function executeDML($query) {
		$a = self::getConnection();
		if($a==false) return false;

		if (!self::getConnection()->query($query)) {
			throw new JError( "ERROR_FATAL_QUERY_".self::getConnection()->errorInfo() ,911);
		} else {
			return true;
		}
	}
	/**
	 * Execute a query
	 *
	 * @param String $query
	 * @return PDO ResultSet Object
	 */
	public static function executeQuery($query) {
		global $__DP;
		// echo $query."\n\n";
		self::$queryCount++;

		if(strlen($query)==0) return "";
		$args = func_num_args();
		$xtransform = array();
		//check if an array is passed as arg
		if($args>1 && is_array(func_get_arg(1))){
			$xtransform = func_get_arg(1);
			array_shift($xtransform);
		}
		else{
			for( $i=1; $i<$args; $i++){
				$a = func_get_arg( $i );
				$xtransform[] = $a;
			}
		}
		
		// print_r($xtransform);
		$a = self::getConnection();
		if($a==false) return false;

		if(self::$utfExecuted==false){
			self::$utfExecuted = true;
		}

		$rs = null;
		// echo ">>>".$query."<BR>";
		if(self::$debugQuery){
			//print_r( $xtransform );
			$xtDebug = $xtransform;
			array_unshift($xtDebug, str_replace("?","%s",$query));

		    $debugQ = call_user_func_array('sprintf', $xtDebug);
			
			echo $debugQ;
		}
		
		if ($stmt = self::getConnection()->prepare($query)) {
			if (self::executePreparedStatement( $stmt, $rs, $xtransform, $query )) {
				return $rs;
			}
		} elseif(!is_file($__DP."/site/def/state/building-1")) {
			if(!JUtil::isProduction()) {
				echo 'Buggy query (2): ' . $query . '<br />';
				die();
			}
			JLog::log("user",'Buggy query (2): ' . $query);
			JUtil::siteHalt("PDOQ");
		}
	}

	/**
	 * shortcut to executeQuery
	 *
	 * @return bool
	 * @author Özgür Köy
	 **/
	public static function q($query)
	{
		return self::executeQuery($query);
	}

	/**
	 * Init a PDO Transaction
	 * TODO : ADD ERROR HANDLER
	 */
	public static function beginTransaction() {
		foreach (self::$connections as $dsn => $connection) {
			if ($connection["transaction"]==0) {
				self::startTransaction($connection["connection"]);
				self::$connections[$dsn]["transaction"] = 1;
			}
		}
		
		self::$isTransAction = 1;
		//echo "\n In Begin Transaction: \n";
		//print_r(self::$connections);
	}
	
	/**
	 * Init a PDO Transaction
	 */
	private static function startTransaction($connection=null) {
		// if(self::$isTransAction <2 ){
		// 	self::$isTransAction = 2;
		if(is_null($connection)) $connection = self::getConnection();
		
		if ($connection->beginTransaction()==false) {
			//echo "\nPDO::errorInfo1(): \n";
			//print_r($connection->errorInfo());
			return false;
		}
		//echo "\nPDO::errorInfo2(): \n";
		//print_r($connection->errorInfo());
		return true;
		// }
	}
	/**
	 * Commit a transaction
	 *
	 */
	public static function commit() {
		if(self::$isTransAction!=1) return false;
		
		foreach (self::$connections as $dsn => $connection) {
			if ($connection["transaction"]==1) {
				
				$commitResult = $connection["connection"]->commit();
				
				if ($commitResult!==false) {
					self::$connections[$dsn]["transaction"] = 0;
				}else{
					JLog::log("user","PDO commit error:".$connection["connection"]->errorInfo());
				}
			}
			
		}
		
		self::$isTransAction = 0;
		//echo "\n In Commit Transaction: \n";
		//print_r(self::$connections);
		
	}
	
	/**
	 * Rollback a transaction
	ADD ERROR HANDLER
	 *
	 */
	public static function rollback() {
		if(self::$isTransAction!=1) return false;
		
		foreach (self::$connections as $dsn => $connection) {
			if ($connection["transaction"]==1) {
				$rollbackResult = $connection["connection"]->rollback();
				if ($rollbackResult!==false) {
					self::$connections[$dsn]["transaction"] = 0;
				} else {
					JLog::log("user","PDO rollback error:".$connection["connection"]->errorInfo());
				}
			}
		}
		
		self::$isTransAction = 0;
	}

	/**
	 * Last insert id
	 *
	 */
	public static function lastId() {
		if (self::getConnection()) {
			return self::getConnection()->lastInsertId();
		}
	}

	/**
	 * column count
	 *
	 * @param String $query
	 */
	public static function count() {
		if (self::getConnection()) {
			return self::$effected;//self::getConnection()->rowCount();
		}
		else
			return false;
	}

	/**
	 * Execute a prepared statement
	 * it is used in executeQuery method
	 *
	 * @param PDOStatement Object $stmt
	 * @param Array $row
	 * @return boolean
	 */
	private static function executePreparedStatement( $stmt, &$row = null, $statements,$query ) {
		global $__DP;
		$boReturn = false;
		// if(self::$isTransAction){
		// 	echo 88;
		// 	self::startTransaction();
		// }
		
		$a = $stmt->execute($statements);
		if ($a) {
			if ($row = $stmt->fetchAll()) {
				$boReturn = true;
			} else {
				$boReturn = false;
			}
			self::$lastExecutionTime = time();
		} else {
			$boReturn = false;
			if(!JUtil::isProduction()  && self::$hideError==false ) {
				echo "JPDO ERROR (2) : dsn : ".self::$dsn." ".$query."-".serialize($statements).' Code:'.$stmt->errorCode().' Info:'.print_r($stmt->errorInfo());
				print_r(get_included_files());
				die();
			}
			if(!is_file($__DP."site/def/state/building-1")  && self::$hideError==false ) {
				JLog::log("gen","JPDO ERROR  : dsn : ".self::$dsn." ".$query."-".serialize($statements).' Code:'.$stmt->errorCode().' Info:'.print_r($stmt->errorInfo(), true));
				JUtil::siteError("PDOQ");
			}
		}
		self::$effected = $stmt->rowCount();

		return $boReturn;
	}

	/**
	 * check if table exists
	 *
	 * @return bool
	 * @author Özgür Köy
	 **/
	public static function tableExists($table)
	{
		self::executeQuery( "SHOW TABLE STATUS LIKE '$table'" );
		return self::$effected>0;
	}

	/**
	 * get table indexes
	 *
	 * @return mixed
	 * @author Özgür Köy
	 **/
	public static function listIndexes($table)
	{
		$recordset = self::executeQuery("SHOW INDEX FROM `$table`");
		if(!sizeof($recordset)>0) return array();
		// foreach ($recordset as $field) {
			// print_r($field);
			// $fieldNames[] = $field['Field'];
		// }
		return $recordset;
	}
	
	
	/**
	 * drop table index
	 *
	 * @return void
	 * @author Özgür Köy
	 **/
	public static function dropIndex($table,$index)
	{
		self::executeQuery("ALTER TABLE `$table` DROP INDEX  `$index`");
	}
	
	

	/**
	 * delete table indexes
	 *
	 * @return void
	 * @author Özgür Köy
	 **/
	public static function deleteIndex($index,$table)
	{
		self::executeQuery("DROP INDEX `{$index}` ON {$table};");
	}

	/**
	 * list table's fields
	 *
	 * @return array
	 * @author Özgür Köy
	 **/
	public static function listFields($table,$detailed=false)
	{
		$recordset = self::executeQuery("SHOW COLUMNS FROM `$table`");
		if(!sizeof($recordset)>0) return false;
		
		foreach ($recordset as $field) {
			if($detailed)
				$fieldNames[$field['Field']] = $field["Type"];
			else
				$fieldNames[] = $field['Field'];
		}
		return $fieldNames;
	}

}
?>