<?php
/************************************
*
*		smarty extended class
*		özgür köy
*
************************************/
require_once "$__DP/core/lib/smarty/libs/Smarty.class.php";
require_once "$__DP/site/def/smartyCachedirs.php";
require_once "$__DP/site/lib/smarty.php";

class JT{
	static $instance = null;
	static $_smarty = null;

	/**
	 * Singleton to call from all other functions
	 */
	public static function singleton(){
		global $__DP, $SYSTEM_EMAILS;
		if( self::$instance == null ){
		 	self::$_smarty = new Smarty();

			self::$_smarty->template_dir		= "$__DP/site/templates";
			self::$_smarty->cache_dir			= "$__DP/temp/templateCache";
			self::$_smarty->compile_dir			= "$__DP/temp/templateCompiled";
			self::$_smarty->plugin_dir			= "$__DP/core/lib/smarty/libs/plugins";


			 self::assignGlobalVariables();


			self::$_smarty->use_sub_dirs	 	= true;
			self::$_smarty->caching				= 2;
			self::$_smarty->compile_check		= IS_PRODUCTION==1?0:1;

			self::$_smarty->config_dir			= "$__DP/site/template/templateConfig";
			self::$_smarty->securePage 			= 'http://'.SERVER_NAME.'/admingle/';


			//register user smarty functions
			$userMethods = get_class_methods("UserSmarty");
			foreach ($userMethods as $method) {
				self::$_smarty->register_function( $method, array( "UserSmarty",$method ));
			}
			self::$_smarty->load_filter('output','stripManySlashes');

		}

		try{
			self::$instance ||
					self::$instance = new JT();
		}
		catch (E $e){
			return false;
		}

		return self::$instance;
	}

	public static function init(){

		self::$_smarty->clear_all_assign();
		self::assignGlobalVariables();
		
	}
	
	public static function assignGlobalVariables(){
		global $__DP, $SYSTEM_EMAILS, $__site;
		self::$_smarty->assign("DP",$__DP);
		self::$_smarty->assign("SITEROOT","http://".SERVER_NAME);
		if (defined('ENABLE_INVITE')) self::$_smarty->assign("ENABLE_INVITE",ENABLE_INVITE);
		if (defined('CURRENCY_SYMBOL')) self::$_smarty->assign("CURRENCY_SYMBOL",CURRENCY_SYMBOL);
		if (defined('SERVER_NAME')) self::$_smarty->assign("SERVER_NAME",SERVER_NAME);
                if (defined('GOOGLE_ANALYTICS')) self::$_smarty->assign("GOOGLE_ANALYTICS",GOOGLE_ANALYTICS);
		if (defined('COUNTRY_CODE')) self::$_smarty->assign("COUNTRY_CODE",COUNTRY_CODE);
		self::$_smarty->assign("SYSTEM_EMAILS",$SYSTEM_EMAILS);
		if (defined('ADMIN_LOGIN_PATH')) self::$_smarty->assign("ADMIN_LOGIN_PATH", ADMIN_LOGIN_PATH);
		if (defined('TWITTER_USER')) self::$_smarty->assign("OFFICIAL_TWITTER_ACCOUNT",TWITTER_USER);
		if (defined('CPC_COST')) self::$_smarty->assign("CPC_COST",CPC_COST);
		if (defined('SHORTDOMAIN')) self::$_smarty->assign("SHORTDOMAIN",SHORTDOMAIN);
		
		if(isset($_COOKIE["adcookiepolicyagree"])){
			self::$_smarty->assign("COOKIELAW", TRUE);
		}else{
			self::$_smarty->assign("COOKIELAW", FALSE);
		}
		if(isset($_SESSION["is_mobile"])){
			self::$_smarty->assign("IS_MOBILE", $_SESSION["is_mobile"]);
		}else{
			self::$_smarty->assign("IS_MOBILE", 0);
		}
		
		$cantConnectToMemcache = false;
//		$memcache = new Memcache;
//		$memcache->connect(MEMCACHE_SERVER, MEMCACHE_PORT) or $cantConnectToMemcache = true;
		$_settings = array();

//		if ($cantConnectToMemcache || !($_settings = $memcache->get($__site.'_SettingsValues')) || !is_array($_settings) || count($_settings) < 7) {
			// $settings = new Setting();
			// $settings->nopop()->load();
			// while($settings->populate()){
			// 	$_settings[$settings->name] = $settings->value;
			// }
			// $memcache->set($__site."_SettingsValues", $_settings, MEMCACHE_COMPRESSED, 300);
//		}
//		if (!$cantConnectToMemcache) $memcache->close();
		//die("<pre>".print_r($_settings,true));
		unset($memcache,$_settings);
	}

	public static function assign( $vl, $ip ){
		self::$_smarty->assign($vl,$ip);
	}

	public static function display( $template, $cacheId="", $compileId=""){
		self::$_smarty->display($template, $cacheId="", $compileId="");
	}

	public static function setCacheTime(){
		self::$_smarty->cache_lifetime = 0;
	}

	public static function cacheCheck( $resource ){
		$args = func_get_args() ;
		JUtil::dropFirst( $args );

		$cacheStr = self::buildCacheString( $resource, $args );

		if($cacheStr["timeout"]==0)
			self::$_smarty->caching				= 0;
		else
			self::$_smarty->caching				= 2;

		return self::$_smarty->is_cached( $cacheStr["dir"] , $cacheStr["str"] ) ;
	}

	public static function pfetch( $resource ){
		/*
		*
		*	changed the smarty path for 0 ers, will try
		*/
		global $__DP;
		$args = func_get_args() ;
		JUtil::dropFirst( $args );
		$cacheStr = self::buildCacheString( $resource, $args );
		self::$_smarty->cache_lifetime 	= intval( $cacheStr["timeout"] );

		if(isset($_SESSION["LAST_ERROR"])){
			$lang = isset($_SESSION["lang"]) ? $_SESSION["lang"] : DEFAULT_LANGUAGE;

			//require_once $__DP . 'site/langFiles/errMessages.php';
			$_errMessages = LangWords::getLangWords("errMessage",$_SESSION["lang"]);

			$error = json_decode($_SESSION["LAST_ERROR"]);
			if (isset($_errMessages[$error->errorCode])) $error = $_errMessages[$error->errorCode];

			self::$_smarty->assign("hasError","Y");
			self::$_smarty->assign("LAST_ERROR",$error);
			unset($_SESSION["LAST_ERROR"]);
		}else{
			self::$_smarty->assign("hasError","N");
			self::$_smarty->assign("LAST_ERROR","");
		}

		if($cacheStr["timeout"]==0){
			self::$_smarty->caching				= 0;
			return self::$_smarty->fetch( $cacheStr["dir"] );
		}
		else{
			self::$_smarty->caching				= 2;
			return self::$_smarty->fetch( $cacheStr["dir"] , $cacheStr["str"] );
		}
	}

	public static function clrCache( $resource ){
		$args = func_get_args() ;
		JUtil::dropFirst( $args );
		$cacheStr = self::buildCacheString( $resource, $args );
		self::$_smarty->clear_cache( null, preg_replace('/\\|0$/', '', $cacheStr["str"]) );
	}

	private static function buildCacheString( $resource, $args ){
		global $smartyCacheDirs;
		$s = $smartyCacheDirs[ $resource ];
		$argCount = substr_count( $s["cacheDir"], "%" );
		if(sizeof($s)==0) {print_r($args);echo $resource;die("fatal smarty config error-> ".$resource);}
		$xstr = $s["cacheDir"];

		if( $argCount != sizeof($args) ){
			//get rid of the %s starting from end.. mostly pages.. but be careful..
			$cnt = 0;
			while( substr_count( $xstr, "%" ) != sizeof($args) ){
				$xstr = preg_replace('/%s(?!.*%s.*).*$/i', '0', $xstr);

				if(++$cnt == 10) {
					JLog::log("cri","fatal smarty error->$resource");
					JUtil::configProblem("fatal smarty error->$resource");
				}
			}
		}

	 	$cacheStr = @vsprintf( $xstr, $args );
		if(!isset($s["cacheTimeout"])) $s["cacheTimeout"] = 0;
		return array( "str"=>$cacheStr,"dir"=>$s["realPath"], "timeout"=>$s["cacheTimeout"] );
	}

	private function __construct(){}
}
?>