<?php
/************************************
 *	
 *		session handling base class 
 *	
 *				
 *		özgür köy
 *
 ************************************/ 
class SessionBase{

	static $instance;
	
	/**
	 * Singleton to call from all other functions
	 */
	static function singleton(){
			//Write here where from to get the servers list from, like

			//$servers = _Config::$memcache_servers;
			try{
				self::$instance || 
						self::$instance = new SEFILE();
			}
			catch (TError $e){
				return false;
			}						
			
			return self::$instance;
	}
	
	public function __construct(){	}
}
	
?>