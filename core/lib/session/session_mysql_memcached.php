<?php
/**
 *		mysql sessions boosted w/memcached
 *		özgür köy 
 *		
 *		to do :: gc for cache. 
 * 
 */

class SESMYSMEMC extends SessionBase{
	private $cacheHandler;	
	
	public function __construct(){
		global $_mmcSess;

		$this->cacheHandler	 = MC::countConnectedServers()>0?MC::countConnectedServers():null;
	}

	public function open($save_path="", $session_name=""){
		return JPDO::connect( JCache::read('mysqlDSN.SESS') );
		//aint got a thing to do...
	}
	
	public function close(){
		return(true);
	}
	
	public function read($id){	
		//check cache.
		if( !is_null($this->cacheHandler) && (strlen($cacheResponse=MC::get($id))>0) ){
			return $cacheResponse;
		}
			
		//not found dig mysql
		$sessionFetch= "SELECT sdata FROM `".JDef::$sessionstable."` where id=?";
		try {
				$r = JPDO::executeQuery( $sessionFetch, $id );
		} catch (TError $e) {}
		
		$this->write($id,$r[0]["sdata"],true);
		
		return $r[0]['sdata'];
	}
	
	public function write($id, $sessdata,$memcachedOnly=false){
		//update cache.
		if($this->cacheHandler!=null) MC::set($id,$sessdata,0,SESSION_TIMEOUT);
		if($memcachedOnly) return true;
			
		$sessdata = $sessdata.(strlen($sessdata)==0?" ":"");

		$sessionFetch= "UPDATE `".JDef::$sessionstable."` SET sdata=?, lastmodified=? WHERE id=?";
		try {
			$r = JPDO::executeQuery( $sessionFetch, (String)$sessdata, time(), $id );
		}
		catch (TError $e) {
			
		}
		
		if(JPDO::count()==0){
			$sessionFetch= "INSERT INTO `".JDef::$sessionstable."`(id,sdata,lastmodified) values(?,?,?)";
			try {
					$r =	JPDO::executeQuery( $sessionFetch,$id, (String)$sessdata, time() );
			} catch (TError $e) {}
		}
	}
	
	public function destroy($id){
	 	//remove from cache
		if($this->cacheHandler!=null)
			MC::delete($id);
			
		$sessionFetch= "DELETE FROM `".JDef::$sessionstable."` WHERE id=?";
		$r = JPDO::executeQuery( $sessionFetch, trim($id) );
		return ;//($r = JPDO::count()>0);
	}
	
	public function gc($maxlifetime=0){
		//hmm, need a gc for cache?	 

		// $sessionFetch= "SELECT count(*) cid FROM `".JDef::$sessionstable."` WHERE lastmodified < ?";
		
		$sessionFetch= "SELECT id FROM `".JDef::$sessionstable."` WHERE lastmodified < ?";
		try {
			$r = JPDO::executeQuery( $sessionFetch, (int)(time() - SESSION_TIMEOUT) );
			if(sizeof($r)>0)	{
				foreach ($r as $r0) {
					$this->destroy($r0["id"]);
				}

			}
				
		} catch (TError $e) {}

		// $sessionFetch= "DELETE FROM `".JDef::$sessionstable."` WHERE lastmodified < ?";
		// 
		// try {
		// 		$r = $r = JPDO::executeQuery( $sessionFetch, (int)(time() - SESSION_TIMEOUT) );
		// } catch (TError $e) {}
	
		return ($r = JPDO::count()>0);
	}
	
}

//try the mysql
$_sessObj = new SESMYSMEMC();
$_sessionEngineUp = true;

?>