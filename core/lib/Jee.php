<?php
/**
 * Class Jee - the runner for the models
 *
 * @package jeelet
 * @author Özgür Köy
 **/
class Jee
{

	/**
	 * groupBy variable for loading
	 *
	 * @var string
	 **/
	public $groupBy=null;

	/**
	 * orderBy variable for loading
	 *
	 * @var string
	 **/
	public $orderBy=null;

	/**
	 * limit variable for loading
	 *
	 * @var string
	 **/
	public $limit=null;


	/**
	 * custom clause to override loading
	 *
	 * @var string
	 **/
	public $customClause=null;

	/**
	 * number of rows
	 *
	 * @var int
	 **/
	public $count;

	/**
	 * fields to load, filled by fields method
	 *
	 * @var array
	 **/
	public $fieldsToLoad=null;

	/**
	 * unused constructor
	 *
	 * @return void
	 **/
	private function __construct()
	{
	}

	/**
	 * main getter related to parameters
	 *
	 * @return array
	 **/
	public function get($type=null,$foreign="")
	{

		global $__DP,$_loadedObjects;

		$type = !is_null($type)?$type:"single";

		$types = array(
				"single"=>"main_queries",
				"joined"=>"joined_queries",
				"foreign"=>"foreign_queries"
			);
		$fieldsToLoad = $clauses = $orders = $groupBy = $limit = array();

		// if(($t = $this->_checkStore($this->_getStoreString())) != false){
		// 	$this->count = $t[1];
		// 	return $t[0];
		// }

		// build configuration
		if(!is_null($this->customClause) && strlen($this->customClause)>0)
			$clauses[$this->selfname] = $this->customClause;
		if(!is_null($this->orderBy) && strlen($this->orderBy)>0)
			$orders[$this->selfname] 	= $this->orderBy;
		if(!is_null($this->groupBy) && strlen($this->groupBy)>0)
			$groupby[$this->selfname] 	= $this->groupBy;
		if(!is_null($this->limit) && strlen($this->limit)>0)
			$limit[$this->selfname] 	= $this->limit;
		if(!is_null($this->fieldsToLoad) && strlen($this->fieldsToLoad)>0)
			$fieldsToLoad[$this->selfname] 	= $this->fieldsToLoad;

		include($__DP."/".JDef::$queryFolder.$this->selfname.".php");

		$q = ${$types[$type]};
		$q = $type=="foreign_queries"?$q[$this->selfname][$foreign]:$q[$this->selfname];

		JPDO::connect( JCache::read('mysqlDSN.'.$this->dsn) );
		$k = JPDO::executeQuery($q);

		$this->count = JPDO::count();


		// $this->_store($this->_getStoreString(),$k,$this->count);



		return $k;

	}

	/**
	 * store object
	 *
	 * @return void
	 **/
	public function _store($storeString,&$resultset,$count)
	{

		// return false;
		global $__DP,$_loadedObjects;
		$storeString = md5($storeString);


		if(!isset($_loadedObjects[$storeString])){
			$_loadedObjects[$storeString] = array($resultset,$count);
		}

		if(!file_exists($_dfold=$__DP.'/temp/objects/'.substr($storeString,0,1))) mkdir($__DP.'/temp/objects/'.substr($storeString,0,1),0777);

		if(file_exists($_dfold."/".$storeString)) return false;

		$s = serialize(array($resultset,$count));

		$fp = fopen($_dfold."/".$storeString, 'w');
		fwrite($fp, $s);

		fclose($fp);
	}

	/**
	 * check and revive
	 *
	 * @return void
	 **/
	public function _checkStore($storeString)
	{
		// return false;
		global $__DP,$_loadedObjects;
		$storeString = md5($storeString);


		if(isset($_loadedObjects[$storeString])){
			return $_loadedObjects[$storeString];
		}

		if(!file_exists($__DP.'/temp/objects/'.substr($storeString,0,1)."/".$storeString)) return false;
		$fp = file_get_contents($__DP.'/temp/objects/'.substr($storeString,0,1)."/".$storeString);

		$_loadedObjects[$storeString] = unserialize($fp);

		return $_loadedObjects[$storeString];

	}

	/**
	 * get store string
	 *
	 * @return void
	 **/
	private function _getStoreString()
	{
		return ($this->selfname."-".$this->customClause."-".$this->orderBy."-".$this->groupBy."-".$this->limit."-".$this->fieldsToLoad);
	}



	/**
	 * build query for loading
	 *
	 * @return object
	 **/
	public function buildQuery()
	{
		$c0 = array();

		//read fields
		if(!isset($this->_fields) || !sizeof($this->_fields)>0) return false;

		foreach ($this->_fields as $_field) {
			if(!is_null($this->$_field) && ((is_array($this->$_field)&&sizeof($this->$_field)>0) || (!is_array($this->$_field)&&strlen($this->$_field)>0)) ){
				$c0[] = '`' . JUtil::tableName($this->selfname) . '`.' . $_field . ' '
						. (is_array($this->$_field) ? 'IN(' . implode(',', JUtil::aposArray($this->$_field)) . ')' : '=\'' . addslashes($this->$_field) . '\'');
			}
		}

		if(count($this->_relatives) > 0) {
			foreach ($this->_relatives as $_field) {
				if( isset($this->$_field) && !is_null($this->$_field) ) {
					if(!is_array($this->$_field) && !is_object($this->$_field) && strlen($this->$_field) > 0) {
						// relative single value

						$c0[] = '`' . JUtil::tableName($this->selfname) . "`.{$_field} ='" . addslashes($this->$_field) . "'";

					} elseif(is_array($this->$_field) && sizeof($this->$_field) > 0) {
						// relative in array

						$c0[] = '`' . JUtil::tableName($this->selfname) . "`.{$_field} IN(" . implode(',', JUtil::aposArray($this->$_field)) . ')';

					} elseif(is_object($this->$_field) && $this->$_field->id > 0) {
						// relative single object given to a parameter before load => $orderItem->order = $orderObject

						$c0[] = '`' . JUtil::tableName($this->selfname) . "`.{$_field} ='" . addslashes($this->$_field->id) . "'";

					}
				}
			}
		}

		//print_r($this->_fields); die();
		//print_r($c0); die();
		if(sizeof($c0)>0){
			$this->customClause = implode(" AND ",$c0);
		}
		return $this;
	}

	/**
	 * fill an array for saving/editing
	 *
	 * @param boolean $withParent insert queries use this as TRUE to write parent column values
	 * @return array
	 */
	public function buildFieldArfray($withParent=FALSE)
	{
		$c0 = array();
		if(!isset($this->_fields) || !sizeof($this->_fields)>0) {
			return false;
		}

		//read fields and fill an array
		foreach ($this->_fields as $_field) {
			$c0[$_field] = is_array($this->$_field) ? (isset($this->$_field[0]) ? $this->$_field[0] : '') : $this->$_field;
		}

		if(TRUE === $withParent) {
			//read parent fields and fill an array
			if(isset($this->_parentFields)) {
				foreach ($this->_parentFields as $_field) {
					if(is_array($this->$_field)) {
						$c0[$_field] = isset($this->$_field[0]) ? $this->$_field[0] : '';
					} elseif(is_object($this->$_field) && isset($this->$_field->id) && $this->$_field->id) {
						$c0[$_field] = $this->$_field->id;
					} else {
						$c0[$_field] = $this->$_field;
					}
				}
			}
		}

		return $c0;
	}

	function savee($type="s"){
		//s for save , u for update
		$fields = array();
		$values = array();

		//left from previous Exocon
		if($this->generateIfNot){
			//create temp table
			$tableExists = $this->my->tableExists( "exocon_".$this->prefix.$this->selfname ) ;
			if( !$tableExists ){
				$q= "CREATE TABLE `exocon_".$this->prefix.$this->selfname."` (
					`id` INT NOT NULL AUTO_INCREMENT ,
					`status` INT DEFAULT '10' NOT NULL ,
					`lang` INT NOT NULL ,
					";
				foreach($fields as $fld){
					if( !in_array($fld,array("id","status","lang")) )
						$q.= "`$fld` TEXT NOT NULL ,";
				}

				$q.="PRIMARY KEY ( `id` ));";

				$this->my->q($q);
			}
			else{
				$prvFields = $this->my->listFields("exocon_".$this->selfname);
				//check for different fieldname(s)
				foreach( $fields as $fld ){
					if( !in_array( $fld, $prvFields ) ) //add necessary..
						$this->my->q("ALTER TABLE `exocon_".$this->prefix.$this->typ."` ADD `$fld` TEXT NOT NULL ;");
				}
			}
		}

		//build fields
		foreach( $this->vals as $al => $cn ){
			$fields[] = $al;

			if( strpos( $al,"date" )>0 ) $cn = reverseDate($cn);
			$cn = str_replace("'","`",$cn);

			$values[] = "'".$cn."'";
		}

		if( $type=="u" && $this->id>0 ){
			$qlast = "UPDATE `exocon_".$this->prefix.$this->selfname."` SET ";

			foreach( $fields as $k=>$fld)
				$qlast .= "$fld=".$values[$k].",";

			$qlast = substr($qlast, 0, strlen($qlast)-1 );
			$qlast .= "WHERE id=$this->id";

			if($this->debug==1)
				echo $qlast."<BR>";

			$this->my->q($qlast);
		}
		else{
			$qlast = "INSERT INTO `exocon_".$this->prefix.$this->selfname."`( lang,".implode(",",$fields)." )
				VALUES($this->langId,".implode(",",$values).")";

			if($this->debug==1)
				echo $qlast."<BR>";

			$this->my->q($qlast);
			$this->id = $this->my->lastId;
			return $this->id;
		}

	}

	function del($id=null){
		if(is_null($id)) $id = array($this->id);
		if(!is_array($id)) $id = array($id);
		$this->my->q("DELETE FROM `exocon_".$this->prefix.$this->selfname."` WHERE id IN(".implode(",", $id).")");

	}

	function loadSingle( $id, $type="single" ){
		$this->load(array("clause"=>$this->selfname.".id=$id","type"=>$type));
		return $v = $this->crawlResults();
	}

	function clear( $type , $ids ){
		$ids = implode(",",$ids);
		$this->my->q("DELETE FROM `exocon_$type` where id IN($ids)");
	}

	function fillToArray(){
		$ret = array();
		while( $v = $this->crawlResults() )
			$ret[] = $v[$value];

		return $ret;
	}

} // END class Jee
