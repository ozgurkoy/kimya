<?php
/**
 * build definition class, sqls etc
 *
 * @package default
 * @author Özgür Köy
 **/
class BuildDef extends JDef 
{
	/**
	 * Field types of no collation
	 *
	 * @var array
	 */
	public $noCollationFieldTypes = array(
		"int(3)", 
		"int(11)", 
		"tinyint(1)", 
		"BOOLEAN", 
		"DATE", 
		"DATETIME", 
		"FLOAT", 
		"bigint(20)"
	);
	
	/**
	 * reserved fields.
	 *
	 * @var array
	 **/
	public $reservedFields = array("id", "jdate", "log", "logEvents", "sessions");
	

	/**
	 * reserved keywords , mysql and php
	 *
	 * @var array
	 **/
	public $reservedKeywords = array(
	"ACCESSIBLE",
	"ADD",
	"ALL",
	"ALTER",
	"ANALYZE",
	"AND",
	"AS",
	"ASC",
	"ASENSITIVE",
	"BEFORE",
	"BETWEEN",
	"BIGINT",
	"BINARY",
	"BLOB",
	"BOTH",
	"BY",
	"CALL",
	"CASCADE",
	"CASE",
	"CHANGE",
	"CHAR",
	"CHARACTER",
	"CHECK",
	"COLLATE",
	"COLUMN",
	"CONDITION",
	"CONSTRAINT",
	"CONTINUE",
	"CONVERT",
	"CREATE",
	"CROSS",
	"CURRENT_DATE",
	"CURRENT_TIME",
	"CURRENT_TIMESTAMP",
	"CURRENT_USER",
	"CURSOR",
	"DATABASE",
	"DATABASES",
	"DAY_HOUR",
	"DAY_MICROSECOND",
	"DAY_MINUTE",
	"DAY_SECOND",
	"DEC",
	"DECIMAL",
	"DECLARE",
	"DEFAULT",
	"DELAYED",
	"DELETE",
	"DESC",
	"DESCRIBE",
	"DETERMINISTIC",
	"DISTINCT",
	"DISTINCTROW",
	"DIV",
	"DOUBLE",
	"DROP",
	"DUAL",
	"EACH",
	"ELSE",
	"ELSEIF",
	"ENCLOSED",
	"ESCAPED",
	"EXISTS",
	"EXIT",
	"EXPLAIN",
	"FALSE",
	"FETCH",
	"FLOAT",
	"FLOAT4",
	"FLOAT8",
	"FOR",
	"FORCE",
	"FOREIGN",
	"FROM",
	"FULLTEXT",
	"GET",
	"GRANT",
	"GROUP",
	"HAVING",
	"HIGH_PRIORITY",
	"HOUR_MICROSECOND",
	"HOUR_MINUTE",
	"HOUR_SECOND",
	"IF",
	"IGNORE",
	"IN",
	"INDEX",
	"INFILE",
	"INNER",
	"INOUT",
	"INSENSITIVE",
	"INSERT",
	"INT",
	"INT1",
	"INT2",
	"INT3",
	"INT4",
	"INT8",
	"INTEGER",
	"INTERVAL",
	"INTO",
	"IO_AFTER_GTIDS",
	"IO_BEFORE_GTIDS",
	"IS",
	"ITERATE",
	"JOIN",
	"KEY",
	"KEYS",
	"KILL",
	"LEADING",
	"LEAVE",
	"LEFT",
	"LIKE",
	"LIMIT",
	"LINEAR",
	"LINES",
	"LOAD",
	"LOCALTIME",
	"LOCALTIMESTAMP",
	"LOCK",
	"LONG",
	"LONGBLOB",
	"LONGTEXT",
	"LOOP",
	"LOW_PRIORITY",
	"MASTER_BIND",
	"MASTER_SSL_VERIFY_SERVER_CERT",
	"MATCH",
	"MAXVALUE",
	"MEDIUMBLOB",
	"MEDIUMINT",
	"MEDIUMTEXT",
	"MIDDLEINT",
	"MINUTE_MICROSECOND",
	"MINUTE_SECOND",
	"MOD",
	"MODIFIES",
	"NATURAL",
	"NOT",
	"NO_WRITE_TO_BINLOG",
	"NULL",
	"NUMERIC",
	"ON",
	"OPTIMIZE",
	"OPTION",
	"OPTIONALLY",
	"OR",
	"ORDER",
	"OUT",
	"OUTER",
	"OUTFILE",
	"PARTITION",
	"PRECISION",
	"PRIMARY",
	"PROCEDURE",
	"PURGE",
	"RANGE",
	"READ",
	"READS",
	"READ_WRITE",
	"REAL",
	"REFERENCES",
	"REGEXP",
	"RELEASE",
	"RENAME",
	"REPEAT",
	"REPLACE",
	"REQUIRE",
	"RESIGNAL",
	"RESTRICT",
	"RETURN",
	"REVOKE",
	"RIGHT",
	"RLIKE",
	"SCHEMA",
	"SCHEMAS",
	"SECOND_MICROSECOND",
	"SELECT",
	"SENSITIVE",
	"SEPARATOR",
	"SET",
	"SHOW",
	"SIGNAL",
	"SMALLINT",
	"SPATIAL",
	"SPECIFIC",
	"SQL",
	"SQLEXCEPTION",
	"SQLSTATE",
	"SQLWARNING",
	"SQL_BIG_RESULT",
	"SQL_CALC_FOUND_ROWS",
	"SQL_SMALL_RESULT",
	"SSL",
	"STARTING",
	"STRAIGHT_JOIN",
	"TABLE",
	"TERMINATED",
	"THEN",
	"TINYBLOB",
	"TINYINT",
	"TINYTEXT",
	"TO",
	"TRAILING",
	"TRIGGER",
	"TRUE",
	"UNDO",
	"UNION",
	"UNIQUE",
	"UNLOCK",
	"UNSIGNED",
	"UPDATE",
	"USAGE",
	"USE",
	"USING",
	"UTC_DATE",
	"UTC_TIME",
	"UTC_TIMESTAMP",
	"VALUES",
	"VARBINARY",
	"VARCHAR",
	"VARCHARACTER",
	"VARYING",
	"WHEN",
	"WHERE",
	"WHILE",
	"WITH",
	"WRITE",
	"XOR",
	"YEAR_MONTH",
	"ZEROFILL",
	"stdClass",
	"Exception",
	"ErrorException",
	"php_user_filter",
	"Closure",
	"Generator",
	"self",
	"static",
	"parent",
	"class",
	"__halt_compiler",
	"abstract",
	"and",
	"array",
	"as",
	"break",
	"callable",
 	"case",
	"catch",
	"class",
	"clone",
	"const",
	"continue",
	"declare",
	"default",
	"die",
	"do",
	"echo",
	"else",
	"elseif",
	"empty",
	"enddeclare",
	"endfor",
	"endforeach",
	"endif",
	"endswitch",
	"endwhile",
	"eval",
	"exit",
	"extends",
	"final",
	"for",
	"foreach",
	"function",
	"global",
	"goto",
 	"if",
	"implements",
	"include",
	"include_once",
	"instanceof",
	"insteadof",
 	"interface",
	"isset",
	"list",
	"namespace",
 	"new",
	"or",
	"print",
	"private",
	"protected",
	"public",
	"require",
	"require_once",
	"return",
	"static",
	"switch",
	"throw",
	"trait",
 	"try",
	"unset",
	"use",
	"var",
	"while",
	"xor",
	"__CLASS__",
	"__DIR__",
 	"__FILE__",
	"__FUNCTION__",
	"__LINE__",
	"__METHOD__",
	"__NAMESPACE__",
 	"__TRAIT__"
	);
	
	/**
	 * available indexes
	 *
	 * @var array
	 **/
	public $indexes=array(
		"index",
		"unique"
	);
	
	/**
	 * connection strings used in yamls
	 *
	 * @var array
	 **/
	public $connectionStrings = array(
		"has a"       =>array("label" =>"1to1", "fieldType" => "int(11)"),
		"has many"    =>array("label" =>"1toN", "fieldType" => "int(11)", "reverse" => true),
		"has lots of" =>array("label" =>"NtoN", "fieldType" => null)
	);
	
	
	/**
	 * field types
	 *
	 * @var array
	 */
	public $fieldTypes = array(
		"date"     => "DATE",
		"dateTime" => "DATETIME",
		"char-5"   => "varchar(5)",
		"char-10"  => "varchar(10)",
		"char-40"  => "varchar(40)",
		"char-100" => "varchar(100)",
		"char-200" => "varchar(200)",
		"char-255" => "varchar(255)",
		"char-400" => "varchar(400)",
		"int"      => "int(11)",
		"bool"     => "tinyint(1)",
		"float"    => "FLOAT",
		"bigint"   => "bigint(20)",
		"file"     =>  null,
		"select"   =>  null,
		"enum"	   =>  null,
		"editable" => "text",
		"text"     => "text",
		"tinyint1" => "tinyint(1)"
	);
	
	public $sqls = array(
		"createTable"=>"CREATE TABLE IF NOT EXISTS `%s` (
				`id` INT NOT NULL AUTO_INCREMENT,
				`jdate` INT NULL,
				PRIMARY KEY (`id`)
			) ENGINE=InnoDB",
		"addField"  =>"ALTER TABLE `%s` ADD `%s` %s %s NULL %s",
		"editField" =>"ALTER TABLE `%s` CHANGE `%s` `%s` %s %s NULL %s",
		"dropField" =>"ALTER TABLE `%s` DROP `%s`;",
		"getFieldDefault"=>"SELECT DEFAULT( `%s` ) as `default`
					 FROM (SELECT 1) AS dummy
					   LEFT JOIN %s
					     ON True
					 LIMIT 1",
		"collation" =>"CHARACTER SET utf8 COLLATE utf8_general_ci",
		"logTable"  =>"CREATE TABLE IF NOT EXISTS `%s` (
				`id` int(11) NOT NULL AUTO_INCREMENT,
				`log` text collate utf8_general_ci NOT NULL,
				`type` varchar(200) CHARACTER SET latin1 NOT NULL,
				`level` int(11) NOT NULL,
				`eventId` int(11) NOT NULL,
				`dateAdded` datetime NOT NULL,
				PRIMARY KEY (`id`)
			) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;",
		"logEventTable"=>"CREATE TABLE IF NOT EXISTS `%s` (
				`id` int(11) NOT NULL AUTO_INCREMENT,
				`eventName` varchar(220) COLLATE utf8_general_ci NOT NULL,
				`startDate` datetime NOT NULL,
				`endDate` datetime NOT NULL,
				PRIMARY KEY (`id`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;",
		"sessionsTable"=>"CREATE TABLE IF NOT EXISTS `%s` (
				`id` varchar(220) COLLATE utf8_general_ci NOT NULL,
				`sdata` text COLLATE utf8_general_ci NOT NULL,
				`lastModified` int(11) NOT NULL,
				`sessionsCustomData` varchar(220) COLLATE utf8_general_ci NOT NULL,
				PRIMARY KEY (`id`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;",
		"createJOTable"=>"CREATE TABLE IF NOT EXISTS `%s` (
				`%s` int(11),
				`%s` int(11),
				`tag` varchar(100),
				`uniqueID` varchar(20),
				`jorder` int(11) NOT NULL DEFAULT  '1',
				`jdate` int(11),
				`dateAdded` datetime NOT NULL default '0000-00-00 00:00:00',
				UNIQUE KEY `uniqueID` (`uniqueID`)
			) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;",
		"getAllFK"=>"SELECT CONSTRAINT_NAME, COLUMN_NAME, POSITION_IN_UNIQUE_CONSTRAINT, COLUMN_NAME, REFERENCED_TABLE_SCHEMA, REFERENCED_TABLE_NAME, REFERENCED_COLUMN_NAME
				FROM  `information_schema`.`KEY_COLUMN_USAGE` 
				WHERE TABLE_SCHEMA =  '%s'
				AND TABLE_NAME =  '%s'
				and REFERENCED_TABLE_NAME IS NOT NULL
				ORDER BY  `KEY_COLUMN_USAGE`.`TABLE_NAME` ASC",
		"findTargetFK"=>"SELECT TABLE_SCHEMA,TABLE_NAME,CONSTRAINT_NAME
			FROM  `information_schema`.`KEY_COLUMN_USAGE` 
			WHERE TABLE_SCHEMA =  '%s'
			and REFERENCED_TABLE_NAME='%s'",
		"addIndex"=>"ALTER TABLE  `%s`.`%s` ADD INDEX `j_%s` (  `%s` )",
		"uniqueIndex"=>"ALTER TABLE  `%s`.`%s` ADD UNIQUE  `ju_%s` (  `%s` )",
		"dropIndex"=>"ALTER TABLE `%s`.`%s` DROP INDEX  `%s`",
		"updateFK"=>"ALTER TABLE `%s`.`%s`
			ADD CONSTRAINT `%s`
			FOREIGN KEY (%s) REFERENCES %s.%s(%s)
			ON UPDATE CASCADE
			ON DELETE SET NULL",
		"deleteFK"=>"ALTER TABLE `%s`.`%s`
			ADD CONSTRAINT `%s`
			FOREIGN KEY (%s) REFERENCES %s.%s(%s)
			ON DELETE CASCADE;",
		"dropColumnFK"=>"ALTER TABLE %s.%s DROP FOREIGN KEY `%s`",
		"innerJoin"=>"INNER JOIN `%s`.`%s` %s ON `%s`.`%s`=`%s`.`%s`",
	);
	
	/**
	 * get sql
	 *
	 * @param string $sql 
	 * @return string
	 * @author Özgür Köy
	 */
	public function sql()
	{
	    $args = func_get_args();
	    $sql = array_shift($args);
		
		if(isset($this->sqls[$sql])){
		    array_unshift($args, $this->sqls[$sql]);
		    $query = call_user_func_array('sprintf', $args);
			// echo "\n".$query."\n";
			return $query;
		}
		else
			die("No good sql title:".$sql);
	}
	
} // END class BuildDef
?>