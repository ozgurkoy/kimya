<?php
/**
 * JDef - Class for some definitions for building and site
 *
 * @package jeelet
 * @author Özgür Köy
 **/
class JDef
{
	/**
	 * query folder
	 *
	 * @var string
	 **/
	public static $queryFolder='site/query/';

	/**
	 * model folder
	 *
	 * @var string
	 **/
	public static $mdlFolder='site/model/';

	/**
	 * controllers folder
	 *
	 * @var string
	 **/
	public static $ctrlFolder = 'site/controller/';

	/**
	 * admin folder
	 *
	 * @var string
	 **/
	public static $admFolder = 'site/admin/';

	/**
	 * log table name
	 *
	 * @var string
	 **/
	public static $logtable;

	/**
	 * log event table name
	 *
	 * @var string
	 **/
	public static $logeventtable;

	/**
	 * connector table name
	 *
	 * @var string
	 **/
	public static $ctable;

	/**
	 * document table name
	 *
	 * @var string
	 **/
	public static $doctable;

	/**
	 * sessions table name
	 *
	 * @var string
	 **/
	public static $sessionstable;

	/**
	 * tables table name
	 *
	 * @var string
	 **/
	public static $tabtable;

	/**
	 * prefix for tables
	 *
	 * @var string
	 **/
	public static $prefix='';

	/**
	 * field types of the fields
	 *
	 * @var array
	 **/
	public $fieldTypes;

	/**
	 * table sqls
	 *
	 * @var array
	 **/
	public $tableSQLs;

	/**
	 * create views for tables
	 *
	 * @var string
	 **/
	public $createViews=false;



	/**
	 * constructer
	 *
	 * @return void
	 **/
	public function __construct()
	{
		self::$logtable 		= self::$prefix."log";
		self::$logeventtable 	= self::$prefix."logEvents";
		self::$sessionstable 	= self::$prefix."sessions";
	}
} // END class JDef
