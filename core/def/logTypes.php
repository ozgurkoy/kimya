<?php
// core log types
$_logTypes = array(
	"gen"=>"General Error",
	"eng"=>"Engine Error",
	"api"=>"API Error",
	"sec"=>"Security",
	"user"=>"User Type Error-thrown",
	"cri"=>"Critical Error",
	"build"=>"Building action",
	"admin"=>"Admin error",
	"twi"=>"Twitter error",
	"911"=>"System halted",
	"youtube"=>"Youtube error",
	"unauthorized"=>"Unauthorized Access",
	"timeOut"=>"Time Out",
	"slsAPI"=>"SHort Link Service API",
	"social"=>"Social Network Error"
);