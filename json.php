<?php
echo "<PRE>";

echo json_encode(
array(
	"SAMPLE_WORD"=>array(
			"isMail"=>1,
			"isDashboard"=>0,
			"isPush"=>1,
			"entries"=>array(
				"EN"=>array("content"=>"DEFINITION IN ENGLISH","body"=>"_OPTIONAL_"),
				"TR"=>array("content"=>"DEFINITION IN TR","body"=>"_OPTIONAL_"),
			)
	  ),
	"SAMPLE_WORD_NON_BODY"=>array(
			"isMail"=>1,
			"isDashboard"=>0,
			"isPush"=>1,
			"entries"=>array(
				"EN"=>array("content"=>"DEFINITION IN ENGLISH"),
				"TR"=>array("content"=>"DEFINITION IN TR"),
			)
	  ),
));

?>